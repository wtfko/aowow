<?php

// Необходима функция creatureinfo
require('includes/allnpcs.php');

$smarty->config_load($conf_file, 'npcs');

global $npc_cols;

// Разделяем из запроса класс и подкласс вещей
point_delim($podrazdel,$type,$family);

global $DB;

$rows = $DB->select('
	SELECT ?#, entry
	FROM creature_template, ?_factiontemplate
	WHERE 1=1
		{AND type=?}
		{AND family=?}
		AND factiontemplateID=faction_A
	ORDER BY name
	',
	$npc_cols[0],
	($type!='')? $type: DBSIMPLE_SKIP,
	(isset($family))? $faily: DBSIMPLE_SKIP
);

$npcs = array();
foreach ($rows as $numRow=>$row)
{
	$npcs[$numRow] = array();
	$npcs[$numRow] = creatureinfo2($row);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('NPCs');
// Путь к этому разделу
$page['path'] = '[0, 4,'.$type.','.$family.']';
$smarty->assign('page', $page);

if (count($npcs>=0))
	$smarty->assign('npcs',$npcs);
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Загружаем страницу
$smarty->display('npcs.tpl');

?>
