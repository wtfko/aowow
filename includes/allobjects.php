<?php
/*
* AoWoW: MaNGOS Web Interface
*
* © Arcano 2007-2008
* Runixer@gmail.com
*
* Released under the terms and conditions of the
* GNU General Public License (http://gnu.org).
* 
* allobjects.php - All functions, constants, etc, which deals with game objects
*/

// Types of Game Objects
define("GAMEOBJECT_TYPE_DOOR", 0);
define("GAMEOBJECT_TYPE_BUTTON", 1);
define("GAMEOBJECT_TYPE_QUESTGIVER", 2);
define("GAMEOBJECT_TYPE_CHEST", 3);
define("GAMEOBJECT_TYPE_BINDER", 4);
define("GAMEOBJECT_TYPE_GENERIC", 5);
define("GAMEOBJECT_TYPE_TRAP", 6);
define("GAMEOBJECT_TYPE_CHAIR", 7);
define("GAMEOBJECT_TYPE_SPELL_FOCUS", 8);
define("GAMEOBJECT_TYPE_TEXT", 9);
define("GAMEOBJECT_TYPE_GOOBER", 10);
define("GAMEOBJECT_TYPE_TRANSPORT", 11);
define("GAMEOBJECT_TYPE_AREADAMAGE", 12);
define("GAMEOBJECT_TYPE_CAMERA", 13);
define("GAMEOBJECT_TYPE_MAP_OBJECT", 14);
define("GAMEOBJECT_TYPE_MO_TRANSPORT", 15);
define("GAMEOBJECT_TYPE_DUEL_ARBITER", 16);
define("GAMEOBJECT_TYPE_FISHINGNODE", 17);
define("GAMEOBJECT_TYPE_RITUAL", 18);
define("GAMEOBJECT_TYPE_MAILBOX", 19);
define("GAMEOBJECT_TYPE_AUCTIONHOUSE", 20);
define("GAMEOBJECT_TYPE_GUARDPOST", 21);
define("GAMEOBJECT_TYPE_SPELLCASTER", 22);
define("GAMEOBJECT_TYPE_MEETINGSTONE", 23);
define("GAMEOBJECT_TYPE_FLAGSTAND", 24);
define("GAMEOBJECT_TYPE_FISHINGHOLE", 25);
define("GAMEOBJECT_TYPE_FLAGDROP", 26);
define("GAMEOBJECT_TYPE_MINI_GAME", 27);
define("GAMEOBJECT_TYPE_LOTTERY_KIOSK", 28);
define("GAMEOBJECT_TYPE_CAPTURE_POINT", 29);
define("GAMEOBJECT_TYPE_AURA_GENERATOR", 30);
define("GAMEOBJECT_TYPE_DUNGEON_DIFFICULTY", 31);
define("GAMEOBJECT_TYPE_UNK", 32);
define("GAMEOBJECT_TYPE_DESTRUCTIBLE_BUILDING", 33);
define("GAMEOBJECT_TYPE_GUILD_BANK", 34);

// Column LockProperties in Lock.dbc
define("LOCK_PROPERTIES_FOOTLOCK",1);
define("LOCK_PROPERTIES_HERBALISM",2);
define("LOCK_PROPERTIES_MINING",3);

// objectinfo required columns
$object_cols[0] = array('entry', 'name', 'type');
$object_cols[1] = array('entry', 'name', 'type', 'data0', 'data1');

// Функция информации о вещи
function objectinfo($id, $level=0)
{
	global $DB;
	global $object_cols;
	$row = $DB->selectRow('
		SELECT ?#
		FROM gameobject_template
		WHERE entry=?d
		LIMIT 1
		',
		$object_cols[$level],
		$id
	);
	return objectinfo2($row, $level);
}

// Функция информации об объекте
//  $Row - ссылка на ассоциативный массив из базы
function objectinfo2(&$Row, $level=0)
{
	global $DB;
	// Номер объекта
	$object['id'] = $Row['entry'];
	// Название объекта
	$object['name'] = $Row['name'];
	// Тип объекта
	$object['type'] = $Row['type'];
	if ($level>0)
	{
		// Loot ID
		$object['lootid'] = $Row['data1'];
		// Тип объекта и требуемый уровень скилла, и какого скилла
		$lock_row = $DB->selectRow('
			SELECT *
			FROM ?_lock
			WHERE lockID=?d
			LIMIT 1
			',
			$Row['data0']
		);
		if ($lock_row)
		{
			for ($j=1;$j<=5;$j++)
			{
				switch ($lock_row['type'.$j]):
					case 0:
						// Не замок
						break;
					case 1:
						// Ключ
						$object['key'] = array();
						$object['key'] = $DB->selectRow('SELECT entry as id, name, quality FROM item_template WHERE entry=?d LIMIT 1', $lock_row['lockproperties'.$j]);
						break;
					case 2:
						// Скилл
						switch ($lock_row['lockproperties'.$j]):
							case LOCK_PROPERTIES_FOOTLOCK:
								// Сундук
								$object['lockpicking'] = $lock_row['requiredskill'.$j];
								break;
							case LOCK_PROPERTIES_HERBALISM:
								// Трава
								$object['herbalism'] = $lock_row['requiredskill'.$j];
								break;
							case LOCK_PROPERTIES_MINING:
								// Руда
								$object['mining'] = $lock_row['requiredskill'.$j];
								break;
						endswitch;
				endswitch;
			}
		}
	}
	return $object;
}

?>
