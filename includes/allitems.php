<?php

require_once 'includes/game.php';
require_once 'includes/allspells.php';
require_once 'includes/allitemsets.php';

// Массивы с названиями столбцов, необходимых для различных уровней вызова функций
// для allitems($level=0) - соответствия номер-иконка
$item_cols[0] = array('entry', 'iconname', 'quality');
// для allitems($level=1) - ajax, тултип
$item_cols[1] = array('entry', 'name', 'quality', 'iconname', 'maxcount', 'bonding', 'startquest', 'Map', 'ContainerSlots', 'class', 'InventoryType', 'subclass', 'dmg_type1','dmg_min1', 'dmg_max1', 'delay', 'dmg_type2', 'dmg_min2', 'dmg_max2', 'dmg_type3', 'dmg_min3', 'dmg_max3', 'dmg_type4', 'dmg_min4', 'dmg_max4', 'dmg_type5', 'dmg_min5', 'dmg_max5', 'armor', 'block', 'GemProperties', 'stat_type1', 'stat_type2', 'stat_type3', 'stat_type4', 'stat_type5', 'stat_type6', 'stat_type7', 'stat_type8', 'stat_type9', 'stat_type10', 'stat_value1', 'stat_value2', 'stat_value3', 'stat_value4', 'stat_value5', 'stat_value6', 'stat_value7', 'stat_value8', 'stat_value9', 'stat_value10', 'holy_res', 'fire_res', 'nature_res', 'frost_res', 'shadow_res', 'arcane_res', 'RandomProperty', 'RandomSuffix', 'socketColor_1', 'socketColor_2', 'socketColor_3', 'socketBonus', 'MaxDurability', 'AllowableClass', 'RequiredLevel', 'RequiredSkill', 'requiredspell', 'RequiredReputationFaction', 'RequiredReputationRank', 'spellid_1', 'spellid_2', 'spellid_3','spellid_4','spellid_5', 'spelltrigger_1', 'spelltrigger_2', 'spelltrigger_3', 'spelltrigger_4', 'spelltrigger_5', 'description', 'PageText', 'BagFamily', 'RequiredSkillRank');
// для iteminfo($level=0) - строчки списка
$item_cols[2] = array('name', 'quality', 'iconname', 'InventoryType', 'ItemLevel', 'RequiredLevel', 'class', 'subclass', 'stackable', 'BuyPrice', 'armor', 'dmg_type1','dmg_min1', 'dmg_max1', 'delay', 'dmg_type2', 'dmg_min2', 'dmg_max2', 'dmg_type3', 'dmg_min3', 'dmg_max3', 'dmg_type4', 'dmg_min4', 'dmg_max4', 'dmg_type5', 'dmg_min5', 'dmg_max5', 'ContainerSlots');
// для iteminfo($level=1)
$item_cols[3] = array('entry', 'name', 'quality', 'iconname', 'maxcount', 'bonding', 'startquest', 'Map', 'ContainerSlots', 'class', 'InventoryType', 'subclass', 'dmg_type1','dmg_min1', 'dmg_max1', 'delay', 'dmg_type2', 'dmg_min2', 'dmg_max2', 'dmg_type3', 'dmg_min3', 'dmg_max3', 'dmg_type4', 'dmg_min4', 'dmg_max4', 'dmg_type5', 'dmg_min5', 'dmg_max5', 'armor', 'block', 'GemProperties', 'stat_type1', 'stat_type2', 'stat_type3', 'stat_type4', 'stat_type5', 'stat_type6', 'stat_type7', 'stat_type8', 'stat_type9', 'stat_type10', 'stat_value1', 'stat_value2', 'stat_value3', 'stat_value4', 'stat_value5', 'stat_value6', 'stat_value7', 'stat_value8', 'stat_value9', 'stat_value10', 'holy_res', 'fire_res', 'nature_res', 'frost_res', 'shadow_res', 'arcane_res', 'RandomProperty', 'RandomSuffix', 'socketColor_1', 'socketColor_2', 'socketColor_3', 'socketBonus', 'MaxDurability', 'AllowableClass', 'RequiredLevel', 'RequiredSkill', 'requiredspell', 'RequiredReputationFaction', 'RequiredReputationRank', 'spellid_1', 'spellid_2', 'spellid_3','spellid_4','spellid_5', 'spelltrigger_1', 'spelltrigger_2', 'spelltrigger_3', 'spelltrigger_4', 'spelltrigger_5', 'description', 'PageText', 'BagFamily', 'RequiredSkillRank', 'ItemLevel', 'stackable', 'BuyPrice', 'DisenchantID', 'SellPrice', 'RequiredDisenchantSkill');

$resz = array('holy_res', 'fire_res', 'nature_res', 'frost_res', 'shadow_res', 'arcane_res');
$resz_desc = array ('Holy Resistance', 'Fire Resistance', 'Nature Resistance', 'Frost Resistance', 'Shadow Resistance', 'Arcane Resistance');
$bag_typez = array(0 => 'Bag', 1 => 'Quiver', 2 => 'Ammo Pouch', 4 => 'Soul Bag', 8 => 'Leatherworking Bag', 32 => 'Herb Bag', 64 => 'Enchanting Bag', 128 => 'Engineering Bag', 512 => 'Gem Bag', 1024 => 'Mining Bag');
$rep_levels = array('unk0', 'unk1', 'unk2', 'Neutral', 'Friendy', 'Honored', 'Revered', 'Exalted');
$bond = array('', '<br />Binds when picked up', '<br />Binds when equipped', '<br />Soulbond', '<br />Quest Item');
$slot = array('', 'Head', 'Neck', 'Shoulder', 'Shirt', 'Chest', 'Waist', 'Legs', 'Feet', 'Wrist', 'Hands', 'Finger', 'Trinket', 'One-Hand', 'Off Hand', 'Ranged', 'Back', 'Two-Hand', '', 'Tabard', 'Chest', 'Main Hand', 'Off Hand', 'Held In Off-Hand', 'Projectile', 'Thrown', 'Ranged', '', 'Relic');
$armor_type = array('', 'Cloth', 'Leather', 'Mail', 'Plate', '', 'Shield', 'Libram', 'Idol', 'Totem');
$weapon_type = array('Axe', 'Axe', 'Bow', 'Gun', 'Mace', 'Mace', 'Polearm', 'Sword', 'Sword', '', 'Staff', '', '', 'Fist Weapon', '', 'Dagger', 'Thrown', '', 'Crossbow', 'Wand', 'Fishing Pole');
$projectile_type = array('', '', 'Arrow', 'Bullet');
$dmg_typez = array ('', 'Holy ', 'Fire ', 'Nature ', 'Frost ', 'Shadow ', 'Arcane ');

// Таблица урона
function inv_dmg($min,$max,$delay,$type)
{
	global $dmg_typez;
	if ($delay!=0)
		return "<table width=\"100%\"><tr><td>$min - $max ".$dmg_typez[$type].' Damage</td><th>Speed '.number_format($delay,2).'</th></tr></table>';
	else
		return "+$min - $max ".$dmg_typez[$type].' Damage<br />';
}

// Типы бонусов
function b_type($type, $value)
{
	global $green;
	switch($type):
		case 3:
			return "+$value Agility<br />";
		case 4:
			return "+$value Strength<br />";
		case 5:
			return "+$value Intellect<br />";
		case 6:
			return "+$value Spirit<br />";
		case 7:
			return "+$value Stamina<br />";
		case 12:
			$green[]="Equip: Increases defense rating by $value.";
			return;
		case 13:
			$green[]="Equip: Improves your dodge rating by $value.";
			return;
		case 14:
			$green[]="Equip: Improves your parry rating by $value.";
			return;
		case 15:
			$green[]="Equip: Improves your shield block rating by $value.";
			return;
		case 18:
			$green[]="Equip: Improves spell hit rating by $value.";
			return;
		case 19:
			$green[]="Equip: Improves melee critical strike rating by $value.";
			return;
		case 20:
			$green[]="Equip: Improves ranged critical strike rating by $value.";
			return;
		case 21:
			$green[]="Equip: Improves spell critical strike rating by $value.";
			return;
		case 30:
			$green[]="Equip: Improves spell haste rating by $value.";
			return;
		case 31:
			$green[]="Equip: Improves hit rating by $value.";
			return;
		case 32:
			$green[]="Equip: Improves critical strike rating by $value.";
			return;
		case 35:
			$green[]="Equip: Improves your resilience rating by $value.";
			return;
		case 36:
			$green[]="Equip: Improves haste rating by $value.";
			return;
		case 37:
			$green[]="Increases your expertise rating by $value.";
			return;
		default:
			return "<br />+$value UnkownBonus($type)";
	endswitch;
}

function socket_type($type)
{
	switch($type):
		case 1:
			return '<span class="socket-meta q0">Meta Socket</span>';
		case 2:
			return '<span class="socket-red q0">Red Socket</span>';
		case 4:
			return '<span class="socket-yellow q0">Yellow Socket</span>';
		case 8:
			return '<span class="socket-blue q0">Blue Socket</span>';
		default:
			return '<a class="q0">Unknown Socket('.$type.')</a>';
	endswitch;
}

function socket_bonus($bonus)
{
	global $DB;
	return $DB->selectCell('SELECT `text` from ?_itemenchantmet WHERE itemenchantmetID=?d LIMIT 1',$bonus); 
}

function req_spell($spell_id)
{
	global $DB;
	return $DB->selectCell('SELECT `spellname` from ?_spell where spellID=?d LIMIT 1', $spell_id);
}

function spell_to_bonus($spell_id, $trigger)
{
	$tooltip = spell_desc($spell_id);
	if ($tooltip=='_empty_')
		return;
	if ($tooltip)
	{
		switch($trigger):
			case 0:
				$t = 'Use: ';
				break;
			case 1:
				$t = 'Equip: ';
				break;
			case 2:
				$t = 'Chance on hit: ';
				break;
			case 6:
				// Обучает
				return;
				break;
			default:
				$t = '';
				break;
		endswitch;
		return $t.'<a href="?spell='.$spell_id.'" class="q2">'.$tooltip.'</a>';
	} else {
		return '<a href=?spell='.$spell_id.'>Error in spell_desc for spell='.$spell_id.'</a>';
	}
}

// Классы, для которых предназначена вещь
function classes($class)
{
	$tmp = '';
	if ($class & CLASS_WARRIOR)
		$tmp = 'Warrior';
	if ($class & CLASS_PALADIN)
		if ($tmp) $tmp = $tmp.', Paladin'; else $tmp = 'Paladin';
	if ($class & CLASS_HUNTER)
		if ($tmp) $tmp = $tmp.', Hunter'; else $tmp = 'Hunter';
	if ($class & CLASS_ROGUE)
		if ($tmp) $tmp = $tmp.', Rogue'; else $tmp = 'Rogue';
	if ($class & CLASS_PRIEST)
		if ($tmp) $tmp = $tmp.', Priest'; else $tmp = 'Priest';
	if ($class & CLASS_SHAMAN)
		if ($tmp) $tmp = $tmp.', Shaman'; else $tmp = 'Shaman';
	if ($class & CLASS_MAGE)
		if ($tmp) $tmp = $tmp.', Mage'; else $tmp = 'Mage';
	if ($class & CLASS_WARLOCK)
		if ($tmp) $tmp = $tmp.', Warlock'; else $tmp = 'Warlock';
	if ($class & CLASS_DRUID)
		if ($tmp) $tmp = $tmp.', Druid'; else $tmp = 'Druid';
	if ($tmp == 'Warrior, Paladin, Hunter, Rogue, Priest, Shaman, Mage, Warlock, Druid')
		return;
	else
		return $tmp;
}

function allitemsinfo2(&$Row, $level=0)
{
	// Пустая строка
	if (!isset($Row['entry']))
		return;
	// Глобальный массив с информацие о вещях
	global $allitems;
	//  Номер очередного элемента
	$num = $Row['entry'];
	// Если уже есть
	if (isset($allitems[$num]))
		return $allitems[$num];
	// Подключение к базе
	global $DB;
	// Записываем id вещи
	$allitems[$num]['id'] = $Row['entry'];
	// Ищем иконку
	$allitems[$num]['icon'] = $Row['iconname'];
	// Качество вещи
	$allitems[$num]['quality'] = $Row['quality'];
	// Заполняем инфу о вещи
	if ($level>0)
	{
		$allitems[$num]['name'] = $Row['name'];
		$allitems[$num]['info'] = render_item_tooltip($Row);
	}

//	if ($level==1)
		return $allitems[$num];
//	else
//		return;
}

function allitemsinfo($id, $level=0)
{
	global $DB;
	global $allitems;
	global $item_cols;

	if (isset($allitems[$id]))
	{
		return $allitems[$id];
	} else {
		$row = $DB->selectRow('
			SELECT ?#
			FROM item_template, ?_icons
			WHERE
				entry=?
				AND id=displayid
			LIMIT 1
			',
			$item_cols[$level],
			$id
		);
		return allitemsinfo2($row, $level);
	}
}

function render_item_tooltip(&$Row)
{
	// БД
	global $DB;
	// Строковые константы
	global $resz, $resz_desc, $bag_typez, $bond, $slot, $armor_type, $weapon_type, $projectile_type;
	// Зеленый текст
	global $green;
	// Столбцы для извлечения
	global $itemset_col;
	
	$green = array();

	$x = '';
	// Начальный тег таблицы
	$x .= '<table><tr><td>';
	// Название и цвет названия
	$x .= '<b class="q'.$Row['quality'].'">'.$Row['name'].'</b>';
	// Биндинг вещи
	$x .= $bond[$Row['bonding']];

	// Уникальность вещи
	if ($Row['maxcount']==1)
		$x .= '<br />Unique';

	if ($Row['maxcount']>1)
		$x .= ' ('.$Row['maxcount'].')';

 	if ($Row['startquest'])
		$x .= '<br />This Item Begins a Quest';

	// Локация, для которой предназначен этот предмет
	if ($Row['Map'])
		$x .= '<br />'.$DB->selectCell('SELECT `name` from ?_zones WHERE mapid=?d LIMIT 1', $Row['Map']);;

	// Теперь в зависимости от типа предмета
	if ($Row['ContainerSlots']>1)
		$x .= '<br />'.$Row['ContainerSlots'].' Slot '.$bag_typez[$Row['BagFamily']];
	if (($Row['class']==4) or ($Row['class']==2) or ($Row['class']==6) or ($Row['class']==7))
	{
		// Броня (4), Оружие(2), Патроны(6)
		// Начало таблицы св-в брони
		$x .= '<table width="100%">';
		$x .= '<tr>';
		// Слот
		$x .= '<td>'.$slot[$Row['InventoryType']].'</td>';
		// Тип брони
		if ($Row['class']==4)
			$x .= '<th>'.$armor_type[$Row['subclass']].'</th>';
		elseif ($Row['class']==2)
			$x .= '<th>'.$weapon_type[$Row['subclass']].'</th>';
		elseif ($Row['class']==6)
			$x .= '<th>'.$projectile_type[$Row['subclass']].'</th>';
		$x .= '</tr></table>';
	} else {
		$x .= '<br />';
	}

	// Урон
	$dps=0;
	for ($j=1;$j<=5;$j++)
	{
		$d_type = $Row['dmg_type'.$j];
		$d_min = $Row['dmg_min'.$j];
		$d_max = $Row['dmg_max'.$j];
		if (($d_max>0) and ($Row['class']!=6))
		{
			$delay = $Row['delay'] / 1000;
			if ($delay>0) {$dps = $dps+round(($d_max+$d_min)/(2*$delay),1);}
			if ($j>1) {$delay=0;}
			$x .= inv_dmg($d_min,$d_max,$delay,$d_type);
		} elseif (($d_max>0) and ($Row['class']==6))
		{
			$x .= 'Adds '.number_format((($d_max+$d_min)/2),1).' damage per second<br />';
		}
	}
	if ($dps>0)
		$x .= '('.number_format($dps,1).' damage per second)<br />';
	// Кол-во брони
	if ($Row['armor'])
		$x .= $Row['armor'].' Armor<br />';
	if ($Row['block'])
		$x .= $Row['block'].' Block<br />';
	if ($Row['GemProperties'])
		$x .= $DB->selectCell('SELECT ?_itemenchantmet.text from ?_itemenchantmet, ?_gemproperties WHERE (?_gemproperties.gempropertiesID=?d and ?_itemenchantmet.itemenchantmetID=?_gemproperties.itemenchantmetID)', $Row['GemProperties']).'<br />';

	// Различные бонусы
	for ($j=1;$j<=10;$j++)
		if (($Row['stat_type'.$j]!=0) and ($Row['stat_value'.$j]!=0))
			$x .= b_type($Row['stat_type'.$j], $Row['stat_value'.$j]);

	// Бонусы к сопротивлениям магий
	foreach ($resz as $j => $RowName)
	{
		if ($Row[$RowName]!=0)
			$x .= '+'.$Row[$RowName].' '.$resz_desc[$j].'<br />';
	}
	// Случайные бонусы
	if ($Row['RandomProperty'] or $Row['RandomSuffix'])
		$green[] = 'Random Bonuses';

	// Сокеты
	for ($j=1;$j<=3;$j++)
		if ($Row['socketColor_'.$j]!=0)
			$x .= socket_type($Row['socketColor_'.$j]).'<br />';

	if ($Row['socketBonus'])
		$x .= '<span class="q0">Socket Bonus: '.socket_bonus($Row['socketBonus']).'</span><br />';
	// Состояние
	if ($Row['MaxDurability'])
		$x .= 'Durability '.$Row['MaxDurability'].' / '.$Row['MaxDurability'].'<br />';
	// Требуемые классы
	if (classes($Row['AllowableClass']))
		$x .= 'Classes: '.classes($Row['AllowableClass']).'<br />';

	// Требуемый уровень
	if ($Row['RequiredLevel']>1)
		$x .= 'Requires Level '.$Row['RequiredLevel'].'<br />';

	// Требуемый скилл (755 - Jewecrafting)
	if (($Row['RequiredSkill']) and ($Row['RequiredSkill']!=755))
	{
		$x .= 'Requires '.$DB->selectCell('SELECT `name` FROM ?_skill WHERE skillID=?d LIMIT 1',$Row['RequiredSkill']);
		if ($Row['RequiredSkillRank'])
			$x .= ' ('.$Row['RequiredSkillRank'].')';
		$x .= '<br />';
	}

	// Требуемый спелл
	if ($Row['requiredspell'])
		$x .= 'Requires '.req_spell($Row['requiredspell']).'<br />';

	// Требуемая репутация
	if ($Row['RequiredReputationFaction'])
	{
		require_once ('includes/game.php');
		global $rep_levels;
		$row = factioninfo($Row['RequiredReputationFaction']);
		$x .= 'Requires '.$row['name'].' - '.$rep_levels[$Row['RequiredReputationRank']];
	}

	$x .= '</td></tr></table>';

	// Спеллы
	for ($j=1;$j<=5;$j++)
	{
		if ($Row['spellid_'.$j])
			$green[]=spell_to_bonus($Row['spellid_'.$j], $Row['spelltrigger_'.$j]);
	}

	// Перебираем все "зеленые" бонусы
	$x .= '<table><tr><td>';
	if ($green)
	{
		foreach ($green as $j => $bonus)
			if ($bonus)
				$x .= '<span class="q2">'.$bonus.'</span><br />';
	}

	if ($Row['description'])
	{
		if ($Row['spelltrigger_2']==6)
			$x .= '<span class="q2"> Use: <a href="?spell='.$Row['spellid_2'].'">'.$Row['description'].'</a></span>';
		else
			$x .= '<span class="q">"'.$Row['description'].'"</span>';
	}
	if ($Row['PageText'])
		$x .= '<br /><span class="q2">&lt;Right Click To Read&gt;</span>';

	// Item Set
	// Временное хранилище всех вещей;
	$x_tmp = '';
	$row = $DB->selectRow('SELECT ?# FROM ?_itemset WHERE (item1=?d or item2=?d or item3=?d or item4=?d or item5=?d or item6=?d or item7=?d or item8=?d or item9=?d or item10=?d) LIMIT 1', $itemset_col[1], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry'], $Row['entry']);
	if ($row)
	{
		$num = 0; // Кол-во вещей в наборе
		for ($i=1;$i<=10;$i++)
		{
			if ($row['item'.$i] >0)
			{
				$num++;
				$name = $DB->selectCell('SELECT `name` FROM item_template WHERE entry=?d LIMIT 1', $row['item'.$i]);
				$x_tmp .= '<span><a href="?item='.$row['item'.$i].'">'.$name.'</a></span><br />';
			}
		}
		$x .= '<span class="q"><a href="?itemset='.$row['itemsetID'].'" class="q">'.$row['name'].'</a> (0/'.$num.')</span>';
		// Если требуется скилл
		if ($row['skillID'])
		{
			$name = $DB->selectCell('SELECT `name` FROM ?_skill WHERE skillID=?d LIMIT 1', $row['skillID']);
			$x .= 'Requires <a href="?spells=11.'.$row['skillID'].'" class="q1">'.$row_skill['name'].'</a>';
			if ($row['skilllevel'])
				$x .= ' ('.$row['skilllevel'].')';
			$x .= '<br />';
		}
		// Перечисление всех составляющих набора
		$x .= '<div class="q0 indent">'.$x_tmp.'</div>';
		// Перечисление всех бонусов набора
		$x .= '<span class="q0">';
		$num = 0;
		for ($j=1;$j<=8;$j++)
			if ($row['spell'.$j])
			{
				$itemset['spells'][$num]['id'] = $row['spell'.$j];
				$itemset['spells'][$num]['tooltip'] = spell_desc($row['spell'.$j]);
				$itemset['spells'][$num]['bonus'] = $row['bonus'.$j];
				$num++;
			}
		// Сортировка бонусов
		$x .= '<span class="q0">';
		for ($i=0;$i<$num;$i++)
		{
			for ($j=$i;$j<=$num-1;$j++)
				if ($itemset['spells'][$j]['bonus'] < $itemset['spells'][$i]['bonus'])
				{
					UnSet($tmp);
					$tmp = $itemset['spells'][$i];
					$itemset['spells'][$i] = $itemset['spells'][$j];
					$itemset['spells'][$j] = $tmp;
				}
			$x .= '<span>('.$itemset['spells'][$i]['bonus'].') Set: <a href="?spell='.$itemset['spells'][$i]['id'].'">'.$itemset['spells'][$i]['tooltip'].'</a></span><br />';
		}
		$x .= '</span></span>';
	}
	$x .= '</td></tr></table>';
	return $x;
}

// Функция информации о вещи
function iteminfo2(&$Row, $level=0)
{
	global $DB;
	global $allitems;
	global $spell_cols;
	
	if (!isset($Row['entry']))
		return;

	$item = array();
	// Номер вещи
	$item['id'] = $Row['entry'];
	// Название вещи
	$item['name'] = $Row['name'];
	// Тип вещи
	$item['type'] = $Row['InventoryType'];
	$item['icon'] = $Row['iconname'];
	// Уровень вещи
	$item['level'] = $Row['ItemLevel'];
	// Качество вещи...
	$item['quality'] = $Row['quality'];
	$item['quality2'] = 6 - $Row['quality'];
	// Требуемый уровень вещи:
	$item['reqlevel'] = $Row['RequiredLevel'];
	// Класс и подкласс вещи
	// TODO: немного неверное определение
	$item['classs'] = $Row['class'];
	$item['subclass'] = $Row['subclass'];
	// Иконка вещи
	$item['iconname'] = $Row['iconname'];
	// Кол-во вещей в пачке
	$item['stackable'] = $Row['stackable'];
	// Стоимость вещи для покупки
	// DPS
	$dps = 0;
	if ($Row['class']==2)
	{
		for ($i=1;$i<=5;$i++)
		{
			$d_type = $Row['dmg_type'.$i];
			$d_min = $Row['dmg_min'.$i];
			$d_max = $Row['dmg_max'.$i];
			if (($d_max>0) and ($Row['class']!=6))
			{
				$delay = $Row['delay'] / 1000;
				if ($delay>0) {$dps = $dps+round(($d_max+$d_min)/(2*$delay),1);}
			}
		}
		$item['dps'] = $dps;
		$item['speed'] = $Row['delay']/1000;
		if (!$item['speed']) $item['speed'] = -1;
	}
	// Armor
	$item['armor'] = $Row['armor'];
	$item['slot'] = $Row['InventoryType'];
	// Bag
	if ($Row['class']==1)
		$item['slots'] = $Row['ContainerSlots'];
	// Добавляем в глобальный массив allitems
	allitemsinfo2($Row, 0);
	if ($level>0)
	{
		$item['BuyPrice'] = $Row['BuyPrice'];
		//
		$item['BagFamily'] = $Row['BagFamily'];
		$item['ContainerSlots'] = $Row['ContainerSlots'];
		$item['DisenchantID'] = $Row['DisenchantID'];
		// Навык энчанта для разборки вещи
		if ($Row['RequiredDisenchantSkill']!=-1)
			$item['disenchantskill'] = $Row['RequiredDisenchantSkill'];
		// Цена на продажу
		$item['sellgold'] = floor($Row['SellPrice']/10000);
		$item['sellsilver'] = floor($Row['SellPrice']%10000/100);
		$item['sellcopper'] = floor($Row['SellPrice']%100);
		// Цена за покупку
		$item['buygold'] = floor($Row['BuyPrice']/10000);
		$item['buysilver'] = floor($Row['BuyPrice']%10000/100);
		$item['buycopper'] = floor($Row['BuyPrice']%100);
		// Информационное окно
		$item['info'] = render_item_tooltip($Row);
		
		// Обучает
		$item['teaches'] = array();
		for ($j=1;$j<=4;$j++)
			if ($Row['spellid_'.$j])
				for ($k=1;$k<=3;$k++)
				{
					$spellrow = $DB->selectRow('
						SELECT s.spellID, ?#
						FROM ?_spell s, ?_spellicons i
						WHERE
							s.spellID=(SELECT effect'.$k.'triggerspell FROM ?_spell WHERE spellID=?d AND (effect'.$k.'id IN (36,57)))
							AND i.id=s.spellicon
						LIMIT 1
						',
						$spell_cols[2],
						$Row['spellid_'.$j]
					);
					if ($spellrow)
					{
						$num = count($item['teaches']);
						$item['teaches'][$num] = array();
						$item['teaches'][$num] = spellinfo2($spellrow);
					}
				}
	}
	return $item;
}

// Функция информации о вещи
function iteminfo($id, $level=0)
{
	global $item_cols;
	global $DB;
	$row = $DB->selectRow('
		SELECT ?#, entry, maxcount
		FROM item_template, ?_icons
		WHERE
			(entry=?d and id=displayid)
		LIMIT 1
		',
		$item_cols[2+$level],
		$id
	);
	return iteminfo2($row, $level);
}

?>
