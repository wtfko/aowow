<?php

require_once('includes/game.php');

global $AoWoWconf;

$quest_cols[2] = array('entry', 'Title', 'QuestLevel', 'MinLevel', 'RequiredRaces', 'RewChoiceItemId1', 'RewChoiceItemId2', 'RewChoiceItemId3', 'RewChoiceItemId4', 'RewChoiceItemId5', 'RewChoiceItemId6', 'RewChoiceItemCount1', 'RewChoiceItemCount2', 'RewChoiceItemCount3', 'RewChoiceItemCount4', 'RewChoiceItemCount5', 'RewChoiceItemCount6', 'RewItemId1', 'RewItemId2', 'RewItemId3', 'RewItemId4', 'RewItemCount1', 'RewItemCount2', 'RewItemCount3', 'RewItemCount4', 'RewMoneyMaxLevel', 'RewOrReqMoney', 'Type', 'ZoneOrSort', 'QuestFlags');
$quest_cols[3] = array('entry', 'Title', 'QuestLevel', 'MinLevel', 'RequiredRaces', 'RewChoiceItemId1', 'RewChoiceItemId2', 'RewChoiceItemId3', 'RewChoiceItemId4', 'RewChoiceItemId5', 'RewChoiceItemId6', 'RewChoiceItemCount1', 'RewChoiceItemCount2', 'RewChoiceItemCount3', 'RewChoiceItemCount4', 'RewChoiceItemCount5', 'RewChoiceItemCount6', 'RewItemId1', 'RewItemId2', 'RewItemId3', 'RewItemId4', 'RewItemCount1', 'RewItemCount2', 'RewItemCount3', 'RewItemCount4', 'RewMoneyMaxLevel', 'RewOrReqMoney', 'Type', 'ZoneOrSort', 'QuestFlags', 'RewRepFaction1', 'RewRepFaction2', 'RewRepFaction3', 'RewRepFaction4', 'RewRepFaction5', 'RewRepValue1', 'RewRepValue2', 'RewRepValue3', 'RewRepValue4', 'RewRepValue5', 'Objectives', 'Details', 'RequestItemsText', 'OfferRewardText', 'ReqCreatureOrGOId1', 'ReqCreatureOrGOId2', 'ReqCreatureOrGOId3', 'ReqCreatureOrGOId4', 'ReqItemId1', 'ReqItemId2', 'ReqItemId3', 'ReqItemId4', 'ReqItemCount1', 'ReqItemCount2', 'ReqItemCount3', 'ReqItemCount4', 'SrcItemId', 'ReqCreatureOrGOCount1', 'ReqCreatureOrGOCount2', 'ReqCreatureOrGOCount3', 'ReqCreatureOrGOCount4', 'ObjectiveText1', 'ObjectiveText2', 'ObjectiveText3', 'ObjectiveText4');

$locale_quest_cols = array('Title_loc'.$AoWoWconf['locale'], 'Details_loc'.$AoWoWconf['locale'], 'Objectives_loc'.$AoWoWconf['locale'], 'OfferRewardText_loc'.$AoWoWconf['locale'], 'RequestItemsText_loc'.$AoWoWconf['locale'], 'EndText_loc'.$AoWoWconf['locale'], 'ObjectiveText1_loc'.$AoWoWconf['locale'], 'ObjectiveText2_loc'.$AoWoWconf['locale'], 'ObjectiveText3_loc'.$AoWoWconf['locale'], 'ObjectiveText4_loc'.$AoWoWconf['locale']);

function QuestReplaceStr($STR)
{
	$toreplace = array (
		0=>array('1'=>'$b', '2'=>'<br />',),
		1=>array('1'=>'$r', '2'=>'&lt;race&gt;',),
		2=>array('1'=>'$c', '2'=>'&lt;class&gt;',),
		3=>array('1'=>'$n', '2'=>'&lt;name&gt;',),
	);
	for ($i=0;$i<=3;$i++)
	{
		$STR = str_replace($toreplace[$i][1], $toreplace[$i][2], $STR);
		$STR = str_replace(strtoupper($toreplace[$i][1]), $toreplace[$i][2], $STR);
	}
	return $STR;
}

function render_quest_tooltip(&$row)
{
	$x = '';
	
	// Название квеста
	$x .= '<table><tr><td>';
	$x .= '<b class="q">'.$row['Title'].'</b>';
	$x .= '</td></tr></table>';

	//  TODO: Описание квеста
	$x .= '<table><tr><td>';
	$x .= '</td></tr></table>';

	// TODO: Требования квеста

	return $x;
}

function allquestsinfo2(&$row, $level=0)
{
	$quest['name'] = $row['Title'];
	if ($level>0)
		$quest['info'] = render_quest_tooltip($row);
	if ($level == 1)
		return $quest;
	else
		return;
}

function allquestsinfo($id, $level=0)
{
	global $DB;

	$row = $DB->selectRow('
		SELECT Title
		FROM quest_template
		WHERE
			entry=?d
		LIMIT 1
		',
		$id
	);

	if ($row)
		return allquestsinfo2($row, $level);
	else
		return;
}

// Функция информации о квесте
//  $Row - ссылка на ассоциативный массив из базы
function questinfo2(&$Row, $level=0)
{
	// $level - колво данных, необходимых для возвращения
	//  0 - самое минимальное, для списка
	//  1 - поподробнее, для просмотра конкретного квеста

	$RewChoiceItemId = array("RewChoiceItemId1", "RewChoiceItemId2", "RewChoiceItemId3", "RewChoiceItemId4", "RewChoiceItemId5", "RewChoiceItemId6");
	$RewChoiceItemCount = array("RewChoiceItemCount1", "RewChoiceItemCount2", "RewChoiceItemCount3", "RewChoiceItemCount4", "RewChoiceItemCount5", "RewChoiceItemCount6");
	$RewItemId = array("RewItemId1", "RewItemId2", "RewItemId3", "RewItemId4");
	$RewItemCount = array("RewItemCount1", "RewItemCount2", "RewItemCount3", "RewItemCount4");
	// $quest - преобразованный для js-скриптов массив
	// Номер квеста
	$quest['id'] = $Row['entry'];
	// Название квеста
	$quest['name'] = $Row['Title'];
	// Уровень квеста
	$quest['level'] = $Row['QuestLevel'];
	// Требуемый уровень квеста
	$quest['reqlevel'] = $Row['MinLevel'];
	// Квест, для:
	// TODO: более логичная система, поменьше непонятных цифер
	switch ($Row['RequiredRaces']):
		case '0':
			// Для всех?
			$quest['side'] = 3;
			$quest['sidename'] = 'Both';
			break;
		case '690':
			// Орда?
			$quest['side'] = 2;
			$quest['sidename'] = 'Horde';
			break;
		case '1101':
			// Альянс?
			$quest['side'] = 1;
			$quest['sidename'] = 'Alliance';
			break;
		default:
			// Неизвестно...
			$quest['side'] = $Row['RequiredRaces'];
	endswitch;

	// Награды вещей
	$k = 0;
	foreach ($RewChoiceItemId as $j => $RowName) {
		$item_id = $Row[$RowName];
		$item_count = $Row[$RewChoiceItemCount[$j]];
		if (($item_id != 0) and ($item_count != 0)) {
			$quest['itemchoices'][$k] = allitemsinfo($item_id, $level);
			$quest['itemchoices'][$k]['count'] = $item_count;
			allitemsinfo($item_id, $level);
			$k++;
		} else {
			$quest['itemchoices'][$k] = 0;
			unset($quest['itemchoices'][$k]);
		}
	}
	if (count($quest['itemchoices']) <= 1)
		unset($quest['itemchoices']);

	$k = 0;
	foreach ($RewItemId as $j => $RowName) {
		$item_id = $Row[$RowName];
		$item_count = $Row[$RewItemCount[$j]];
		if (($item_id != 0) and ($item_count != 0)) {
			$quest['itemrewards'][$k] = allitemsinfo($item_id, $level);
			$quest['itemrewards'][$k]['count'] = $item_count;
			$k++;
		}
	}

	// Вознаграждение репутацией
	if ($level>0)
	{
		$k = 0;
		for ($j=1;$j<=5;$j++)
			if (($Row['RewRepFaction'.$j] != 0) and ($Row['RewRepValue'.$j] != 0)) {
				$quest['reprewards'][$k] = array();
				$quest['reprewards'][$k] = factioninfo($Row['RewRepFaction'.$j]);
				$quest['reprewards'][$k]['value'] = $Row['RewRepValue'.$j];
				$k++;
			}
	}
	// Вознаграждение опытом
	/*
	Формула расчёта ХР из значения в данном поле:
	QuestLevel >= 65: XP = RewMoneyMaxLevel / 6.0
	QuestLevel == 64: XP = RewMoneyMaxLevel / 4.8
	QuestLevel == 63: XP = RewMoneyMaxLevel / 3.6
	QuestLevel == 62: XP = RewMoneyMaxLevel / 2.4
	QuestLevel == 61: XP = RewMoneyMaxLevel / 1.2
	QuestLevel <= 60: XP = RewMoneyMaxLevel / 0.6
	*/
	if ($Row['QuestLevel']>=65)
		$xp_mn = 6.0;
	elseif ($Row['QuestLevel']==64)
		$xp_mn = 4.8;
	elseif ($Row['QuestLevel']==63)
		$xp_mn = 3.6;
	elseif ($Row['QuestLevel']==62)
		$xp_mn = 2.4;
	elseif ($Row['QuestLevel']==61)
		$xp_mn = 1.2;
	elseif ($Row['QuestLevel']<=60)
		$xp_mn = 0.6;
	else
		$xp_mn = 1;

	$quest['xp'] = round($Row['RewMoneyMaxLevel'] / $xp_mn);

	// Вознаграждение деньгами
	$quest['money'] = $Row['RewOrReqMoney'];
	// Тип квеста
	$quest['type'] = $Row['Type'];
	if ($quest['type'] == 1)
		$quest['typename'] = 'Group';
	else
		$quest['typename'] = $quest['type'];
	// Категория 1
	$quest['category'] = $Row['ZoneOrSort'];
	// Категория 2 ???
	$quest['category2'] = $Row['QuestFlags'];
	return $quest;
}

function questinfo($id, $level=0)
{
	echo "questinfo() stub;";
	return;
}

?>
