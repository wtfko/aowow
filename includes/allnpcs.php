<?php

// Для списка creatureinfo()
$npc_cols[0] = array('name', 'subname', 'minlevel', 'maxlevel', 'type', 'rank', 'faction_A','faction_H');
$npc_cols[1] = array('subname', 'minlevel', 'maxlevel', 'type', 'rank', 'minhealth', 'maxhealth', 'minmana', 'maxmana', 'mingold', 'maxgold', 'lootid', 'spell1', 'spell2', 'spell3', 'spell4', 'faction_A', 'faction_H');

// Функция информации о создании
function creatureinfo2(&$Row)
{
	// Номер создания
	$creature['id'] = $Row['entry'];
	// Имя создания
	$creature['name'] = $Row['name'];
	// Подимя создания
	$creature['subname'] = $Row['subname'];
	// Min/Max уровни
	$creature['minlevel'] = $Row['minlevel'];
	$creature['maxlevel'] = $Row['maxlevel'];
	// TODO: Месторасположение
	//	$creature['location'] = location($creature['id'],'creature');
	// TODO: Реакция на фракции
	$creature['react'] = ($Row['faction_A']).','.($Row['faction_H']);
	// Тип NPC
	$creature['type'] = $Row['type'];
	// Тег NPC
	$creature['tag'] = str_normalize($Row['subname']);
	// Ранг NPC
	$creature['classification'] = $Row['rank'];
	return $creature;
}

// Функция информации о создании
function creatureinfo($id)
{
	global $DB;
	global $npc_cols;
	$row = $DB->selectRow('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			entry=?d
			AND factiontemplateID=faction_A
		LIMIT 1
		',
		$npc_cols[0],
		$id);
	return creatureinfo2($row);
}

?>
