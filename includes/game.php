<?php

require_once ('includes/allitems.php');

// Классы персонажей
define ("CLASS_WARRIOR", 1);
define ("CLASS_PALADIN", 2);
define ("CLASS_HUNTER", 4);
define ("CLASS_ROGUE", 8);
define ("CLASS_PRIEST", 16);
define ("CLASS_SHAMAN", 64);
define ("CLASS_MAGE", 128);
define ("CLASS_WARLOCK", 256);
define ("CLASS_DRUID", 1024);

// Типы разделов
global $types;
$types = array(
	1 => 'npc',
	2 => 'object',
	3 => 'item',
	4 => 'itemset',
	5 => 'quest',
	6 => 'spell',
	7 => 'zone',
	8 => 'faction'
);

function sum_subarrays_by_key( $tab, $key ) {
	$sum = 0;
	foreach($tab as $sub_array) {
		$sum += $sub_array[$key];
	}
	return $sum;
}

function coord_mangos2wow($mapid, $x, $y, $global)
{
	// Карты
	global $map_images;
	// Подключение к базе
	global $DB;

	//echo "<font color=yellow>MapID: $mapid</font><br>";

	$rows = $DB->select("SELECT * FROM ?_zones WHERE (mapID=? and x_min<? and x_max>? and y_min<? and y_max>?)", $mapid, $x, $x, $y, $y);

	foreach ($rows as $numRow=>$row) {
		// Сохраяняем имя карты и координаты
		$wow['zone'] = $row['areatableID'];
		$wow['name'] = $row['name'];

		//echo "..Zone: ".$wow['zone']."; Name: ".$wow['name']."<br>";

		// Т.к. в игре координаты начинают отсчёт с левого верхнего угла
		//  а в системе координат сервера с правого нижнего,
		//  делаем соответствующее преобразование.
		$tx = 100 - ($y - $row["y_min"]) / (($row["y_max"] - $row["y_min"]) / 100);
		$ty = 100 - ($x - $row["x_min"]) / (($row["x_max"] - $row["x_min"]) / 100);

		// А если ещё и с цветом совпала - нах цикл, это всё наше :) Оо
		// Если ещё не загружена - загружаем.
		if (!isset($map_images[$wow['zone']])) {
			$mapname = str_replace("\\", "/", getcwd()).'/images/tmp/'.$row['areatableID'].'.png';
			if (file_exists($mapname)) {
				$map_images[$wow['zone']] = @ImageCreateFromPNG($mapname);
				//echo "<font color=green>....MapImage ".$mapname." Exists (ID=".$wow['zone'].")";
				if (!$map_images[$wow['zone']]) {
					echo "<font color=red> But Cannot Be Loaded!</font>";
				} else {
					//echo " And Loaded.";
				}
				//echo "</font><br>";
			} else {
				echo "<font color=red>....Map $mapname not founded (ID=".$wow['zone'].")</font><br>";
			}
		}

		// Если так и не загрузилась... Возможно такой карты ещё просто нету :)
		if ($map_images[$wow['zone']]) {
			//echo "....MapImage ".$wow['zone']." Loaded (Color = ".ImageColorAt($map_images[$wow['zone']], round($tx*10), round($ty*10))." At [".round($tx*10).",".round($ty*10)."])<br>";
			if (@ImageColorAt($map_images[$wow['zone']], round($tx * 10), round($ty * 10)) === 0) {
				//echo "<font color=blue>....MapImage ".$wow['zone']." Selected</font><br>";
				break;
			} else {
				//echo "....MapImage ".$wow['zone']." Was Not Selected<br><br>";
			}
		}
	}

	if (count($rows)==0)
	{
		// Ничего не найдено. Мб инста??

		$row = $DB->selectRow("SELECT * FROM ?_zones WHERE (mapID=? and x_min=0 and x_max=0 and y_min=0 and y_max=0)", $mapid);
		if ($row) {
			$wow['zone'] = $row['areatableID'];
			$wow['name'] = $row['name'];
		} else {
			echo "<font color=red>....Location for Map with ID=$mapid not founded</font><br>";
			return;
		}
	}

	// округляем до 2 цифер после запятой
	if (isset($tx, $ty)) {
		$wow["x"] = round($tx, 2);
		$wow["y"] = round($ty, 2);
	}

	return $wow;
}

// Преобразование целого массива координат
// Всегда пользовацца только им!
function mass_coord(&$data)
{
	// Карты
	global $map_images;
	// Объявляем новый массив с преобразованными данными
	$xdata = array();
	// Перебираем по порядку все координаты, посланные функции
	//  Если таких же координат (уже преобразованных) ещё нет, добавляем в новый массив
	foreach ($data as $ndata) {
		$tmp = coord_mangos2wow($ndata['m'], $ndata['x'], $ndata['y'], false);
		if (!in_array($tmp, $xdata))
			$xdata[] = $tmp;
	}
	// Освобождаем всю память выделенную под карты
	if ($map_images)
		foreach ($map_images as $map_image)
			imagedestroy($map_image);

	// Возвращаем новый массив
	return $xdata;
}

// Функция информации о фракции
function factioninfo($id)
{
	global $DB;
	$row = $DB->selectRow("select name	from ?_factions where (factionID=?) limit 1", $id);
	$faction['name'] = $row['name'];
	$faction['id'] = $id;
	return $faction;
}

// Лут
function loot($table, $lootid)
{
	global $DB;
	$rows = $DB->select('
	SELECT l.ChanceOrQuestChance, l.mincountOrRef, l.maxcount, l.item
	FROM ?# l
	WHERE
		l.entry=?
	',
	$table,
	$lootid
);
	$loot = array();
	foreach ($rows as $i => $row)
	{
		if ($row['mincountOrRef'] < 0)
			$loot = array_merge($loot, loot($table, -$row['mincountOrRef']));
		else
		{
			$num = count($loot);
			$loot[$num] = array();
			$loot[$num] = iteminfo($row['item']);
			$loot[$num]['mincount'] = $row['mincountOrRef'];
			$loot[$num]['maxcount'] = $row['maxcount'];
			$loot[$num]['percent'] = abs($row['ChanceOrQuestChance']);
		}
	}
	return $loot;
}

// Кто дропает
function drop($table, $item)
{
	global $DB;
	$rows = $DB->select('
		SELECT l.ChanceOrQuestChance, l.mincountOrRef, l.maxcount, l.entry
		FROM ?# l
		WHERE
			l.item=?
		',
		$table,
		$item
	);
	$drop = array();
	foreach ($rows as $i => $row)
	{
		if ($row['mincountOrRef'] > 0)
		{
			$num = $row['entry'];
			$drop[$num] = array();
			$drop[$num]['percent'] = abs($row['ChanceOrQuestChance']);
			$drop[$num]['mincount'] = $row['mincountOrRef'];
			$drop[$num]['maxcount'] = $row['maxcount'];
			
			// Ищем лут, который ссылается на этот лут
			$refrows = $DB->select('SELECT entry FROM ?# WHERE mincountOrRef=?',$table, -$num);
			foreach ($refrows as $i => $refrow)
			{
				$num = $refrow['entry'];
				$drop[$num] = array();
				$drop[$num]['percent'] = abs($row['ChanceOrQuestChance']);
				$drop[$num]['mincount'] = $row['mincountOrRef'];
				$drop[$num]['maxcount'] = $row['maxcount'];
			}
		}
	}
	return $drop;
}
?>
