<?php
// Действие
$action = $_REQUEST['account'];
if (($action=='signin') and (isset($_POST['username'])) and (isset($_POST['password'])))
{
	$usersend_pass = create_usersend_pass($_POST['username'], $_POST['password']);
	$user = CheckPwd($_POST['username'], $usersend_pass);
	if ($user==-1)
	{
		del_user_cookie();
		if (isset($_SESSION['username']))
			UnSet($_SESSION['username']);
		$signin_error = 'Не верное имя пользователя';
		$action = 'signin_false';
	} elseif ($user==0) {
		del_user_cookie();
		if (isset($_SESSION['username']))
			UnSet($_SESSION['username']);
		$signin_error = 'Не верный пароль';
		$action = 'signin_false';
	} else {
		// Имя пользователя и пароль совпадают
		$_SESSION['username'] = $user['name'];
		$_SESSION['shapass'] = $usersend_pass;
		$action = 'signin_true';
		if ($_POST['remember_me']=='yes')
		{
			// Запоминаем пользователя
			$remember_time = time() + 3000000;
			SetCookie('remember_me',$_POST['username'].$usersend_pass,$remember_time);
		}
	}
}

switch($action):
	case '':
		// TODO: Настройки аккаунта (Account Settings)
		break;
	case 'signin_false':
		$smarty->assign('error', $signin_error);
	case 'signin':
		// Вход в систему
		$smarty->display('signin.tpl');
		break;
	case 'signup_false':
		$smarty->assign('signup_error', 'Создание такого аккаунта по тем или иным причинам невозможно');
	case 'signup':
		// Регистрация аккаунта
		$smarty->display('signup.tpl');
		break;
	case 'signout':
		// Выход из пользователя
		UnSet($user);
		session_unset();
		session_destroy();
		$_SESSION = array();
		del_user_cookie();
	case 'signin_true':
	default:
		// На главную страницу
		// Срабатывает при:
		//  1. $action = 'signout' (выход)
		//  2. $action = 'signok' (успешная авторизация)
		//  3. Неизвестное значение $action
		// TODO: Загрузить главную страницу
		if ($_REQUEST['next']=='?account=signin')
			$_REQUEST['next']='';
		echo '<meta http-equiv="Refresh" content="0; URL=?'.$_REQUEST['next'].'">';
		echo '<style type="text/css">';
		echo 'body {background-color: black;}';
		echo '</style>';
		break;
endswitch;
?>
