<?php
// Настройка шаблонизатора и ДБ
include ('includes/kernel.php');

//phpinfo();

// Объект шаблонизатора
$smarty = new Smarty_AoWoW('wowhead');

// Имя пользователя и пасс
session_start();

if (IsSet($_COOKIE['remember_me']) and !(IsSet($_SESSION['username'])))
{
	$_SESSION['username'] = substr($_COOKIE['remember_me'], 0, strlen($_COOKIE['remember_me'])-40);
	$_SESSION['shapass'] = substr($_COOKIE['remember_me'], strlen($_COOKIE['remember_me'])-40, 40);
}

if (IsSet($_SESSION['username']) and IsSet($_SESSION['shapass']))
{
	$user = array();
	$user = CheckPwd($_SESSION['username'], $_SESSION['shapass']);
	$_SESSION['userid'] = $user['id'];
	$_SESSION['roles'] = $user['roles'];
	if ($user>0)
		$smarty->assign('user', $user);
	else
		UnSet($user);
}

// Язык сайта
if (!IsSet($_SESSION['locale']))
	$smarty->assign('locale', $AoWoWconf['locale']);
else
	$smarty->assign('locale', $_SESSION['locale']);
$smarty->assign('language', $languages[$smarty->get_template_vars('locale')]);



// Параметры передаваемые скрипту
$queryx = $_SERVER['QUERY_STRING'];
@list($razdel, $podrazdel) = explode('=', $_SERVER['QUERY_STRING']);

// Tidy... ломает html
//$smarty->load_filter('output','tidyrepairhtml');

// Язык, настройки
$conf_file = $smarty->get_template_vars('language').'.conf';
$smarty->assign('conf_file', $conf_file);
$smarty->assign('query', $_SERVER['QUERY_STRING']);

// В зависимости от раздела, выбираем что открывать:
switch($razdel):
	case 'locale':
		// Изменение языка сайта
		$_SESSION['locale'] = $podrazdel;
		echo '<meta http-equiv="Refresh" content="0; URL='.$_SERVER["HTTP_REFERER"].'">';
		echo '<style type="text/css">';
		echo 'body {background-color: black;}';
		echo '</style>';
		break;
	case 'account':
		include 'account.php';
		break;
	case 'comment':
		include 'comment.php';
		break;
	case 'faction':
		include 'faction.php';
		break;
	case 'factions':
		include 'factions.php';
		break;
	case 'item':
		include 'item.php';
		break;
	case 'items':
		include 'items.php';
		break;
	case 'itemset':
		include 'itemset.php';
		break;
	case 'itemsets':
		include 'itemsets.php';
		break;
	case 'latest':
		include 'latest.php';
		break;
	case 'maps':
		// Карты...
		include 'maps.php';
		break;
	case 'npc':
		// NPC...
		include 'npc.php';
		break;
	case 'npcs':
		include 'npcs.php';
		break;
	case 'object':
		include 'object.php';
		break;
	case 'objects':
		include 'objects.php';
		break;
	case 'quest':
		include 'quest.php';
		break;
	case 'quests':
		include 'quests.php';
		break;
	case 'search':
		include 'search.php';
		break;
	case 'spell':
		include 'spell.php';
		break;
	case 'spells':
		include 'spells.php';
		break;
	default:
		include 'main.php';
		break;
endswitch;

?>
