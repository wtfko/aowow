{config_load file="$conf_file"}
<html>

<head>
{include file='head.tpl'}
</head>

<body class="home">
	<div id="debugPanel"></div>
	<div id="layers"></div>
	<table class="home-table">
		<tr>
			<td>
				<div class="home-panel">
					<div class="home-logo"></div>
					<form method="get" action="./" name="f">
						<input id="home-search-input" type="text" title="WOWDB Search" maxlength="256" name="search" />                    
						<script>document.f.search.focus();</script>
						<button class="home-search-button" type="submit">
							<span>Search</span>
						</button>
					</form>
					<div id="spanMainMenu" class="spanMenuButtons"></div>
					<div id="spanHomeNews">Новый шаблон для AoWoW!</div>
				</div>
				<div id="pageFooter">
					Мммм. Аркано.
				</div>
			</td>
		</tr>
	</table>
	<script src="templates/wowdb/js/compiled.js" type=text/javascript></script>
</body>
</html>
