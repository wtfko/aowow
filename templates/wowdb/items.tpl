{config_load file="$conf_file"}
<html>

<head>
{include file='head.tpl'}
</head>

<body>
	<div id="layers"></div>
	<table id="main-layout" cellpadding="0">
		<tr>
			<td id="main-layout-l"><div id="main-bg-l"></div></td>
			<td>
				<table id="main-layout-center" cellpadding="0">
					<tr>
						<td style="height: 128px;">
							<div id="divHeader">
								<div id="divHeaderLogo"><h2 title="World of Warcraft Database">WOWDB.com</h2><a href="./"></a></div>
								<div id="divAdContainerTop"><iframe scrolling="no" src="top.html" frameborder=0></iframe></div>
								<div id="divSearchInput">
									<div id="spanBookmarks"></div>
									<form id="searchform" action="?search" onsubmit="return submitSearch()">
										<input name="search" type="text" value="Search the database..." id="titleSearchInput" class="search-input" onfocus="handleSearchInputFocus(this);" />
										<button onclick="submitSearch(this);" class="smallButton"><span>Search</span></button>
									</form>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td style="height: 39px;">
							<div id="divTopbar">
								<div id="menu-bg-l"></div><div id="menu-bg-r"></div>
								<div id="spanMainMenu" class="spanMenuButtons"></div>
								<div id="divTopbarMenuRight">
									<div id="divLoginOptions">Welcome to WOWDB!&nbsp;&nbsp;<a onclick="navToLogin();">Login</a> or <a href="register.aspx">Register</a></div>
									<div id="spanUserMenu" class="spanMenuButtons"></div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="divWrapper">
								<div id="divMain" class="noads">
									<div id="divMainPrecontents">
										<div id="divBrowsePathRight"></div>
										<div id="divBrowsePathLeft" class="divBrowsePathLeft"></div>                
									</div>
									<div id="divMainWrapper">
										<div id="divMainBGLeft"></div>
										<div id="divMainBGRight"></div>
										<div id="divMainFooter"></div>
										<div id="divMainContents">
											<div id="searchResultsSummary"></div>
											<div class="tabs-container" id="tabsContainer"></div>
											<div class="content-panel" id="resultsContentPanel">
												<div class="content-panel-bg"></div>
												<div class="content-panel-r"></div>
												<div class="content-panel-b"></div>
												<div class="content-panel-br"></div>
												<div class="content-panel-t"></div>
												<div class="content-panel-tr"></div>
												<div class="content-panel-wrapper">
													<div class="divResultsView" id="divItemListContainer">
														<div id="divSearchResultsCount"></div>
														<div id="divSearchFilters">
															<div id="divItemListPaging"></div>
															<div id="divRefineContainer">
																<span onmouseover="showTooltip('Start typing the name of what you\'re searching for, and the results will update instantly.',event,'r');" onmouseout="hideTooltip();" class="tip">Refine by name:</span>
																<div id="inputSearchFilterContainer">
																	<input type="text" id="inputSearchFilterName" />
																</div>
																<input type="radio" id="inputSearchFilterTypeStarts" name="inputSearchFilterType" />
																<label for="inputSearchFilterTypeStarts"> Starts With </label>
																<input checked type="radio" name="inputSearchFilterType"  id="inputSearchFilterTypeContains" />
																<label for="inputSearchFilterTypeContains"> Contains  </label>&nbsp;&nbsp;
																<select id="inputSearchPerPage" onchange="handleResultsChange(event);">
																	<option value=15>15</option>
																	<option value=25>25</option>
																	<option value=50>50</option>
																	<option value=100>100</option>
																</select> Results Per Page
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="pageFooter">
									<img src="templates/wowdb/images/clear.gif" width="986" height="1" /><br />
									Ммм. Аркано :)
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td id="main-layout-r"><div id="main-bg-r"></div></td>
		</tr>
	</table>
	<div class="tooltip" id="itemPanel">
		<table>
			<tr>
				<td id="itemPanelText" >                  
				<th style="background-position: right top;">
			</tr>
			<tr>
				<th style="background-position: left bottom;"></th>
				<th style="background-position: right bottom;"></th>
			</tr>
		</table>
	</div>
	<div class="tooltip" id="itemPanel1">
		<table>
			<tr>
				<td id="itemPanelText1" >                  
				<th style="background-position: right top;">
			</tr>
			<tr>
				<th style="background-position: left bottom;"></th>
				<th style="background-position: right bottom;"></th>
			</tr>
		</table>
	</div>
	<div class="tooltip" id="itemPanel2">
		<table>
			<tr>
				<td id="itemPanelText2" >                  
				<th style="background-position: right top;">
			</tr>
			<tr>
				<th style="background-position: left bottom;"></th>
				<th style="background-position: right bottom;"></th>
			</tr>
		</table>
	</div>
	<script src="templates/wowdb/js/compiled.js" type=text/javascript></script>
	
	<script>_searchDescription = '';
		_browseDescription = [["1","Items"],["3","Gems"],["0","Red"]];
		addBrowseMenu("divBrowsePathLeft", _browseDescription);
		{include file='bricks/allitems_table.tpl' data=$allitems}
	</script>
	<script>
		_searchBrowseString = '1.3.0';
		{include file='bricks/item_table.tpl' data=$items}
	</script>
	<script>
		handleResultsLoading();
	</script>
</body>
</html>
