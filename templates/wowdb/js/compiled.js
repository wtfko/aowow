function AscLookup() {
    this.Lookup = mLookup;
    this.Add = mAdd;
    this.LookupList = mLookupList;
    this.Add("category_quests_misc", "365", "Ahn'Qiraj War");
    this.Add("category_quests_misc", "25", "Battlegrounds");
    this.Add("category_quests_misc", "370", "Brewfest");
    this.Add("category_quests_misc", "364", "Darkmoon Faire");
    this.Add("category_quests_misc", "1", "Epic");
    this.Add("category_quests_misc", "368", "Invasion");
    this.Add("category_quests_misc", "344", "Legendary");
    this.Add("category_quests_misc", "366", "Lunar Festival");
    this.Add("category_quests_misc", "369", "Midsummer");
    this.Add("category_quests_misc", "367", "Reputation");
    this.Add("category_quests_misc", "22", "Seasonal");
    this.Add("category_quests_misc", "284", "Special");
    this.Add("category_quests_misc", "221", "Treasure Map");
    this.Add("class_id", "11", "Druid");
    this.Add("class_id", "3", "Hunter");
    this.Add("class_id", "8", "Mage");
    this.Add("class_id", "2", "Paladin");
    this.Add("class_id", "5", "Priest");
    this.Add("class_id", "4", "Rogue");
    this.Add("class_id", "7", "Shaman");
    this.Add("class_id", "9", "Warlock");
    this.Add("class_id", "1", "Warrior");
    this.Add("client_version", "8089,8125", "2.4");
    this.Add("client_version", "7799", "2.3.3");
    this.Add("client_version", "7741", "2.3.2");
    this.Add("client_version", "7561,7741,7799", "2.3");
    this.Add("client_version", "7359", "2.2.3");
    this.Add("client_version", "7318", "2.2.2");
    this.Add("client_version", "7272,7318,7359", "2.2");
    this.Add("client_version", "6692", "2.1");
    this.Add("client_version", "6546", "2.0");
    this.Add("filter_item_drop_mode_id", "1", "Normal Mode Only");
    this.Add("filter_item_drop_mode_id", "2", "Heroic Mode Only");
    this.Add("filter_item_drop_mode_id", "1|2", "Normal or Heroic Mode");
    this.Add("filter_item_drop_mode_id", "1^2", "Normal and Heroic Mode");
    this.Add("filter_item_socket_color_id", "1|2|4|8", "Any");
    this.Add("filter_item_socket_color_id", "8", "Blue");
    this.Add("filter_item_socket_color_id", "1", "Meta");
    this.Add("filter_item_socket_color_id", "2", "Red");
    this.Add("filter_item_socket_color_id", "4", "Yellow");
    this.Add("filter_quest_side_id", "2|4|*", "Any");
    this.Add("filter_quest_side_id", "2", "Alliance");
    this.Add("filter_quest_side_id", "4", "Horde");
    this.Add("filter_quest_side_id", "1", "Both");
    this.Add("filter_yes_no", "1", "Yes");
    this.Add("filter_yes_no", "0", "No");
    this.Add("item_binding_type_id", "2", "Binds when equipped");
    this.Add("item_binding_type_id", "1", "Binds when picked up");
    this.Add("item_binding_type_id", "3", "Binds when used");
    this.Add("item_binding_type_id", "4", "Quest Item");
    this.Add("item_damage_type_id", "6", "Arcane");
    this.Add("item_damage_type_id", "2", "Fire");
    this.Add("item_damage_type_id", "4", "Frost");
    this.Add("item_damage_type_id", "1", "Holy");
    this.Add("item_damage_type_id", "3", "Nature");
    this.Add("item_damage_type_id", "0", "Physical");
    this.Add("item_damage_type_id", "5", "Shadow");
    this.Add("item_disenchant_chance_id", "4", "");
    this.Add("item_disenchant_chance_id", "1", "Extremely Low (1% - 2%)");
    this.Add("item_disenchant_chance_id", "6", "Guaranteed (100%)");
    this.Add("item_disenchant_chance_id", "5", "High (51% - 99%)");
    this.Add("item_disenchant_chance_id", "3", "Low (15% - 24%)");
    this.Add("item_disenchant_chance_id", "2", "Very Low (3% - 14%)");
    this.Add("item_drop_mode_id", "2", "Drops in a Heroic Instance");
    this.Add("item_drop_mode_id", "1", "Drops in a Normal Instance");
    this.Add("item_rarity_id", "0", "Poor");
    this.Add("item_rarity_id", "1", "Common");
    this.Add("item_rarity_id", "2", "Uncommon");
    this.Add("item_rarity_id", "3", "Rare");
    this.Add("item_rarity_id", "4", "Epic");
    this.Add("item_rarity_id", "5", "Legendary");
    this.Add("item_rarity_id", "6", "Artifact");
    this.Add("item_required_faction_level", "3", "Neutral");
    this.Add("item_required_faction_level", "4", "Friendly");
    this.Add("item_required_faction_level", "5", "Honored");
    this.Add("item_required_faction_level", "6", "Revered");
    this.Add("item_required_faction_level", "7", "Exalted");
    this.Add("item_slot_id", "24", "Ammo");
    this.Add("item_slot_id", "18", "Bag");
    this.Add("item_slot_id", "4", "Body");
    this.Add("item_slot_id", "5", "Chest");
    this.Add("item_slot_id", "16", "Cloak");
    this.Add("item_slot_id", "8", "Feet");
    this.Add("item_slot_id", "11", "Finger");
    this.Add("item_slot_id", "10", "Hand");
    this.Add("item_slot_id", "1", "Head");
    this.Add("item_slot_id", "23", "Held in Off-hand");
    this.Add("item_slot_id", "7", "Legs");
    this.Add("item_slot_id", "21", "Main Hand");
    this.Add("item_slot_id", "2", "Neck");
    this.Add("item_slot_id", "22", "Off Hand");
    this.Add("item_slot_id", "13", "One-Hand");
    this.Add("item_slot_id", "15", "Ranged");
    this.Add("item_slot_id", "26", "Ranged");
    this.Add("item_slot_id", "27", "Ranged");
    this.Add("item_slot_id", "28", "Relic");
    this.Add("item_slot_id", "14", "Shield");
    this.Add("item_slot_id", "3", "Shoulder");
    this.Add("item_slot_id", "19", "Tabard");
    this.Add("item_slot_id", "25", "Thrown");
    this.Add("item_slot_id", "12", "Trinket");
    this.Add("item_slot_id", "17", "Two-Hand");
    this.Add("item_slot_id", "6", "Waist");
    this.Add("item_slot_id", "9", "Wrist");
    this.Add("item_socket_color_id", "8", "Blue Socket");
    this.Add("item_socket_color_id", "1", "Meta Socket");
    this.Add("item_socket_color_id", "2", "Red Socket");
    this.Add("item_socket_color_id", "4", "Yellow Socket");
    this.Add("item_source_id", "4", "Crafted");
    this.Add("item_source_id", "11", "Disenchanting");
    this.Add("item_source_id", "1", "Drop");
    this.Add("item_source_id", "6", "Gathered");
    this.Add("item_source_id", "5", "Mined");
    this.Add("item_source_id", "10", "Pick Pocketed");
    this.Add("item_source_id", "7", "Prospected");
    this.Add("item_source_id", "8", "PvP");
    this.Add("item_source_id", "3", "Quest");
    this.Add("item_source_id", "9", "Skinned");
    this.Add("item_source_id", "2", "Vendor");
    this.Add("location_category_id", "0", "Eastern Kingdoms");
    this.Add("location_category_id", "1", "Kalimdor");
    this.Add("location_category_id", "2", "Outland");
    this.Add("location_category_id", "3", "Dungeons");
    this.Add("location_category_id", "4", "Raids");
    this.Add("location_category_id", "5", "Battlegrounds");
    this.Add("location_category_id", "6", "Arenas");
    this.Add("location_expansion_id", "1", "The Burning Crusade");
    this.Add("location_faction_id", "2", "Alliance");
    this.Add("location_faction_id", "1", "Contested");
    this.Add("location_faction_id", "4", "Horde");
    this.Add("location_type_id", "0", "");
    this.Add("location_type_id", "4", "Arena");
    this.Add("location_type_id", "3", "Battleground");
    this.Add("location_type_id", "7", "City");
    this.Add("location_type_id", "1", "Dungeon");
    this.Add("location_type_id", "5", "Heroic Dungeon");
    this.Add("location_type_id", "2", "Raid");
    this.Add("location_type_id", "6", "Transit");
    this.Add("npc_classification_id", "3", "Boss");
    this.Add("npc_classification_id", "1", "Elite");
    this.Add("npc_classification_id", "4", "Rare");
    this.Add("npc_classification_id", "2", "Rare Elite");
    this.Add("npc_family_id", "36", "Sea Lion");
    this.Add("npc_type_id", "1", "Beast");
    this.Add("npc_type_id", "8", "Critter");
    this.Add("npc_type_id", "3", "Demon");
    this.Add("npc_type_id", "2", "Dragonkin");
    this.Add("npc_type_id", "4", "Elemental");
    this.Add("npc_type_id", "5", "Giant");
    this.Add("npc_type_id", "7", "Humanoid");
    this.Add("npc_type_id", "9", "Mechanical");
    this.Add("npc_type_id", "12", "Small Pets");
    this.Add("npc_type_id", "10", "Uncategorized");
    this.Add("npc_type_id", "6", "Undead");
    this.Add("object_type_id", "9", "Books");
    this.Add("object_type_id", "3", "Containers");
    this.Add("object_type_id", "25", "Fish Schools");
    this.Add("object_type_id", "-2", "Herbs");
    this.Add("object_type_id", "-3", "Locked Chests");
    this.Add("object_type_id", "-1", "Mineral Veins");
    this.Add("object_type_id", "2", "Quest");
    this.Add("patch_label", "6546", "2.0");
    this.Add("patch_label", "6692", "2.1");
    this.Add("patch_label", "6898", "2.1.3");
    this.Add("patch_label", "7272", "2.2");
    this.Add("patch_label", "7318", "2.2");
    this.Add("patch_label", "7561", "2.3");
    this.Add("patch_label", "7799", "2.3");
    this.Add("patch_label", "7741", "2.3.2");
    this.Add("patch_label", "7897", "2.4");
    this.Add("patch_label", "7923", "2.4");
    this.Add("patch_label", "7948", "2.4");
    this.Add("patch_label", "7958", "2.4");
    this.Add("patch_label", "7962", "2.4");
    this.Add("patch_label", "7979", "2.4");
    this.Add("patch_label", "7994", "2.4");
    this.Add("patch_label", "8031", "2.4");
    this.Add("patch_label", "8063", "2.4");
    this.Add("patch_label", "8089", "2.4");
    this.Add("patch_label", "8125", "2.4");
    this.Add("patch_label", "8016", "2.4");
    this.Add("patch_label", "8049", "2.4");
    this.Add("profession_id", "171", "Alchemy");
    this.Add("profession_id", "164", "Blacksmithing");
    this.Add("profession_id", "185", "Cooking");
    this.Add("profession_id", "333", "Enchanting");
    this.Add("profession_id", "202", "Engineering");
    this.Add("profession_id", "129", "First Aid");
    this.Add("profession_id", "356", "Fishing");
    this.Add("profession_id", "182", "Herbalism");
    this.Add("profession_id", "755", "Jewelcrafting");
    this.Add("profession_id", "165", "Leatherworking");
    this.Add("profession_id", "186", "Mining");
    this.Add("profession_id", "762", "Riding");
    this.Add("profession_id", "393", "Skinning");
    this.Add("profession_id", "197", "Tailoring");
    this.Add("quest_side_id", "2", "Alliance");
    this.Add("quest_side_id", "1", "Both");
    this.Add("quest_side_id", "4", "Horde");
    this.Add("quest_sort", "365", "Ahn'Qiraj War");
    this.Add("quest_sort", "181", "Alchemy");
    this.Add("quest_sort", "121", "Blacksmithing");
    this.Add("quest_sort", "370", "Brewfest");
    this.Add("quest_sort", "304", "Cooking");
    this.Add("quest_sort", "364", "Darkmoon Faire");
    this.Add("quest_sort", "263", "Druid");
    this.Add("quest_sort", "201", "Engineering");
    this.Add("quest_sort", "1", "Epic");
    this.Add("quest_sort", "324", "First Aid");
    this.Add("quest_sort", "101", "Fishing");
    this.Add("quest_sort", "24", "Herbalism");
    this.Add("quest_sort", "261", "Hunter");
    this.Add("quest_sort", "368", "Invasion");
    this.Add("quest_sort", "182", "Leatherworking");
    this.Add("quest_sort", "344", "Legendary");
    this.Add("quest_sort", "366", "Lunar Festival");
    this.Add("quest_sort", "161", "Mage");
    this.Add("quest_sort", "369", "Midsummer");
    this.Add("quest_sort", "141", "Paladin");
    this.Add("quest_sort", "262", "Priest");
    this.Add("quest_sort", "367", "Reputation");
    this.Add("quest_sort", "25", "REUSE - old scarlet monastery");
    this.Add("quest_sort", "241", "REUSE - old sunken temple");
    this.Add("quest_sort", "41", "REUSE - old uldaman");
    this.Add("quest_sort", "23", "REUSE - old undercity one");
    this.Add("quest_sort", "21", "REUSE - old wailing caverns");
    this.Add("quest_sort", "162", "Rogue");
    this.Add("quest_sort", "22", "Seasonal");
    this.Add("quest_sort", "82", "Shaman");
    this.Add("quest_sort", "284", "Special");
    this.Add("quest_sort", "264", "Tailoring");
    this.Add("quest_sort", "221", "Treasure Map");
    this.Add("quest_sort", "61", "Warlock");
    this.Add("quest_sort", "81", "Warrior");
    this.Add("quest_type_id", "0", "Normal");
    this.Add("quest_type_id", "1", "Group");
    this.Add("quest_type_id", "81", "Dungeon");
    this.Add("quest_type_id", "62", "Raid");
    this.Add("quest_type_id", "41", "PvP");
    this.Add("quest_type_id", "84", "Escort");
    this.Add("quest_type_id", "85", "Heroic");
    this.Add("race_id", "10", "Blood Elf");
    this.Add("race_id", "11", "Draenei");
    this.Add("race_id", "3", "Dwarf");
    this.Add("race_id", "7", "Gnome");
    this.Add("race_id", "1", "Human");
    this.Add("race_id", "4", "Night Elf");
    this.Add("race_id", "2", "Orc");
    this.Add("race_id", "6", "Tauren");
    this.Add("race_id", "8", "Troll");
    this.Add("race_id", "5", "Undead");
    this.Add("skill_line_category_id", "-2", "Talents");
    this.Add("spell_cooldown_unit", "hours", "hours");
    this.Add("spell_cooldown_unit", "minutes", "mins");
    this.Add("spell_cooldown_unit", "seconds", "secs");
    this.Add("spell_cooldown_unit_long", "hour", "hour");
    this.Add("spell_cooldown_unit_long", "hours", "hours");
    this.Add("spell_cooldown_unit_long", "minute", "minute");
    this.Add("spell_cooldown_unit_long", "minutes", "minutes");
    this.Add("spell_cooldown_unit_long", "second", "second");
    this.Add("spell_cooldown_unit_long", "seconds", "seconds");
    this.Add("spell_focus_object_id", "1459", "Blackhoof Village Windmill");
    this.Add("spell_focus_object_id", "1478", "Brewfest - Mug Request");
    this.Add("spell_focus_object_id", "1462", "Burning Troll Hut");
    this.Add("spell_focus_object_id", "1463", "Entrance to Onyxia's Lair");
    this.Add("spell_focus_object_id", "1460", "Grimtotem Tent");
    this.Add("spell_focus_object_id", "1461", "Hyal Family Monument");
    this.Add("spell_focus_object_id", "1458", "Shipwreck Debris");
    this.Add("spell_focus_object_id", "1471", "Standing at the Booth Counter");
    this.Add("spell_focus_object_id", "1464", "Swamplight Manor Dock");
    this.Add("spell_range", "155", "Hunter Range (TEST)");
    this.Add("spell_school_id", "6", "Arcane");
    this.Add("spell_school_id", "2", "Fire");
    this.Add("spell_school_id", "4", "Frost");
    this.Add("spell_school_id", "1", "Holy");
    this.Add("spell_school_id", "3", "Nature");
    this.Add("spell_school_id", "0", "Physical");
    this.Add("spell_school_id", "5", "Shadow");
    this.Add("talent_category_class_id", "11", "Druid");
    this.Add("talent_category_class_id", "3", "Hunter");
    this.Add("talent_category_class_id", "8", "Mage");
    this.Add("talent_category_class_id", "2", "Paladin");
    this.Add("talent_category_class_id", "5", "Priest");
    this.Add("talent_category_class_id", "4", "Rogue");
    this.Add("talent_category_class_id", "7", "Shaman");
    this.Add("talent_category_class_id", "9", "Warlock");
    this.Add("talent_category_class_id", "1", "Warrior");
    this.Add("totem_category_id", "3", "Air Totem");
    this.Add("totem_category_id", "14", "Arclight Spanner");
    this.Add("totem_category_id", "13", "Blacksmith Hammer");
    this.Add("totem_category_id", "2", "Earth Totem");
    this.Add("totem_category_id", "4", "Fire Totem");
    this.Add("totem_category_id", "15", "Gyromatic Micro-Adjustor");
    this.Add("totem_category_id", "21", "Master Totem");
    this.Add("totem_category_id", "11", "Mining Pick");
    this.Add("totem_category_id", "12", "Philosopher's Stone");
    this.Add("totem_category_id", "62", "Runed Adamantite Rod");
    this.Add("totem_category_id", "10", "Runed Arcanite Rod");
    this.Add("totem_category_id", "6", "Runed Copper Rod");
    this.Add("totem_category_id", "63", "Runed Eternium Rod");
    this.Add("totem_category_id", "41", "Runed Fel Iron Rod");
    this.Add("totem_category_id", "8", "Runed Golden Rod");
    this.Add("totem_category_id", "7", "Runed Silver Rod");
    this.Add("totem_category_id", "9", "Runed Truesilver Rod");
    this.Add("totem_category_id", "1", "Skinning Knife");
    this.Add("totem_category_id", "5", "Water Totem");
    this.Add("location_name", "2079", "Alcaz Island");
    this.Add("location_name", "36", "Alterac Mountains");
    this.Add("location_name", "2597", "Alterac Valley");
    this.Add("location_name", "3526", "Ammen Vale");
    this.Add("location_name", "3358", "Arathi Basin");
    this.Add("location_name", "45", "Arathi Highlands");
    this.Add("location_name", "331", "Ashenvale");
    this.Add("location_name", "3790", "Auchenai Crypts");
    this.Add("location_name", "16", "Azshara");
    this.Add("location_name", "268", "Azshara Crater");
    this.Add("location_name", "3524", "Azuremyst Isle");
    this.Add("location_name", "3", "Badlands");
    this.Add("location_name", "3959", "Black Temple");
    this.Add("location_name", "719", "Blackfathom Deeps");
    this.Add("location_name", "1584", "Blackrock Depths");
    this.Add("location_name", "25", "Blackrock Mountain");
    this.Add("location_name", "1583", "Blackrock Spire");
    this.Add("location_name", "2677", "Blackwing Lair");
    this.Add("location_name", "3702", "Blade's Edge Arena");
    this.Add("location_name", "3522", "Blade's Edge Mountains");
    this.Add("location_name", "4", "Blasted Lands");
    this.Add("location_name", "3525", "Bloodmyst Isle");
    this.Add("location_name", "35", "Booty Bay");
    this.Add("location_name", "46", "Burning Steppes");
    this.Add("location_name", "221", "Camp Narache");
    this.Add("location_name", "1941", "Caverns of Time");
    this.Add("location_name", "3565", "Cenarion Refuge");
    this.Add("location_name", "2918", "Champions' Hall");
    this.Add("location_name", "3905", "Coilfang Reservoir");
    this.Add("location_name", "132", "Coldridge Valley");
    this.Add("location_name", "279", "Dalaran");
    this.Add("location_name", "148", "Darkshore");
    this.Add("location_name", "1657", "Darnassus");
    this.Add("location_name", "41", "Deadwind Pass");
    this.Add("location_name", "154", "Deathknell");
    this.Add("location_name", "2257", "Deeprun Tram");
    this.Add("location_name", "405", "Desolace");
    this.Add("location_name", "2557", "Dire Maul");
    this.Add("location_name", "269", "Dun Algaz");
    this.Add("location_name", "1", "Dun Morogh");
    this.Add("location_name", "14", "Durotar");
    this.Add("location_name", "10", "Duskwood");
    this.Add("location_name", "15", "Dustwallow Marsh");
    this.Add("location_name", "139", "Eastern Plaguelands");
    this.Add("location_name", "12", "Elwynn Forest");
    this.Add("location_name", "3430", "Eversong Woods");
    this.Add("location_name", "3820", "Eye of the Storm");
    this.Add("location_name", "1116", "Feathermoon Stronghold");
    this.Add("location_name", "361", "Felwood");
    this.Add("location_name", "357", "Feralas");
    this.Add("location_name", "3478", "Gates of Ahn'Qiraj");
    this.Add("location_name", "3433", "Ghostlands");
    this.Add("location_name", "721", "Gnomeregan");
    this.Add("location_name", "3923", "Gruul's Lair");
    this.Add("location_name", "2917", "Hall of Legends");
    this.Add("location_name", "3535", "Hellfire Citadel");
    this.Add("location_name", "3483", "Hellfire Peninsula");
    this.Add("location_name", "3562", "Hellfire Ramparts");
    this.Add("location_name", "267", "Hillsbrad Foothills");
    this.Add("location_name", "3606", "Hyjal Summit");
    this.Add("location_name", "1537", "Ironforge");
    this.Add("location_name", "4080", "Isle of Quel'Danas");
    this.Add("location_name", "3457", "Karazhan");
    this.Add("location_name", "21", "Kul Tiras");
    this.Add("location_name", "38", "Loch Modan");
    this.Add("location_name", "170", "Lordamere Lake");
    this.Add("location_name", "4095", "Magisters' Terrace");
    this.Add("location_name", "3836", "Magtheridon's Lair");
    this.Add("location_name", "3792", "Mana-Tombs");
    this.Add("location_name", "2100", "Maraudon");
    this.Add("location_name", "2717", "Molten Core");
    this.Add("location_name", "493", "Moonglade");
    this.Add("location_name", "215", "Mulgore");
    this.Add("location_name", "3518", "Nagrand");
    this.Add("location_name", "3698", "Nagrand Arena");
    this.Add("location_name", "3456", "Naxxramas");
    this.Add("location_name", "3523", "Netherstorm");
    this.Add("location_name", "24", "Northshire Abbey");
    this.Add("location_name", "9", "Northshire Valley");
    this.Add("location_name", "2367", "Old Hillsbrad Foothills");
    this.Add("location_name", "2159", "Onyxia's Lair");
    this.Add("location_name", "1637", "Orgrimmar");
    this.Add("location_name", "2037", "Quel'thalas");
    this.Add("location_name", "2437", "Ragefire Chasm");
    this.Add("location_name", "722", "Razorfen Downs");
    this.Add("location_name", "491", "Razorfen Kraul");
    this.Add("location_name", "220", "Red Cloud Mesa");
    this.Add("location_name", "44", "Redridge Mountains");
    this.Add("location_name", "3429", "Ruins of Ahn'Qiraj");
    this.Add("location_name", "3968", "Ruins of Lordaeron");
    this.Add("location_name", "702", "Rut'theran Village");
    this.Add("location_name", "796", "Scarlet Monastery");
    this.Add("location_name", "2057", "Scholomance");
    this.Add("location_name", "51", "Searing Gorge");
    this.Add("location_name", "3607", "Serpentshrine Cavern");
    this.Add("location_name", "3791", "Sethekk Halls");
    this.Add("location_name", "3789", "Shadow Labyrinth");
    this.Add("location_name", "209", "Shadowfang Keep");
    this.Add("location_name", "188", "Shadowglen");
    this.Add("location_name", "3520", "Shadowmoon Valley");
    this.Add("location_name", "3703", "Shattrath City");
    this.Add("location_name", "1377", "Silithus");
    this.Add("location_name", "3487", "Silvermoon City");
    this.Add("location_name", "130", "Silverpine Forest");
    this.Add("location_name", "3679", "Skettis");
    this.Add("location_name", "406", "Stonetalon Mountains");
    this.Add("location_name", "1519", "Stormwind City");
    this.Add("location_name", "33", "Stranglethorn Vale");
    this.Add("location_name", "2017", "Stratholme");
    this.Add("location_name", "1417", "Sunken Temple");
    this.Add("location_name", "3431", "Sunstrider Isle");
    this.Add("location_name", "4075", "Sunwell Plateau");
    this.Add("location_name", "8", "Swamp of Sorrows");
    this.Add("location_name", "440", "Tanaris");
    this.Add("location_name", "141", "Teldrassil");
    this.Add("location_name", "3845", "Tempest Keep");
    this.Add("location_name", "3428", "Temple of Ahn'Qiraj");
    this.Add("location_name", "3519", "Terokkar Forest");
    this.Add("location_name", "330", "Thandol Span");
    this.Add("location_name", "3848", "The Arcatraz");
    this.Add("location_name", "17", "The Barrens");
    this.Add("location_name", "3696", "The Barrier Hills");
    this.Add("location_name", "2366", "The Black Morass");
    this.Add("location_name", "3713", "The Blood Furnace");
    this.Add("location_name", "3847", "The Botanica");
    this.Add("location_name", "1581", "The Deadmines");
    this.Add("location_name", "3557", "The Exodar");
    this.Add("location_name", "308", "The Forbidding Sea");
    this.Add("location_name", "214", "The Great Sea");
    this.Add("location_name", "207", "The Great Sea");
    this.Add("location_name", "47", "The Hinterlands");
    this.Add("location_name", "3849", "The Mechanar");
    this.Add("location_name", "3455", "The North Sea");
    this.Add("location_name", "3714", "The Shattered Halls");
    this.Add("location_name", "3717", "The Slave Pens");
    this.Add("location_name", "3715", "The Steamvault");
    this.Add("location_name", "717", "The Stockade");
    this.Add("location_name", "1477", "The Temple of Atal'Hakkar");
    this.Add("location_name", "3716", "The Underbog");
    this.Add("location_name", "457", "The Veiled Sea");
    this.Add("location_name", "293", "Thoradin's Wall");
    this.Add("location_name", "400", "Thousand Needles");
    this.Add("location_name", "1638", "Thunder Bluff");
    this.Add("location_name", "1769", "Timbermaw Hold");
    this.Add("location_name", "85", "Tirisfal Glades");
    this.Add("location_name", "3540", "Twisting Nether");
    this.Add("location_name", "1337", "Uldaman");
    this.Add("location_name", "1497", "Undercity");
    this.Add("location_name", "490", "Un'Goro Crater");
    this.Add("location_name", "363", "Valley of Trials");
    this.Add("location_name", "718", "Wailing Caverns");
    this.Add("location_name", "3277", "Warsong Gulch");
    this.Add("location_name", "28", "Western Plaguelands");
    this.Add("location_name", "40", "Westfall");
    this.Add("location_name", "11", "Wetlands");
    this.Add("location_name", "618", "Winterspring");
    this.Add("location_name", "3521", "Zangarmarsh");
    this.Add("location_name", "3805", "Zul'Aman");
    this.Add("location_name", "1176", "Zul'Farrak");
    this.Add("location_name", "1977", "Zul'Gurub");
    this.Add("location_has_modes", "3790", "3790");
    this.Add("location_has_modes", "3562", "3562");
    this.Add("location_has_modes", "4095", "4095");
    this.Add("location_has_modes", "3792", "3792");
    this.Add("location_has_modes", "2367", "2367");
    this.Add("location_has_modes", "3791", "3791");
    this.Add("location_has_modes", "3789", "3789");
    this.Add("location_has_modes", "3848", "3848");
    this.Add("location_has_modes", "2366", "2366");
    this.Add("location_has_modes", "3713", "3713");
    this.Add("location_has_modes", "3847", "3847");
    this.Add("location_has_modes", "3849", "3849");
    this.Add("location_has_modes", "3714", "3714");
    this.Add("location_has_modes", "3717", "3717");
    this.Add("location_has_modes", "3715", "3715");
    this.Add("location_has_modes", "3716", "3716");
    this.Add("skill_line_id", "355", "Affliction");
    this.Add("skill_line_id", "171", "Alchemy");
    this.Add("skill_line_id", "237", "Arcane");
    this.Add("skill_line_id", "26", "Arms");
    this.Add("skill_line_id", "253", "Assassination");
    this.Add("skill_line_id", "44", "Axes");
    this.Add("skill_line_id", "574", "Balance");
    this.Add("skill_line_id", "653", "Bat");
    this.Add("skill_line_id", "210", "Bear");
    this.Add("skill_line_id", "50", "Beast Mastery");
    this.Add("skill_line_id", "261", "Beast Training");
    this.Add("skill_line_id", "164", "Blacksmithing");
    this.Add("skill_line_id", "756", "Blood Elf Racial");
    this.Add("skill_line_id", "211", "Boar");
    this.Add("skill_line_id", "45", "Bows");
    this.Add("skill_line_id", "213", "Carrion Bird");
    this.Add("skill_line_id", "209", "Cat");
    this.Add("skill_line_id", "415", "Cloth");
    this.Add("skill_line_id", "38", "Combat");
    this.Add("skill_line_id", "185", "Cooking");
    this.Add("skill_line_id", "214", "Crab");
    this.Add("skill_line_id", "212", "Crocilisk");
    this.Add("skill_line_id", "226", "Crossbows");
    this.Add("skill_line_id", "173", "Daggers");
    this.Add("skill_line_id", "95", "Defense");
    this.Add("skill_line_id", "354", "Demonology");
    this.Add("skill_line_id", "593", "Destruction");
    this.Add("skill_line_id", "613", "Discipline");
    this.Add("skill_line_id", "207", "Doomguard");
    this.Add("skill_line_id", "760", "Draenei Racial");
    this.Add("skill_line_id", "763", "Dragonhawk");
    this.Add("skill_line_id", "118", "Dual Wield");
    this.Add("skill_line_id", "101", "Dwarven Racial");
    this.Add("skill_line_id", "375", "Elemental Combat");
    this.Add("skill_line_id", "333", "Enchanting");
    this.Add("skill_line_id", "202", "Engineering");
    this.Add("skill_line_id", "373", "Enhancement");
    this.Add("skill_line_id", "189", "Felhunter");
    this.Add("skill_line_id", "134", "Feral Combat");
    this.Add("skill_line_id", "8", "Fire");
    this.Add("skill_line_id", "129", "First Aid");
    this.Add("skill_line_id", "356", "Fishing");
    this.Add("skill_line_id", "473", "Fist Weapons");
    this.Add("skill_line_id", "6", "Frost");
    this.Add("skill_line_id", "256", "Fury");
    this.Add("skill_line_id", "270", "Generic");
    this.Add("skill_line_id", "183", "GENERIC (DND)");
    this.Add("skill_line_id", "215", "Gorilla");
    this.Add("skill_line_id", "46", "Guns");
    this.Add("skill_line_id", "182", "Herbalism");
    this.Add("skill_line_id", "56", "Holy");
    this.Add("skill_line_id", "594", "Holy");
    this.Add("skill_line_id", "654", "Hyena");
    this.Add("skill_line_id", "188", "Imp");
    this.Add("skill_line_id", "206", "Infernal");
    this.Add("skill_line_id", "755", "Jewelcrafting");
    this.Add("skill_line_id", "98", "Language: Common");
    this.Add("skill_line_id", "113", "Language: Darnassian");
    this.Add("skill_line_id", "139", "Language: Demon Tongue");
    this.Add("skill_line_id", "138", "Language: Draconic");
    this.Add("skill_line_id", "111", "Language: Dwarven");
    this.Add("skill_line_id", "313", "Language: Gnomish");
    this.Add("skill_line_id", "673", "Language: Gutterspeak");
    this.Add("skill_line_id", "141", "Language: Old Tongue");
    this.Add("skill_line_id", "109", "Language: Orcish");
    this.Add("skill_line_id", "115", "Language: Taurahe");
    this.Add("skill_line_id", "137", "Language: Thalassian");
    this.Add("skill_line_id", "140", "Language: Titan");
    this.Add("skill_line_id", "315", "Language: Troll");
    this.Add("skill_line_id", "414", "Leather");
    this.Add("skill_line_id", "165", "Leatherworking");
    this.Add("skill_line_id", "633", "Lockpicking");
    this.Add("skill_line_id", "54", "Maces");
    this.Add("skill_line_id", "413", "Mail");
    this.Add("skill_line_id", "163", "Marksmanship");
    this.Add("skill_line_id", "186", "Mining");
    this.Add("skill_line_id", "764", "Nether Ray");
    this.Add("skill_line_id", "126", "Night Elf Racial");
    this.Add("skill_line_id", "125", "Orc Racial");
    this.Add("skill_line_id", "655", "Owl");
    this.Add("skill_line_id", "293", "Plate Mail");
    this.Add("skill_line_id", "40", "Poisons");
    this.Add("skill_line_id", "229", "Polearms");
    this.Add("skill_line_id", "257", "Protection");
    this.Add("skill_line_id", "267", "Protection");
    this.Add("skill_line_id", "753", "Racial - Gnome");
    this.Add("skill_line_id", "754", "Racial - Human");
    this.Add("skill_line_id", "733", "Racial - Troll");
    this.Add("skill_line_id", "220", "Racial - Undead");
    this.Add("skill_line_id", "217", "Raptor");
    this.Add("skill_line_id", "767", "Ravager");
    this.Add("skill_line_id", "573", "Restoration");
    this.Add("skill_line_id", "374", "Restoration");
    this.Add("skill_line_id", "184", "Retribution");
    this.Add("skill_line_id", "762", "Riding");
    this.Add("skill_line_id", "236", "Scorpid");
    this.Add("skill_line_id", "768", "Serpent");
    this.Add("skill_line_id", "78", "Shadow Magic");
    this.Add("skill_line_id", "433", "Shield");
    this.Add("skill_line_id", "393", "Skinning");
    this.Add("skill_line_id", "203", "Spider");
    this.Add("skill_line_id", "765", "Sporebat");
    this.Add("skill_line_id", "136", "Staves");
    this.Add("skill_line_id", "39", "Subtlety");
    this.Add("skill_line_id", "205", "Succubus");
    this.Add("skill_line_id", "51", "Survival");
    this.Add("skill_line_id", "43", "Swords");
    this.Add("skill_line_id", "197", "Tailoring");
    this.Add("skill_line_id", "218", "Tallstrider");
    this.Add("skill_line_id", "124", "Tauren Racial");
    this.Add("skill_line_id", "176", "Thrown");
    this.Add("skill_line_id", "251", "Turtle");
    this.Add("skill_line_id", "172", "Two-Handed Axes");
    this.Add("skill_line_id", "160", "Two-Handed Maces");
    this.Add("skill_line_id", "55", "Two-Handed Swords");
    this.Add("skill_line_id", "162", "Unarmed");
    this.Add("skill_line_id", "204", "Voidwalker");
    this.Add("skill_line_id", "228", "Wands");
    this.Add("skill_line_id", "766", "Warp Stalker");
    this.Add("skill_line_id", "656", "Wind Serpent");
    this.Add("skill_line_id", "208", "Wolf");
    this.Add("filter_required_skill_line_id", "171", "Alchemy");
    this.Add("filter_required_skill_line_id", "164", "Blacksmithing");
    this.Add("filter_required_skill_line_id", "185", "Cooking");
    this.Add("filter_required_skill_line_id", "333", "Enchanting");
    this.Add("filter_required_skill_line_id", "202", "Engineering");
    this.Add("filter_required_skill_line_id", "129", "First Aid");
    this.Add("filter_required_skill_line_id", "356", "Fishing");
    this.Add("filter_required_skill_line_id", "182", "Herbalism");
    this.Add("filter_required_skill_line_id", "148", "Horse Riding");
    this.Add("filter_required_skill_line_id", "755", "Jewelcrafting");
    this.Add("filter_required_skill_line_id", "165", "Leatherworking");
    this.Add("filter_required_skill_line_id", "186", "Mining");
    this.Add("filter_required_skill_line_id", "533", "Raptor Riding");
    this.Add("filter_required_skill_line_id", "762", "Riding");
    this.Add("filter_required_skill_line_id", "197", "Tailoring");
    this.Add("filter_crafted_by", "171", "Alchemy");
    this.Add("filter_crafted_by", "164", "Blacksmithing");
    this.Add("filter_crafted_by", "185", "Cooking");
    this.Add("filter_crafted_by", "333", "Enchanting");
    this.Add("filter_crafted_by", "202", "Engineering");
    this.Add("filter_crafted_by", "129", "First Aid");
    this.Add("filter_crafted_by", "755", "Jewelcrafting");
    this.Add("filter_crafted_by", "165", "Leatherworking");
    this.Add("filter_crafted_by", "186", "Mining");
    this.Add("filter_crafted_by", "197", "Tailoring");
    this.Add("filter_reagent_for", "171", "Alchemy");
    this.Add("filter_reagent_for", "164", "Blacksmithing");
    this.Add("filter_reagent_for", "185", "Cooking");
    this.Add("filter_reagent_for", "333", "Enchanting");
    this.Add("filter_reagent_for", "202", "Engineering");
    this.Add("filter_reagent_for", "129", "First Aid");
    this.Add("filter_reagent_for", "755", "Jewelcrafting");
    this.Add("filter_reagent_for", "165", "Leatherworking");
    this.Add("filter_reagent_for", "186", "Mining");
    this.Add("filter_reagent_for", "197", "Tailoring");
    this.Add("talent_category_id", "6", "Frost");
    this.Add("talent_category_id", "8", "Fire");
    this.Add("talent_category_id", "26", "Arms");
    this.Add("talent_category_id", "38", "Combat");
    this.Add("talent_category_id", "39", "Subtlety");
    this.Add("talent_category_id", "50", "Beast Mastery");
    this.Add("talent_category_id", "51", "Survival");
    this.Add("talent_category_id", "56", "Holy");
    this.Add("talent_category_id", "78", "Shadow");
    this.Add("talent_category_id", "134", "Feral Combat");
    this.Add("talent_category_id", "163", "Marksmanship");
    this.Add("talent_category_id", "184", "Retribution");
    this.Add("talent_category_id", "237", "Arcane");
    this.Add("talent_category_id", "253", "Assassination");
    this.Add("talent_category_id", "256", "Fury");
    this.Add("talent_category_id", "257", "Protection");
    this.Add("talent_category_id", "267", "Protection");
    this.Add("talent_category_id", "354", "Demonology");
    this.Add("talent_category_id", "355", "Affliction");
    this.Add("talent_category_id", "373", "Enhancement");
    this.Add("talent_category_id", "374", "Restoration");
    this.Add("talent_category_id", "375", "Elemental");
    this.Add("talent_category_id", "573", "Restoration");
    this.Add("talent_category_id", "574", "Balance");
    this.Add("talent_category_id", "593", "Destruction");
    this.Add("talent_category_id", "594", "Holy");
    this.Add("talent_category_id", "613", "Discipline");
    this.Add("item_type.-1", "-1", "NewItem");
    this.Add("item_type.0", "0", "Consumable");
    this.Add("item_type.0", "1", "Potion");
    this.Add("item_type.0", "2", "Elixir");
    this.Add("item_type.0", "3", "Flask");
    this.Add("item_type.0", "4", "Scroll");
    this.Add("item_type.0", "5", "Food & Drink");
    this.Add("item_type.0", "6", "Item Enhancement");
    this.Add("item_type.0", "7", "Bandage");
    this.Add("item_type.0", "8", "Other");
    this.Add("item_type.1", "0", "Bag");
    this.Add("item_type.1", "1", "Soul Bag");
    this.Add("item_type.1", "2", "Herb Bag");
    this.Add("item_type.1", "3", "Enchanting Bag");
    this.Add("item_type.1", "4", "Engineering Bag");
    this.Add("item_type.1", "5", "Gem Bag");
    this.Add("item_type.1", "6", "Mining Bag");
    this.Add("item_type.1", "7", "Leatherworking Bag");
    this.Add("item_type.2", "0", "One-Handed Axe");
    this.Add("item_type.2", "1", "Two-Handed Axe");
    this.Add("item_type.2", "2", "Bow");
    this.Add("item_type.2", "3", "Gun");
    this.Add("item_type.2", "4", "One-Handed Mace");
    this.Add("item_type.2", "5", "Two-Handed Mace");
    this.Add("item_type.2", "6", "Polearm");
    this.Add("item_type.2", "7", "One-Handed Sword");
    this.Add("item_type.2", "8", "Two-Handed Sword");
    this.Add("item_type.2", "9", "Obsolete");
    this.Add("item_type.2", "10", "Staff");
    this.Add("item_type.2", "11", "One-Handed Exotic");
    this.Add("item_type.2", "12", "Two-Handed Exotic");
    this.Add("item_type.2", "13", "Fist Weapon");
    this.Add("item_type.2", "14", "Miscellaneous");
    this.Add("item_type.2", "15", "Dagger");
    this.Add("item_type.2", "16", "Thrown");
    this.Add("item_type.2", "17", "Spear");
    this.Add("item_type.2", "18", "Crossbow");
    this.Add("item_type.2", "19", "Wand");
    this.Add("item_type.2", "20", "Fishing Pole");
    this.Add("item_type.3", "0", "Red Gem");
    this.Add("item_type.3", "1", "Blue Gem");
    this.Add("item_type.3", "2", "Yellow Gem");
    this.Add("item_type.3", "3", "Purple Gem");
    this.Add("item_type.3", "4", "Green Gem");
    this.Add("item_type.3", "5", "Orange Gem");
    this.Add("item_type.3", "6", "Meta Gem");
    this.Add("item_type.3", "7", "Simple Gem");
    this.Add("item_type.3", "8", "Prismatic Gem");
    this.Add("item_type.4", "-5", "Off-hand Frill");
    this.Add("item_type.4", "-4", "Trinket");
    this.Add("item_type.4", "-3", "Amulet");
    this.Add("item_type.4", "-2", "Ring");
    this.Add("item_type.4", "-1", "Cloak");
    this.Add("item_type.4", "0", "Miscellaneous");
    this.Add("item_type.4", "1", "Cloth");
    this.Add("item_type.4", "2", "Leather");
    this.Add("item_type.4", "3", "Mail");
    this.Add("item_type.4", "4", "Plate");
    this.Add("item_type.4", "5", "Bucklers");
    this.Add("item_type.4", "6", "Shield");
    this.Add("item_type.4", "7", "Librams");
    this.Add("item_type.4", "8", "Idol");
    this.Add("item_type.4", "9", "Totem");
    this.Add("item_type.5", "0", "Reagent");
    this.Add("item_type.6", "2", "Arrow");
    this.Add("item_type.6", "3", "Bullet");
    this.Add("item_type.7", "0", "Trade Good");
    this.Add("item_type.7", "1", "Parts");
    this.Add("item_type.7", "2", "Explosive");
    this.Add("item_type.7", "3", "Device");
    this.Add("item_type.7", "4", "Gems");
    this.Add("item_type.7", "5", "Cloth");
    this.Add("item_type.7", "6", "Leather");
    this.Add("item_type.7", "7", "Metal & Stone");
    this.Add("item_type.7", "8", "Meat");
    this.Add("item_type.7", "9", "Herb");
    this.Add("item_type.7", "10", "Elemental");
    this.Add("item_type.7", "11", "Other");
    this.Add("item_type.7", "12", "Enchanting");
    this.Add("item_type.9", "0", "Book");
    this.Add("item_type.9", "1", "Leatherworking");
    this.Add("item_type.9", "2", "Tailoring");
    this.Add("item_type.9", "3", "Engineering");
    this.Add("item_type.9", "4", "Blacksmithing");
    this.Add("item_type.9", "5", "Cooking");
    this.Add("item_type.9", "6", "Alchemy");
    this.Add("item_type.9", "7", "First Aid");
    this.Add("item_type.9", "8", "Enchanting");
    this.Add("item_type.9", "9", "Fishing");
    this.Add("item_type.9", "10", "Jewelcrafting");
    this.Add("item_type.11", "2", "Quiver");
    this.Add("item_type.11", "3", "Ammo Pouch");
    this.Add("item_type.12", "0", "Quest");
    this.Add("item_type.13", "0", "Key");
    this.Add("item_type.13", "1", "Lockpick");
    this.Add("item_type.14", "0", "Permanent");
    this.Add("item_type.15", "-1", "Mount");
    this.Add("item_type.15", "0", "Junk");
    this.Add("item_type.15", "1", "Reagent");
    this.Add("item_type.15", "2", "Pet");
    this.Add("item_type.15", "3", "Holiday");
    this.Add("item_type.15", "4", "Other");
    this.Add("faction_id", "529", "Argent Dawn");
    this.Add("faction_id", "1012", "Ashtongue Deathsworn");
    this.Add("faction_id", "910", "Brood of Nozdormu");
    this.Add("faction_id", "609", "Cenarion Circle");
    this.Add("faction_id", "942", "Cenarion Expedition");
    this.Add("faction_id", "946", "Honor Hold");
    this.Add("faction_id", "67", "Horde");
    this.Add("faction_id", "989", "Keepers of Time");
    this.Add("faction_id", "978", "Kurenai");
    this.Add("faction_id", "1011", "Lower City");
    this.Add("faction_id", "1015", "Netherwing");
    this.Add("faction_id", "1038", "Ogri'la");
    this.Add("faction_id", "1031", "Sha'tari Skyguard");
    this.Add("faction_id", "1077", "Shattered Sun Offensive");
    this.Add("faction_id", "970", "Sporeggar");
    this.Add("faction_id", "932", "The Aldor");
    this.Add("faction_id", "933", "The Consortium");
    this.Add("faction_id", "941", "The Mag'har");
    this.Add("faction_id", "990", "The Scale of the Sands");
    this.Add("faction_id", "934", "The Scryers");
    this.Add("faction_id", "935", "The Sha'tar");
    this.Add("faction_id", "967", "The Violet Eye");
    this.Add("faction_id", "59", "Thorium Brotherhood");
    this.Add("faction_id", "947", "Thrallmar");
    this.Add("faction_id", "576", "Timbermaw Hold");
    this.Add("faction_id", "922", "Tranquillien");
    this.Add("faction_id", "270", "Zandalar Tribe");
    this.Add("filter_purchased_with", "20560", "Alterac Valley Mark of Honor");
    this.Add("filter_purchased_with", "32572", "Apexis Crystal");
    this.Add("filter_purchased_with", "32569", "Apexis Shard");
    this.Add("filter_purchased_with", "20559", "Arathi Basin Mark of Honor");
    this.Add("filter_purchased_with", "29736", "Arcane Rune");
    this.Add("filter_purchased_with", "29434", "Badge of Justice");
    this.Add("filter_purchased_with", "34853", "Belt of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "34855", "Belt of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "34856", "Boots of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "34848", "Bracers of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "34852", "Bracers of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "34169", "Breeches of Natural Aggression");
    this.Add("filter_purchased_with", "33455", "Brewfest Prize Ticket");
    this.Add("filter_purchased_with", "34186", "Chain Links of the Tumultuous Storm");
    this.Add("filter_purchased_with", "29754", "Chestguard of the Fallen Champion");
    this.Add("filter_purchased_with", "29753", "Chestguard of the Fallen Defender");
    this.Add("filter_purchased_with", "29755", "Chestguard of the Fallen Hero");
    this.Add("filter_purchased_with", "31089", "Chestguard of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "31091", "Chestguard of the Forgotten Protector");
    this.Add("filter_purchased_with", "31090", "Chestguard of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "30236", "Chestguard of the Vanquished Champion");
    this.Add("filter_purchased_with", "30237", "Chestguard of the Vanquished Defender");
    this.Add("filter_purchased_with", "30238", "Chestguard of the Vanquished Hero");
    this.Add("filter_purchased_with", "24368", "Coilfang Armaments");
    this.Add("filter_purchased_with", "34245", "Cover of Ursol the Wise");
    this.Add("filter_purchased_with", "34332", "Cowl of Gul'dan");
    this.Add("filter_purchased_with", "34339", "Cowl of Light's Purity");
    this.Add("filter_purchased_with", "34345", "Crown of Anasterian");
    this.Add("filter_purchased_with", "34244", "Duplicitous Guise");
    this.Add("filter_purchased_with", "34208", "Equilibrium Epaulets");
    this.Add("filter_purchased_with", "29024", "Eye of the Storm Mark of Honor");
    this.Add("filter_purchased_with", "34180", "Felfury Legplates");
    this.Add("filter_purchased_with", "34229", "Garments of Serene Shores");
    this.Add("filter_purchased_with", "34350", "Gauntlets of the Ancient Shadowmoon");
    this.Add("filter_purchased_with", "29757", "Gloves of the Fallen Champion");
    this.Add("filter_purchased_with", "29758", "Gloves of the Fallen Defender");
    this.Add("filter_purchased_with", "29756", "Gloves of the Fallen Hero");
    this.Add("filter_purchased_with", "31092", "Gloves of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "31094", "Gloves of the Forgotten Protector");
    this.Add("filter_purchased_with", "31093", "Gloves of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "30239", "Gloves of the Vanquished Champion");
    this.Add("filter_purchased_with", "30240", "Gloves of the Vanquished Defender");
    this.Add("filter_purchased_with", "30241", "Gloves of the Vanquished Hero");
    this.Add("filter_purchased_with", "24245", "Glowcap");
    this.Add("filter_purchased_with", "26045", "Halaa Battle Token");
    this.Add("filter_purchased_with", "26044", "Halaa Research Token");
    this.Add("filter_purchased_with", "34342", "Handguards of the Dawn");
    this.Add("filter_purchased_with", "34211", "Harness of Carnal Instinct");
    this.Add("filter_purchased_with", "34243", "Helm of Burning Righteousness");
    this.Add("filter_purchased_with", "29760", "Helm of the Fallen Champion");
    this.Add("filter_purchased_with", "29761", "Helm of the Fallen Defender");
    this.Add("filter_purchased_with", "29759", "Helm of the Fallen Hero");
    this.Add("filter_purchased_with", "31097", "Helm of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "31095", "Helm of the Forgotten Protector");
    this.Add("filter_purchased_with", "31096", "Helm of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "30242", "Helm of the Vanquished Champion");
    this.Add("filter_purchased_with", "30243", "Helm of the Vanquished Defender");
    this.Add("filter_purchased_with", "30244", "Helm of the Vanquished Hero");
    this.Add("filter_purchased_with", "34216", "Heroic Judicator's Chestguard");
    this.Add("filter_purchased_with", "29735", "Holy Dust");
    this.Add("filter_purchased_with", "29766", "Leggings of the Fallen Champion");
    this.Add("filter_purchased_with", "29767", "Leggings of the Fallen Defender");
    this.Add("filter_purchased_with", "29765", "Leggings of the Fallen Hero");
    this.Add("filter_purchased_with", "31098", "Leggings of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "31100", "Leggings of the Forgotten Protector");
    this.Add("filter_purchased_with", "31099", "Leggings of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "34188", "Leggings of the Immortal Night");
    this.Add("filter_purchased_with", "30245", "Leggings of the Vanquished Champion");
    this.Add("filter_purchased_with", "30246", "Leggings of the Vanquished Defender");
    this.Add("filter_purchased_with", "30247", "Leggings of the Vanquished Hero");
    this.Add("filter_purchased_with", "2931", "Maiden's Anguish");
    this.Add("filter_purchased_with", "24579", "Mark of Honor Hold");
    this.Add("filter_purchased_with", "32897", "Mark of the Illidari");
    this.Add("filter_purchased_with", "24581", "Mark of Thrallmar");
    this.Add("filter_purchased_with", "34170", "Pantaloons of Calming Strife");
    this.Add("filter_purchased_with", "34192", "Pauldrons of Perseverance");
    this.Add("filter_purchased_with", "29763", "Pauldrons of the Fallen Champion");
    this.Add("filter_purchased_with", "29764", "Pauldrons of the Fallen Defender");
    this.Add("filter_purchased_with", "29762", "Pauldrons of the Fallen Hero");
    this.Add("filter_purchased_with", "31101", "Pauldrons of the Forgotten Conqueror");
    this.Add("filter_purchased_with", "31103", "Pauldrons of the Forgotten Protector");
    this.Add("filter_purchased_with", "31102", "Pauldrons of the Forgotten Vanquisher");
    this.Add("filter_purchased_with", "30248", "Pauldrons of the Vanquished Champion");
    this.Add("filter_purchased_with", "30249", "Pauldrons of the Vanquished Defender");
    this.Add("filter_purchased_with", "30250", "Pauldrons of the Vanquished Hero");
    this.Add("filter_purchased_with", "34233", "Robes of Faltered Light");
    this.Add("filter_purchased_with", "34234", "Shadowed Gauntlets of Paroxysm");
    this.Add("filter_purchased_with", "34202", "Shawl of Wonderment");
    this.Add("filter_purchased_with", "34195", "Shoulderpads of Vehemence");
    this.Add("filter_purchased_with", "4291", "Silken Thread");
    this.Add("filter_purchased_with", "34209", "Spaulders of Reclamation");
    this.Add("filter_purchased_with", "34193", "Spaulders of the Thalassian Savior");
    this.Add("filter_purchased_with", "28558", "Spirit Shard");
    this.Add("filter_purchased_with", "34212", "Sunglow Vest");
    this.Add("filter_purchased_with", "34664", "Sunmote");
    this.Add("filter_purchased_with", "34351", "Tranquil Majesty Wraps");
    this.Add("filter_purchased_with", "34215", "Warharness of Reckless Fury");
    this.Add("filter_purchased_with", "20558", "Warsong Gulch Mark of Honor");
    this.Add("entity_label", "20", "");
    this.Add("entity_link", "20", "");
    this.Add("entity_label", "106", "");
    this.Add("entity_link", "106", "wishlist.aspx");
    this.Add("entity_label", "105", "");
    this.Add("entity_link", "105", "talent.aspx");
    this.Add("entity_label", "109", "");
    this.Add("entity_link", "109", "");
    this.Add("entity_label", "102", "");
    this.Add("entity_link", "102", "");
    this.Add("entity_label", "101", "");
    this.Add("entity_link", "101", "");
    this.Add("entity_label", "100", "");
    this.Add("entity_link", "100", "");
    this.Add("entity_label", "104", "");
    this.Add("entity_link", "104", "");
    this.Add("entity_label", "103", "");
    this.Add("entity_link", "103", "");
    this.Add("entity_label", "8", "Faction");
    this.Add("entity_link", "8", "faction.aspx");
    this.Add("entity_label", "7", "Item Set");
    this.Add("entity_link", "7", "itemset.aspx");
    this.Add("entity_label", "6", "Spell");
    this.Add("entity_link", "6", "spell.aspx");
    this.Add("entity_label", "5", "Object");
    this.Add("entity_link", "5", "object.aspx");
    this.Add("entity_label", "4", "Quest");
    this.Add("entity_link", "4", "quest.aspx");
    this.Add("entity_label", "3", "Zone");
    this.Add("entity_link", "3", "location.aspx");
    this.Add("entity_label", "2", "NPC");
    this.Add("entity_link", "2", "npc.aspx");
    this.Add("entity_label", "1", "Item");
    this.Add("entity_link", "1", "item.aspx");
}
var _objLookups = new AscLookup();
var mn_1_2 = [[0, "One-Handed Axes", "?items=2.0"], [1, "Two-Handed Axes", "?items=2.1"], [2, "Bows", "?items=2.2"], [3, "Guns", "?items=2.3"], [4, "One-Handed Maces", "?items=2.4"], [5, "Two-Handed Maces", "?items=2.5"], [6, "Polearms", "?items=2.6"], [7, "One-Handed Swords", "?items=2.7"], [8, "Two-Handed Swords", "?items=2.8"], [10, "Staves", "?items=2.10"], [13, "Fist Weapons", "?items=2.13"], [14, "Miscellaneous", "?items=2.14"], [15, "Daggers", "?items=2.15"], [16, "Thrown", "?items=2.16"], [18, "Crossbows", "?items=2.18"], [19, "Wands", "?items=2.19"], [20, "Fishing Poles", "?items=2.20"]];
var mn_1_4 = [[1, "Cloth", "?items=4.1"], [2, "Leather", "?items=4.2"], [3, "Mail", "?items=4.3"], [4, "Plate", "?items=4.4"], [8, "Idols", "?items=4.8"], [7, "Librams", "?items=4.7"], [0, "Miscellaneous", "?items=4.0"], [6, "Shields", "?items=4.6"], [9, "Totems", "?items=4.9"]];
var mn_1_0 = [[7, "Bandages", "?items=0.7"], [2, "Elixirs", "?items=0.2"], [3, "Flasks", "?items=0.3"], [5, "Food & Drinks", "?items=0.5"], [6, "Item Enhancements", "?items=0.6"], [8, "Others", "?items=0.8"], [1, "Potions", "?items=0.1"], [4, "Scrolls", "?items=0.4"], [0, "Consumables", "?items=0.0"]];
var mn_1_1 = [[7, "Leatherworking Bag", "?items=1.7"], [0, "Bag", "?items=1.0"], [1, "Soul Bag", "?items=1.1"], [2, "Herb Bag", "?items=1.2"], [3, "Enchanting Bag", "?items=1.3"], [4, "Engineering Bag", "?items=1.4"], [5, "Gem Bag", "?items=1.5"], [6, "Mining Bag", "?items=1.6"]];
var mn_1_7 = [[5, "Cloth", "?items=7.5"], [10, "Elemental", "?items=7.10"], [12, "Enchanting", "?items=7.12"], [9, "Herb", "?items=7.9"], [6, "Leather", "?items=7.6"], [8, "Meat", "?items=7.8"], [7, "Metal & Stone", "?items=7.7"], [11, "Other", "?items=7.11"], [0, "Trade Goods", "?items=7.0"], [1, "Parts", "?items=7.1"], [2, "Explosives", "?items=7.2"], [3, "Devices", "?items=7.3"], [4, "Jewelcrafting", "?items=7.4"]];
var mn_1_6 = [[2, "Arrow", "?items=6.2"], [3, "Bullet", "?items=6.3"]];
var mn_1_11 = [[2, "Quiver", "?items=11.2"], [3, "Ammo Pouch", "?items=11.3"]];
var mn_1_9 = [[0, "Book", "?items=9.0"], [6, "Alchemy", "?items=9.6"], [4, "Blacksmithing", "?items=9.4"], [5, "Cooking", "?items=9.5"], [8, "Enchanting", "?items=9.8"], [3, "Engineering", "?items=9.3"], [7, "First Aid", "?items=9.7"], [9, "Fishing", "?items=9.9"], [10, "Jewelcrafting", "?items=9.10"], [1, "Leatherworking", "?items=9.1"], [2, "Tailoring", "?items=9.2"]];
var mn_1_3 = [[0, "Red", "?items=3.0"], [1, "Blue", "?items=3.1"], [2, "Yellow", "?items=3.2"], [3, "Purple", "?items=3.3"], [4, "Green", "?items=3.4"], [5, "Orange", "?items=3.5"], [6, "Meta", "?items=3.6"], [7, "Simple", "?items=3.7"], [8, "Prismatic", "?items=3.8"]];
var mn_1_15 = [[3, "Holiday", "?items=15.3"], [0, "Junk", "?items=15.0"], [4, "Other", "?items=15.4"], [2, "Pet", "?items=15.2"], [1, "Reagent", "?items=15.1"]];
var mn_1 = [[2, "Weapons", "?items=2", mn_1_2], [4, "Armor", "?items=4", mn_1_4], [0, "Consumables", "?items=0", mn_1_0], [1, "Containers", "?items=1", mn_1_1], [7, "Trade Goods", "?items=7", mn_1_7], [6, "Projectiles", "?items=6", mn_1_6], [11, "Quivers", "?items=11", mn_1_11], [9, "Recipes", "?items=9", mn_1_9], [5, "Reagents", "?items=5"], [3, "Gems", "?items=3", mn_1_3], [15, "Miscellaneous", "?items=15", mn_1_15], [12, "Quest", "?items=12"], [13, "Keys", "?items=13"]];
var mn_2 = [[1, "Beast", ], [8, "Critter", ], [3, "Demon", ], [2, "Dragonkin", ], [4, "Elemental", ], [5, "Giant", ], [7, "Humanoid", ], [9, "Mechanical", ], [12, "Small Pets", ], [10, "Uncategorized", ], [6, "Undead", ]];
var mn_3 = [[0, "Eastern Kingdoms", ], [1, "Kalimdor", ], [2, "Outland", ], [3, "Dungeons", ], [4, "Raids", ], [5, "Battlegrounds", ], [6, "Arenas", ]];
var mn_4_0 = [[36, "Alterac Mountains", ], [45, "Arathi Highlands", ], [3, "Badlands", ], [25, "Blackrock Mountain", ], [4, "Blasted Lands", ], [35, "Booty Bay", ], [46, "Burning Steppes", ], [132, "Coldridge Valley", ], [41, "Deadwind Pass", ], [154, "Deathknell", ], [2257, "Deeprun Tram", ], [1, "Dun Morogh", ], [10, "Duskwood", ], [139, "Eastern Plaguelands", ], [12, "Elwynn Forest", ], [3430, "Eversong Woods", ], [3433, "Ghostlands", ], [267, "Hillsbrad Foothills", ], [1537, "Ironforge", ], [4080, "Isle of Quel'Danas", ], [38, "Loch Modan", ], [24, "Northshire Abbey", ], [9, "Northshire Valley", ], [44, "Redridge Mountains", ], [51, "Searing Gorge", ], [3487, "Silvermoon City", ], [130, "Silverpine Forest", ], [1519, "Stormwind City", ], [33, "Stranglethorn Vale", ], [3431, "Sunstrider Isle", ], [8, "Swamp of Sorrows", ], [47, "The Hinterlands", ], [85, "Tirisfal Glades", ], [1497, "Undercity", ], [28, "Western Plaguelands", ], [40, "Westfall", ], [11, "Wetlands", ]];
var mn_4_1 = [[2079, "Alcaz Island", ], [3526, "Ammen Vale", ], [331, "Ashenvale", ], [16, "Azshara", ], [3524, "Azuremyst Isle", ], [3525, "Bloodmyst Isle", ], [221, "Camp Narache", ], [279, "Dalaran", ], [148, "Darkshore", ], [1657, "Darnassus", ], [405, "Desolace", ], [14, "Durotar", ], [15, "Dustwallow Marsh", ], [1116, "Feathermoon Stronghold", ], [361, "Felwood", ], [357, "Feralas", ], [493, "Moonglade", ], [215, "Mulgore", ], [1637, "Orgrimmar", ], [220, "Red Cloud Mesa", ], [702, "Rut'theran Village", ], [188, "Shadowglen", ], [1377, "Silithus", ], [406, "Stonetalon Mountains", ], [440, "Tanaris", ], [141, "Teldrassil", ], [17, "The Barrens", ], [3557, "The Exodar", ], [400, "Thousand Needles", ], [1638, "Thunder Bluff", ], [1769, "Timbermaw Hold", ], [490, "Un'Goro Crater", ], [363, "Valley of Trials", ], [618, "Winterspring", ]];
var mn_4_2 = [[3522, "Blade's Edge Mountains", ], [3565, "Cenarion Refuge", ], [3905, "Coilfang Reservoir", ], [3483, "Hellfire Peninsula", ], [3518, "Nagrand", ], [3523, "Netherstorm", ], [3520, "Shadowmoon Valley", ], [3703, "Shattrath City", ], [3679, "Skettis", ], [3519, "Terokkar Forest", ], [3696, "The Barrier Hills", ], [3840, "The Black Temple", ], [3521, "Zangarmarsh", ]];
var mn_4_3 = [[3790, "Auchenai Crypts", ], [3917, "Auchindoun", ], [719, "Blackfathom Deeps", ], [1584, "Blackrock Depths", ], [1583, "Blackrock Spire", ], [1941, "Caverns of Time", ], [2557, "Dire Maul", ], [721, "Gnomeregan", ], [3535, "Hellfire Citadel", ], [4095, "Magisters' Terrace", ], [3792, "Mana-Tombs", ], [2100, "Maraudon", ], [2437, "Ragefire Chasm", ], [722, "Razorfen Downs", ], [491, "Razorfen Kraul", ], [796, "Scarlet Monastery", ], [2057, "Scholomance", ], [3789, "Shadow Labyrinth", ], [209, "Shadowfang Keep", ], [2017, "Stratholme", ], [1417, "Sunken Temple", ], [1581, "The Deadmines", ], [3717, "The Slave Pens", ], [3715, "The Steamvault", ], [717, "The Stockade", ], [3716, "The Underbog", ], [1337, "Uldaman", ], [718, "Wailing Caverns", ], [1176, "Zul'Farrak", ]];
var mn_4_4 = [[2677, "Blackwing Lair", ], [3606, "Hyjal Summit", ], [3457, "Karazhan", ], [3836, "Magtheridon's Lair", ], [2717, "Molten Core", ], [3456, "Naxxramas", ], [2159, "Onyxia's Lair", ], [3429, "Ruins of Ahn'Qiraj", ], [3607, "Serpentshrine Cavern", ], [4075, "Sunwell Plateau", ], [3845, "Tempest Keep", ], [3428, "Temple of Ahn'Qiraj", ], [3805, "Zul'Aman", ], [1977, "Zul'Gurub", ]];
var mn_4_5 = [[2597, "Alterac Valley", ], [3358, "Arathi Basin", ], [3820, "Eye of the Storm", ], [3277, "Warsong Gulch", ]];
var mn_4_$1 = [[11, "Druid", ], [3, "Hunter", ], [8, "Mage", ], [2, "Paladin", ], [5, "Priest", ], [4, "Rogue", ], [7, "Shaman", ], [9, "Warlock", ], [1, "Warrior", ]];
var mn_4_$2 = [[171, "Alchemy", ], [164, "Blacksmithing", ], [185, "Cooking", ], [202, "Engineering", ], [129, "First Aid", ], [356, "Fishing", ], [182, "Herbalism", ], [165, "Leatherworking", ], [197, "Tailoring", ]];
var mn_4_$3 = [[365, "Ahn'Qiraj War", ], [25, "Battlegrounds", ], [370, "Brewfest", ], [364, "Darkmoon Faire", ], [1, "Epic", ], [368, "Invasion", ], [344, "Legendary", ], [366, "Lunar Festival", ], [369, "Midsummer", ], [367, "Reputation", ], [22, "Seasonal", ], [284, "Special", ], [221, "Treasure Map", ]];
var mn_4 = [[0, "Eastern Kingdoms", , mn_4_0], [1, "Kalimdor", , mn_4_1], [2, "Outland", , mn_4_2], [3, "Dungeons", , mn_4_3], [4, "Raids", , mn_4_4], [5, "Battlegrounds", , mn_4_5], [ - 1, "Classes", , mn_4_$1], [ - 2, "Professions", , mn_4_$2], [ - 3, "Miscellaneous", , mn_4_$3]];
var mn_5 = [[9, "Books", ], [3, "Containers", ], [25, "Fish Schools", ], [ - 2, "Herbs", ], [ - 3, "Locked Chests", ], [ - 1, "Mineral Veins", ], [2, "Quest", ]];
var mn_6_7_11 = [[574, "Balance", ], [134, "Feral Combat", ], [573, "Restoration", ]];
var mn_6_7_3 = [[50, "Beast Mastery", ], [261, "Beast Training", ], [163, "Marksmanship", ], [51, "Survival", ]];
var mn_6_7_8 = [[237, "Arcane", ], [8, "Fire", ], [6, "Frost", ]];
var mn_6_7_2 = [[594, "Holy", ], [267, "Protection", ], [184, "Retribution", ]];
var mn_6_7_5 = [[613, "Discipline", ], [56, "Holy", ], [78, "Shadow Magic", ]];
var mn_6_7_4 = [[253, "Assassination", ], [38, "Combat", ], [633, "Lockpicking", ], [40, "Poisons", ], [39, "Subtlety", ]];
var mn_6_7_7 = [[375, "Elemental Combat", ], [373, "Enhancement", ], [374, "Restoration", ]];
var mn_6_7_9 = [[355, "Affliction", ], [354, "Demonology", ], [593, "Destruction", ]];
var mn_6_7_1 = [[26, "Arms", ], [256, "Fury", ], [257, "Protection", ]];
var mn_6_7 = [[11, "Druid", , mn_6_7_11], [3, "Hunter", , mn_6_7_3], [8, "Mage", , mn_6_7_8], [2, "Paladin", , mn_6_7_2], [5, "Priest", , mn_6_7_5], [4, "Rogue", , mn_6_7_4], [7, "Shaman", , mn_6_7_7], [9, "Warlock", , mn_6_7_9], [1, "Warrior", , mn_6_7_1]];
var mn_6_$2_11 = [[574, "Balance", ], [134, "Feral Combat", ], [573, "Restoration", ]];
var mn_6_$2_3 = [[50, "Beast Mastery", ], [163, "Marksmanship", ], [51, "Survival", ]];
var mn_6_$2_8 = [[237, "Arcane", ], [8, "Fire", ], [6, "Frost", ]];
var mn_6_$2_2 = [[594, "Holy", ], [267, "Protection", ], [184, "Retribution", ]];
var mn_6_$2_5 = [[613, "Discipline", ], [56, "Holy", ], [78, "Shadow", ]];
var mn_6_$2_4 = [[253, "Assassination", ], [38, "Combat", ], [39, "Subtlety", ]];
var mn_6_$2_7 = [[375, "Elemental", ], [373, "Enhancement", ], [374, "Restoration", ]];
var mn_6_$2_9 = [[355, "Affliction", ], [354, "Demonology", ], [593, "Destruction", ]];
var mn_6_$2_1 = [[26, "Arms", ], [256, "Fury", ], [257, "Protection", ]];
var mn_6_$2 = [[11, "Druid", , mn_6_$2_11], [3, "Hunter", , mn_6_$2_3], [8, "Mage", , mn_6_$2_8], [2, "Paladin", , mn_6_$2_2], [5, "Priest", , mn_6_$2_5], [4, "Rogue", , mn_6_$2_4], [7, "Shaman", , mn_6_$2_7], [9, "Warlock", , mn_6_$2_9], [1, "Warrior", , mn_6_$2_1]];
var mn_6_11_164 = [[9787, "Weaponsmith", ], [9788, "Armorsmith", ], [17039, "Master Swordsmith", ], [17040, "Master Hammersmith", ], [17041, "Master Axesmith", ]];
var mn_6_11_202 = [[20219, "Gnomish Engineer", ], [20222, "Goblin Engineer", ]];
var mn_6_11_165 = [[10656, "Dragonscale Leatherworking", ], [10658, "Elemental Leatherworking", ], [10660, "Tribal Leatherworking", ]];
var mn_6_11_197 = [[26798, "Mooncloth Tailoring", ], [26801, "Shadoweave Tailoring", ], [26797, "Spellfire Tailoring", ]];
var mn_6_11 = [[171, "Alchemy", ], [164, "Blacksmithing", , mn_6_11_164], [333, "Enchanting", ], [202, "Engineering", , mn_6_11_202], [182, "Herbalism", ], [755, "Jewelcrafting", ], [165, "Leatherworking", , mn_6_11_165], [186, "Mining", ], [393, "Skinning", ], [197, "Tailoring", , mn_6_11_197]];
var mn_6_$3 = [[653, "Bat", ], [210, "Bear", ], [211, "Boar", ], [213, "Carrion Bird", ], [209, "Cat", ], [214, "Crab", ], [212, "Crocilisk", ], [763, "Dragonhawk", ], [189, "Felhunter", ], [270, "Generic", ], [215, "Gorilla", ], [654, "Hyena", ], [188, "Imp", ], [764, "Nether Ray", ], [655, "Owl", ], [217, "Raptor", ], [767, "Ravager", ], [236, "Scorpid", ], [768, "Serpent", ], [203, "Spider", ], [205, "Succubus", ], [218, "Tallstrider", ], [251, "Turtle", ], [204, "Voidwalker", ], [766, "Warp Stalker", ], [656, "Wind Serpent", ], [208, "Wolf", ]];
var mn_6_9 = [[185, "Cooking", ], [129, "First Aid", ], [356, "Fishing", ], [762, "Riding", ]];
var mn_6_$4 = [[756, "Blood Elf Racial", ], [760, "Draenei Racial", ], [101, "Dwarven Racial", ], [126, "Night Elf Racial", ], [125, "Orc Racial", ], [753, "Racial - Gnome", ], [754, "Racial - Human", ], [733, "Racial - Troll", ], [220, "Racial - Undead", ], [124, "Tauren Racial", ]];
var mn_6 = [[7, "Class Skills", , mn_6_7], [ - 2, "Talents", , mn_6_$2], [11, "Professions", , mn_6_11], [ - 3, "Pet Skills", , mn_6_$3], [9, "Secondary Skills", , mn_6_9], [ - 4, "Racial Traits", , mn_6_$4], [6, "Weapon Skills", ], [10, "Languages", ], [8, "Armor Proficiencies", ]];
var mn_7 = [[ - 1, "Any Class", ], [11, "Druid", ], [3, "Hunter", ], [8, "Mage", ], [2, "Paladin", ], [5, "Priest", ], [4, "Rogue", ], [7, "Shaman", ], [9, "Warlock", ], [1, "Warrior", ]];
var mn_8 = [[469, "Alliance", ], [891, "Alliance Forces", ], [67, "Horde", ], [892, "Horde Forces", ], [980, "Outland", ], [936, "Shattrath City", ], [169, "Steamwheedle Cartel", ], [ - 1, "Other", ]];
var mn_9_3 = [[11, "Druid", "talent.aspx?id=11"], [3, "Hunter", "talent.aspx?id=3"], [8, "Mage", "talent.aspx?id=8"], [2, "Paladin", "talent.aspx?id=2"], [5, "Priest", "talent.aspx?id=5"], [4, "Rogue", "talent.aspx?id=4"], [7, "Shaman", "talent.aspx?id=7"], [9, "Warlock", "talent.aspx?id=9"], [1, "Warrior", "talent.aspx?id=1"]];
var mn_9 = [[3, "Talent Calculator", "talent.aspx?id=11", mn_9_3], [4, "Wish Lists", "javascript:navToWishlist();"], [2, "Download the Curse Client", "client.aspx"], [1, "Bug Reporting & Feedback", "bugs.aspx"], [5, "Syndication", "syndication.aspx"], [6, "Browser Plugins", "plugins.aspx"], [7, "Latest Comments", "latestComments.aspx"], [8, "Latest Additions", "latestAdditions.aspx"]];
var mn_Main = [[1, "Items", "?items", mn_1], [2, "NPCs", , mn_2], [3, "Zones", , mn_3], [4, "Quests", , mn_4], [5, "Objects", , mn_5], [6, "Spells", , mn_6], [7, "Item Sets", , mn_7], [8, "Factions", , mn_8], [9, "Tools", "$nonav$", mn_9]];
addMainMenu("spanMainMenu", mn_Main);
var isIE = false;
var isIE6 = false;
if (document.all) {
    isIE = navigator.appName.indexOf("Microsoft") != -1;
    isIE6 = navigator.appVersion.indexOf("MSIE 6") != -1;
}
function getElementsByClassName(sClassName, sTag, oContainer, returnFirst) {
    var searchObj;
    var results = new Array();
    if (!oContainer) {
        oContainer = document;
    }
    if (sTag == "*" || !sTag) {
        if (document.all) {
            searchObj = oContainer.all;
        } else {
            searchObj = oContainer.getElementsByTagName("*");
        }
    } else {
        searchObj = oContainer.getElementsByTagName(sTag);
    }
    for (var i = 0, el; ((searchObj.all && !neo.bw.isIE6up) ? el = searchObj(i) : el = searchObj.item(i)); i++) {
        if (el.className == sClassName) {
            if (returnFirst) {
                return el;
            }
            results.push(el);
        }
    }
    return results;
}
function insertAfter(parent, node, referenceNode) {
    parent.insertBefore(node, referenceNode.nextSibling);
}
function _scrollTop() {
    if (self.pageYOffset) {
        return self.pageYOffset;
    } else if (document.documentElement && !document.documentElement.scrollTop) {
        return 0;
    } else if (document.documentElement && document.documentElement.scrollTop) {
        return document.documentElement.scrollTop;
    } else if (document.body && document.body.scrollTop) {
        return document.body.scrollTop;
    }
}
function mod(divisee, base) {
    return Math.round(divisee - (Math.floor(divisee / base) * base));
}
function mouseCoords(ev) {
    var mouseX;
    var mouseY;
    if (window.event) {
        mouseX = window.event.clientX;
        mouseY = window.event.clientY;
        mouseY += _scrollTop();
    } else {
        mouseX = ev.pageX;
        mouseY = ev.pageY;
    }
    return {
        x: mouseX,
        y: mouseY
    };
}
function getEventObject(e) {
    if (isIE) {
        if (event) {
            return event;
        }
        return;
    } else {
        return e;
    }
}
function getEventTarget(e) {
    if (isIE) {
        if (event) {
            return event.srcElement;
        }
        return;
    } else {
        if (e) {
            return e.target;
        }
    }
}
function _addEventListener(eventSource, eventName, eventHandler) {
    if (eventSource.addEventListener) {
        eventSource.addEventListener(eventName, eventHandler, false);
    } else if (eventSource.attachEvent) {
        eventName = "on" + eventName;
        eventSource.attachEvent(eventName, eventHandler);
    }
}
function _removeEventListener(eventSource, eventName) {
    if (eventSource.addEventListener) {
        eventSource.removeEventListener(eventName, addUserLocation, false);
    } else if (eventSource.detachEvent) {
        eventSource.detachEvent("on" + eventName);
    }
}
function getEventRelatedTarget(e) {
    if (isIE) {
        return event.toElement;
    } else {
        return e.relatedTarget;
    }
}
function getPosition(e) {
    var left = 0;
    var top = 0;
    while (e.offsetParent) {
        left += e.offsetLeft;
        if (e.clientLeft) {
            left += e.clientLeft;
        }
        top += e.offsetTop;
        if (e.clientTop) {
            top += e.clientTop;
        }
        e = e.offsetParent;
    }
    left += e.offsetLeft;
    top += e.offsetTop;
    return {
        x: left,
        y: top
    };
}
function setCookie(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape(value);
    if (exp_y) {
        if (exp_m = null) {
            exp_m = 1;
        }
        if (exp_d = null) {
            exp_m = 1;
        }
        var expires = new Date;
        expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24 * 31));
        cookie_string += "; expires=" + expires.toGMTString();
    }
    if (path) cookie_string += "; path=" + escape(path);
    if (domain) cookie_string += "; domain=" + escape(domain);
    if (secure) cookie_string += "; secure";
    document.cookie = cookie_string;
}
function IsNumeric(sText) {
    var ValidChars = "0123456789.";
    var IsNumber = true;
    var Char;
    for (i = 0; i < sText.length && IsNumber == true; i++) {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
    }
    return IsNumber;
}
function getCookie(cookie_name) {
    var results = document.cookie.match(cookie_name + '=(.*?)(;|$)');
    if (results) return (unescape(results[1]));
    else return null;
}
function contains(a, b) {
    if (!b) {
        return false;
    }
    while (b.parentNode) {
        if ((b = b.parentNode) == a) {
            return true;
        }
    }
    return false;
}
if (!isIE) {
    HTMLElement.prototype.contains = function(oEl) {
        if (oEl == this) return true;
        if (oEl == null) return false;
        return this.contains(oEl.parentNode);
    };
}
String.prototype.trim = function() {
    var a = this.replace(/^\s+/, '');
    return a.replace(/\s+$/, '');
};
if (!Array.indexOf) {
    Array.prototype.indexOf = function(obj) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == obj) {
                return i;
            }
        }
    }
}
function _cancelBubbling(e) {
    if (!e.stopPropagation) {
        e = window.event;
        e.cancelBubble = true;
    } else {
        e.stopPropagation();
    }
}
function isArray(obj) {
    if (obj.constructor.toString().toLowerCase().indexOf("function") == -1 && obj.constructor.toString().toLowerCase().indexOf("array") == -1) {
        return false;
    } else {
        if (!obj.length) {
            return false;
        }
        return true;
    }
}
function mLookup(strKeyName) {
    if (this[strKeyName] != null) {
        return (this[strKeyName]);
    } else {
        return "";
    }
}
function mAdd() {
    for (c = 0; c < mAdd.arguments.length; c += 3) {
        this[mAdd.arguments[c] + "." + mAdd.arguments[c + 1]] = mAdd.arguments[c + 2];
        if (!this["LookupList_" + mAdd.arguments[c]]) {
            this["LookupList_" + mAdd.arguments[c]] = [];
        }
        this["LookupList_" + mAdd.arguments[c]].push([mAdd.arguments[c + 1], mAdd.arguments[c + 2]])
    }
}
function mLookupList() {
    if (this["LookupList_" + mLookupList.arguments[0]]) {
        return (this["LookupList_" + mLookupList.arguments[0]]);
    } else {
        return "";
    }
}
function _$() {
    var elements = new Array();
    for (var i = 0; i < arguments.length; i++) {
        var element = arguments[i];
        if (typeof element == 'string') element = document.getElementById(element);
        if (arguments.length == 1) return element;
        elements.push(element);
    }
    return elements;
}
if (typeof deconcept == "undefined") {
    var deconcept = new Object();
}
if (typeof deconcept.util == "undefined") {
    deconcept.util = new Object();
}
if (typeof deconcept.SWFObjectUtil == "undefined") {
    deconcept.SWFObjectUtil = new Object();
}
deconcept.SWFObject = function(_1, id, w, h, _5, c, _7, _8, _9, _a) {
    if (!document.getElementById) {
        return;
    }
    this.DETECT_KEY = _a ? _a: "detectflash";
    this.skipDetect = deconcept.util.getRequestParameter(this.DETECT_KEY);
    this.params = new Object();
    this.variables = new Object();
    this.attributes = new Array();
    if (_1) {
        this.setAttribute("swf", _1);
    }
    if (id) {
        this.setAttribute("id", id);
    }
    if (w) {
        this.setAttribute("width", w);
    }
    if (h) {
        this.setAttribute("height", h);
    }
    if (_5) {
        this.setAttribute("version", new deconcept.PlayerVersion(_5.toString().split(".")));
    }
    this.installedVer = deconcept.SWFObjectUtil.getPlayerVersion();
    if (!window.opera && document.all && this.installedVer.major > 7) {
        deconcept.SWFObject.doPrepUnload = true;
    }
    if (c) {
        this.addParam("bgcolor", c);
    }
    var q = _7 ? _7: "high";
    this.addParam("quality", q);
    this.setAttribute("useExpressInstall", false);
    this.setAttribute("doExpressInstall", false);
    var _c = (_8) ? _8: window.location;
    this.setAttribute("xiRedirectUrl", _c);
    this.setAttribute("redirectUrl", "");
    if (_9) {
        this.setAttribute("redirectUrl", _9);
    }
};
deconcept.SWFObject.prototype = {
    useExpressInstall: function(_d) {
        this.xiSWFPath = !_d ? "expressinstall.swf": _d;
        this.setAttribute("useExpressInstall", true);
    },
    setAttribute: function(_e, _f) {
        this.attributes[_e] = _f;
    },
    getAttribute: function(_10) {
        return this.attributes[_10];
    },
    addParam: function(_11, _12) {
        this.params[_11] = _12;
    },
    getParams: function() {
        return this.params;
    },
    addVariable: function(_13, _14) {
        this.variables[_13] = _14;
    },
    getVariable: function(_15) {
        return this.variables[_15];
    },
    getVariables: function() {
        return this.variables;
    },
    getVariablePairs: function() {
        var _16 = new Array();
        var key;
        var _18 = this.getVariables();
        for (key in _18) {
            _16[_16.length] = key + "=" + _18[key];
        }
        return _16;
    },
    getSWFHTML: function() {
        var _19 = "";
        if (navigator.plugins && navigator.mimeTypes && navigator.mimeTypes.length) {
            if (this.getAttribute("doExpressInstall")) {
                this.addVariable("MMplayerType", "PlugIn");
                this.setAttribute("swf", this.xiSWFPath);
            }
            _19 = "<embed type=\"application/x-shockwave-flash\" src=\"" + this.getAttribute("swf") + "\" width=\"" + this.getAttribute("width") + "\" height=\"" + this.getAttribute("height") + "\" style=\"" + this.getAttribute("style") + "\"";
            _19 += " id=\"" + this.getAttribute("id") + "\" name=\"" + this.getAttribute("id") + "\" ";
            var _1a = this.getParams();
            for (var key in _1a) {
                _19 += [key] + "=\"" + _1a[key] + "\" ";
            }
            var _1c = this.getVariablePairs().join("&");
            if (_1c.length > 0) {
                _19 += "flashvars=\"" + _1c + "\"";
            }
            _19 += "/>";
        } else {
            if (this.getAttribute("doExpressInstall")) {
                this.addVariable("MMplayerType", "ActiveX");
                this.setAttribute("swf", this.xiSWFPath);
            }
            _19 = "<object id=\"" + this.getAttribute("id") + "\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"" + this.getAttribute("width") + "\" height=\"" + this.getAttribute("height") + "\" style=\"" + this.getAttribute("style") + "\">";
            _19 += "<param name=\"movie\" value=\"" + this.getAttribute("swf") + "\" />";
            var _1d = this.getParams();
            for (var key in _1d) {
                _19 += "<param name=\"" + key + "\" value=\"" + _1d[key] + "\" />";
            }
            var _1f = this.getVariablePairs().join("&");
            if (_1f.length > 0) {
                _19 += "<param name=\"flashvars\" value=\"" + _1f + "\" />";
            }
            _19 += "</object>";
        }
        return _19;
    },
    write: function(_20) {
        if (this.getAttribute("useExpressInstall")) {
            var _21 = new deconcept.PlayerVersion([6, 0, 65]);
            if (this.installedVer.versionIsValid(_21) && !this.installedVer.versionIsValid(this.getAttribute("version"))) {
                this.setAttribute("doExpressInstall", true);
                this.addVariable("MMredirectURL", escape(this.getAttribute("xiRedirectUrl")));
                document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                this.addVariable("MMdoctitle", document.title);
            }
        }
        if (this.skipDetect || this.getAttribute("doExpressInstall") || this.installedVer.versionIsValid(this.getAttribute("version"))) {
            var n = (typeof _20 == "string") ? document.getElementById(_20) : _20;
            n.innerHTML = this.getSWFHTML();
            return true;
        } else {
            if (this.getAttribute("redirectUrl") != "") {
                document.location.replace(this.getAttribute("redirectUrl"));
            }
        }
        return false;
    }
};
deconcept.SWFObjectUtil.getPlayerVersion = function() {
    var _23 = new deconcept.PlayerVersion([0, 0, 0]);
    if (navigator.plugins && navigator.mimeTypes.length) {
        var x = navigator.plugins["Shockwave Flash"];
        if (x && x.description) {
            _23 = new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split("."));
        }
    } else {
        if (navigator.userAgent && navigator.userAgent.indexOf("Windows CE") >= 0) {
            var axo = 1;
            var _26 = 3;
            while (axo) {
                try {
                    _26++;
                    axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + _26);
                    _23 = new deconcept.PlayerVersion([_26, 0, 0]);
                } catch(e) {
                    axo = null;
                }
            }
        } else {
            try {
                var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
            } catch(e) {
                try {
                    var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
                    _23 = new deconcept.PlayerVersion([6, 0, 21]);
                    axo.AllowScriptAccess = "always";
                } catch(e) {
                    if (_23.major == 6) {
                        return _23;
                    }
                }
                try {
                    axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
                } catch(e) {}
            }
            if (axo != null) {
                _23 = new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));
            }
        }
    }
    return _23;
};
deconcept.PlayerVersion = function(_29) {
    this.major = _29[0] != null ? parseInt(_29[0]) : 0;
    this.minor = _29[1] != null ? parseInt(_29[1]) : 0;
    this.rev = _29[2] != null ? parseInt(_29[2]) : 0;
};
deconcept.PlayerVersion.prototype.versionIsValid = function(fv) {
    if (this.major < fv.major) {
        return false;
    }
    if (this.major > fv.major) {
        return true;
    }
    if (this.minor < fv.minor) {
        return false;
    }
    if (this.minor > fv.minor) {
        return true;
    }
    if (this.rev < fv.rev) {
        return false;
    }
    return true;
};
deconcept.util = {
    getRequestParameter: function(_2b) {
        var q = document.location.search || document.location.hash;
        if (_2b == null) {
            return q;
        }
        if (q) {
            var _2d = q.substring(1).split("&");
            for (var i = 0; i < _2d.length; i++) {
                if (_2d[i].substring(0, _2d[i].indexOf("=")) == _2b) {
                    return _2d[i].substring((_2d[i].indexOf("=") + 1));
                }
            }
        }
        return "";
    }
};
deconcept.SWFObjectUtil.cleanupSWFs = function() {
    var _2f = document.getElementsByTagName("OBJECT");
    for (var i = _2f.length - 1; i >= 0; i--) {
        _2f[i].style.display = "none";
        for (var x in _2f[i]) {
            if (typeof _2f[i][x] == "function") {
                _2f[i][x] = function() {};
            }
        }
    }
};
if (deconcept.SWFObject.doPrepUnload) {
    if (!deconcept.unloadSet) {
        deconcept.SWFObjectUtil.prepUnload = function() {
            __flash_unloadHandler = function() {};
            __flash_savedUnloadHandler = function() {};
            window.attachEvent("onunload", deconcept.SWFObjectUtil.cleanupSWFs);
        };
        window.attachEvent("onbeforeunload", deconcept.SWFObjectUtil.prepUnload);
        deconcept.unloadSet = true;
    }
}
if (!document.getElementById && document.all) {
    document.getElementById = function(id) {
        return document.all[id];
    };
}
var getQueryParamValue = deconcept.util.getRequestParameter;
var FlashObject = deconcept.SWFObject;
var SWFObject = deconcept.SWFObject;
jx = {
    http: false,
    format: 'text',
    callback: function(data) {},
    handler: false,
    error: false,
    opt: new Object(),
    getHTTPObject: function() {
        var http = false;
        if (typeof ActiveXObject != 'undefined') {
            try {
                http = new ActiveXObject("Msxml2.XMLHTTP");
            } catch(e) {
                try {
                    http = new ActiveXObject("Microsoft.XMLHTTP");
                } catch(E) {
                    http = false;
                }
            }
        } else if (XMLHttpRequest) {
            try {
                http = new XMLHttpRequest();
            } catch(e) {
                http = false;
            }
        }
        return http;
    },
    load: function(url, callback, format, method, allowCache) {
        this.init();
        if (!this.http || !url) return;
        if (this.http.overrideMimeType) this.http.overrideMimeType('text/xml');
        this.callback = callback;
        if (!method) var method = "GET";
        if (!format) var format = "text";
        this.format = format.toLowerCase();
        method = method.toUpperCase();
        var ths = this;
        if (!allowCache) {
            var now = "uid=" + new Date().getTime();
            url += (url.indexOf("?") + 1) ? "&": "?";
            url += now;
        }
        var parameters = null;
        if (method == "POST") {
            var parts = url.split("\?");
            url = parts[0];
            parameters = parts[1];
        }
        this.http.open(method, url, true);
        if (method == "POST") {
            this.http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            this.http.setRequestHeader("Content-length", parameters.length);
            if (!isIE) {
                this.http.setRequestHeader("Connection", "close");
            }
        }
        if (this.handler) {
            this.http.onreadystatechange = this.handler;
        } else {
            this.http.onreadystatechange = function() {
                if (!ths) return;
                var http = ths.http;
                if (http.readyState == 4) {
                    if (http.status == 200) {
                        var result = "";
                        if (http.responseText) result = http.responseText;
                        if (ths.format.charAt(0) == "j") {
                            result = result.replace(/[\n\r]/g, "");
                            result = eval('(' + result + ')');
                        } else if (ths.format.charAt(0) == "x") {
                            result = http.responseXML;
                        }
                        if (ths.callback) ths.callback(result);
                    } else {
                        if (ths.opt.loadingIndicator) document.getElementsByTagName("body")[0].removeChild(ths.opt.loadingIndicator);
                        if (ths.opt.loading) document.getElementById(ths.opt.loading).style.display = "none";
                        if (ths.error) ths.error(http.status);
                    }
                }
            }
        }
        this.http.send(parameters);
    },
    bind: function(user_options) {
        var opt = {
            'url': '',
            'onSuccess': false,
            'onError': false,
            'format': "text",
            'method': "GET",
            'update': "",
            'loading': "",
            'loadingIndicator': ""
        }
        for (var key in opt) {
            if (user_options[key]) {
                opt[key] = user_options[key];
            }
        }
        this.opt = opt;
        if (!opt.url) return;
        if (opt.onError) this.error = opt.onError;
        var div = false;
        if (opt.loadingIndicator) {
            div = document.createElement("div");
            div.setAttribute("style", "position:absolute;top:0px;left:0px;");
            div.setAttribute("class", "loading-indicator");
            div.innerHTML = opt.loadingIndicator;
            document.getElementsByTagName("body")[0].appendChild(div);
            this.opt.loadingIndicator = div;
        }
        if (opt.loading) document.getElementById(opt.loading).style.display = "block";
        this.load(opt.url,
        function(data) {
            if (opt.onSuccess) opt.onSuccess(data);
            if (opt.update) document.getElementById(opt.update).innerHTML = data;
            if (div) document.getElementsByTagName("body")[0].removeChild(div);
            if (opt.loading) document.getElementById(opt.loading).style.display = "none";
        },
        opt.format, opt.method);
    },
    init: function() {
        this.http = this.getHTTPObject();
    }
};
function getQueryStringParam(param) {
	var begin, end;
	if (self.location.search.length > 1) {
		begin = self.location.search.indexOf(param);
		if (begin == -1) {
			return "";
		}
		begin = begin + param.length + 1;
		end = self.location.search.indexOf("&", begin);
		if (end == ( - 1))
			end = self.location.search.length;
		return (self.location.search.substring(begin, end));
	} else if (self.location.hash.length > 1) {
		begin = self.location.hash.indexOf(param) + param.length + 1;
		end = self.location.hash.indexOf("&", begin);
		if (end == ( - 1))
			end = self.location.hash.length;
		return (self.location.hash.substring(begin, end));
	} else return ("");
}
function getLookupsFromCookie(name, delim1, delim2) {
    var lookupList = [];
    var cookieList = getCookie("Login." + name);
    if (!delim1) {
        delim1 = ",";
    }
    if (!delim2) {
        delim2 = "^";
    }
    if (cookieList) {
        cookieList = cookieList.split(delim1);
        for (var i = 0; i < cookieList.length; i++) {
            lookupList.push(cookieList[i].split(delim2));
        }
    }
    return lookupList;
}
function getLookupSelectBox(lookupName, selectName, container, hideEmpty, onchange, fromCookie, delim1, delim2, emptyLabel) {
    if (fromCookie) {
        var lookupList = getLookupsFromCookie(lookupName, delim1, delim2);
    } else {
        var lookupList = _objLookups.LookupList(lookupName)
    }
    var objSelect = document.createElement("select");
    objSelect.name = selectName;
    objSelect.id = "fi_" + selectName;
    if (onchange) {
        objSelect.onchange = function() {
            eval(onchange);
        };
    }
    if (!hideEmpty) {
        var objOption = document.createElement("option");
        if (!emptyLabel) {
            emptyLabel = "";
        }
        objOption.text = emptyLabel;
        objOption.value = "";
        objSelect.options.add(objOption)
    }
    for (var i = 0; i < lookupList.length; i++) {
        var objOption = document.createElement("option");
        objOption.text = lookupList[i][1].replace("<br>", " - ");
        objOption.value = lookupList[i][0];
        objSelect.options.add(objOption)
    }
    if (container) {
        container.appendChild(objSelect);
        return objSelect
    } else {
        return objSelect;
    }
}
function endsWith(str, s) {
    var reg = new RegExp(s + "$");
    return reg.test(str);
}
var _arrFilteredResultsData = [];
var _arrOriginalResultsData = [];
var _arrFilteredResultsDataStore = [];
var _arrCurrentSortStore = [];
var _arrCurrentSortOrderStore = [];
var _arrResultsDataCollection = [];
var _arrResultsStartingIndex = [];
var _arrResultsDataDescriptors = [];
var _arrResultsDataDescriptorLabels = [];
var _arrResultsNoDataMessage = [];
var _arrResultsFields = [];
var _arrResultsHasData = [];
var _arrResultsDefaultSort = [];
var _arrResultsCounts = [];
var _currentDescriptorIndex;
var _arrItemResultsData = [];
var _resultsPerPage = 15;
var _arrCharacterData = [];
var _searchText;
var _searchDescription;
var _browseDescription;
var _lastProfileOpened;
var _userInitialized = false;
var _userIsMod = false;
var _userID = "";
var _userDisplayName;
var _userTimeOffset = -5;
var _searchBrowseString = "";
var _arrCharacters = [];
var _arrEnchantData = [];
var _arrItemSetData = [];
var arrItemData = [];
var arrSpellData = [];
var dropTargets = [];
var _showAllTabs = false;
var _showTabs = false;
var _detailsArticle = null;
var _detailsEntityName = null;
var _detailsEntityID = null;
var _detailsPreviewIDs = null;
var _detailsEntityTypeID = null;
var _detailsEntityTypeLabel = null;
var _detailsHasPreview = null;
var _detailsDisplayID = null;
var _tooltipClass = null;
var _selectedObj = null;
var _targetObj = null;
var offsetx;
var offsety;
var nowX;
var nowY;
var currentItem = null;
var _isDragging = false;
var itemOriginalX;
var itemOriginalY;
var _cancelMouseOver = false;
var _verificationText = "";
var _currentMenuID = null;
var mn_User;
var mn_User_1 = null;
var isMapEdit = true;
var _menuTimeout = null;
var _menuTimeoutPointer;
var _commentHideRating = 4;
var Client = {
    viewportWidth: function() {
        return self.innerWidth || (document.documentElement.clientWidth || document.body.clientWidth);
    },
    viewportHeight: function() {
        return self.innerHeight || (document.documentElement.clientHeight || document.body.clientHeight);
    },
    viewportSize: function() {
        return {
            width: this.viewportWidth(),
            height: this.viewportHeight()
        };
    }
};
function showTip(tipName) {
    switch (tipName) {
    case "overview":
        displayAlert("This appears to be your first visit to WOWDB!<br>Click <a href=\"overview.aspx\">here</a> for an overview of our unique features, or click Close and continue browsing.", "Welcome to WOWDB", null, "navTo(event,'overview.aspx')^Take the Tour|closeAlert()^Close|disableTips()^Disable Tips", null, null, null, true);
        break;
    }
}
function cookiesDisabled() {
    setCookie("_cookieTest", true);
    return getCookie("_cookieTest") == null;
}
function showCookieMessage() {
    if (cookiesDisabled()) {
        displayAlert("Your browser appears to be blocking cookies. You will not be able to login to WOWDB", "Cookies Blocked");
    }
}
function disableTips() {
    closeAlert();
    setCookie("_tipsDisabled", true, 10);
}
function setFirstVisit() {
    if (cookiesDisabled()) {
        return;
    }
    if (_userID == "" && getCookie("_visited") == null) {
        if (self.location.href.indexOf("overview") == -1) {
            showTip("overview");
        }
        setCookie("_visited", true, 10);
    }
}
function resetSession() {
    setCookie("Login.Initialized", null);
}
function initSession() {
    if (cookiesDisabled()) {
        return;
    }
    var sessionTimeout = 1000 * 60 * 10;
    setTimeout(resetSession, sessionTimeout);
    var sessionid = getCookie("networkcookie");
    var loginInitialized = getCookie("Login.Initialized");
    if (sessionid != null && loginInitialized != null) {
        setUser();
        return;
    }
    var iframe = document.createElement("IFRAME");
    iframe.src = "ajaxSession.aspx";
    iframe.style.display = "none";
    document.body.appendChild(iframe);
}
function handleInitSession(status) {
    setUser();
}
initSession();
function AscEnchant(id, name, stats, icon) {
    this.id = id;
    this.name = name;
    this.stats = stats;
    if (icon != null) this.iconID = icon;
}
function AscItem(id, iconID, slot, displayID, description, has3D) {
    this.id = id;
    this.description = description;
    this.displayID = displayID;
    this.slot = slot;
    this.iconID = iconID;
    if (has3D && has3D == 1) {
        this.has3d = true;
    } else {
        this.has3d = false;
    }
}
function AscFaction(id, name, description, groupName) {
    this.id = id;
    this.name = name;
    this.description = description;
    if (groupName == "") {
        groupName = "&nbsp;";
    }
    this.groupName = groupName;
}
function AscSpell(id, description, iconID) {
    this.id = id;
    this.iconID = iconID;
    this.description = description;
}
function getLocalDate(rawDate) {
    var localizedDate = new Date(rawDate);
    var localOffset = ( - 1 * new Date().getTimezoneOffset()) / 60;
    var curHours = localizedDate.getHours();
    var newHours = curHours + localOffset;
    localizedDate.setHours(newHours);
    return localizedDate;
}
function getShortFriendlyTime(rawDate) {
    var currentDate = new Date();
    var shortFriendlyTime;
    var timeDifferenceMinutes = parseInt((currentDate.getTime() - rawDate.getTime()) / 1000 / 60, null);
    var timeDifferenceHours = parseInt(timeDifferenceMinutes / 60);
    if (timeDifferenceMinutes <= 1) {
        return "in the last minute";
    }
    if (timeDifferenceHours < 1) {
        return parseInt(timeDifferenceMinutes, null) + " min ago";
    }
    if (timeDifferenceHours < 24) {
        var extraMins = mod(timeDifferenceMinutes, 60);
        shortFriendlyTime = parseInt(timeDifferenceHours, null) + " hr";
        if (extraMins > 0) {
            shortFriendlyTime += " " + extraMins + " min";
        }
        shortFriendlyTime += " ago";
        return shortFriendlyTime;
    }
    var timeDifferenceDays = parseInt(timeDifferenceHours / 24);
    if (timeDifferenceDays < 7) {
        var extraMins = mod(timeDifferenceMinutes, 60);
        var extraHours = mod(timeDifferenceHours, 24);
        if (extraHours > 0) {
            extraHours += "hr ";
        } else {
            extraHours = "";
        }
        return timeDifferenceDays + getPlural(timeDifferenceDays, " day ", " days ") + extraHours + extraMins + " min ago";
    }
    return (rawDate.getMonth() + 1) + "/" + rawDate.getDate() + "/" + rawDate.getFullYear() + " at " + rawDate.toLocaleTimeString();
}
function getPlural(value, ifSingular, ifPlural) {
    if (value == 1) {
        return ifSingular;
    }
    return ifPlural;
}
function getFriendlyDate(rawDate, hideTime) {
    var currentDate = new Date();
    var timeDifferenceMinutes = parseInt((currentDate.getTime() - rawDate.getTime()) / 1000 / 60, null);
    var timeDifferenceHours = parseInt(timeDifferenceMinutes / 60);
    if (timeDifferenceMinutes <= 1) {
        return "in the last minute";
    } else if (timeDifferenceMinutes < 60) {
        return parseInt(timeDifferenceMinutes, null) + " minutes ago";
    } else if (currentDate.getDate() == rawDate.getDate() && currentDate.getMonth() == rawDate.getMonth() && currentDate.getYear() == rawDate.getYear()) {
        if (hideTime) {
            return "Today";
        }
        return "Today at " + rawDate.toLocaleTimeString();
    } else {
        if (hideTime) {
            return "on " + (rawDate.getMonth() + 1) + "/" + rawDate.getDate() + "/" + rawDate.getFullYear();
        }
        return "on " + (rawDate.getMonth() + 1) + "/" + rawDate.getDate() + "/" + rawDate.getFullYear() + " at " + rawDate.toLocaleTimeString();
    }
}
function AscArticle(id, dateCreated, createdUserID, createdUserName, dateModified, modifiedUserID, modifiedUserName, articleBody, articleLink, articleSourceID, articleSourceName, articleSourceLink) {
    this.id = id;
    this.dateCreated = getFriendlyDate(getLocalDate(dateCreated));
    this.createdUserName = createdUserName;
    if (dateModified) {
        this.dateModified = getFriendlyDate(getLocalDate(dateModified));
    }
    this.modifiedUserID = modifiedUserID;
    this.modifiedUserName = modifiedUserName;
    this.body = articleBody;
    this.link = articleLink;
    this.sourceID = articleSourceID;
    this.sourceName = articleSourceName;
    this.sourceLink = articleSourceLink;
}
function AscScreenshot(id, userID, userName, dateCreated, caption, isApproved) {
    this.id = id;
    this.dateCreated = getFriendlyDate(getLocalDate(dateCreated), true);
    this.userID = userID;
    this.userName = userName;
    this.caption = caption;
    if (isApproved == 1) {
        this.isApproved = true;
    } else {
        this.isApproved = false;
    }
}
var _mapLocations = null;
var _mapCurrentIndex = 0;
var _mapCurrentZoom = 1;
function addMapLocations(data) {
    _mapLocations = data;
    if (data && data[0] == null) return;
    if (data) {
        processMap();
    } else {
        var mapContainer = _$("mapContainer");
        mapContainer.innerHTML = "The location of this " + _detailsEntityTypeLabel.toLowerCase() + " is not known.";
    }
}
function testAddMapLocations(data) {
    if (!data) {
        return;
    }
    for (var i = 0; i < data.length; i++) {
        var fnd = false;
        for (var j = 0; j < _mapLocations.length; j++) {
            if (_mapLocations[j].locationID == data[i].locationID) {
                fnd = true;
                for (var k = 0; k < data[i].coords.length; k++) {
                    _mapLocations[j].coords.push(data[i].coords[k]);
                }
            }
        }
        if (!fnd) {
            _mapLocations.push(data[i]);
        }
    }
}
function setMapLinks_Default() {
    return;
    var mapContainer = _$("mapContainer");
    var locationLinks = getElementsByClassName("map-links", "div", mapContainer, true);
    if (!locationLinks || locationLinks.length == 0) {
        locationLinks = document.createElement("div");
        mapContainer.appendChild(locationLinks);
    } else {
        locationLinks.innerHTML = "";
    }
    locationLinks.className = "map-links";
    if (_detailsEntityTypeID != 3) {
        locationLinks.innerHTML += "This " + _detailsEntityTypeLabel.toLowerCase() + " can be found in ";
    }
    for (i = 0; i < _mapLocations.length; i++) {
        var locationID = _mapLocations[i].locationID;
        var locationName = getLookupLabel("location_name", locationID);
        var locationCount = _mapLocations[i].coords.length;
        var currentLink = "";
        if (i > 0) {
            currentLink += ", ";
        }
        currentLink += "<a ";
        if (locationCount > 0) {
            currentLink += " onclick=\"updateMap(event,this," + locationID + ")\">";
        } else {
            currentLink += " href=\"location.aspx?id=" + locationID + "\">";
        }
        currentLink += "</a>";
        currentLink += locationName;
        if (locationCount > 0) {
            currentLink += " (" + locationCount + ")";
        }
        locationLinks.innerHTML += currentLink;
    }
    var locationLinks = mapContainer.firstChild.getElementsByTagName("A");
    var firstLocationLink = locationLinks[0];
    var firstLocationIndex = 0;
    for (i = 0; i < locationLinks.length; i++) {
        if (locationLinks[i].onclick) {
            firstLocationLink = locationLinks[i];
            firstLocationIndex = i;
            break;
        }
    }
    if (_mapLocations[firstLocationIndex].coords.length > 0 || _detailsEntityTypeID == 3) {
        updateMap(null, firstLocationLink, _mapLocations[firstLocationIndex].locationID);
    }
}
function clearMapLinks() {
    var mapContainer = _$("mapContainer");
    var locationLinks = getElementsByClassName("map-links", "div", mapContainer, true);
    if (locationLinks != "") {
        locationLinks.style.display = "none";
        locationLinks.innerHTML = "";
    }
}
function getMapLinks(mapContainer) {
    var locationLinks = getElementsByClassName("map-links", "div", mapContainer, true);
    if (!locationLinks || locationLinks.length == 0) {
        return null;
    } else {
        return locationLinks;
    }
}
function setMapLinks_Herbs_Minerals(mapIndex) {
    var distinctEntitySearchValues = [];
    var distinctEntityValues = getDistinctFilterValues(mapIndex, "entityNames");
    for (var i = 0; i < distinctEntityValues.length; i++) {
        var currentFilter = distinctEntityValues[i].split("|");
        distinctEntitySearchValues.push(currentFilter);
    }
    var mapLinks = getMapFilterLinks(distinctEntityValues, distinctEntitySearchValues, mapIndex, "entityNames");
    var mapContainer = _$("mapContainer");
    var locationLinks = getMapLinks(mapContainer);
    if (!locationLinks) {
        locationLinks = document.createElement("div");
        locationLinks.className = "map-links";
        mapContainer.appendChild(locationLinks);
    }
    locationLinks.innerHTML = mapLinks;
    locationLinks.style.display = "block";
    var locationLinks = getElementsByClassName("map-links", "div", mapContainer, true).getElementsByTagName("button");
    var firstLocationLink = locationLinks[0];
    if (firstLocationLink) {
        firstLocationLink.onclick();
    }
}
function setMapLinks_Quest(mapIndex) {
    var mapContainer = _$("mapContainer");
    var locationLinks = getMapLinks(mapContainer);
    if (!locationLinks) {
        locationLinks = document.createElement("div");
        locationLinks.className = "map-links";
        mapContainer.appendChild(locationLinks);
    } else {
        locationLinks.innerHTML = "";
    }
    var mapLinks;
    var nCount = getMapFilterCount(mapIndex, "territoryID", [1]);
    var aCount = getMapFilterCount(mapIndex, "territoryID", [2]);
    var hCount = getMapFilterCount(mapIndex, "territoryID", [4]);
    if (aCount > 0 && hCount > 0) {
        mapLinks = "Show quest givers for: " + getMapFilterLinks(["Alliance", "Horde"], [[1, 2], [1, 4]], 0, "territoryID");
    } else if (hCount > 0) {
        mapLinks = "This zone has " + hCount + " Horde-friendly quest NPCs / objects.";
    } else if (aCount > 0) {
        mapLinks = "This zone has " + aCount + " Alliance-friendly quest NPCs / objects.";
    } else if (nCount > 0) {
        mapLinks = "This zone has " + nCount + " neutral quest NPCs / objects.";
    }
    locationLinks.innerHTML = "<div>" + mapLinks + "</div>";
    locationLinks.style.display = "block";
    var locationLinks = getElementsByClassName("map-links", "div", mapContainer, true).getElementsByTagName("button");
    var firstLocationLink = locationLinks[0];
    if (firstLocationLink) {
        firstLocationLink.onclick();
    } else {
        updateMap(null, null, _mapLocations[0].locationID);
    }
}
function setMapLinks(mapIndex) {
    clearMapLinks();
    if (_mapLocations[mapIndex].mapType == "quest") {
        setMapLinks_Quest(mapIndex);
    } else if (_mapLocations[mapIndex].mapType == "mineral" || _mapLocations[mapIndex].mapType == "herb") {
        setMapLinks_Herbs_Minerals(mapIndex);
    }
}
function updateMapIndex(link, tabIndex, newIndex) {
    _mapCurrentIndex = newIndex;
    if (_mapLocations[newIndex]) {
        setMapLinks(newIndex);
        setCurrentTab(_$("tabGroupMap"), tabIndex);
        updateMap(null, null, _mapLocations[newIndex].locationID, _mapCurrentZoom, newIndex);
    } else {
        updateMap(null, null, _detailsEntityID, _mapCurrentZoom, 0);
    }
}
function processMap() {
    var mapContainer = _$("mapContainer");
    var mapTabs = document.createElement("div");
    mapTabs.className = "map-tabs";
    var tabLabels = [];
    var tabValues = [];
    for (var i = 0; i < _mapLocations.length; i++) {
        tabLabels.push(_mapLocations[i].mapLabel + " (" + _mapLocations[i].coords.length + ")");
        tabValues.push(i);
    }
    if (tabLabels.length > 0) {
        mapTabs.id = "tabGroupMap";
        mapContainer.appendChild(mapTabs);
        if (tabLabels.length <= 5) {
            createTabGroup(mapTabs, "tabGroupMap", tabLabels.join("|"), tabValues.join("|"), 0, "updateMapIndex");
        } else {
            createLinkGroup(mapTabs, "tabGroupMap", tabLabels.join("|"), tabValues.join("|"), 0, "updateMapIndex");
        }
        var locationLinks = mapTabs.getElementsByTagName("A");
        var firstLocationIndex = 0;
        for (i = 0; i < locationLinks.length; i++) {
            if (locationLinks[i].onclick) {
                locationLinks[i].onclick();
                break;
            }
        }
    }
    var noMapLinks = [];
    for (var i = 0; i < _mapLocations.length; i++) {
        if (_mapLocations[i].coords.length == 0) {
            noMapLinks.push("<a href=\"location.aspx?id=" + _mapLocations[i].locationID + "\">" + _mapLocations[i].mapLabel + "</a>");
        }
    }
    if (noMapLinks.length > 0) {
        var mapContainer = _$("mapContainer");
        var mapLinksLower = document.createElement("div");
        var foundWord = "";
        if (tabLabels.length > 1) {
            foundWord = " also";
        }
        if (_detailsEntityTypeID != 3) {
            mapLinksLower.innerHTML = "This " + _detailsEntityTypeLabel.toLowerCase() + " can" + foundWord + " be found in: " + noMapLinks.join(", ");
        }
        mapContainer.appendChild(mapLinksLower);
    }
    if (_detailsEntityTypeID == 3) {
        var editMapInfo = document.createElement('div');
        editMapInfo.id = "editMapInfo";
        mapContainer.insertBefore(editMapInfo, _$("tabGroupMap"));
        editMapClick();
    }
}
function buildLocMapSelect() {
    var mapContainer = _$("mapContainer");
    var mSelect = document.createElement('select');
    mSelect.id = "mapNameSelect";
    mSelect.onchange = function() {
        buildLocLocationSelect();
    };
    var lSelect = document.createElement('select');
    lSelect.id = "locationNameSelect";
    lSelect.onchange = function() {
        highLightLocation();
    };
    mSelect.appendChild(document.createElement('option'));
    lSelect.appendChild(document.createElement('option'));
    var sSelect = document.createElement('select');
    sSelect.id = "locationApprovalSelect";
    sSelect.appendChild(new Option('', '0'));
    sSelect.appendChild(new Option('Approve', '1'));
    sSelect.appendChild(new Option('Deny', '2'));
    for (var i = 0; i < _mapLocations.length; i++) {
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = _mapLocations[i].mapLabel;
        mSelect.appendChild(opt);
    }
    mapContainer.appendChild(mSelect);
    mapContainer.appendChild(lSelect);
    mapContainer.appendChild(sSelect);
    var sButton = document.createElement('button');
    sButton.innerHTML = "<span>Submit</span>";
    sButton.onclick = submitLocationApproval;
    mapContainer.appendChild(sButton);
    buildLocLocationSelect();
}
function submitLocationApproval() {
    var mindex = _$("mapNameSelect").value;
    var lids = _$("locationNameSelect").value.split("^");
    var lid = lids[1];
    var lindex = lids[0];
    if ((_$("locationNameSelect").value == "") || (_$("locationApprovalSelect").value == "")) return;
    var aval = "";
    jx.load("ajaxMapEdit.aspx?locapprove=" + lid + "^" + _$("locationApprovalSelect").value,
    function(data) {
        handleLocationSubmissionData(data, mindex, lindex);
    },
    "text", "post");
}
function handleLocationSubmissionData(data, mindex, lindex) {
    var ids = data.split("|");
    if (ids[0] != 0) {
        displayAlert(ids[1]);
        return;
    }
    displayAlert(ids[1]);
    _mapLocations[mindex].coords.splice(lindex, 1);
    updateMap(null, null, _mapLocations[mindex].locationID, _mapCurrentZoom, mindex);
    buildLocLocationSelect();
}
function buildLocLocationSelect() {
    while (_$("locationNameSelect").childNodes[1] != null) {
        _$("locationNameSelect").removeChild(_$("locationNameSelect").childNodes[1]);
    }
    var index = _$("mapNameSelect").value;
    if (index == "") {
        for (var i = 0; i < _mapLocations.length; i++) {
            for (var j = 0; j < _mapLocations[i].coords.length; j++) {
                var opt = document.createElement('option');
                opt.value = j + "^" + _mapLocations[i].coords[j][5];
                opt.innerHTML = _mapLocations[i].coords[j][6];
                _$("locationNameSelect").appendChild(opt);
            }
        }
    } else {
        for (var j = 0; j < _mapLocations[index].coords.length; j++) {
            var opt = document.createElement('option');
            opt.value = j + "^" + _mapLocations[index].coords[j][5];
            opt.innerHTML = _mapLocations[index].coords[j][6];
            _$("locationNameSelect").appendChild(opt);
        }
        updateMapIndex(null, index, index);
    }
}
function editMapClick() {
    isMapEdit = !isMapEdit;
    if (isMapEdit) {
        _$("editMapInfo").innerHTML = "<table style=\"width: 486px;\"><tr><td class=small>This map is currently being edited. Click the Cancel button to exit edit mode.</td><td align=right><button onclick=\"editMapClick();\"><span>Cancel</span></button></td></tr></table>";
        _$("mapSpan").onclick = null;
        _addEventListener(_$("mapSpan"), "click", addUserLocation);
        _$("mapSpan").style.cursor = "crosshair";
    } else {
        _$("editMapInfo").innerHTML = "<table style=\"width: 486px;\"><tr><td class=small>To <span class=\"tip\" onmouseover=\"showTooltip('Locations you add will be submitted for moderation. Once approved, they will be visible to all WOWDB users.', event, 'r');\" onmouseout=\"hideTooltip();\">edit this map</span>, click the Edit Map button, then click the desired point on the map.</td><td><button onclick=\"editMapClick();\"><span>Edit Map</span></button></td></tr></table>";
        _removeEventListener(_$("mapSpan"), "click");
        _$("mapSpan").style.cursor = "default";
        var currentLocationID = _mapLocations[_mapCurrentIndex].locationID;
        var newZoomLevel = _mapCurrentZoom + 1;
        if (newZoomLevel == 4) {
            newZoomLevel = 1;
        }
        if (_$("mapSpan").onclick == null) {
            var fn = new Function("updateMap(window.event, null,'" + currentLocationID + "'," + newZoomLevel + "," + _mapCurrentIndex + ")");
            _$("mapSpan").onclick = fn;
        }
    }
}
function addUserLocation(e) {
    e = getEventObject(e);
    var mousePos = mouseCoords(e);
    var elemPos = findPosition(_$("mapSpan"));
    var y = Math.round(((mousePos.y - elemPos.y) / (_$("mapSpan").offsetHeight * 1.0)) * 10.0 * 100.0) / 10.0;
    var x = Math.round(((mousePos.x - elemPos.x) / (_$("mapSpan").offsetWidth * 1.0)) * 10.0 * 100.0) / 10.0;
    var pophtml = "Select the type of location, and enter a name for it using the form below.<br><br><input type=\"hidden\" id=\"posX\" value=\"" + x + "\"><input type=\"hidden\" id=\"posY\" value=\"" + y + "\"><table><tr><td class=\"label\">Type:</td><td class=\"alertInput\"><select id=\"locTypeSelect\"><option value=\"-1\"></option><option value=\"0\">Point of Interest</option><option value=\"2\">NPC</option><option value=\"5\">Object</option></select><br><span align=\"top\"></td></tr><tr><td class=\"label\">Name:</td><td class=\"alertInput\"><input type=\"text\" id=\"locEntityName\"><div class=\"desc\">Enter up to 50 characters</div></td></tr></table>";
    displayAlert(pophtml, "Add Map Location", "", "addNewLocation()^Ok|closeAlert()^Cancel", null, null, null, true);
}
function addNewLocation() {
    var type = _$("locTypeSelect").value;
    var name = _$("locEntityName").value;
    if (type == "" || name == "") {
        return;
    }
    var x = _$("posX").value;
    var y = _$("posY").value;
    var url = "ajaxMapEdit.aspx?maploc=" + type + "^" + name + "^" + x + "^" + y + "^" + _mapLocations[_mapCurrentIndex].locationID + "^" + _userID;
    jx.load(url, handleLocationSubmitPostback, 'text', 'post');
    closeAlert();
}
function handleLocationSubmitPostback(data) {
    var postResult = data.split("|");
    displayAlert(postResult[1], "Add Map Location");
}
function findPosition(obj) {
    var cleft = 0;
    var oobj = obj;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            cleft += obj.offsetLeft;
            obj = obj.offsetParent;
        }
    } else if (obj.x) cleft += obj.x;
    var ctop = 0;
    obj = oobj;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            ctop += obj.offsetTop;
            obj = obj.offsetParent;
        }
    } else if (obj.y) ctop += obj.y;
    return {
        x: cleft,
        y: ctop
    };
}
function getMapFilterLinks(filterLabelArray, filterValueArray, mapIndex, filterName) {
    var mapFilterLinks = [];
    mapFilterLinks.push("<button class=\"smallTab\" onclick=\"this.blur();clearMapFilters(this, " + mapIndex + ");\"><span>Any (" + _mapLocations[mapIndex].coords.length + ")</span></button>");
    for (var i = 0; i < filterLabelArray.length; i++) {
        var currentValueArray = filterValueArray[i];
        var currentFilterCount = getMapFilterCount(mapIndex, filterName, currentValueArray);
        if (currentFilterCount <= 0) {
            continue;
        }
        var currentLabel = filterLabelArray[i];
        for (var j = 0; j < currentValueArray.length; j++) {
            if (currentValueArray[j].constructor.toString().indexOf("String") > 0) {
                currentValueArray[j] = "'" + currentValueArray[j].replace(/'/g, "\\'") + "'";
            }
        }
        mapFilterLinks.push("<button class=\"smallTab\" onclick=\"this.blur();filterMapLocations(this, " + mapIndex + ",'" + filterName + "',[" + currentValueArray + "]);\"><span>" + currentLabel + " (" + currentFilterCount + ")</span></button>");
    }
    return mapFilterLinks.join(" ");
}
function getMapFilterValue(mapRecord, filterName) {
    switch (filterName) {
    case "territoryID":
        return mapRecord[4].territoryID;
        break;
    case "entityNames":
        return mapRecord[4].entityNames;
        break;
    }
}
function getDistinctFilterValues(mapIndex, filterName) {
    var mapCoords = _mapLocations[mapIndex].coords;
    var distinctValues = [];
    for (var i = 0; i < mapCoords.length; i++) {
        var currentValue = getMapFilterValue(mapCoords[i], filterName);
        if (isArray(currentValue)) {
            for (var j = 0; j < currentValue.length; j++) {
                var index = distinctValues.indexOf(currentValue[j]);
                if (index == null || index < 0) {
                    distinctValues.push(currentValue[j]);
                }
            }
        } else {
            if (distinctValues.indexOf(currentValue) < 0) {
                distinctValues.push(currentValue);
            }
        }
    }
    return distinctValues;
}
function getMatchStatus(searchValueArray, pointValueArray) {
    if (isArray(pointValueArray)) {
        for (var i = 0; i < pointValueArray.length; i++) {
            if (searchValueArray.indexOf(pointValueArray[i]) >= 0) {
                return true;
            }
        }
        return false;
    } else {
        return searchValueArray.indexOf(pointValueArray) >= 0;
    }
}
function getMapFilterCount(mapIndex, filterName, filterValueArray) {
    var mapCoords = _mapLocations[mapIndex].coords;
    var newCoords = [];
    var filterCount = 0;
    for (i = 0; i < mapCoords.length; i++) {
        if (getMatchStatus(filterValueArray, getMapFilterValue(mapCoords[i], filterName))) {
            filterCount += 1;
        }
    }
    return filterCount;
}
function clearMapFilters(link, mapIndex) {
    var mapCoords = _mapLocations[mapIndex].coords;
    var newCoords = [];
    for (i = 0; i < mapCoords.length; i++) {
        mapCoords[i].IsHidden = false;
    }
    updateMap(null, link, _mapLocations[mapIndex].locationID, null, mapIndex);
}
function filterMapLocations(link, mapIndex, filterName, filterValues) {
    var mapCoords = _mapLocations[mapIndex].coords;
    var newCoords = [];
    for (i = 0; i < mapCoords.length; i++) {
        if (getMatchStatus(filterValues, getMapFilterValue(mapCoords[i], filterName))) {
            mapCoords[i].IsHidden = false;
        } else {
            mapCoords[i].IsHidden = true;
        }
    }
    updateMap(null, link, _mapLocations[mapIndex].locationID, null, mapIndex);
}
function getMapLocations(locationID, mapIndex) {
    for (i = 0; i < _mapLocations.length; i++) {
        if (_mapLocations[i].locationID == locationID) {
            return _mapLocations[i];
        }
    }
}
function updateMap(e, link, locationID, zoomLevel, mapIndex) {
    var targetObj = getEventTarget(e);
    if (targetObj && targetObj.parentNode && targetObj.parentNode.className.indexOf("map-pin") == 0) {
        return;
    }
    hideTooltip();
    if (link) {
        if (link.className == "selected") {
            return;
        }
        setSelectedLink(link.parentNode, link, "button", "smallTab", "smallTabSelected");
    }
    var mapContainer = _$("mapContainer");
    var map = getElementsByClassName("map", "DIV", mapContainer, true);
    if (map == "") {
        map = document.createElement("DIV");
        map.className = "map";
        mapContainer.appendChild(map);
    }
    var mapCache = getElementsByClassName("map-cache", "DIV", mapContainer, true);
    if (mapCache == "") {
        mapCache = document.createElement("DIV");
        mapCache.className = "map-cache";
        mapContainer.appendChild(mapCache);
    }
    if (zoomLevel == null) {
        zoomLevel = _mapCurrentZoom;
    }
    var mapLocations = null;
    if (mapIndex) {
        mapLocations = _mapLocations[mapIndex];
    } else {
        mapLocations = getMapLocations(locationID);
    }
    var clickZoom = 2;
    if (zoomLevel == 1) {
        mapWidth = "480px";
        mapHeight = "321px";
        if (mapLocations.iheight != null) {
            mapHeight = mapLocations.iheight + "px";
        }
    } else if (zoomLevel == 2) {
        mapWidth = "720px";
        mapHeight = "480px";
        if (mapLocations.iheight != null) {
            mapHeight = ((720.0 / 480.0) * mapLocations.iheight) + "px";
        }
        clickZoom = 3;
    } else if (zoomLevel == 3) {
        mapWidth = "1000px";
        mapHeight = "663px";
        if (mapLocations.iheight != null) {
            mapHeight = ((1000.0 / 480.0) * mapLocations.iheight) + "px";
        }
        clickZoom = 1;
    }
    var mapLinks = getMapLinks(mapContainer);
    if (mapLinks) {
        mapLinks.style.width = mapWidth;
    }
    _mapCurrentZoom = zoomLevel;
    mapCache.innerHTML = "<img src=\"maps/en/" + locationID + "-" + clickZoom + ".jpg>";
    map.style.width = mapWidth;
    map.style.height = mapHeight;
    var mapCoords = mapLocations.coords;
    var mapCoordsHTML = [];
    mapCoordsHTML.push("<span id=\"mapSpan\" onclick=\"updateMap(event, null,'" + locationID + "'," + clickZoom + "," + mapIndex + ")\" class=\"map-bg\" style=\"background: transparent url(maps/en/" + locationID + "-" + zoomLevel + ".jpg) repeat scroll 0%;width:" + mapWidth + ";height:" + mapHeight + ";\">");
    for (var i = 0; i < mapCoords.length; i++) {
        var currentCoord = mapCoords[i];
        var currentCoordTooltip = "";
        var currentCoordLink = "";
        var currentCoordClass = "";
        if (currentCoord == null) continue;
        if (currentCoord.IsHidden) {
            continue;
        }
        if (currentCoord.length > 2) {
            currentCoordTooltip = currentCoord[2];
            if (currentCoordTooltip) {
                currentCoordTooltip = currentCoordTooltip.replace(/'/g, "\\'");
                currentCoordTooltip = currentCoordTooltip.replace(/"/g, "\\'");
            } else {
                currentCoordTooltip = currentCoord[0] + ", " + currentCoord[1];
            }
        } else {
            currentCoordTooltip = currentCoord[0] + ", " + currentCoord[1];
        }
        if (currentCoord.length > 3) {
            currentCoordLink = currentCoord[3];
        }
        if (currentCoord.length > 5 && currentCoord[5] != "") {
            currentCoordClass = " pin-" + currentCoord[5];
        }
        mapCoordsHTML.push("<div class=\"map-pin" + currentCoordClass + "\" style=\"left: " + currentCoord[0] + "%; top: " + currentCoord[1] + "%;\"><a id=\"mPin-" + mapIndex + "-" + i + "\" onmouseover=\"showTooltip('" + currentCoordTooltip + "',event)\" onmouseout=\"hideTooltip();\"");
        if (currentCoordLink == "") {
            mapCoordsHTML.push(" onclick=\"navToNothing(event)\" style=\"cursor: default;\"");
        } else {
            mapCoordsHTML.push(" href=\"" + currentCoordLink + "\"");
        }
        mapCoordsHTML.push("></a></div>");
    }
    mapCoordsHTML.push("</span>");
    mapCoordsHTML.push(getGlowHTML("Click the map to toggle zoom", "glow"));
    map.innerHTML = mapCoordsHTML.join("");
}
function addArticle(data) {
    arrRawData = data.split("^");
    _detailsArticle = new AscArticle(arrRawData[0], arrRawData[1], arrRawData[2], arrRawData[3], arrRawData[4], arrRawData[5], arrRawData[6], arrRawData[7], arrRawData[8], arrRawData[9], arrRawData[10], arrRawData[11]);
    processArticle();
}
function setExpandPolicy(link, expandByDefault) {
    if (expandByDefault) {
        expandArticle(link);
        setCookie("_expandArticles", "1", 1);
    } else {
        contractArticle(link);
        setCookie("_expandArticles", "0", 1);
    }
}
function expandArticle(expandLink) {
    var oArticleContainer = _$("detailsArticleContainer");
    oArticleContainer.style.display = "inline";
    oArticleContainer.style.overflow = "visible";
    oArticleContainer.style.maxHeight = "none";
    expandLink.parentNode.style.display = "none";
    var articleContractor = getElementsByClassName("article-contractor", "DIV", oArticleContainer.parentNode, true);
    articleContractor.style.display = "block";
}
function fixIETables() {
    var oArticleContainer = _$("detailsArticleContainer");
    var articleTables = oArticleContainer.getElementsByTagName("TABLE");
    for (var i = 0; i < articleTables.length; i++) {
        articleTables[i].style.display = "block";
    }
}
var _detailsTabTop = null;
function detailsTabTop() {
    if (_detailsTabTop != null) {
        return _detailsTabTop;
    }
    var viewportHeight = Client.viewportHeight();
    var articleContainer = _$("detailsArticleContainer");
    var position = getPosition(articleContainer);
    var tabContentPad = 300;
    var tabTop = viewportHeight - position.y - tabContentPad;
    if (tabTop < 100) {
        tabTop = 100;
    }
    if (tabTop > 500) {}
    _detailsTabTop = tabTop;
    return _detailsTabTop;
}
function contractArticle(contractLink) {
    _$("detailsArticleContainer").style.display = "block";
    _$("detailsArticleContainer").style.overflow = "hidden";
    _$("detailsArticleContainer").style.maxHeight = detailsTabTop() + "px";
    if (contractLink) {
        contractLink.parentNode.style.display = "none";
    }
    var articleExpander = getElementsByClassName("article-expander", "DIV", _$("detailsArticleContainer").parentNode, true);
    articleExpander.style.display = "block";
}
function processArticle() {
    if (!_$("detailsArticle")) {
        return;
    }
    if (_detailsArticle) {
        var tabTop = detailsTabTop();
        _detailsArticle.formattedBody = getFormattedText(_detailsArticle.body, false, false);
        var articleHTML = _detailsArticle.formattedBody + "<div class=\"article-source\" style=\"background-image: url(images/article_source_" + _detailsArticle.sourceID + ".gif)\">This article was gathered by <a>" + _detailsArticle.createdUserName + "</a> " + _detailsArticle.dateCreated + " from <a href=\"" + _detailsArticle.sourceLink + "\" target=\"_blank\">" + _detailsArticle.sourceName + "</a> | Read the <a href=\"" + _detailsArticle.link + "\" target=\"_blank\">Full Article</a></div>";
        _$("detailsArticle").innerHTML = articleHTML;
        if (_mapLocations) {
            var spacer = document.createElement("div");
            spacer.className = "spacerSmall";
            _$("detailsArticleContainer").parentNode.insertBefore(spacer, _$("detailsArticleContainer"));
        }
        if (_$("detailsArticleContainer").offsetHeight > tabTop && _$("detailsArticleContainer").offsetHeight - tabTop > 20) {
            var expandArticlePolicy = getCookie("_expandArticles");
            var articleContractor = getElementsByClassName("article-contractor", "DIV", _$("detailsArticle").parentNode.parentNode, true);
            if (!articleContractor || articleContractor.length == 0) {
                articleContractor = document.createElement("div");
                articleContractor.innerHTML = "<a onclick=\"contractArticle(this)\">Contract Article Contents</a> | <a onclick=\"setExpandPolicy(this, false)\">Contract By Default</a>";
                articleContractor.className = "article-contractor";
                if (expandArticlePolicy != "1") {
                    articleContractor.style.display = "none";
                }
                _$("detailsArticle").parentNode.insertBefore(articleContractor, _$("detailsArticle"));
            }
            var articleExpander = getElementsByClassName("article-expander", "DIV", _$("detailsArticle").parentNode.parentNode, true);
            if (articleExpander.length == 0) {
                articleExpander = document.createElement("div");
                articleExpander.innerHTML = "...<br><br>There's more to this article: <a onclick=\"expandArticle(this)\">Expand Article Contents</a> | <a onclick=\"setExpandPolicy(this, true)\">Expand By Default</a>";
                articleExpander.className = "article-expander";
                if (expandArticlePolicy == "1") {
                    articleExpander.style.display = "none";
                }
                insertAfter(_$("detailsArticleContainer").parentNode, articleExpander, _$("detailsArticleContainer"));
            }
            if (expandArticlePolicy != "1" && _$("detailsArticleContainer").offsetHeight > tabTop) {
                contractArticle();
            }
        }
    }
    if (!_userIsMod) {
        return;
    }
    if (_$("detailsArticle")) {
        var articleLinks = getElementsByClassName("article-links", "DIV", _$("detailsArticle").parentNode, true);
        if (!articleLinks || articleLinks.length == 0) {
            articleLinks = document.createElement("div");
            articleLinks.className = "article-links";
            var mainContainer = _$("detailsArticleContainer").parentNode;
            var editArticleContainer = _$("divMainContents").getElementsByTagName("h1")[0];
            editArticleContainer.insertBefore(articleLinks, editArticleContainer.childNodes[0]);
            articleLinks.style.textAlign = "left";
            articleLinks.style.cssFloat = "right";
        }
    }
    if (!_detailsArticle && _$("divMainContents").getElementsByTagName("h1")[0]) {
        var addArticleContainer = _$("divMainContents").getElementsByTagName("h1")[0];
        articleLinks = document.createElement("div");
        articleLinks.className = "article-links";
        articleLinks.style.textAlign = "left";
        articleLinks.style.cssFloat = "right";
        articleLinks.innerHTML = "<span><a onclick=\"editArticle(this)\">Add a Wiki Article</a></span>";
        addArticleContainer.insertBefore(articleLinks, addArticleContainer.childNodes[0]);
        _$("detailsArticle").innerHTML = "";
        return;
    } else if (_detailsArticle) {
        articleLinks.innerHTML = "<span><a onclick=\"editArticle(this," + _detailsArticle.id + ")\">Edit Article</a> | </span><span><a onclick=\"deleteArticle(this," + _detailsArticle.id + ")\">Delete Article</a></span>";
    }
}
function AscCommentPreview(id, userID, userDisplayName, commentDate, commentBodyPreview, rating, deleted, relatedEntityTypeID, relatedEntityID, relatedEntityName) {
    this.id = id;
    this.userID = userID;
    this.userDisplayName = userDisplayName;
    var localizedCommentDate = getLocalDate(commentDate);
    this.comment_date = localizedCommentDate.getTime();
    this.comment_date_Display = getShortFriendlyTime(localizedCommentDate);
    this.name = relatedEntityName;
    this.entityLink = getLookupLabel("entity_link", relatedEntityTypeID) + "?id=" + relatedEntityID + "#comments";
    this.entityLabel = getEntityLabel(relatedEntityTypeID);
    this.comment_body = commentBodyPreview;
    this.rating_Sort = -1 * rating;
    if (parseInt(rating, null) > 0) {
        this.rating = "+" + rating;
    } else if (rating == 0) {
        this.rating = "Unrated";
    } else {
        this.rating = rating;
    }
}
function AscComment(id, userID, userDisplayName, commentDate, commentBody, rating, raters, language, lastEditByID, lastEditByName, lastEditDate) {
    this.id = id;
    this.userID = userID;
    this.userDisplayName = userDisplayName;
    var localizedCommentDate = getLocalDate(commentDate);
    this.comment_date = localizedCommentDate.getTime();
    this.comment_date_Display = getFriendlyDate(localizedCommentDate);
    if (lastEditDate) {
        this.lastEditDate = getFriendlyDate(getLocalDate(lastEditDate));
    }
    this.lastEditByID = lastEditByID;
    this.lastEditByName = lastEditByName;
    this.name = commentBody;
    this.rating_Sort = -1 * rating;
    if (parseInt(rating, null) > 0) {
        this.rating = "+" + rating;
    } else {
        this.rating = rating;
    }
    if (raters == null) {
        raters = "";
    }
    this.raters = raters.split(",");
    for (var i = 0; i < this.raters.length; i++) {
        this.raters[i] = this.raters[i].split("&");
    }
}
function AscNPCResult(id, name, description, minLevel, maxLevel, locationIDs, typeID, classificationID, reactAlliance, reactHorde, displayID, dropCount, dropCountTotal, wishlistDate, wishlistContainerID, latestDate, latestVersion) {
    this.id = id;
    this.name = name;
    this.description = description;
    if (minLevel === "") {
        minLevel = "-1";
    }
    this.minLevel = minLevel;
    if (maxLevel === "") {
        maxLevel = "-1";
    }
    this.maxLevel = maxLevel;
    if (this.maxLevel == "-1") {
        this.levelRange_Sort = parseInt(this.minLevel, null) + parseInt(this.minLevel, null);
    } else {
        this.levelRange_Sort = parseInt(this.minLevel, null) + parseInt(this.maxLevel, null);
    }
    this.typeID = typeID;
    this.typeName = "<a href=\"search.aspx?browse=2." + typeID + "\">" + getLookupLabel("npc_type_id", typeID); + "</a>";
    this.locationIDs = locationIDs;
    this.locationNames = "";
    if (this.locationIDs != null) {
        var locationIDArray = this.locationIDs.split(",");
        for (var i = 0; i < locationIDArray.length; i++) {
            if (i > 0) {
                this.locationNames += ", ";
            }
            this.locationNames += getLookupLabel("location_name", locationIDArray[i]);
        }
    }
    this.classification = getLookupLabel("npc_classification_id", classificationID);
    if (displayID && displayID != "") {
        this.has3d = true;
        this.displayID = displayID;
    }
    var npcReaction = "";
    var reaction_Sort = 0;
    if (reactAlliance != null && reactAlliance != "") {
        reaction_Sort += parseInt(reactAlliance, null);
        npcReaction += "<span class=\"r" + getReactionClass(reactAlliance) + "\"> A </span>";
    } else {
        reaction_Sort += 100;
    }
    if (reactHorde != null && reactHorde != "") {
        reaction_Sort += parseInt(reactHorde, null);
        npcReaction += "<span class=\"r" + getReactionClass(reactHorde) + "\"> H </span>";
    } else {
        reaction_Sort += 100;
    }
    if (npcReaction == "") {
        npcReaction = "<span class=\"r0\">n/a</span>";
        reaction_Sort = 1000;
    }
    this.reaction = npcReaction;
    this.reaction_Sort = reaction_Sort;
    setDropRate(this, dropCount, dropCountTotal);
    setWishlistData(this, wishlistDate, wishlistContainerID);
    setLatestData(this, latestDate, latestVersion);
}
function setDropRate(obj, dropCount, dropCountTotal) {
    if (dropCount != null && dropCountTotal != null && dropCountTotal != "") {
        obj.dropRate_Sort = (dropCount / dropCountTotal) * 100;
        if (obj.dropRate_Sort < 0.01) {
            obj.dropRate = obj.dropRate_Sort.toFixed(3) + "%";
        } else if (obj.dropRate_Sort < 0.1) {
            obj.dropRate = obj.dropRate_Sort.toFixed(2) + "%";
        } else {
            obj.dropRate = obj.dropRate_Sort.toFixed(1) + "%";
        }
        if (obj.dropRate_Sort == 0) {
            obj.dropRate = "???";
        }
    } else {
        obj.dropRate_Sort = "-1";
        obj.dropRate = "-";
    }
}
function getReactionClass(reactionID) {
    switch (reactionID) {
    case "1":
        return "2";
    case "2":
        return "";
    case "3":
        return "7";
    }
    return "";
}
function AscSpellResult(id, iconID, level, schoolID, skillList, reagents, producedItemID, name, rank, talentCategoryID, spellTypeID, latestDate, latestVersion) {
    this.id = id;
    this.iconID = iconID;
    this.rarityID = 0;
    if (producedItemID && producedItemID != "") {
        var producedItemArray = producedItemID.split(",");
        for (var i = 0; i < 1; i++) {
            var currentItem = producedItemArray[i].split("=");
            this.producedItemAmount = currentItem[1];
            var producedItem = getItem(currentItem[0]);
            if (producedItem) {
                this.iconID = producedItem.iconID;
                this.tooltipID = producedItem.id;
                this.rarityID = producedItem.description.substr(producedItem.description.indexOf("<b class=r") + 10, 1);
            }
        }
    }
    if (level == "") {
        this.level = -1;
    } else {
        this.level = level;
    }
    this.schoolID = schoolID;
    this.schoolName = _objLookups.Lookup("spell_school_id." + schoolID);
    if (spellTypeID && talentCategoryID && spellTypeID == 2) {
        this.skillList = "<div class=\"small\"><a class=r1 href=\"search.aspx?browse=6.-2." + talentCategoryID + "\">" + getLookupLabel("talent_category_id", talentCategoryID.split(".")[1]) + "</a></div>";
    } else if (skillList != "") {
        var skillArray = skillList.split(",");
        var skillListHTML = [];
        for (i = 0; i < skillArray.length; i++) {
            var currentSkill = skillArray[i].split("&");
            if (currentSkill.length == 3 && currentSkill[2] != "") {
                skillListHTML.push(currentSkill[2]);
                this.skillLevel = currentSkill[2];
            }
            var skillLabel = getLookupLabel("skill_line_id", currentSkill[1]);
            skillListHTML.push("<div class=\"small\">");
            skillListHTML.push("<a class=r1 href=\"search.aspx?browse=6.");
            skillListHTML.push(currentSkill[0]);
            skillListHTML.push(".");
            skillListHTML.push(currentSkill[1]);
            skillListHTML.push("\">");
            skillListHTML.push(skillLabel);
            skillListHTML.push("</a>");
            skillListHTML.push("</div>");
            if (this.skillLevel) {
                var skillLevelSort;
                if (this.skillLevel < 10) {
                    skillLevelSort = "00" + this.skillLevel;
                } else if (this.skillLevel < 100) {
                    skillLevelSort = "0" + this.skillLevel;
                } else {
                    skillLevelSort = this.skillLevel;
                }
            } else {
                skillLevelSort = "-1";
            }
            this.skill_Sort = skillLevelSort + skillLabel;
            this.skill_SortLabel = skillLabel + skillLevelSort;
        }
        this.skillList = skillListHTML.join("");
    } else {
        this.skillList = "&nbsp;";
    }
    this.reagents = reagents.split(",");
    if (reagents == "") {
        this.reagents_Sort = -1;
    } else {
        this.reagents_Sort = (this.reagents.length * 100);
    }
    this.producedItemID = producedItemID;
    this.name = name;
    var rankNum = "";
    if (rank != null) {
        var rankRegex = /([0-9]+)/;
        var rankMatch = rankRegex.exec(rank);
        if (rankMatch) {
            rankNum = parseInt(rankMatch[1], null);
            rankNum = " !" + _alphaArrayRank[rankNum];
        }
    }
    this.spellName_sort = name + rankNum;
    this.rank = rank;
    if (spellTypeID == 3 && (_browseDescription == null || (_browseDescription && _browseDescription.length >= 2 && _browseDescription[1][0] == "7"))) {
        this.rank = rank + " (Talent)";
    }
    setLatestData(this, latestDate, latestVersion);
}
function AscItemSetResult(id, rarity, name, level, typeID, itemIDs, classIDs, tag) {
    this.id = id;
    this.name = name;
    this.rarity = rarity;
    this.itemName_Sort = _alphaArray[this.rarity] + this.name;
    this.level = level;
    this.typeID = typeID;
    this.typeName = "<a href=\"search.aspx?browse=1." + typeID + "\">" + _objLookups.Lookup("item_type." + typeID) + "</a>";
    if (itemIDs != "" && itemIDs != null) {
        this.itemIDs = itemIDs;
        this.itemSetPieces = itemIDs.split(",");
        this.rewards_Sort = this.itemSetPieces.length * 10;
    } else {
        this.itemIDs = null;
        this.rewards_Sort = -1;
    }
    this.classNames = "";
    if (classIDs != "" && classIDs != null) {
        var classIDArray = classIDs.split(",");
        for (var i = 0; i < classIDArray.length; i++) {
            var currentClass = _objLookups.Lookup("class_id." + classIDArray[i]);
            this.classNames_Sort += ", " + currentClass;
            this.classNames += ", <a href=\"search.aspx?browse=7." + classIDArray[i] + "\">" + currentClass + "</a>";
        }
        if (this.classNames != "") {
            this.classNames = this.classNames.substring(2);
        }
    } else {
        this.classNames_Sort = "";
        this.classNames = "&nbsp;";
    }
    this.tag = tag;
}
function AscQuestResult(id, name, level, typeID, sideID, rewardItems, rewardItemChoices, rewardXP, rewardCoins, categoryID, wishlistDate, wishlistContainerID, factionLevel, latestDate, latestVersion) {
    this.id = id;
    this.name = name;
    this.level = level;
    this.typeID = typeID;
    if (typeID > 0) {
        this.typeName = _objLookups.Lookup("quest_type_id." + typeID);
    }
    this.sideID = sideID;
    this.sideName = _objLookups.Lookup("quest_side_id." + sideID);
    if (this.sideName == "") {
        this.sideName = "&nbsp;";
    }
    var rewards_Sort = 0;
    if (rewardItems != "") {
        this.rewardItems = rewardItems.split(",");
        rewards_Sort += this.rewardItems.length * 10;
    } else {
        this.rewardItems = null;
    }
    if (rewardItemChoices != "") {
        this.rewardItemChoices = rewardItemChoices.split(",");
        rewards_Sort += this.rewardItemChoices.length * 10;
    } else {
        this.rewardItemChoices = null;
    }
    if (rewardXP != "") {
        this.rewardXP = rewardXP;
        rewards_Sort += (parseInt(rewardXP, null) / 100000);
    }
    this.rewardCoins = rewardCoins;
    if (rewardCoins != "") {
        rewards_Sort += (parseInt(rewardCoins, null) / 100000);
    }
    this.rewards_Sort = rewards_Sort;
    this.categoryID = categoryID;
    if (categoryID != "") {
        this.categoryName_Sort = getCategoryLabel(categoryID);
        this.categoryName = "<a href=\"search.aspx?browse=" + categoryID.replace("$", "-") + "\">" + this.categoryName_Sort + "</a>";
    } else {
        this.categoryName_Sort = "";
    }
    setWishlistData(this, wishlistDate, wishlistContainerID);
    setLatestData(this, latestDate, latestVersion);
}
function setLatestData(obj, latestDate, latestVersion) {
    if (!latestDate) {
        return;
    }
    if (latestDate != null && latestDate != "") {
        var localizedDate = getLocalDate(latestDate);
        obj.latestDate_Sort = localizedDate.getTime();
        obj.latestDate = getShortFriendlyTime(localizedDate, false);
        if (latestVersion != "") {
            obj.latestVersion = getLookupLabel("patch_label", latestVersion);
        }
    } else {
        obj.latestDate = "";
        obj.latestDate_Sort = "-1";
    }
}
function setWishlistData(obj, wishlistDate, wishlistContainerID) {
    if (!wishlistDate) {
        return;
    }
    if (wishlistDate != null && wishlistDate != "") {
        var localizedDate = getLocalDate(wishlistDate);
        obj.wishlistDate_Sort = localizedDate.getTime();
        obj.wishlistDate = getFriendlyDate(localizedDate, true);
    } else {
        obj.wishlistDate = "";
        obj.wishlistDate_Sort = "-1";
    }
    if (wishlistContainerID) {
        obj.wishlistContainerID = wishlistContainerID;
    } else {
        obj.wishlistContainerID = 1;
    }
}
function getLookupLabel(lookupName, lookupKey) {
    if (lookupKey == "") {
        return "";
    }
    return _objLookups.Lookup(lookupName + "." + lookupKey);
}
function getCategoryLabel(categoryKey) {
    var categoryArrayName = categoryKey.substring(0, categoryKey.lastIndexOf(".")).replace(/\./g, "_");
    var categoryValue = categoryKey.substring(categoryKey.lastIndexOf(".") + 1);
    var categoryLabel;
    var categoryArray;
    try {
        categoryArray = eval("mn_" + categoryArrayName);
    } catch(ex) {}
    if (!categoryArray) {
        return "";
    }
    for (var i = 0; i < categoryArray.length; i++) {
        if (categoryArray[i][0] == categoryValue) {
            categoryLabel = categoryArray[i][1];
            break;
        }
    }
    return categoryLabel;
}
function AscLocationResult(id, name, minLevel, maxLevel, sideID, typeID, categoryID, expansionID, playerLimit) {
    this.id = id;
    this.name = name;
    if (minLevel === "") {
        minLevel = "-1";
    }
    this.minLevel = minLevel;
    if (maxLevel === "") {
        maxLevel = "-1";
    }
    this.maxLevel = maxLevel;
    if (this.maxLevel == "-1") {
        this.levelRange_Sort = parseInt(this.minLevel, null) + parseInt(this.minLevel, null);
    } else {
        this.levelRange_Sort = parseInt(this.minLevel, null) + parseInt(this.maxLevel, null);
    }
    this.sideID = sideID;
    this.sideName = getLookupLabel("location_faction_id", sideID);
    this.typeID = typeID;
    this.typeName = getLookupLabel("location_type_id", typeID);
    if (this.typeName == "") {
        this.typeName = "&nbsp;";
    }
    this.categoryID = categoryID;
    if (categoryID != "") {
        this.categoryName_Sort = getCategoryLabel(categoryID);
        this.categoryName = "<a href=\"search.aspx?browse=" + categoryID.replace("$", "-") + "\">" + this.categoryName_Sort + "</a>";
    } else {
        this.categoryName_Sort = "";
    }
    this.expansionID = expansionID;
    this.playerLimit = playerLimit;
    this.locationType_Sort = this.typeName + this.playerLimit;
}
function AscItemResult(id, iconID, name, rarity, slots, level, requiredLevel, armour, dps, speed, type, displayID, sourceID, dropStackMin, disenchantChance, dropCount, dropCountTotal, price, wishlistDate, wishlistContainerID, factionLevel, latestDate, latestVersion) {
    this.id = id;
    this.iconID = iconID;
    this.name = name;
    this.rarity = rarity;
    this.itemName_Sort = _alphaArray[this.rarity] + this.name;
    if (level === "") {
        level = "-1";
    }
    this.level = level;
    if (requiredLevel === "") {
        requiredLevel = "-1";
    }
    this.requiredLevel = requiredLevel;
    if (armour === "") {
        armour = "0";
    }
    this.armour = armour;
    if (dps === "") {
        dps = "-1";
    }
    this.dps = dps;
    this.slotid = slots;
    this.slots = _objLookups.Lookup("item_slot_id." + slots);
    if (speed === "") {
        speed = "-1";
    }
    this.speed = speed;
    if (type != "") {
        var typeLabel = _objLookups.Lookup("item_type." + type);
        this.type = "<a href=\"search.aspx?browse=1." + type + "\">" + typeLabel + "</a>";
        this.type_Sort = typeLabel;
    } else {
        this.type = "";
        this.type_Sort = "";
    }
    this.displayID = displayID;
    if (sourceID != null && sourceID != "") {
        var sourceArray = sourceID.split("&");
        var itemSources = "";
        for (var i = 0; i < sourceArray.length; i++) {
            itemSources += ", " + getLookupLabel("item_source_id", sourceArray[i]);
        }
        itemSources = itemSources.substring(2);
        this.source = itemSources;
    } else {
        this.source = "&nbsp;";
    }
    this.dropStackRange = null;
    if (dropStackMin != null) {
        if (dropStackMin.indexOf("[") >= 0) {
            dropStackMin = eval(dropStackMin);
            if (dropStackMin[0] != dropStackMin[1]) {
                this.dropStackRange = dropStackMin[0] + "-" + dropStackMin[1];
            } else {
                if (dropStackMin[0] > 1) {
                    this.dropStackRange = dropStackMin[0];
                }
            }
        } else {
            this.dropStackRange = dropStackMin;
        }
    }
    setWishlistData(this, wishlistDate, wishlistContainerID);
    setLatestData(this, latestDate, latestVersion);
    if (disenchantChance != null) {
        this.disenchantChance_Sort = disenchantChance;
        this.disenchantChance = getLookupLabel("item_disenchant_chance_id", disenchantChance);
    } else {
        this.disenchantChance_Sort = "-1";
        this.disenchantChance = "-";
    }
    var myItem = getItem(this.id);
    if (myItem) {
        this.has3d = myItem.has3d;
        this.isCharacterItem = isCharacterItem(myItem.slot);
        this.isEquipableItem = isEquipableItem(myItem.slot);
    }
    setDropRate(this, dropCount, dropCountTotal);
    setVendorPrice(this, price);
    this.factionLevel = factionLevel;
}
function setVendorPrice(obj, price, react) {
    if (price) {
        price = eval("[" + price + "]");
        obj.price_Sort = price[0];
        obj.price = getCoinsHTML(price[0]);
        if (price[1] > 0) {
            obj.price += " <span class=\"moneyhorde\">" + price[1] + "</span>";
        }
        if (price[2] > 0) {
            obj.price += " <span class=\"moneyarena\">" + price[2] + "</span>";
        }
        if (price[3]) {
            for (i = 0; i < price[3].length; i++) {
                var currentItemPrice = price[3][i];
                var costItem = getItem(currentItemPrice[0]);
                if (!costItem) {
                    continue;
                }
                obj.price_Sort += currentItemPrice[1];
                obj.price += " <a onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event)\"  id=\"item_" + costItem.id + "\" href=\"?item=" + currentItemPrice[0] + "\" class=\"moneyitem\" style=\"background-image: url(icons/t/" + costItem.iconID + ".gif);\">" + currentItemPrice[1] + "</a>"
            }
        }
    }
}
function AscObjectResult(id, name, typeID, locationIDs, skillLevel, dropCount, dropCountTotal, latestDate, latestVersion) {
    this.id = id;
    this.name = name;
    this.typeID = typeID;
    this.typeName = getLookupLabel("object_type_id", typeID);
    this.skillLevel = skillLevel;
    this.locationIDs = locationIDs;
    this.locationNames = "";
    if (locationIDs != "") {
        var locationIDArray = this.locationIDs.split(",");
        for (var i = 0; i < locationIDArray.length; i++) {
            if (i > 0) {
                this.locationNames += ",";
            }
            this.locationNames += getLookupLabel("location_name", locationIDArray[i]);
        }
    }
    setDropRate(this, dropCount, dropCountTotal);
    setLatestData(this, latestDate, latestVersion);
}
function AscItemType(id, category, slots) {
    this.id = id;
    this.category = category;
    this.slots = slots;
}
function toggleImage(e, imageObj) {
    e = getEventObject(e);
    var targetObj = getEventTarget(e);
    if (imageObj) {
        targetObj = imageObj;
    }
    var imageSource = "";
    imageSource = targetObj.src;
    if (imageSource.indexOf("clear.gif") >= 0) {}
    var fileExtension = imageSource.substring(imageSource.lastIndexOf(".") + 1);
    var fileName = imageSource.substring(imageSource.lastIndexOf("/") + 1, imageSource.lastIndexOf("."));
    fileName = fileName.replace("_on", "").replace("_sel", "");
    var imageState;
    if (e.type == "mouseover") {
        imageState = "_on";
    } else if (e.type == "mouseout") {
        imageState = "";
    } else if (e.type == "mousedown") {
        imageState = "_sel";
    } else if (e.type == "mouseup") {
        imageState = "_on";
    }
    targetObj.src = "templates/wowdb/images/" + fileName + imageState + "." + fileExtension;
}
function getEntityRecord(descriptorIndex, recordID) {
    if (!isArray(descriptorIndex)) {
        descriptorIndex = descriptorIndex.split(",");
    }
    for (var j = 0; j < descriptorIndex.length; j++) {
        var dataArray = _arrResultsDataCollection[descriptorIndex[j]];
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i].id == recordID) {
                return dataArray[i];
            }
        }
    }
    return null;
}
function getItem(itemID) {
    for (var i = 0; i < arrItemData.length; i++) {
        if (arrItemData[i].id == itemID) {
            return arrItemData[i];
        }
    }
    return null;
}
function getSpell(spellID) {
    for (var i = 0; i < arrSpellData.length; i++) {
        if (arrSpellData[i].id == spellID) {
            return arrSpellData[i];
        }
    }
    return null;
}
function setItemSets(itemSetArray) {
    _arrItemSetData = itemSetArray;
}
function setEnchants(enchantArray) {
    _arrEnchantData = enchantArray;
}
function addItems(itemData) {
    var arrRawRecord = null;
    var arrRawData = null;
    var newLoad = false;
    arrRawData = itemData.split("|");
    if (arrItemData.length === 0) {
        newLoad = true;
    }
    for (var i = 0; i < arrRawData.length; i++) {
        arrRawRecord = arrRawData[i].split("^");
        if (newLoad || getItem(arrRawRecord[0]) === null) {
            arrItemData.push(new AscItem(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5]));
        }
    }
}
function addSpells(spellData) {
    var arrRawRecord = null;
    var arrRawData = null;
    var newLoad = false;
    arrRawData = spellData.split("|");
    if (arrItemData.length === 0) {
        newLoad = true;
    }
    for (var i = 0; i < arrRawData.length; i++) {
        arrRawRecord = arrRawData[i].split("^");
        arrSpellData.push(new AscSpell(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2]));
    }
}
function getTargetWindow(elementObj) {
    var targetWindow = elementObj;
    while (targetWindow.className && targetWindow.className.indexOf("WindowContainer") < 0) {
        targetWindow = targetWindow.parentNode;
    }
    if (targetWindow && targetWindow.className && targetWindow.className.indexOf("WindowContainer") >= 0) {
        return targetWindow;
    } else {
        return null;
    }
}
function bringWindowToFront(targetWindow) {
    return;
    if (targetWindow === null) {
        return;
    }
    if (targetWindow.style.zIndex == "21") {}
    var windowCol = getElementsByClassName("journalWindowContainer", "DIV", document.body);
    for (var i = 0; i < windowCol.length; i++) {
        if (parseInt(windowCol[i].style.zIndex, null) > "20") {
            windowCol[i].style.zIndex = "20";
        }
    }
    targetWindow.style.zIndex = "21";
}
function centerElement(oElement) {
    var viewportHeight = Client.viewportHeight();
    var viewportWidth = Client.viewportWidth();
    var newTop = (viewportHeight / 2) - (oElement.offsetHeight / 2);
    var newLeft = (viewportWidth / 2) - (oElement.offsetWidth / 2);
    oElement.style.top = (newTop + _scrollTop()) + "px";
    oElement.style.left = newLeft + "px";
}
function positionItemPanel(e, num) {
    if (num == null) num = 1;
    var mousePos = mouseCoords(e);
    var panels = new Array();
    panels[0] = _$("itemPanel");
    panels[1] = _$("itemPanel1");
    panels[2] = _$("itemPanel2");
    if (!panels[0]) {
        return;
    }
    if (panels[0].style.display != "block") {
        return;
    }
    var viewportHeight = Client.viewportHeight();
    var viewportWidth = Client.viewportWidth();
    var paddingWidth = 15;
    var paddingHeight = 20;
    var newTop = mousePos.y + paddingHeight;
    var newLeft = mousePos.x + paddingWidth;
    var inversePosition = false;
    if (_tooltipClass == "tip") {
        inversePosition = true;
    }
    if (inversePosition) {
        paddingWidth = 10;
    }
    if (inversePosition || (newTop + panels[0].offsetHeight) >= viewportHeight) {
        newTop = mousePos.y - panels[0].offsetHeight - paddingHeight;
        var mx = panels[0].offsetHeight;
        for (var i = 1; i < 3; i++) {
            if (num > i) {
                if (panels[i].offsetHeight > mx) mx = panels[i].offsetHeight;
            }
        }
        if (newTop < 0) {
            newTop = (viewportHeight - mx) / 2;
        }
    } else {
        newTop = mousePos.y + paddingHeight;
    }
    if (newTop < _scrollTop()) {
        newTop = mousePos.y + paddingHeight;
    }
    if (inversePosition || (newLeft + panels[0].offsetWidth) >= viewportWidth) {
        newLeft = mousePos.x - panels[0].offsetWidth - paddingWidth;
    }
    if (num > 1) {
        if (newLeft + (num * panels[0].offsetWidth) >= viewportWidth) {
            newLeft -= (panels[0].offsetWidth * (num - 1));
        }
    }
    var modx = 0;
    for (var i = 0; i < num; i++) {
        panels[i].style.top = newTop + "px";
        if (i > 0) {
            modx += panels[i - 1].offsetWidth;
        }
        panels[i].style.left = newLeft + modx + "px";
    }
}
function dbg(text) {
    return;
    if (!_$("debugPanel")) {
        return;
    }
    if (_$("debugPanel").style.display == "none") {
        return;
    }
    _$("debugPanel").appendChild(document.createElement("br"));
    _$("debugPanel").appendChild(document.createTextNode(text));
}
function buildTooltipIds(id, slot) {
    if ((slot == "17") || (slot == "21")) {
        if ((getCookie("Settings.PinnedItem.item_17") != null) && (getCookie("Settings.PinnedItem.item_17") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_17");
        else if ((getCookie("Settings.PinnedItem.item_21") != null) && (getCookie("Settings.PinnedItem.item_21") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_21");
        else if ((getCookie("Settings.PinnedItem.item_13") != null) && (getCookie("Settings.PinnedItem.item_13") != "null")) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            return id + "|" + vals[0];
        }
        return id + "|" + getCookie("Settings.PinnedItem.item_" + slot);
    }
    if ((slot == "5") || (slot == "20")) {
        if ((getCookie("Settings.PinnedItem.item_5") != null) && (getCookie("Settings.PinnedItem.item_5") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_5");
        else if ((getCookie("Settings.PinnedItem.item_20") != null) && (getCookie("Settings.PinnedItem.item_20") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_20");
    } else if ((slot == "22") || (slot == "14") || (slot == "23")) {
        if ((getCookie("Settings.PinnedItem.item_22") != null) && (getCookie("Settings.PinnedItem.item_22") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_22");
        else if ((getCookie("Settings.PinnedItem.item_14") != null) && (getCookie("Settings.PinnedItem.item_14") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_14");
        else if ((getCookie("Settings.PinnedItem.item_23") != null) && (getCookie("Settings.PinnedItem.item_23") != "null")) return id + "|" + getCookie("Settings.PinnedItem.item_23");
        else if ((getCookie("Settings.PinnedItem.item_13") != null) && (getCookie("Settings.PinnedItem.item_13") != "null")) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            if ((vals[1] != "") && (vals[1] != null)) return id + "|" + vals[1];
        }
        return id + "|" + getCookie("Settings.PinnedItem.item_" + slot);
    } else if (slot == "13") {
        var txt = id;
        if ((getCookie("Settings.PinnedItem.item_13") != "null") && (getCookie("Settings.PinnedItem.item_13") != null)) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            if ((vals[0] != "") && (vals[0] != null) && (vals[0] != "null")) txt += "|" + vals[0];
        }
        if ((getCookie("Settings.PinnedItem.item_17") != "null") && (getCookie("Settings.PinnedItem.item_17") != null)) txt += "|" + getCookie("Settings.PinnedItem.item_17");
        if ((getCookie("Settings.PinnedItem.item_21") != "null") && (getCookie("Settings.PinnedItem.item_21") != null)) txt += "|" + getCookie("Settings.PinnedItem.item_21");
        if ((getCookie("Settings.PinnedItem.item_22") != "null") && (getCookie("Settings.PinnedItem.item_22") != null)) txt += "|" + getCookie("Settings.PinnedItem.item_22");
        if ((getCookie("Settings.PinnedItem.item_14") != "null") && (getCookie("Settings.PinnedItem.item_14") != null)) txt += "|" + getCookie("Settings.PinnedItem.item_14");
        if ((getCookie("Settings.PinnedItem.item_23") != "null") && (getCookie("Settings.PinnedItem.item_23") != null)) txt += "|" + getCookie("Settings.PinnedItem.item_23");
        if ((getCookie("Settings.PinnedItem.item_13") != "null") && (getCookie("Settings.PinnedItem.item_13") != null)) {
            if ((vals[1] != "") && (vals[1] != null) && (vals[1] != "null")) txt += "|" + vals[1];
        }
        return txt;
    } else {
        if ((getCookie("Settings.PinnedItem.item_" + slot) != null) && (getCookie("Settings.PinnedItem.item_" + slot) != "null")) {
            var txt = id;
            vals = getCookie("Settings.PinnedItem.item_" + slot).split("|");
            if ((vals[0] != null) && (vals[0] != "")) txt += "|" + vals[0];
            if ((vals[1] != null) && (vals[1] != "")) txt += "|" + vals[1];
            return txt;
        } else return id;
    }
}
function handleSmallIconMouseOver(obj, e) {
    if (obj.className && obj.className == "hover") {
        obj.parentNode.lastChild.style.display = "block";
    }
    if (obj.id.indexOf("item") >= 0) {
        var itidreg = new RegExp("item_(\\d+)_?(\\d+)?_?(\\d+)?");
        var matches = itidreg.exec(obj.id);
        tooltipID = matches[1];
        itemSlot = matches[2];
        charid = matches[3];
        var citem = buildTooltipIds(tooltipID, itemSlot);
        var cids = new Array(3);
        if (!citem) {
            cids[0] = tooltipID;
        } else {
            cids = citem.split("|");
            if (cids[1] && cids[1] == "null") {
                cids[1] = null;
            }
            if (cids[2] && cids[2] == "null") {
                cids[2] = null;
            }
        }
        showItemPanel(cids[0], e, cids[1], cids[2], charid);
        return;
    }
    if (obj.id.indexOf("spell") >= 0) {
        tooltipID = obj.id.replace("spell_", "");
        showSpellPanel(tooltipID, e);
        return;
    }
}
function hideTooltip() {
    _$("itemPanel").style.display = "none";
    _$("itemPanel1").style.display = "none";
    _$("itemPanel2").style.display = "none";
}
function handleSmallIconMouseOut(obj) {
    if (obj.className && obj.className == "hover") {
        obj.parentNode.lastChild.style.display = "none";
    }
    _$("itemPanel").style.display = "none";
    _$("itemPanel1").style.display = "none";
    _$("itemPanel2").style.display = "none";
    return true;
}
function handleGlobalMouseOver(e) {
    if (_cancelMouseOver) {
        _cancelMouseOver = false;
        return false;
    }
    var eventTarget = getEventTarget(e);
    var targetPosition;
    if (eventTarget.className == "cellItemListHeader") {
        eventTarget.style.backgroundColor = "#425166";
        return false;
    }
    return;
}
function handleRowMouseOut(e) {
    var eventTarget = getEventTarget(e);
    while (eventTarget.tagName != "TR") {
        eventTarget = eventTarget.parentNode;
    }
    eventTarget.style.backgroundColor = "";
    return false;
}
function handleRowMouseClick(e, sUrl) {
    var eventTarget = getEventTarget(e);
    if (eventTarget.tagName == "A") {
        return;
    }
    self.location = sUrl;
}
function handleRowMouseOver(e) {
    var eventTarget = getEventTarget(e);
    while (eventTarget.tagName != "TR") {
        eventTarget = eventTarget.parentNode;
    }
    eventTarget.style.backgroundColor = "#08161F";
    return false;
}
function handleGlobalMouseOut(e) {
    var eventTarget = getEventTarget(e);
    var relatedTarget = getEventRelatedTarget(e);
    var profileWindow;
    if (!eventTarget) {
        return;
    }
    if (!relatedTarget) {
        if (eventTarget.className == "hover") {
            itemBrowserToggleItem(e, eventTarget);
            return false;
        }
        return;
    }
    if (eventTarget.id.indexOf("characterLink_") === 0) {
        var characterID = eventTarget.id.replace("characterLink_", "");
        profileWindow = _$("profile" + characterID);
        if (profileWindow && !profileWindow.ascKeepWindowOpen) {
            closeWindow(null, profileWindow);
        }
        return false;
    }
    if (eventTarget.className == "hover") {
        itemBrowserToggleItem(e, eventTarget);
        return false;
    } else if (eventTarget.id.indexOf("item_") === 0) {
        if (relatedTarget.id === "itemBrowserIconHighlight") {
            return true;
        }
        itemBrowserToggleItem(e, eventTarget);
        return false;
    } else if (eventTarget.className.indexOf("imageButton") === 0) {
        toggleImage(e, eventTarget);
        return false;
    } else if (eventTarget.className == "cellItemListHeader" && relatedTarget.className != "sortBtn") {
        eventTarget.style.backgroundColor = "";
        return false;
    }
    return true;
}
function getRelativeLocation() {
    var relativeLocation = self.location.href;
    var arrRelativeLocation = relativeLocation.split("/");
    relativeLocation = escape(arrRelativeLocation[arrRelativeLocation.length - 1]);
    return relativeLocation;
}
function navToNothing(e) {
    _cancelBubbling(e);
}
function navTo(e, location) {
    if (e) {
        _cancelBubbling(e);
    }
    self.location = location;
}
function navToScreenshot(id) {
    self.location = "screenshot.aspx?id=" + _detailsEntityID + "|" + _detailsEntityTypeID + "#" + id;
}
function navToWishlist() {
    self.location = "wishlist.aspx?id=" + _userID;
}
function navToLogin() {
    var relativeLocation = getRelativeLocation();
    if (relativeLocation.toLowerCase().indexOf("login.aspx") >= 0) {
        self.location = "login.aspx";
    } else {
        self.location = "login.aspx?referrer=" + relativeLocation;
    }
}
function navToLogout() {
    self.location = "login.aspx?action=logout&referrer=" + getRelativeLocation();
}
function sort_numeric(a, b) {
    return parseFloat(a) - parseFloat(b);
}
function sort_alpha(a, b) {
    if (a == b) {
        return 0;
    }
    if (a < b) {
        return - 1;
    }
    return 1;
}
function addEntityResults(data, dataCount, descriptor, descriptorLabel, columnList, defaultSort, noDataMessage) {
    var arrRawRecord = null;
    var arrRawData = null;
    var arrResultsData = [];
    arrRawData = data.split("|");
    if (arrRawData != "") {
        for (var i = 0; i < arrRawData.length; i++) {
            arrRawRecord = arrRawData[i].split("^");
            switch (descriptor) {
            case "objects":
                arrResultsData.push(new AscObjectResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8]));
                break;
            case "spells":
                arrResultsData.push(new AscSpellResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10], arrRawRecord[11], arrRawRecord[12]));
                break;
            case "locations":
                arrResultsData.push(new AscLocationResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10], arrRawRecord[11]));
                break;
            case "itemsets":
                arrResultsData.push(new AscItemSetResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7]));
                break;
            case "quests":
                arrResultsData.push(new AscQuestResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10], arrRawRecord[11], arrRawRecord[12], arrRawRecord[13], arrRawRecord[14]));
                break;
            case "npcs":
                arrResultsData.push(new AscNPCResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10], arrRawRecord[11], arrRawRecord[12], arrRawRecord[13], arrRawRecord[14], arrRawRecord[15], arrRawRecord[16]));
                break;
            case "items":
                arrResultsData.push(new AscItemResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10], arrRawRecord[11], arrRawRecord[12], arrRawRecord[13], arrRawRecord[14], arrRawRecord[15], arrRawRecord[16], arrRawRecord[17], arrRawRecord[18], arrRawRecord[19], arrRawRecord[20], arrRawRecord[21], arrRawRecord[22]));
                break;
            case "comments":
                arrResultsData.push(new AscComment(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10]));
                break;
            case "commentsPreview":
                arrResultsData.push(new AscCommentPreview(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9], arrRawRecord[10]));
                break;
            case "screenshots":
                arrResultsData.push(new AscScreenshot(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5]));
                break;
            case "factions":
                arrResultsData.push(new AscFaction(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3]));
                break;
            }
        }
    }
    _arrResultsDataCollection.push(arrResultsData);
    _arrResultsCounts.push(dataCount);
    _arrResultsDataDescriptors.push(descriptor);
    _arrResultsDataDescriptorLabels.push(descriptorLabel);
    _arrResultsStartingIndex.push(0);
    _arrFilteredResultsDataStore.push(null);
    _arrCurrentSortStore.push(defaultSort);
    if (defaultSort.indexOf(" ") > 0) {
        _arrCurrentSortOrderStore.push("descending");
    } else {
        _arrCurrentSortOrderStore.push(null);
    }
    if (noDataMessage) {
        noDataMessage = noDataMessage.replace(/\$0/g, _detailsEntityName);
    }
    _arrResultsNoDataMessage.push(noDataMessage);
    _arrResultsFields.push(columnList);
    _arrResultsDefaultSort.push(defaultSort);
    if (arrResultsData.length > 0) {
        _arrResultsHasData.push(true);
    } else {
        _arrResultsHasData.push(false);
    }
}
function addQuestResults(questData, questCount) {
    var arrRawRecord = null;
    var arrRawData = null;
    var arrQuestResultsData = [];
    arrRawData = questData.split("|");
    for (var i = 0; i < arrRawData.length; i++) {
        arrRawRecord = arrRawData[i].split("^");
        arrQuestResultsData.push(new AscQuestResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9]));
    }
    _arrResultsDataCollection.push(arrQuestResultsData);
    _arrResultsDataDescriptors.push("Quests");
    _arrResultsDataDescriptorLabels.push("Quests");
    _arrResultsDefaultSort.push(null);
    _arrResultsHasData.push(true);
    _arrResultsCounts.push(questCount);
}
function addNPCResults(npcData, npcCount) {
    var arrRawRecord = null;
    var arrRawData = null;
    var arrNPCResultsData = [];
    arrRawData = npcData.split("|");
    for (var i = 0; i < arrRawData.length; i++) {
        arrRawRecord = arrRawData[i].split("^");
        arrNPCResultsData.push(new AscNPCResult(arrRawRecord[0], arrRawRecord[1], arrRawRecord[2], arrRawRecord[3], arrRawRecord[4], arrRawRecord[5], arrRawRecord[6], arrRawRecord[7], arrRawRecord[8], arrRawRecord[9]));
    }
    _arrResultsDataCollection.push(arrNPCResultsData);
    _arrResultsDataDescriptors.push("NPCs");
    _arrResultsDataDescriptorLabels.push("NPCs");
    _arrResultsDefaultSort.push(null);
    _arrResultsHasData.push(true);
    _arrResultsCounts.push(npcCount);
}
function toggleItem(e, imageObj, iconHighlight) {
    var itemInfo;
    var itemID = null;
    myEvent = getEventObject(e);
    if (!iconHighlight) {
        var targetWindow = getTargetWindow(imageObj);
        iconHighlight = getElementsByClassName("itemIconHighlight", "IMG", targetWindow, true);
    }
    if (myEvent.type == "mouseout") {
        iconHighlight.style.display = "none";
        _$("itemPanel").style.display = "none";
    } else if (myEvent.type == "mouseover") {
        if (imageObj.ascItemID === null || imageObj.ascItemID == 0) {
            return;
        } else {
            itemID = imageObj.ascItemID;
        }
        iconHighlight.style.top = imageObj.offsetTop + "px";
        iconHighlight.style.left = imageObj.offsetLeft + "px";
        iconHighlight.style.display = "block";
        currentItem = imageObj;
        itemInfo = getItem(itemID);
        if (itemInfo != null) {
            _$("itemPanel").innerHTML = itemInfo.description;
        }
        _$("itemPanel").style.display = "block";
    }
}
function itemBrowserToggleItem(e, imageObj) {
    var targPos;
    var itemID;
    var itemInfo;
    var myEvent = getEventObject(e);
    if (myEvent.type === "mouseout") {
        _$("itemPanel").style.display = "none";
        return true;
    } else if (myEvent.type === "mouseover") {
        targPos = getPosition(imageObj);
        currentItem = imageObj;
        itemID = imageObj.id.replace("item_", "");
        showItemPanel(itemID, e);
        return true;
    }
    return false;
}
function showItemPanel(itemID, e, itemID2, itemID3, charid) {
    if (charid && charid != "") {
        itemInfo = getCharItem(itemID, getCharacter(charid));
    } else {
        itemInfo = getItem(itemID);
    }
    if (itemInfo == null) {
        return;
    }
    var desc2,
    desc3;
    if ((itemID2 != null) && (itemID2 != "") && (itemID2 != "null")) {
        itemInfo2 = getItem(itemID2);
        if (itemInfo2 == null) {
            jx.load('ajaxTooltip.aspx?id=' + itemID2, loadTooltip, 'text', 'post');
            desc2 = "loading";
        } else desc2 = itemInfo2.description;
    }
    if ((itemID3 != null) && (itemID3 != "") && (itemID3 != "null")) {
        itemInfo3 = getItem(itemID3);
        if (itemInfo3 == null) {
            jx.load('ajaxTooltip.aspx?id=' + itemID3, loadTooltip2, 'text', 'post');
            desc3 = "loading";
        } else desc3 = itemInfo3.description;
    }
    showTooltip(itemInfo.description, e, null, desc2, desc3);
}
function loadTooltip(data) {
    var splits = data.split("^");
    if (!addItems) {
        return;
    }
    addItems(data);
    itemInfo = getItem(splits[0]);
    _$("itemPanelText1").innerHTML = "<div>" + itemInfo.description + "</div>";
}
function loadTooltip2(data) {
    var splits = data.split("^");
    if (!addItems) {
        return;
    }
    addItems(data);
    itemInfo = getItem(splits[0]);
    _$("itemPanelText2").innerHTML = "<div>" + itemInfo.description + "</div>";
}
function showItemPanels(e, aobj) {
    var itidreg = new RegExp("item_(\\d+)_?(\\d+)?");
    var matches = itidreg.exec(aobj.id);
    tooltipID = matches[1];
    itemSlot = matches[2];
    var cids = new Array(3);
    var tooltipIDs = buildTooltipIds(tooltipID, itemSlot);
    if (tooltipIDs) {
        cids = tooltipIDs.split("|");
        showItemPanel(cids[0], e, cids[1], cids[2]);
    } else {
        showItemPanel(tooltipID, e);
    }
}
function showSpellPanel(spellID, e) {
    spellInfo = getSpell(spellID);
    if (spellInfo == null) {
        return;
    }
    _$("itemPanelText").style.maxWidth = "350px";
    showTooltip(spellInfo.description, e);
}
function showTooltip(tooltipHTML, e, className, tooltipHTML2, tooltipHTML3) {
    _$("itemPanel").style.display = "block";
    if (className) {
        _$("itemPanelText").innerHTML = "<div class=" + className + ">" + tooltipHTML + "</div>";
    } else {
        _$("itemPanelText").innerHTML = tooltipHTML;
    }
    var cnt = 1;
    if (tooltipHTML2 != null) {
        _$("itemPanel1").style.display = "block";
        cnt++;
        if (className) {
            _$("itemPanelText1").innerHTML = "<div class=" + className + ">" + tooltipHTML2 + "</div>";
        } else {
            _$("itemPanelText1").innerHTML = tooltipHTML2;
        }
    }
    if (tooltipHTML3 != null) {
        _$("itemPanel2").style.display = "block";
        cnt++;
        if (className) {
            _$("itemPanelText2").innerHTML = "<div class=" + className + ">" + tooltipHTML3 + "</div>";
        } else {
            _$("itemPanelText2").innerHTML = tooltipHTML3;
        }
    }
    _tooltipClass = className;
    positionItemPanel(e, cnt);
}
function navPage(newIndex) {
    setViewState(1, newIndex);
    processViewState(true);
    refreshAds();
}
function handleResultsClick(e) {
    var myEvent = getEventObject(e);
    var targetObj = getEventTarget(myEvent);
    var newSearchText;
    if (targetObj.className == "cellItemListHeader" || targetObj.className == "sortBtn") {
        sortResults(myEvent);
        refreshAds();
        return false;
    }
    if (targetObj.id == "inputSearchFilterTypeStarts" || targetObj.id == "inputSearchFilterTypeContains") {
        newSearchText = _$("inputSearchFilterName").value;
        filterSearchResultsByName(newSearchText);
        return true;
    }
    if (targetObj.id.indexOf("resultsTab") === 0) {
        processResultsTabClick(targetObj);
        return false;
    }
    return;
}
function processResultsTabClick(tabObj) {
    var newTabIndex = tabObj.id.replace("resultsTab_", "");
    if (newTabIndex == _currentDescriptorIndex) {
        return;
    }
    _processingViewState = true;
    setViewState("0,1,2", newTabIndex + "," + _arrResultsStartingIndex[newTabIndex] + "," + getResultsSortHash(newTabIndex));
    _processingViewState = false;
    processViewState();
}
function handleGlobalKeyPress(e) {
    var myEvent = getEventObject(e);
    var targetObj = getEventTarget(myEvent);
    if (myEvent.keyCode == 27) {
        closePreview();
        closeAlert();
    } else if (myEvent.keyCode == 13 && targetObj.id == "titleSearchInput") {
        if (submitSearch(targetObj)) {}
        return false;
    }
    if (targetObj.id != "inputSearchFilterName" || myEvent.keyCode == 13) {
        return true;
    }
    var newSearchText = _$("inputSearchFilterName").value;
    filterSearchResultsByName(newSearchText);
}
function filterSearchResultsByName(nameFilter) {
    var itemName;
    var nameFilterLength = nameFilter.length;
    _arrFilteredResultsData = [];
    var t = _arrOriginalResultsData.length;
    var useContainsFilter = false;
    useContainsFilter = _$("inputSearchFilterTypeContains").checked;
    for (var i = 0; i < t; i++) {
        itemName = _arrOriginalResultsData[i].name;
        if (useContainsFilter) {
            if (itemName.toLowerCase().indexOf(nameFilter.toLowerCase()) >= 0) {
                _arrFilteredResultsData.push(_arrOriginalResultsData[i]);
            }
        } else {
            if (itemName.substring(0, nameFilterLength).toLowerCase() == nameFilter.toLowerCase()) {
                _arrFilteredResultsData.push(_arrOriginalResultsData[i]);
            }
        }
    }
    if (_arrFilteredResultsData.length < _arrResultsStartingIndex[_currentDescriptorIndex]) {
        _arrResultsStartingIndex[_currentDescriptorIndex] = 0;
    }
    _arrFilteredResultsDataStore[_currentDescriptorIndex] = _arrFilteredResultsData;
    sortResults(null, _arrCurrentSortStore[_currentDescriptorIndex]);
}
function setSelectedLink(linkContainer, link, tagName, regState, selState) {
    if (regState == null) {
        regState = "";
    }
    if (selState == null) {
        selState = "selected";
    }
    if (tagName == null) {
        tagName = "A";
    }
    links = linkContainer.getElementsByTagName(tagName);
    for (i = 0; i < links.length; i++) {
        links[i].className = regState;
    }
    link.className = selState;
}
function getResultsSortHash(tabIndex) {
    var sortColumnIndex = getResultsColumnIndex(_arrCurrentSortStore[tabIndex], tabIndex);
    var sortColumnDirection = _arrCurrentSortOrderStore[tabIndex];
    if (sortColumnDirection == "descending") {
        return "-" + sortColumnIndex;
    } else {
        return sortColumnIndex;
    }
}
function getResultsColumnID(columnIndex) {
    var oResultsCols = getElementsByClassName("cellItemListHeader", "td", _$("tableResultsList_" + _currentDescriptorIndex));
    if (!oResultsCols[columnIndex]) {
        return 0;
    }
    return oResultsCols[columnIndex].ascColumnID;
}
function getResultsColumnIndex(columnID, tabIndex) {
    if (!tabIndex) {
        tabIndex = _currentDescriptorIndex;
    }
    if (!IsNumeric(columnID) && columnID.indexOf(" ") > 0) {
        columnID = columnID.substring(0, columnID.indexOf(" "));
    }
    var oResultsCols = getElementsByClassName("cellItemListHeader", "td", _$("tableResultsList_" + tabIndex));
    if (!oResultsCols) {
        return 0;
    }
    for (var i = 0; i < oResultsCols.length; i++) {
        if (oResultsCols[i].ascColumnID == columnID) {
            return i;
        }
    }
    return 0;
}
function setResultsSortState(columnID, direction) {
    var directionState = "";
    if (direction == "descending") {
        directionState = "-";
    }
    setViewState(2, directionState + getResultsColumnIndex(columnID));
}
function sortResults(e, columnID) {
    var myEvent = getEventObject(e);
    var targetObj = getEventTarget(myEvent);
    var initialSortDescending = false;
    var forceSort = false;
    var i;
    if (targetObj && targetObj.className === "sortBtn") {
        targetObj = targetObj.parentNode;
    } else if (targetObj && targetObj.tagName === "A") {
        sortLinks = targetObj.parentNode.getElementsByTagName("A");
        for (i = 0; i < sortLinks.length; i++) {
            sortLinks[i].className = "";
        }
        targetObj.className = "selected";
    }
    if (!targetObj && columnID != null) {
        forceSort = true;
    } else if (targetObj && !targetObj.ascColumnID && columnID == null) {
        return;
    }
    if (columnID == null) {
        columnID = targetObj.ascColumnID;
    }
    if (!IsNumeric(columnID) && columnID.indexOf(" ") > 0) {
        columnID = columnID.substring(0, columnID.indexOf(" "));
        initialSortDescending = true;
    }
    var comp_func;
    var sortButtons;
    var selectedSortButton;
    sortButtons = getElementsByClassName("sortBtn", "IMG", _$("tableResultsList_" + _currentDescriptorIndex), false);
    for (i = 0; i < sortButtons.length; i++) {
        var curSortCol = sortButtons[i].parentNode.ascColumnID;
        if (curSortCol == columnID || curSortCol == columnID) {
            selectedSortButton = sortButtons[i];
            sortButtons[i].style.display = "inline";
        } else {
            selectedSortButton = sortButtons[i];
            sortButtons[i].style.display = "none";
        }
    }
    if (columnID != _arrCurrentSortStore[_currentDescriptorIndex]) {
        _arrCurrentSortOrderStore[_currentDescriptorIndex] = null;
    }
    if (initialSortDescending) {
        forceSort = true;
        if (selectedSortButton) {
            selectedSortButton.src = "templates/wowdb/images/sort_desc_new.gif";
        }
    }
    if (forceSort) {
        if (initialSortDescending) {
            if (selectedSortButton) {
                selectedSortButton.src = "templates/wowdb/images/sort_desc_new.gif";
            }
            if (targetObj || _arrCurrentSortOrderStore[_currentDescriptorIndex] == null) {
                _arrCurrentSortOrderStore[_currentDescriptorIndex] = "descending";
            }
        } else {
            if (selectedSortButton) {
                selectedSortButton.src = "templates/wowdb/images/sort_asc_new.gif";
            }
            if (targetObj || _arrCurrentSortOrderStore[_currentDescriptorIndex] == null) {
                _arrCurrentSortOrderStore[_currentDescriptorIndex] = "ascending";
            }
        }
    } else if (columnID == _arrCurrentSortStore[_currentDescriptorIndex]) {
        if (_arrCurrentSortOrderStore[_currentDescriptorIndex] == null || _arrCurrentSortOrderStore[_currentDescriptorIndex] == "descending") {
            if (selectedSortButton) {
                selectedSortButton.src = "templates/wowdb/images/sort_asc_new.gif";
            }
            _arrFilteredResultsData.reverse();
            if (targetObj || _arrCurrentSortOrderStore[_currentDescriptorIndex] == "null") {
                _arrCurrentSortOrderStore[_currentDescriptorIndex] = "ascending";
            }
            _arrCurrentSortStore[_currentDescriptorIndex] = columnID;
            renderResults(_arrResultsStartingIndex[_currentDescriptorIndex]);
            setResultsSortState(columnID, _arrCurrentSortOrderStore[_currentDescriptorIndex]);
            return;
        } else {
            if (selectedSortButton) {
                selectedSortButton.src = "templates/wowdb/images/sort_desc_new.gif";
            }
            _arrFilteredResultsData.reverse();
            if (targetObj || _arrCurrentSortOrderStore[_currentDescriptorIndex] == "null") {
                _arrCurrentSortOrderStore[_currentDescriptorIndex] = "descending";
            }
            _arrCurrentSortStore[_currentDescriptorIndex] = columnID;
            renderResults(_arrResultsStartingIndex[_currentDescriptorIndex]);
            setResultsSortState(columnID, _arrCurrentSortOrderStore[_currentDescriptorIndex]);
            return;
        }
    }
    setResultsSortState(columnID, _arrCurrentSortOrderStore[_currentDescriptorIndex]);
    if (_arrCurrentSortOrderStore[_currentDescriptorIndex] == null) {
        _arrCurrentSortOrderStore[_currentDescriptorIndex] = "ascending";
    }
    _arrCurrentSortStore[_currentDescriptorIndex] = columnID;
    switch (columnID) {
    case "name":
        comp_func = sort_alpha;
        break;
    case "groupName":
        comp_func = sort_alpha;
        break;
    case "userDisplayName":
        comp_func = sort_alpha;
        break;
    case "spellName_sort":
        comp_func = sort_alpha;
        break;
    case "schoolName":
        comp_func = sort_alpha;
        break;
    case "source":
        comp_func = sort_alpha;
        break;
    case "itemName_Sort":
        comp_func = sort_alpha;
        break;
    case "locationNames":
        comp_func = sort_alpha;
        break;
    case "locationName":
        comp_func = sort_alpha;
        break;
    case "skill_Sort":
        comp_func = sort_alpha;
        break;
    case "sideName":
        comp_func = sort_alpha;
        break;
    case "categoryName":
        comp_func = sort_alpha;
        break;
    case "categoryName_Sort":
        comp_func = sort_alpha;
        break;
    case "charName":
        comp_func = sort_alpha;
        break;
    case "slots":
        comp_func = sort_alpha;
        break;
    case "type":
        comp_func = sort_alpha;
        break;
    case "type_Sort":
        comp_func = sort_alpha;
        break;
    case "typeName":
        comp_func = sort_alpha;
        break;
    case "locationType_Sort":
        comp_func = sort_alpha;
        break;
    case "className":
        comp_func = sort_alpha;
        break;
    case "classNames_Sort":
        comp_func = sort_alpha;
        break;
    case "guildName":
        comp_func = sort_alpha;
        break;
    case "serverName":
        comp_func = sort_alpha;
        break;
    default:
        comp_func = sort_numeric;
    }
    var b = 0;
    var t = _arrFilteredResultsData.length - 1;
    var swap = true;
    var q;
    while (swap) {
        swap = false;
        for (i = b; i < t; ++i) {
            if (comp_func(_arrFilteredResultsData[i][columnID], _arrFilteredResultsData[i + 1][columnID]) > 0) {
                q = _arrFilteredResultsData[i];
                _arrFilteredResultsData[i] = _arrFilteredResultsData[i + 1];
                _arrFilteredResultsData[i + 1] = q;
                swap = true;
            }
        }
        t--;
        if (!swap) {
            break;
        }
        for (i = t; i > b; --i) {
            if (comp_func(_arrFilteredResultsData[i][columnID], _arrFilteredResultsData[i - 1][columnID]) < 0) {
                q = _arrFilteredResultsData[i];
                _arrFilteredResultsData[i] = _arrFilteredResultsData[i - 1];
                _arrFilteredResultsData[i - 1] = q;
                swap = true;
            }
        }
        b++;
    }
    if (_arrCurrentSortOrderStore[_currentDescriptorIndex] == "descending") {
        _arrFilteredResultsData.reverse();
    }
    renderResults(_arrResultsStartingIndex[_currentDescriptorIndex]);
}
var _alphaArray = ["z", "y", "x", "w", "v", "u", "t"];
var _alphaArrayRank = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
function closePreview() {
    if (!_$("previewPanel")) {
        return;
    }
    _$("previewPanel").style.display = "none";
}
function getPlayer() {
    var isIE = navigator.appName.indexOf("Microsoft") >= 0;
    if (isIE) {
        return _$("previewPanelFlash");
    } else {
        return _$("previewPanelFlash");
    }
}
function getMobsPlayer() {
    var isIE = navigator.appName.indexOf("Microsoft") >= 0;
    if (isIE) {
        return _$("previewPanelMobsMobsFlash");
    } else {
        return _$("previewPanelFlashMobsEmbed");
    }
}
function zoomIn() {
    flash = getPlayer();
    flash.zoomIn();
}
function zoomOut() {
    flash = getPlayer();
    flash.zoomOut();
}
function rotateRight() {
    flash = getPlayer();
    flash.rotRight();
}
function rotateLeft() {
    flash = getPlayer();
    flash.rotLeft();
}
function rotateUp() {
    flash = getPlayer();
    flash.rotUp();
}
function rotateDown() {
    flash = getPlayer();
    flash.rotDown();
}
var _keepZooming = false;
function startRotateDown() {
    if (!_keepZooming) {
        return;
    }
    rotateDown();
    setTimeout(startRotateDown, 50);
}
function startRotateUp() {
    if (!_keepZooming) {
        return;
    }
    rotateUp();
    setTimeout(startRotateUp, 50);
}
function startRotateLeft() {
    if (!_keepZooming) {
        return;
    }
    rotateLeft();
    setTimeout(startRotateLeft, 50);
}
function startRotateRight() {
    if (!_keepZooming) {
        return;
    }
    rotateRight();
    setTimeout(startRotateRight, 50);
}
function startZoom() {
    if (!_keepZooming) {
        return;
    }
    zoomIn();
    setTimeout(startZoom, 10);
}
function startZoomOut() {
    if (!_keepZooming) {
        return;
    }
    zoomOut();
    setTimeout(startZoomOut, 10);
}
function stopZoom() {
    _keepZooming = false;
}
var _modelToLoad = 0;
function addPreviewPanel() {
    var div = document.createElement("div");
    div.id = "previewPanel";
    div.className = "previewWindowContainer";
    div.innerHTML = "<div class=\"moveWidget\"></div><a id=\"previewPanelClose\" onclick=\"closePreview();\"></a><a id=\"previewPanelClose2\" onclick=\"closePreview();\"></a><a id=\"previewPanelZoomOut\" onclick=\"zoomOut();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startZoomOut();\"></a><a id=\"previewPanelZoomIn\" onclick=\"zoomIn();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startZoom();\"></a><a id=\"previewPanelRotLeft\" onclick=\"rotateLeft();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startRotateLeft();\"></a><a id=\"previewPanelRotRight\" onclick=\"rotateRight();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startRotateRight();\"></a><a id=\"previewPanelRotUp\" onclick=\"rotateUp();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startRotateUp();\"></a><a id=\"previewPanelRotDown\" onclick=\"rotateDown();\" onmouseout=\"stopZoom()\" onmouseup=\"stopZoom();\" onmousedown=\"_keepZooming=true;startRotateDown();\"></a><div id=\"previewContainer\" style=\"position: absolute; top: 38px;left: 21px;width: 447px; height: 391px;\"></div>";
    document.body.appendChild(div);
}
var _detailsItemSetIDs;
function showPreview_ItemSet(btn, ids, showSet) {
    loadPreviewThumbnail(ids);
    if (showSet) {
        _detailsItemSetIDs = ids;
        btn.firstChild.innerHTML = "View Item";
        var fn = new Function("showPreview_ItemSet(this,'" + _detailsEntityID + "',false);");
    } else {
        btn.firstChild.innerHTML = "View Set";
        var fn = new Function("showPreview_ItemSet(this,'" + _detailsItemSetIDs + "',true);");
    }
    var enlargeFn = new Function("showPreview(null,'" + ids + "','item');");
    _$("btnEnlargePreview").onclick = enlargeFn;
    btn.onclick = fn;
}
function getViewerRace() {
    var raceCookie = getCookie("Settings.Character.Race");
    if (!raceCookie) {
        raceCookie = 1;
    }
    return raceCookie;
}
function getViewerGender() {
    var genderCookie = getCookie("Settings.Character.Gender");
    if (!genderCookie) {
        genderCookie = 0;
    }
    return genderCookie;
}
function showPreview(e, entityID, type, showOnCharacter, displayID) {
    if (isIE && window.event) {
        window.event.cancelBubble = true;
    } else if (e) {
        e.cancelBubble = true;
    }
    var flashVars = "";
    var flashSource = "wow" + type + "view.swf";
    if (type == "item") {
        var itemIDs = entityID.toString().split(",");
        var myItem = getItem(entityID);
        if (showOnCharacter || itemIDs.length > 1 || isCharacterItem(myItem.slot)) {
            var itemsToLoad = "";
            for (var i = 0; i < itemIDs.length; i++) {
                myItem = getItem(itemIDs[i]);
                if (myItem.has3d) {
                    itemsToLoad += "," + myItem.displayID + "|" + myItem.slot;
                }
            }
            flashSource = "wowcharview.swf";
            flashVars = "items=" + itemsToLoad.substring(1) + "&race=" + getViewerRace() + "&gender=" + getViewerGender();
        } else {
            if (!myItem.has3d) {
                displayAlert("This item does not have a 3D preview.");
                return;
            }
            displayID = myItem.displayID;
            if (parseInt(displayID) < 0) {
                flashVars = "id=" + ( - 1 * parseInt(myItem.displayID)) + "&rotate=yes&zoom=5";
                flashSource = "wowmobview.swf";
            } else {
                flashVars = "id=" + myItem.displayID + "&rotate=yes&zoom=5";
            }
        }
    } else if (type == "mob") {
        if (!displayID) {
            var npcRecord = getEntityRecord(getDescriptorIndexByName("npcs"), entityID);
            displayID = npcRecord.displayID;
        }
        flashVars = "id=" + displayID + "&rotate=no&zoom=20";
    }
    if (!_$("previewPanel")) {
        addPreviewPanel();
    }
    if (_$("previewPanel").style.display != "block") {
        _$("previewPanel").style.visibility = "hidden";
        _$("previewPanel").style.display = "block";
        centerElement(_$("previewPanel"));
        _$("previewPanel").style.visibility = "visible";
    }
    if (_$("previewPanelThumbnailFlash")) {
        stopThumbRotation();
    }
    var so = new SWFObject(flashSource, "previewPanelFlash", "447", "391", "8", "#000000");
    so.addParam("quality", "high");
    so.addParam("wmode", "Opaque");
    so.addParam("FlashVars", flashVars);
    so.write("previewContainer");
    var flashObject = _$("previewPanelFlash");
}
function toggleThumbRotation(button) {
    if (button.childNodes[0].innerHTML.indexOf("Start") >= 0) {
        startThumbRotation();
        button.childNodes[0].innerHTML = "Stop Rotation";
    } else {
        stopThumbRotation();
        button.childNodes[0].innerHTML = "Start Rotation";
    }
}
function startThumbRotation() {
    _$("previewPanelThumbnailFlash").startRotating();
}
function stopThumbRotation() {
    _$("previewPanelThumbnailFlash").stopRotating();
}
var _characterArmorSlots = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 16, 20];
var _characterHeldSlots = [12, 13, 14, 15, 17, 21, 22, 23, 25, 26, 28];
function isCharacterItem(slot) {
    if (_characterArmorSlots.indexOf(parseInt(slot, null)) >= 0) {
        return true;
    }
    return false;
}
function isEquipableItem(slot) {
    if (_characterArmorSlots.indexOf(parseInt(slot, null)) >= 0 || _characterHeldSlots.indexOf(parseInt(slot, null)) >= 0) {
        return true;
    }
    return false;
}
function loadPreviewThumbnail(itemIDs) {
    var flashHeight = "220";
    var flashSource = "";
    var flashVars = "";
    var itemsToLoad = "";
    if (_detailsEntityTypeID == "1" || _detailsEntityTypeID == "7") {
        itemIDs = itemIDs.split(",");
        var myItem = getItem(itemIDs[0]);
        if (itemIDs.length > 1 || isCharacterItem(myItem.slot)) {
            for (var i = 0; i < itemIDs.length; i++) {
                myItem = getItem(itemIDs[i]);
                if (!myItem) {
                    continue;
                }
                if (myItem.has3d) {
                    itemsToLoad += "," + myItem.displayID + "|" + myItem.slot;
                }
            }
            if (itemsToLoad == "") {
                return;
            }
            flashSource = "wowcharview.swf";
            flashHeight = "250";
            flashVars = "items=" + itemsToLoad.substring(1) + "&race=" + getViewerRace() + "&gender=" + getViewerGender();
        } else {
            if (parseInt(myItem.displayID) < 0) {
                flashHeight = "240";
                flashSource = "wowmobview.swf";
                flashVars = "id=" + ( - 1 * parseInt(myItem.displayID)) + "&rotate=no&zoom=4";
            } else {
                flashSource = "wowitemview.swf";
                flashVars = "id=" + myItem.displayID + "&rotate=no&zoom=4";
            }
        }
    } else if (_detailsEntityTypeID == "2") {
        flashHeight = "240";
        flashSource = "wowmobview.swf";
        flashVars = "id=" + _detailsDisplayID + "&rotate=no";
    }
    var so = new SWFObject(flashSource, "previewPanelThumbnailFlash", "100%", flashHeight, "8", "#000000");
    so.onload = handleFlashPreviewLoad;
    so.addParam("quality", "high");
    so.addParam("wmode", "Opaque");
    so.addParam("FlashVars", flashVars);
    so.write("previewThumbnailContainer");
    handleFlashPreviewLoad();
}
function handleFlashPreviewLoad() {
    _$("previewThumbnailContainer").style.backgroundImage = "none";
}
var _renderedResults;
var _lastViewStateProcessed;
var _processingViewState = false;
var _firstLoad = true;
function getViewState(index) {
    var currentHashArray = self.location.hash.substring(1).split(":");
    if (currentHashArray.length < index + 1) {
        return null;
    }
    return currentHashArray[index];
}
function processViewState(forceProcess) {
    if (_processingViewState) {
        return;
    }
    _processingViewState = true;
    if (!forceProcess && _lastViewStateProcessed == self.location.hash) {
        _processingViewState = false;
        setTimeout("processViewState()", 250);
        return false;
    }
    var currentHashArray = self.location.hash.substring(1).split(":");
    var tabIndexChanged = false;
    var startingIndex = 0;
    var startingSort;
    var newTabIndex = 0;
    if (!IsNumeric(currentHashArray[0])) {
        var descriptorIndex = getDescriptorIndexByName(currentHashArray[0]);
        if (!descriptorIndex) {
            return;
        }
        newTabIndex = descriptorIndex;
    } else if (currentHashArray[0] != "") {
        newTabIndex = parseInt(currentHashArray[0], null);
    }
    if (currentHashArray[0] != "") {
        if (newTabIndex > _arrResultsDataDescriptors.length - 1) {
            newTabIndex = 0;
        }
        if (newTabIndex != _currentDescriptorIndex) {
            tabIndexChanged = true;
        }
    }
    setCurrentResultsTab(newTabIndex);
    toggleSearchFilters();
    if (currentHashArray.length < 2) {
        startingIndex = 0;
        _arrResultsStartingIndex[_currentDescriptorIndex] = startingIndex;
    } else if (currentHashArray.length >= 2 && currentHashArray[1] != "") {
        startingIndex = parseInt(currentHashArray[1]);
        _arrResultsStartingIndex[_currentDescriptorIndex] = startingIndex;
    }
    if (currentHashArray.length >= 3 && currentHashArray[2] != "") {
        startingSort = currentHashArray[2];
    }
    if (startingSort == null) {
        startingSort = _arrResultsDefaultSort[_currentDescriptorIndex];
    } else {
        if (startingSort[0] == "-") {
            startingSort = startingSort.substring(1, startingSort.length);
            startingSort = getResultsColumnID(startingSort) + " -";
        } else {
            startingSort = getResultsColumnID(startingSort);
        }
    }
    _arrOriginalResultsData = _arrResultsDataCollection[_currentDescriptorIndex];
    if (_arrFilteredResultsDataStore[_currentDescriptorIndex] == null) {
        _arrFilteredResultsData = _arrOriginalResultsData;
    } else {
        _arrFilteredResultsData = _arrFilteredResultsDataStore[_currentDescriptorIndex];
    }
    if (tabIndexChanged && _$("tableResultsList_" + _currentDescriptorIndex).rows.length > 1) {
        setPagingHTML();
    } else {
        if (_arrResultsHasData[_currentDescriptorIndex]) {
            if (tabIndexChanged || _firstLoad) {
                sortResults(null, startingSort);
            } else {
                renderResults(_arrResultsStartingIndex[_currentDescriptorIndex]);
            }
        }
    }
    _firstLoad = false;
    _lastViewStateProcessed = self.location.hash;
    _processingViewState = false;
    setTimeout("processViewState()", 250);
    return true;
}
function setViewState(stateIndex, stateValue) {
    var indexArray = stateIndex.toString().split(",");
    var valueArray = stateValue.toString().split(",");
    var currentHashArray = self.location.hash.split(":");
    for (var i = 0; i < indexArray.length; i++) {
        if (indexArray[i] > (currentHashArray.length - 1)) {
            for (var j = currentHashArray.length - 1; j < indexArray[i]; j++) {
                currentHashArray.push("");
            }
        }
        currentHashArray[indexArray[i]] = valueArray[i];
    }
    var hashString = currentHashArray.join(":");
    if (_firstLoad) {
        return;
    }
    self.location.hash = hashString;
    lastViewStateProcessed = self.location.hash;
}
function setPagingHTML() {
    getLastPageIndex();
    var startingIndex = _arrResultsStartingIndex[_currentDescriptorIndex];
    var endingIndex = startingIndex + _resultsPerPage - 1;
    if (endingIndex >= _arrFilteredResultsData.length) {
        endingIndex = _arrFilteredResultsData.length - 1;
    }
    pagingControlsHTML = "<b>" + (startingIndex + 1) + " - " + (endingIndex + 1) + "</b> of <b>" + _arrFilteredResultsData.length + "</b>";
    if (startingIndex === 0 && endingIndex === 0 || _arrFilteredResultsData.length < _resultsPerPage) {} else if (startingIndex === 0) {
        pagingControlsHTML = "<a class=\"btnPaging btnPrevDis\">Previous</a>&nbsp;&nbsp;" + pagingControlsHTML;
        if (_arrFilteredResultsData.length <= _resultsPerPage) {
            pagingControlsHTML += "&nbsp;&nbsp;<a class=\"btnPaging btnNext\" style=\"color: #cccccc;\">Next</a>";
        } else {
            pagingControlsHTML += "&nbsp;&nbsp;<a class=\"btnPaging btnNext\" onclick=\"navPage(" + (endingIndex + 1) + ")\">Next</a>";
        }
    } else if (endingIndex == _arrFilteredResultsData.length - 1) {
        pagingControlsHTML = "<a class=\"btnPaging btnPrev\" onclick=\"navPage(" + (startingIndex - _resultsPerPage) + ")\">Previous</a>&nbsp;&nbsp;" + pagingControlsHTML;
        pagingControlsHTML += "&nbsp;&nbsp;<a class=\"btnPaging btnNextDis\">Next</a>";
    } else {
        pagingControlsHTML = "<a class=\"btnPaging btnPrev\" onclick=\"navPage(" + (startingIndex - _resultsPerPage) + ")\"> Previous</a>&nbsp;&nbsp;" + pagingControlsHTML;
        pagingControlsHTML += "&nbsp;&nbsp;<a class=\"btnPaging btnNext\" onclick=\"navPage(" + (endingIndex + 1) + ")\">Next</a>";
    }
    _$("divItemListPaging").innerHTML = pagingControlsHTML;
    _$("divItemListPagingBottom").innerHTML = _$("divItemListPaging").innerHTML;
    if (!_showAllTabs && _arrResultsCounts[_currentDescriptorIndex] != null && _arrResultsCounts[_currentDescriptorIndex] != 0) {
        _$("divSearchResultsCount").innerHTML = _arrResultsCounts[_currentDescriptorIndex] + " matches were found.";
        var searchFilterForm = _$("searchFilterForm");
        if (searchFilterForm) {
            _$("divSearchResultsCount").innerHTML += " You can get fewer by <span class=\"tip\" onmouseout=\"hideTooltip();\" onmouseover=\"showTooltip('By filtering your results, you can find exactly what you are searching for, and see up to 500 matches.', event,'r');\">filtering</span> your results. <button onclick=\"toggleFilterForm(null);\"><span>Create a Filter</span></button>";
        }
    } else {
        _$("divSearchResultsCount").style.display = "none";
    }
}
function renderResults(startingIndex) {
    var pagingControlsHTML = "";
    var i;
    var tableDescriptor;
    var rowCount;
    var startingTime = new Date();
    _renderedResults = [];
    _resultsPerPage = parseInt(_resultsPerPage, null);
    startingIndex = parseInt(startingIndex, null);
    _arrResultsStartingIndex[_currentDescriptorIndex] = startingIndex;
    tableDescriptor = _arrResultsDataDescriptors[_currentDescriptorIndex];
    itemTable = _$("tableResultsList_" + _currentDescriptorIndex);
    rowCount = itemTable.rows.length;
    for (i = 1; i < rowCount; i++) {
        itemTable.deleteRow(1);
    }
    _$("divItemListPaging").innerHTML = "No Results Found";
    _$("divItemListPagingBottom").innerHTML = "";
    if (startingIndex == _arrFilteredResultsData.length) {
        return;
    }
    var endingIndex = startingIndex + _resultsPerPage - 1;
    if (endingIndex >= _arrFilteredResultsData.length) {
        endingIndex = _arrFilteredResultsData.length - 1;
    }
    setPagingHTML();
    var entityRecord;
    var itemRow;
    var itemCell;
    var tbody;
    var tbodyClass = "";
    if (tableDescriptor != "screenshots" && tableDescriptor != "comments") {
        tbodyClass = "tbodyResultsList";
    }
    if (itemTable.tBodies && itemTable.tBodies.length > 0) {
        tbody = itemTable.tBodies[0];
        tbody.className = tbodyClass;
    } else {
        tbody = document.createElement("TBODY");
        tbody.className = tbodyClass;
        itemTable.appendChild(tbody);
    }
    var rowIndex = -1;
    var totalRows = endingIndex - startingIndex + 1;
    for (i = startingIndex; i <= endingIndex; i++) {
        rowIndex += 1;
        entityRecord = _arrFilteredResultsData[i];
        if (tableDescriptor == "comments") {
            addResultRowComments(tbody, entityRecord);
        } else if (tableDescriptor == "commentsPreview") {
            addResultRowCommentsPreview(tbody, entityRecord);
        } else if (tableDescriptor == "screenshots") {
            addResultRowScreenshots(tbody, entityRecord, rowIndex, totalRows);
        } else if (tableDescriptor == "npcs") {
            addResultRowNPC(tbody, entityRecord);
        } else if (tableDescriptor == "locations") {
            addResultRowZones(tbody, entityRecord);
        } else if (tableDescriptor == "quests") {
            addResultRowQuests(tbody, entityRecord);
        } else if (tableDescriptor == "items") {
            addResultRowItems(tbody, entityRecord);
        } else if (tableDescriptor == "objects") {
            addResultRowObjects(tbody, entityRecord);
        } else if (tableDescriptor == "spells") {
            addResultRowSpells(tbody, entityRecord);
        } else if (tableDescriptor == "itemsets") {
            addResultRowItemSets(tbody, entityRecord);
        } else if (tableDescriptor == "factions") {
            addResultRowFactions(tbody, entityRecord);
        } else {
            itemRow = tbody.insertRow( - 1);
            itemRow.className = "rowItemList";
            _addEventListener(itemRow, "mouseover", handleRowMouseOver);
            _addEventListener(itemRow, "mouseout", handleRowMouseOut);
        }
    }
    var endTime = new Date();
    if (document.all) {
        var temp = document.createElement("SPAN");
        temp.innerHTML = "<table><tbody class=\"" + tbodyClass + "\">" + _renderedResults.join("");
        var tb = _$("tableResultsList_" + _currentDescriptorIndex).tBodies[0];
        tb.parentNode.replaceChild(temp.firstChild.firstChild, tb);
        var temp = null;
    } else {
        var startingTime = new Date();
        tbody.innerHTML = _renderedResults.join("");
        var endTime = new Date();
    }
}
function getGlowHTML(amount, className) {
    var amountHTML = "";
    if (!className) {
        className = "glow r1";
    }
    amountHTML = "<span class=\"" + className + "\" style=\"position: absolute; right: 0pt; bottom: 0pt;\">";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: -1px; top: -1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: -1px; top: 0px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: -1px; top: 1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 0px; top: -1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 0px; top: 0px; z-index: 4;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 0px; top: 1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 1px; top: -1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 1px; top: 0px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<div style=\"position: absolute; white-space: nowrap; left: 1px; top: 1px; color: black; z-index: 2;\">" + amount + "</div>";
    amountHTML += "<span style=\"visibility: hidden;\">" + amount + "</span>";
    amountHTML += "</span>";
    return amountHTML;
}
function userHasRatedEntity(entityRecord) {
    if (!entityRecord.raters) {
        return false;
    }
    for (var i = 0; i < entityRecord.raters.length; i++) {
        if (entityRecord.raters[i][0] == _userID) {
            return true;
        }
    }
    return false;
}
function addResultRowScreenshots(objTable, entityRecord, rowNumber, totalRows) {
    var hideScreenshot = (!entityRecord.isApproved && entityRecord.userID != _userID && !_userIsMod);
    if (rowNumber % 4 == 0) {
        if (rowNumber > 0) {
            _renderedResults.push("</tr>");
        }
        _renderedResults.push("<tr>");
    }
    _renderedResults.push("<td class=\"screenshot-cell\" valign=\"bottom\" onclick=\"navToScreenshot(");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push(")\">");
    if (hideScreenshot) {
        _renderedResults.push("<div style=\"padding-top: 75px;padding-bottom: 75px;\">This screenshot is awaiting moderation</div>");
    } else {
        _renderedResults.push("<a><img src=\"screenshots/thumbnails/");
        _renderedResults.push(entityRecord.id);
        _renderedResults.push(".jpg\" /></a>");
        _renderedResults.push("<div class=\"screenshot-cell-user\">Submitted by <a href=\"user.aspx?id=");
        _renderedResults.push(entityRecord.userID);
        _renderedResults.push("\">");
        _renderedResults.push(entityRecord.userName);
        _renderedResults.push("</a> ");
        _renderedResults.push(entityRecord.dateCreated);
        _renderedResults.push("</div>");
        _renderedResults.push("<div class=\"screenshot-caption\">");
        if (!_userIsMod && !entityRecord.isApproved && entityRecord.userID == _userID) {
            _renderedResults.push("<small>Note: Your screenshot is awaiting moderation.</small>");
        } else {
            _renderedResults.push(entityRecord.caption);
        }
        _renderedResults.push("</div>");
        if (!entityRecord.isApproved && _userIsMod) {
            _renderedResults.push("<div class=\"screenshot-links\"><span><a onclick=\"modScreenshot(event,true,this,");
            _renderedResults.push(entityRecord.id);
            _renderedResults.push(");\">Approve</a> | </span><span><a onclick=\"modScreenshot(event,false,this,");
            _renderedResults.push(entityRecord.id);
            _renderedResults.push(");\">Delete</a></div>");
        }
    }
    _renderedResults.push("</td>");
    if ((rowNumber + 1) == totalRows) {
        var rowsNeeded = rowNumber + 1;
        while (rowsNeeded % 4 != 0) {
            rowsNeeded += 1;
            _renderedResults.push("<td class=\"screenshot-empty-cell\"></td>");
        }
    }
}
function addResultRowCommentsPreview(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event);\" onmouseout=\"handleRowMouseOut(event);\" onclick=\"handleRowMouseClick(event,'" + entityRecord.entityLink + "');\">");
    _renderedResults.push("<td style=text-align:left><a style=\"font-family: Verdana,sans-serif;\" href=\"");
    _renderedResults.push(entityRecord.entityLink);
    _renderedResults.push("\">");
    _renderedResults.push(entityRecord.name);
    _renderedResults.push("</a>");
    if (entityRecord.entityLabel) {
        _renderedResults.push("<div class=\"small\">");
        _renderedResults.push(entityRecord.entityLabel);
        _renderedResults.push("</div>");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td style=text-align:left><div style=\"overflow:hidden\">");
    _renderedResults.push(entityRecord.comment_body);
    _renderedResults.push("</div><div class=\"smallGray\">By ");
    _renderedResults.push("<a>");
    _renderedResults.push(entityRecord.userDisplayName);
    _renderedResults.push("</a> - Rating: ");
    _renderedResults.push(entityRecord.rating);
    _renderedResults.push("</div>");
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    _renderedResults.push(entityRecord.comment_date_Display);
    _renderedResults.push("</td>");
    _renderedResults.push("</tr>");
}
function addResultRowComments(objTable, entityRecord) {
    var commentDisplay;
    if (entityRecord.rating_Sort >= _commentHideRating) {
        commentDisplay = "style=\"display:none;\"";
    }
    _renderedResults.push("<tr><td style=\"text-align: left;\" id=\"comment_");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push("\">");
    _renderedResults.push("<div class=\"comment\"><div class=\"comment-header\">");
    _renderedResults.push("<div class=\"comment-rating\">");
    if (entityRecord.rating_Sort >= _commentHideRating) {
        _renderedResults.push("<a onclick=\"showHiddenComment(this);\">Show Comment</a> ");
    }
    _renderedResults.push("<b>Rating: <span>");
    _renderedResults.push(entityRecord.rating);
    _renderedResults.push("</span>");
    if (entityRecord.userID != _userID && !userHasRatedEntity(entityRecord)) {
        _renderedResults.push(" <span><a class=\"ratePositive\" onclick=\"rateComment(this," + entityRecord.id + ", -1)\">[-]</a> <a class=\"rateNegative\" onclick=\"rateComment(this," + entityRecord.id + ", 1)\">[+]</a></span>");
    }
    _renderedResults.push("</b></div>By ");
    _renderedResults.push("<a>");
    _renderedResults.push(entityRecord.userDisplayName);
    _renderedResults.push("</a> ");
    _renderedResults.push(entityRecord.comment_date_Display);
    _renderedResults.push("</div>");
    _renderedResults.push("<div class=\"comment-body\"");
    _renderedResults.push(commentDisplay);
    _renderedResults.push(">");
    _renderedResults.push(getFormattedText(entityRecord.name));
    _renderedResults.push("</div>");
    if (entityRecord.lastEditByID) {
        _renderedResults.push("<div class=\"comment-lastedit\"");
        _renderedResults.push(commentDisplay);
        _renderedResults.push(">");
        _renderedResults.push("Last edited by <a>");
        _renderedResults.push(entityRecord.lastEditByName);
        _renderedResults.push("</a> ");
        _renderedResults.push(entityRecord.lastEditDate);
        _renderedResults.push("</div>");
    }
    _renderedResults.push("<div class=\"comment-links\"");
    _renderedResults.push(commentDisplay);
    _renderedResults.push(">");
    if (_userIsMod || (_userID != "" && _userID == entityRecord.userID)) {
        _renderedResults.push("<span><a onclick=\"editComment(this," + entityRecord.id + ");\">Edit</a> | </span>");
        _renderedResults.push("<span><a onclick=\"deleteComment(this," + entityRecord.id + ");\">Delete</a> | </span>");
    }
    if (_userID == "") {
        _renderedResults.push("<a onclick=\"navToLogin();\">Reply</a>");
    } else {
        _renderedResults.push("<a onclick=\"scrollToCommentBox();\">Reply</a>");
    }
    _renderedResults.push("</div>");
    _renderedResults.push("</div></td></tr>");
    return;
}
function causesLineBreak(tagName) {
    if (tagName == "[th]" || tagName == "[/th]" || tagName == "[td]" || tagName == "[/td]" || tagName == "[tr]" || tagName == "[/tr]" || tagName == "[table]" || tagName == "[/table]" || tagName == "[h4]" || tagName == "[/h4]" || tagName == "[h]" || tagName == "[/h]" || tagName == "[/li]" || tagName == "[ul]" || tagName == "[/ul]" || tagName == "[ol]" || tagName == "[/ol]" || tagName == "[/quote]" || tagName == "[/item]" || tagName == "[/spell]") {
        return true;
    }
    return false;
}
function getFormattedText(rawText, previewMode, handleBreaks) {
    var startTime = new Date();
    var newLineCount = 0;
    var noSpacesCount = 0;
    var newText = "";
    var headerCount = 0;
    var newText = "";
    var newTextArray = [];
    if (handleBreaks == null) {
        handleBreaks = true;
    }
    if (handleBreaks) {
        for (var i = 0; i < rawText.length; i++) {
            if (rawText.charAt(i) != " " && rawText.charAt(i) != "\n") {
                noSpacesCount += 1;
            } else {
                noSpacesCount = 0;
            }
            if (rawText.charAt(i) == "\n") {
                newLineCount += 1;
                if (newLineCount <= 2) {
                    var lastTag = "";
                    var nextTag = "";
                    if (i >= 1) {
                        if (rawText.substr(i - 1, 1) == "]") {
                            var tagStart = rawText.substr(0, i).lastIndexOf("[");
                            if (tagStart >= 0) {
                                var tagLength = i - 1 - tagStart;
                                lastTag = rawText.substr(tagStart, tagLength + 1);
                            }
                        }
                        if (rawText.substr(i + 1, 1) == "[") {
                            var tagEnd = rawText.substring(i + 1).indexOf("]");
                            if (tagEnd >= 0) {
                                nextTag = rawText.substr(i + 1, tagEnd + 1);
                            }
                        }
                    }
                    if (i + 3 < rawText.length && !causesLineBreak(nextTag) && !causesLineBreak(lastTag)) {
                        newTextArray.push("<br>");
                    }
                }
            } else {
                newLineCount = 0;
                newTextArray.push(rawText.charAt(i));
            }
            if (noSpacesCount > 200) {
                if (rawText.charAt(i + 1) != ";" && rawText.charAt(i + 2) != ";" && rawText.charAt(i + 3) != ";") {
                    newTextArray.push(" ");
                    noSpacesCount = 0;
                }
            }
        }
        newText = newTextArray.join("");
    } else {
        newText = rawText.replace(/\n\n(?!\[h)/g, "<br><br>");
    }
    newText = getMoneyHTML(newText);
    newText = getEntityLink(newText, "npc");
    newText = getEntityLink(newText, "itemset");
    newText = getEntityLink(newText, "faction");
    newText = getEntityLink(newText, "quest");
    newText = getEntityLink(newText, "location");
    newText = getEntityLink(newText, "object");
    newText = getTooltipLinks(newText, "item", getFullItemLink, previewMode);
    newText = getTooltipLinks(newText, "itemlink", getTextItemLink, previewMode);
    newText = getTooltipLinks(newText, "itemicon", getIconItemLink, previewMode);
    newText = getTooltipLinks(newText, "spell", getFullSpellLink, previewMode);
    newText = getTooltipLinks(newText, "spelllink", getTextSpellLink, previewMode);
    newText = getTooltipLinks(newText, "spellicon", getIconSpellLink, previewMode);
    newText = newText.replace(/\[b\]/g, "<b>").replace(/\[\/b\]/g, "</b>");
    newText = newText.replace(/\[table\]/g, "<table>").replace(/\[\/table\]/g, "</table>");
    newText = newText.replace(/\[tr\]/g, "<tr>").replace(/\[\/tr\]/g, "</tr>");
    newText = newText.replace(/\[td\]/g, "<td>").replace(/\[\/td\]/g, "</td>");
    newText = newText.replace(/\[th\]/g, "<th>").replace(/\[\/th\]/g, "</th>");
    newText = newText.replace(/\[i\]/g, "<i>").replace(/\[\/i\]/g, "</i>");
    newText = newText.replace(/\[u\]/g, "<u>").replace(/\[\/u\]/g, "</u>");
    newText = newText.replace(/\[s\]/g, "<s>").replace(/\[\/s\]/g, "</s>");
    newText = newText.replace(/\[ul\]/g, "<ul>").replace(/\[\/ul\]/g, "</ul>");
    newText = newText.replace(/\[ol\]/g, "<ol>").replace(/\[\/ol\]/g, "</ol>");
    newText = newText.replace(/\[li\]/g, "<li><div>").replace(/\[\/li\]/g, "</div></li>");
    newText = newText.replace(/\[small\]/g, "<small>").replace(/\[\/small\]/g, "</small>");
    newText = newText.replace(/\[center\]/g, "<center>").replace(/\[\/center\]/g, "</center>");
    newText = newText.replace(/\[quote\]/g, "<div class=\"quote\">").replace(/\[\/quote\]/g, "</div>");
    var headerRegex = /\[(h([0-9])?)\]/;
    var headerMatch = headerRegex.exec(newText);
    while (headerMatch != null) {
        var currentHeaderTag = "h3";
        if (headerMatch[2] != null) {
            currentHeaderTag = "h" + headerMatch[2];
        }
        headerCount += 1;
        if (headerMatch.index == 0 && headerCount == 1) {
            newText = newText.replace(headerRegex, "<" + currentHeaderTag + " class=\"first\">");
        } else {
            newText = newText.replace(headerRegex, "<" + currentHeaderTag + ">");
        }
        newText = newText.replace(/\[\/h(?:[0-9])?\]/, "</" + currentHeaderTag + ">");
        headerMatch = headerRegex.exec(newText);
    }
    newText = newText.replace(/\[\/span\]/g, "</span>");
    var spanRegex = /\[span(.*?)\]/;
    var spanMatch = spanRegex.exec(newText);
    while (spanMatch != null) {
        newText = newText.replace(spanRegex, "<span " + spanMatch[1] + ">");
        spanMatch = spanRegex.exec(newText);
    }
    var thRegex = /\[th (.*?)\]/;
    var thMatch = thRegex.exec(newText);
    while (thMatch != null) {
        newText = newText.replace(thRegex, "<th " + thMatch[1] + ">");
        thMatch = thRegex.exec(newText);
    }
    var tdRegex = /\[td (.*?)\]/;
    var tdMatch = tdRegex.exec(newText);
    while (tdMatch != null) {
        newText = newText.replace(tdRegex, "<td " + tdMatch[1] + ">");
        tdMatch = tdRegex.exec(newText);
    }
    var liRegex = /\[li (.*?)\]/;
    var liMatch = liRegex.exec(newText);
    while (liMatch != null) {
        newText = newText.replace(liRegex, "<li " + liMatch[1] + "><div>");
        liMatch = liRegex.exec(newText);
    }
    var urlRegex = /\[url=(.*?)\](.*?)\[\/url\]/;
    var urlRegexMatch = urlRegex.exec(newText);
    while (urlRegexMatch != null) {
        if (urlRegexMatch[1].indexOf("http://") > 0) {
            newText = newText.replace(urlRegex, "<a href=\"" + urlRegexMatch[1] + "\">" + urlRegexMatch[2] + "</a>");
        } else {
            newText = newText.replace(urlRegex, "<a target=\"_blank\" href=\"" + urlRegexMatch[1] + "\">" + urlRegexMatch[2] + "</a>");
        }
        urlRegexMatch = urlRegex.exec(newText);
    }
    var endTime = new Date();
    return newText;
}
function getEntityLink(text, tag) {
    var entityLinkRegEx = new RegExp("\\[" + tag + "=([0-9]+)\](.*?)\\[\/" + tag + "]");
    var entityLinkMatch = entityLinkRegEx.exec(text);
    while (entityLinkMatch != null) {
        var entityLinkHTML = "<a href=\"" + tag + ".aspx?id=" + entityLinkMatch[1] + "\">" + entityLinkMatch[2] + "</a>";
        text = text.replace(entityLinkRegEx, entityLinkHTML);
        entityLinkMatch = entityLinkRegEx.exec(text);
    }
    return text;
}
function getMoneyHTML(text) {
    var moneyRegEx = new RegExp("\\[money type=(\\\")?([a-z]+)(\\\")?( id=[0-9]+)?\\]([0-9,]+)\\[/money\\]");
    var moneyMatch = moneyRegEx.exec(text);
    while (moneyMatch != null) {
        var moneyType = moneyMatch[2].replace("honor", "neutral");
        var moneyHTML = "";
        if (moneyType == "item") {
            var moneyItemID = moneyMatch[4].split("=")[1];
            var moneyItem = getItem(moneyItemID);
            if (moneyItem) {
                moneyHTML = "<a onmouseover=\"showItemPanel(" + moneyItemID + ",event);\" onmouseout=\"hideTooltip();\" href=\"?item=" + moneyItemID + "\"class=\"moneyitem\" style=\"background-image: url(icons/t/" + moneyItem.iconID + ".gif);\">" + moneyMatch[5] + "</a>";
            }
        } else {
            moneyHTML = "<span class=\"money" + moneyType + "\">" + moneyMatch[5] + "</span>";
        }
        text = text.replace(moneyRegEx, moneyHTML);
        moneyMatch = moneyRegEx.exec(text);
    }
    return text;
}
function getTooltipLinks(text, tag, formattingFunction, previewMode) {
    var tooltipRegEx = new RegExp("\\[" + tag + "(=[0-9]+)?( amount=)?([0-9]+)?( size=)?([a-z]+)?\\](.+?)\\[\/" + tag + "\\]");
    var tooltipMatch = tooltipRegEx.exec(text);
    while (tooltipMatch != null) {
        var tooltipAmount = null;
        var tooltipID = null;
        var tooltipLabel = null;
        var tooltipSize = null;
        if (tooltipMatch[1] && tooltipMatch[1] != "") {
            tooltipID = tooltipMatch[1].substring(1);
            tooltipLabel = tooltipMatch[6];
        } else if (tooltipMatch[6]) {
            tooltipID = tooltipMatch[6];
        }
        if (tooltipMatch[3]) {
            tooltipAmount = tooltipMatch[3];
        }
        if (tooltipMatch[5]) {
            tooltipSize = tooltipMatch[5];
        }
        text = text.replace(tooltipRegEx, formattingFunction(tooltipID, previewMode, tooltipLabel, tooltipAmount, tooltipSize));
        tooltipMatch = tooltipRegEx.exec(text);
    }
    return text;
}
function getTextItemLink(itemID, previewMode, tooltipLabel) {
    if (!IsNumeric(itemID)) {
        return "";
    }
    var myItem = getItem(itemID);
    if (myItem == null) {
        if (previewMode) {
            return "<b>[Items appear after posting]</b>";
        }
        return "";
    }
    if (!tooltipLabel) {
        tooltipLabel = myItem.description.substring(2, myItem.description.indexOf("</b>"));
    } else {
        tooltipLabel = ">" + tooltipLabel;
    }
    var itemLink = "<a href=\"?item=" + myItem.id + "\" onmouseout=\"hideTooltip();\" onmouseover=\"showItemPanel(" + myItem.id + ",event);\"" + tooltipLabel + "</a>";
    return itemLink;
}
function getIconItemLink(itemID, previewMode, tooltipLabel, tooltipAmount, tooltipSize) {
    return getFullItemLink(itemID, previewMode, tooltipLabel, tooltipAmount, tooltipSize, true);
}
function getFullItemLink(itemID, previewMode, tooltipLabel, tooltipAmount, tooltipSize, iconOnly) {
    if (iconOnly == null) {
        iconOnly = false;
    }
    if (!IsNumeric(itemID)) {
        return "";
    }
    var myItem = getItem(itemID);
    if (myItem == null) {
        if (previewMode) {
            return "<b>[Items appear after posting]</b>";
        }
        return "";
    }
    var itemLinkHTML = [];
    if (!tooltipLabel) {
        tooltipLabel = myItem.description.substring(2, myItem.description.indexOf("</b>"));
    } else {
        tooltipLabel = ">" + tooltipLabel;
    }
    var itemName = "<a href=\"?item=" + myItem.id + "\" onmouseout=\"hideTooltip();\" onmouseover=\"showItemPanel(" + myItem.id + ",event);\"" + tooltipLabel + "</a>";
    if (!tooltipSize) {
        tooltipSize = "medium";
    }
    if (!iconOnly) {
        itemLinkHTML.push("<table class=\"tableTooltipLink\"><tr><td>");
    }
    itemLinkHTML.push("<div class=\"icon" + tooltipSize + "\" style=\"background-image:url(icons/" + tooltipSize.substring(0, 1) + "/");
    itemLinkHTML.push(myItem.iconID);
    itemLinkHTML.push(".gif);\"><div class=\"tile\">");
    if (tooltipAmount && tooltipAmount > 1) {
        itemLinkHTML.push(getGlowHTML(tooltipAmount));
    }
    itemLinkHTML.push("<div class=\"hover\" onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event)\"  id=\"item_" + myItem.id + "_" + myItem.slot + "\"><a href=\"?item=" + myItem.id + "\"></div><div class=\"hilite\" style=\"display: none;\"></div></div></div>");
    if (!iconOnly) {
        itemLinkHTML.push("</td>");
        itemLinkHTML.push("<td style=\"padding-left: 4px;\">" + itemName + "</td></tr></table>");
    }
    return itemLinkHTML.join("");
}
function getSpellLink(objSpell, linkLabel) {
    if (!linkLabel) {
        linkLabel = objSpell.description.substring(objSpell.description.indexOf("<b>") + 2, objSpell.description.indexOf("</b>"));
    } else {
        linkLabel = ">" + linkLabel;
    }
    return "<a href=\"spell.aspx?id=" + objSpell.id + "\" onmouseout=\"hideTooltip();\" onmouseover=\"showSpellPanel(" + objSpell.id + ",event);\"" + linkLabel + "</a>";
}
function getTextSpellLink(spellID, previewMode, tooltipLabel) {
    if (!IsNumeric(spellID)) {
        return "";
    }
    var mySpell = getSpell(spellID);
    if (mySpell == null) {
        if (previewMode) {
            return "<b>[Spells appear after posting]</b>";
        }
        return "";
    }
    return getSpellLink(mySpell, tooltipLabel);
}
function getIconSpellLink(spellID, previewMode, tooltipLabel) {
    return getFullSpellLink(spellID, previewMode, tooltipLabel, true);
}
function getFullSpellLink(spellID, previewMode, tooltipLabel, iconOnly) {
    if (!IsNumeric(spellID)) {
        return "";
    }
    if (iconOnly == null) {
        iconOnly = false;
    }
    var mySpell = getSpell(spellID);
    if (mySpell == null) {
        if (previewMode) {
            return "<b>[Spells appear after posting]</b>";
        }
        return "";
    }
    var spellLinkHTML = [];
    var spellName = getSpellLink(mySpell, tooltipLabel);
    if (!iconOnly) {
        spellLinkHTML.push("<table class=\"tableTooltipLink\"><tr><td>");
    }
    spellLinkHTML.push("<div class=\"iconmedium\" style=\"background-image:url(icons/m/");
    spellLinkHTML.push(mySpell.iconID);
    spellLinkHTML.push(".gif);\"><div class=\"tile\">");
    spellLinkHTML.push("<div class=\"hover\" onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event)\"  id=\"spell_" + mySpell.id + "\"></div><div class=\"hilite\" style=\"display: none;\"></div></div></div>");
    if (!iconOnly) {
        spellLinkHTML.push("</td>");
        spellLinkHTML.push("<td style=\"padding-left: 4px;\">" + spellName + "</td></tr></table>");
    }
    return spellLinkHTML.join("");
}
function addResultRowNPC(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'npc.aspx?id=" + entityRecord.id + "');\">");
    _renderedResults.push("<td style=\"text-align:left;white-space: nowrap;\"><a href=\"npc.aspx?id=");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push("\" style=font-family\"Verdana,sans-serif\">");
    _renderedResults.push(entityRecord.name);
    _renderedResults.push("</a>");
    if (entityRecord.description != "") {
        _renderedResults.push("<div class=\"small\">&lt;");
        _renderedResults.push(entityRecord.description);
        _renderedResults.push("&gt;</div>");
    }
    if (entityRecord.has3d) {
        _renderedResults.push("<div class=\"smallPad\">");
        _renderedResults.push("<a onclick=\"showPreview(event,'");
        _renderedResults.push(entityRecord.id);
        _renderedResults.push("','mob');\">Preview</a>");
        _renderedResults.push("</div>");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    if (entityRecord.minLevel != "-1") {
        _renderedResults.push(entityRecord.minLevel);
        if (entityRecord.maxLevel != "-1") {
            _renderedResults.push(" - ");
            _renderedResults.push(entityRecord.maxLevel);
        }
    } else {
        if (entityRecord.classification != "") {
            _renderedResults.push("???");
        } else {
            _renderedResults.push("-");
        }
    }
    if (entityRecord.classification != "") {
        _renderedResults.push("<div class=\"small\">");
        _renderedResults.push(entityRecord.classification);
        _renderedResults.push("</div>");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    if (entityRecord.locationNames != "") {
        var locationIDs = entityRecord.locationIDs.split(",");
        var locationNames = entityRecord.locationNames.split(",");
        for (var i = 0; i < locationIDs.length; i++) {
            if (locationIDs[i] == "-1") {
                _renderedResults.push(", ...");
            } else {
                if (i > 0) {
                    _renderedResults.push(",");
                }
                _renderedResults.push("<a href=\"location.aspx?id=" + locationIDs[i] + "\" class=r1>");
                _renderedResults.push(locationNames[i]);
                _renderedResults.push("</a>");
            }
        }
    } else {
        _renderedResults.push("-");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    _renderedResults.push(entityRecord.reaction);
    _renderedResults.push("</td>");
    _renderedResults.push("<td class=\"small r1\">");
    _renderedResults.push(entityRecord.typeName);
    _renderedResults.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("dropRate") >= 0) {
        _renderedResults.push("<td>");
        _renderedResults.push(entityRecord.dropRate);
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("wishlistDate") >= 0) {
        _renderedResults.push("<td class=small>");
        _renderedResults.push(entityRecord.wishlistDate);
        _renderedResults.push("</td>");
        _renderedResults.push("<td>");
        _renderedResults.push("<button class=\"smallButton\" onclick=\"removeFromWishList(event,this," + entityRecord.id + ",2," + entityRecord.wishlistContainerID + ");\"><span>Remove</span></button>");
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestCells(entityRecord, _renderedResults);
    }
    _renderedResults.push("</tr>");
    return;
}
function addResultRowZones(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'location.aspx?id=" + entityRecord.id + "');\">");
    _renderedResults.push("<td style=text-align:left><a href=\"location.aspx?id=" + entityRecord.id + "\" style=\"font-family:Verdana,sans-serif\">");
    if (entityRecord.expansionID != "") {
        _renderedResults.push("<span class=expansion_");
        _renderedResults.push(entityRecord.expansionID);
        _renderedResults.push(">");
        _renderedResults.push(entityRecord.name);
        _renderedResults.push("</span");
    } else {
        _renderedResults.push(entityRecord.name);
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    if (entityRecord.minLevel != "-1") {
        _renderedResults.push(entityRecord.minLevel);
        if (entityRecord.maxLevel != "-1") {
            _renderedResults.push(" - ");
            _renderedResults.push(entityRecord.maxLevel);
        }
    } else {
        _renderedResults.push("-");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    _renderedResults.push("<span class=territory_");
    _renderedResults.push(entityRecord.sideID);
    _renderedResults.push(">");
    _renderedResults.push(entityRecord.sideName);
    _renderedResults.push("</span>");
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    _renderedResults.push("<span class=locationtype_");
    _renderedResults.push(entityRecord.typeID);
    _renderedResults.push(">");
    _renderedResults.push(entityRecord.typeName);
    if (entityRecord.playerLimit != "") {
        _renderedResults.push(" (");
        _renderedResults.push(entityRecord.playerLimit);
        _renderedResults.push(")");
    }
    _renderedResults.push("</span>");
    _renderedResults.push("</td>");
    _renderedResults.push("<td class=\"small r1\">");
    _renderedResults.push("<a href=#>");
    _renderedResults.push(entityRecord.categoryName);
    _renderedResults.push("</a>");
    _renderedResults.push("</td>");
    _renderedResults.push("</tr>");
}
function addResultRowObjects(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'object.aspx?id=" + entityRecord.id + "');\">");
    _renderedResults.push("<td style=text-align:left><a href=\"object.aspx?id=");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push("\" style=\"font-family:Verdana,sans-serif\">");
    _renderedResults.push(entityRecord.name);
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    if (entityRecord.locationNames != "") {
        var locationIDs = entityRecord.locationIDs.split(",");
        var locationNames = entityRecord.locationNames.split(",");
        for (var i = 0; i < locationIDs.length; i++) {
            if (locationIDs[i] == "-1") {
                _renderedResults.push(", ...");
            } else {
                if (i > 0) {
                    _renderedResults.push(", ");
                }
                _renderedResults.push("<a href=\"location.aspx?id=" + locationIDs[i] + "\" class=r1>");
                _renderedResults.push(locationNames[i]);
                _renderedResults.push("</a>");
            }
        }
    } else {
        _renderedResults.push("-");
    }
    _renderedResults.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("skillLevel") >= 0) {
        _renderedResults.push("<td>");
        _renderedResults.push(entityRecord.skillLevel);
        _renderedResults.push("</td>");
    }
    _renderedResults.push("<td class=\"small r1\">");
    _renderedResults.push("<a href=\"search.aspx?browse=5." + entityRecord.typeID + "\">");
    _renderedResults.push(entityRecord.typeName);
    _renderedResults.push("</a>");
    _renderedResults.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("dropRate") >= 0) {
        _renderedResults.push("<td>");
        _renderedResults.push(entityRecord.dropRate);
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestCells(entityRecord, _renderedResults);
    }
    _renderedResults.push("</tr>");
    return;
}
function addResultRowSpells(objTable, entityRecord) {
    if (entityRecord.resultRowHTML) {
        _renderedResults.push(entityRecord.resultRowHTML);
        return;
    }
    _resultRowHTML = [];
    _resultRowHTML.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'spell.aspx?id=" + entityRecord.id + "');\">");
    _resultRowHTML.push("<td style=\"border-right: none; padding: 0pt; width: 36px;position: relative;\">");
    _resultRowHTML.push("<div class=\"iconmedium\" style='background-image:url(images/icons/medium/");
    _resultRowHTML.push(entityRecord.iconID);
    _resultRowHTML.push(".png)'><div class=\"tile\">");
    if (entityRecord.producedItemAmount && entityRecord.producedItemAmount > 1) {
        _resultRowHTML.push(getGlowHTML(entityRecord.producedItemAmount));
    }
    _resultRowHTML.push("<div class=\"hover\" onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\"");
    if (entityRecord.tooltipID != null) {
        var it = getItem(entityRecord.tooltipID);
        _resultRowHTML.push("id=\"item_");
        _resultRowHTML.push(it.id);
        _resultRowHTML.push("_" + it.slot);
    } else {
        _resultRowHTML.push("id=\"spell_");
        _resultRowHTML.push(entityRecord.id);
    }
    _resultRowHTML.push("\">");
    if (entityRecord.tooltipID != null) {
        _resultRowHTML.push("<a href=\"?item=" + entityRecord.tooltipID + "\"></a>");
    } else {}
    _resultRowHTML.push("</div><div class=\"hilite\" style=\"display: none;\"></div></div></div>");
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td style=\"border-left: none;text-align: left;\">");
    _resultRowHTML.push("<a onmouseout=\"hideTooltip();\" onmouseover=\"showSpellPanel(");
    _resultRowHTML.push(entityRecord.id);
    _resultRowHTML.push(",event);\"");
    if (entityRecord.rarityID) {
        _resultRowHTML.push(" class=r");
        _resultRowHTML.push(entityRecord.rarityID);
    }
    _resultRowHTML.push(" style=\"font-family: Verdana,sans-serif;\" href=\"spell.aspx?id=");
    _resultRowHTML.push(entityRecord.id);
    _resultRowHTML.push("\">");
    _resultRowHTML.push(entityRecord.name);
    _resultRowHTML.push("</a>");
    if (entityRecord.rank != "") {
        _resultRowHTML.push("<div class=\"smallPad\">");
        _resultRowHTML.push(entityRecord.rank);
        _resultRowHTML.push("</div>");
    }
    _resultRowHTML.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("reagents") >= 0) {
        if (!entityRecord.reagentsHTML) {
            entityRecord.reagentsHTML = getReagentsHTML(entityRecord);
        }
        _resultRowHTML.push("<td style=\"padding: 0pt;text-align: left;\">");
        _resultRowHTML.push(entityRecord.reagentsHTML);
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("level") >= 0) {
        _resultRowHTML.push("<td>");
        if (entityRecord.level == "-1") {
            _resultRowHTML.push("-");
        } else {
            _resultRowHTML.push(entityRecord.level);
        }
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("school") >= 0) {
        _resultRowHTML.push("<td>");
        if (entityRecord.schoolName == "-1") {
            _resultRowHTML.push("-");
        } else {
            _resultRowHTML.push(entityRecord.schoolName);
        }
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("skill") >= 0) {
        _resultRowHTML.push("<td>");
        if (entityRecord.skillList == "") {
            _resultRowHTML.push("-");
        } else {
            _resultRowHTML.push(entityRecord.skillList);
        }
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestCells(entityRecord, _resultRowHTML);
    }
    entityRecord.resultRowHTML = _resultRowHTML.join("");
    _renderedResults.push(entityRecord.resultRowHTML);
}
function addResultRowFactions(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event);\" onmouseout=\"handleRowMouseOut(event);\" onclick=\"handleRowMouseClick(event,'faction.aspx?id=" + entityRecord.id + "');\">");
    _renderedResults.push("<td style=text-align:left><a href=\"faction.aspx?id=");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push("\" style=\"font-family:Verdana,sans-serif\">");
    _renderedResults.push(entityRecord.name);
    _renderedResults.push("</a>");
    if (entityRecord.description) {
        _renderedResults.push("<div class=\"small\">");
        _renderedResults.push(entityRecord.description);
        _renderedResults.push("</div>");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    _renderedResults.push(entityRecord.groupName);
    _renderedResults.push("</td>");
    _renderedResults.push("</tr>");
}
function addResultRowItems(objTable, entityRecord) {
    _renderedResults.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event);\" onmouseout=\"handleRowMouseOut(event);\" onclick=\"handleRowMouseClick(event,'?item=" + entityRecord.id + "');\">");
    _renderedResults.push("<td style=\"position: relative; border-right: none; border-left: none; padding: 0pt; width: 1px;\">");
    _renderedResults.push("<div class=\"iconmedium\" style='background-image:url(images/icons/medium/");
    _renderedResults.push(entityRecord.iconID);
    _renderedResults.push(".png)'><div class=\"tile\">");
    if (entityRecord.dropStackRange) {
        _renderedResults.push(getGlowHTML(entityRecord.dropStackRange));
    }
    _renderedResults.push("<div class=\"hover\" onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\" id=\"item_" + entityRecord.id + "_" + entityRecord.slotid + "\"><a href=\"?item=" + entityRecord.id + "\"></a></div><div class=\"hilite\" style=\"display: none;\"></div></div></div>");
    _renderedResults.push("</td>");
    _renderedResults.push("<td style=\"border-left: none;text-align: left;\">");
    _renderedResults.push("<a href=\"?item=");
    _renderedResults.push(entityRecord.id);
    _renderedResults.push("\" style=\"font-family: Verdana,sans-serif;\" onmouseout=\"hideTooltip();\" id=\"item_" + entityRecord.id + "_" + entityRecord.slotid + "\" onmouseover=\"showItemPanels(event,this);\"");
    _renderedResults.push(" class=r");
    _renderedResults.push(entityRecord.rarity);
    _renderedResults.push(">");
    _renderedResults.push(entityRecord.name);
    _renderedResults.push("</a>");
    if (entityRecord.has3d) {
        _renderedResults.push("<div class=\"smallPad\">");
        if (!entityRecord.isCharacterItem) {
            _renderedResults.push("<a onclick=\"showPreview(event,'");
            _renderedResults.push(entityRecord.id);
            _renderedResults.push("','item');\">Preview</a>");
        }
        if (!entityRecord.isCharacterItem && entityRecord.isEquipableItem) {
            _renderedResults.push(" | ");
        }
        if (entityRecord.isEquipableItem) {
            _renderedResults.push("<a onclick=\"showPreview(event,'");
            _renderedResults.push(entityRecord.id);
            _renderedResults.push("','item',true);\">Preview on Character</a>");
        }
        _renderedResults.push("</div>");
    }
    _renderedResults.push("</td>");
    _renderedResults.push("<td>");
    if (entityRecord.level == "-1") {
        _renderedResults.push("-");
    } else {
        _renderedResults.push(entityRecord.level);
        if (entityRecord.requiredLevel && entityRecord.requiredLevel != "-1") {
            _renderedResults.push("<div class=\"r0 small\">Reqd. ");
            _renderedResults.push(entityRecord.requiredLevel);
            _renderedResults.push("</div>");
        }
    }
    _renderedResults.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("dps") >= 0) {
        _renderedResults.push("<td>");
        if (entityRecord.dps == "-1") {
            _renderedResults.push("-");
        } else {
            _renderedResults.push(entityRecord.dps);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("speed") >= 0) {
        _renderedResults.push("<td>");
        if (entityRecord.speed == "-1") {
            _renderedResults.push("-");
        } else {
            _renderedResults.push(entityRecord.speed);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("armor") >= 0) {
        _renderedResults.push("<td>");
        if (entityRecord.armour == "0") {
            _renderedResults.push("-");
        } else {
            _renderedResults.push(entityRecord.armour);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("slot") >= 0) {
        _renderedResults.push("<td>");
        if (entityRecord.slots === "") {
            _renderedResults.push("-");
        } else {
            _renderedResults.push(entityRecord.slots);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("source") >= 0) {
        _renderedResults.push("<td class=\"small r1\">");
        if (!entityRecord.source === "") {
            renderedResults.push("&nbsp;");
        } else {
            _renderedResults.push(entityRecord.source);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("type") >= 0) {
        _renderedResults.push("<td class=\"small r1\">");
        if (entityRecord.type === "") {
            _renderedResults.push("-");
        } else {
            _renderedResults.push(entityRecord.type);
        }
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("disenchantChance") >= 0) {
        _renderedResults.push("<td class=small>");
        _renderedResults.push(entityRecord.disenchantChance);
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("factionLevel") >= 0) {
        _renderedResults.push("<td class=small>");
        _renderedResults.push(getLookupLabel("item_required_faction_level", entityRecord.factionLevel));
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("dropRate") >= 0) {
        _renderedResults.push("<td>");
        _renderedResults.push(entityRecord.dropRate);
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("price") >= 0) {
        _renderedResults.push("<td>");
        _renderedResults.push(entityRecord.price);
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("wishlistDate") >= 0) {
        _renderedResults.push("<td class=small>");
        _renderedResults.push(entityRecord.wishlistDate);
        _renderedResults.push("</td>");
        _renderedResults.push("<td>");
        _renderedResults.push("<button class=\"smallButton\" onclick=\"removeFromWishList(event,this," + entityRecord.id + ",1," + entityRecord.wishlistContainerID + ");\"><span>Remove</span></button>");
        _renderedResults.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestCells(entityRecord, _renderedResults);
    }
    _renderedResults.push("</tr>");
}
function addLatestCells(entityRecord, output) {
    output.push("<td class=small>");
    output.push(entityRecord.latestVersion);
    output.push("</td>");
    output.push("<td class=small>");
    output.push(entityRecord.latestDate);
    output.push("</td>");
}
function addResultRowQuests(objTable, entityRecord) {
    if (entityRecord.resultRowHTML) {
        _renderedResults.push(entityRecord.resultRowHTML);
        return;
    }
    _resultRowHTML = [];
    _resultRowHTML.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'quest.aspx?id=" + entityRecord.id + "');\">");
    _resultRowHTML.push("<td style=text-align:left><a href=\"quest.aspx?id=" + entityRecord.id + "\" style=font-family\"Verdana,sans-serif\">");
    _resultRowHTML.push(entityRecord.name);
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td>");
    if (entityRecord.level != "-1") {
        _resultRowHTML.push(entityRecord.level);
        if (entityRecord.typeName != "") {
            _resultRowHTML.push("<div class=small>");
            _resultRowHTML.push(entityRecord.typeName);
            _resultRowHTML.push("</div>");
        }
    } else {
        _resultRowHTML.push("-");
    }
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td><span class=\"territory_");
    _resultRowHTML.push(entityRecord.sideID);
    _resultRowHTML.push("\">");
    _resultRowHTML.push(entityRecord.sideName);
    _resultRowHTML.push("</span></td>");
    if (!entityRecord.rewardsHTML) {
        entityRecord.rewardsHTML = getRewardsHTML(entityRecord);
    }
    _resultRowHTML.push("<td>");
    _resultRowHTML.push(entityRecord.rewardsHTML);
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td class=\"small r1\">");
    _resultRowHTML.push("<a href=#>");
    _resultRowHTML.push(entityRecord.categoryName);
    _resultRowHTML.push("</a>");
    _resultRowHTML.push("</td>");
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("wishlistDate") >= 0) {
        _resultRowHTML.push("<td class=small>");
        _resultRowHTML.push(entityRecord.wishlistDate);
        _resultRowHTML.push("</td>");
        _resultRowHTML.push("<td>");
        _resultRowHTML.push("<button class=\"smallButton\" onclick=\"removeFromWishList(event,this," + entityRecord.id + ",4," + entityRecord.wishlistContainerID + ");\"><span>Remove</span></button>");
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("factionLevel") >= 0) {
        _resultRowHTML.push("<td class=small>");
        _resultRowHTML.push(getLookupLabel("item_required_faction_level", entityRecord.factionLevel));
        _resultRowHTML.push("</td>");
    }
    if (_arrResultsFields[_currentDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestCells(entityRecord, _resultRowHTML);
    }
    entityRecord.resultRowHTML = _resultRowHTML.join("");
    _renderedResults.push(entityRecord.resultRowHTML);
}
function addResultRowItemSets(objTable, entityRecord) {
    if (entityRecord.resultRowHTML) {
        _renderedResults.push(entityRecord.resultRowHTML);
        return;
    }
    _resultRowHTML = [];
    _resultRowHTML.push("<tr class=\"rowItemList\" onmouseover=\"handleRowMouseOver(event)\" onmouseout=\"handleRowMouseOut(event)\" onclick=\"handleRowMouseClick(event,'itemset.aspx?id=" + entityRecord.id + "');\">");
    _resultRowHTML.push("<td style=\"text-align: left;\">");
    _resultRowHTML.push("<a href=\"itemset.aspx?id=" + entityRecord.id + "\" style=\"font-family: Verdana,sans-serif;\" class=\"r");
    _resultRowHTML.push(entityRecord.rarity);
    _resultRowHTML.push("\">");
    _resultRowHTML.push(entityRecord.name);
    _resultRowHTML.push("</a>");
    if (entityRecord.tag) {
        _resultRowHTML.push("<div class=\"small\">" + entityRecord.tag + "</div>");
    }
    _resultRowHTML.push("<div class=\"smallPad\"><a onclick=\"showPreview(event,'");
    _resultRowHTML.push(entityRecord.itemIDs);
    _resultRowHTML.push("','item');\">Preview on Character</a></div>");
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td>");
    if (entityRecord.level != "-1") {
        _resultRowHTML.push(entityRecord.level);
    } else {
        _resultRowHTML.push("-");
    }
    _resultRowHTML.push("</td>");
    if (!entityRecord.itemSetPiecesHTML) {
        entityRecord.itemSetPiecesHTML = getItemSetPiecesHTML(entityRecord);
    }
    _resultRowHTML.push("<td style=\"position: relative;\">");
    _resultRowHTML.push(entityRecord.itemSetPiecesHTML);
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td class=\"small r1\">");
    _resultRowHTML.push(entityRecord.typeName);
    _resultRowHTML.push("</td>");
    _resultRowHTML.push("<td class=\"small r1\">");
    _resultRowHTML.push(entityRecord.classNames);
    _resultRowHTML.push("</td>");
    entityRecord.resultRowHTML = _resultRowHTML.join("");
    _renderedResults.push(entityRecord.resultRowHTML);
}
function getReagentsHTML(entityRecord) {
    var reagentsHTML = [];
    var i;
    var itemID;
    var itemAmount;
    var objItem;
    var itemIconID;
    if (!entityRecord.reagents) {
        return "";
    }
    reagentsHTML.push("<div style=\"width: " + (44 * entityRecord.reagents.length) + "px;\">");
    for (i = 0; i < entityRecord.reagents.length; i++) {
        itemID = entityRecord.reagents[i].split("=")[0];
        itemAmount = entityRecord.reagents[i].split("=")[1];
        objItem = getItem(itemID);
        if (!objItem) {
            break;
        }
        reagentsHTML.push("<div class=iconmedium style=\"background-image: url(icons/m/" + objItem.iconID + ".gif); float: left;\">");
        reagentsHTML.push("<div class=tile>");
        if (itemAmount > 1) {
            reagentsHTML.push(getGlowHTML(itemAmount));
        }
        reagentsHTML.push("<div class=hover onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\" id=\"item_" + objItem.id + "_" + objItem.slot + "\"><a href=\"?item=" + objItem.id + "\"></a></div>");
        reagentsHTML.push("<div class=\"hilite\" style=\"display:none;\"></div>");
        reagentsHTML.push("</div>");
        reagentsHTML.push("</div>");
    }
    reagentsHTML.push("</div>");
    return reagentsHTML.join("");
}
function getItemSetPiecesHTML(entityRecord) {
    var itemSetPiecesHTML = [];
    if (!entityRecord.itemSetPieces) {
        return "";
    }
    itemSetPiecesHTML.push("<div style=\"position: relative;margin: 0pt auto; text-align: left; width: " + (44 * entityRecord.itemSetPieces.length) + "px;\">");
    for (i = 0; i < entityRecord.itemSetPieces.length; i++) {
        itemID = entityRecord.itemSetPieces[i];
        objItem = getItem(itemID);
        if (!objItem) {
            break;
        }
        itemSetPiecesHTML.push("<div class=iconmedium style=\"background-image: url(icons/m/" + objItem.iconID + ".gif); float: left;\">");
        itemSetPiecesHTML.push("<div class=tile>");
        itemSetPiecesHTML.push("<div class=hover onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\" id=\"item_" + objItem.id + "_" + objItem.slot + "\"><a href=\"?item=" + objItem.id + "\"></a></div>");
        itemSetPiecesHTML.push("<div class=\"hilite\" style=\"display:none;\"></div>");
        itemSetPiecesHTML.push("</div>");
        itemSetPiecesHTML.push("</div>");
    }
    itemSetPiecesHTML.push("</div>");
    return itemSetPiecesHTML.join("");
}
function getRewardsHTML(entityRecord) {
    var rewardsHTML = [];
    var i;
    var itemID;
    var itemAmount;
    var objItem;
    var itemIconID;
    if (entityRecord.rewardItemChoices) {
        rewardsHTML.push("<div style=\"margin: 0pt auto; text-align: left; width: " + (26 * entityRecord.rewardItemChoices.length) + "px;\">");
        rewardsHTML.push("<div style=\"position: relative; width: 1px;\">");
        rewardsHTML.push("<div class=r0 style=\"position: absolute; right: 2px; line-height: 26px; font-size: 11px; white-space: nowrap;\">Choose:</div>");
        rewardsHTML.push("</div>");
        for (i = 0; i < entityRecord.rewardItemChoices.length; i++) {
            itemID = entityRecord.rewardItemChoices[i].split("=")[0];
            itemAmount = entityRecord.rewardItemChoices[i].split("=")[1];
            objItem = getItem(itemID);
            var itemSlot = "";
            if (!objItem) {
                itemIconID = 3677;
            } else {
                itemIconID = objItem.iconID;
                itemSlot = objItem.slot;
                itemID = objItem.id;
            }
            rewardsHTML.push("<div class=iconsmall style=\"background-image: url(icons/s/" + itemIconID + ".gif); float: left;\">");
            rewardsHTML.push("<div class=tile>");
            if (itemAmount > 1) {
                rewardsHTML.push(getGlowHTML(itemAmount));
            }
            rewardsHTML.push("<div class=hover onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\" id=\"item_" + itemID + "_" + itemSlot + "\"><a href=\"?item=" + itemID + "\"></a></div>");
            rewardsHTML.push("<div class=\"hilite\" style=\"display:none;\"></div>");
            rewardsHTML.push("</div>");
            rewardsHTML.push("</div>");
        }
        rewardsHTML.push("</div>");
    }
    if (entityRecord.rewardItems) {
        rewardsHTML.push("<div class=\"clear\"></div>");
        rewardsHTML.push("<div style=\"margin: 0pt auto; text-align: left; width: " + (26 * entityRecord.rewardItems.length) + "px;\">");
        if (entityRecord.rewardItemChoices) {
            rewardsHTML.push("<div style=\"position: relative; width: 1px;\"><div class=r0 style=\"position: absolute; right: 2px; line-height: 26px; font-size: 11px; white-space: nowrap;\">Also:</div></div>");
        }
        for (i = 0; i < entityRecord.rewardItems.length; i++) {
            itemID = entityRecord.rewardItems[i].split("=")[0];
            itemAmount = entityRecord.rewardItems[i].split("=")[1];
            objItem = getItem(itemID);
            var itemSlot = "";
            if (!objItem) {
                itemIconID = 3677;
            } else {
                itemIconID = objItem.iconID;
                itemSlot = objItem.slot;
                itemID = objItem.id;
            }
            rewardsHTML.push("<div class=iconsmall style=\"background-image: url(icons/s/" + itemIconID + ".gif); float: left;\">");
            rewardsHTML.push("<div class=tile>");
            if (itemAmount > 1) {
                rewardsHTML.push(getGlowHTML(itemAmount));
            }
            rewardsHTML.push("<div class=hover onmouseout=\"handleSmallIconMouseOut(this);\" onmouseover=\"handleSmallIconMouseOver(this, event);\" id=\"item_" + itemID + "_" + itemSlot + "\"><a href=\"?item=" + itemID + "\"></a></div>");
            rewardsHTML.push("<div class=\"hilite\" style=\"display:none;\"></div>");
            rewardsHTML.push("</div>");
            rewardsHTML.push("</div>");
        }
        rewardsHTML.push("</div>");
    }
    if (entityRecord.rewardXP || entityRecord.rewardCoins) {
        if (rewardsHTML != "") {
            rewardsHTML.push("<div class=\"clear\"></div>");
        }
        rewardsHTML.push("<div style=\"padding: 4px;\">");
        if (entityRecord.rewardXP) {
            rewardsHTML.push(entityRecord.rewardXP + " XP");
        }
        if (entityRecord.rewardCoins) {
            var coinValue = parseInt(entityRecord.rewardCoins, null);
            if (coinValue > 0) {
                if (entityRecord.rewardXP) {
                    rewardsHTML.push(" + ");
                }
                if (coinValue >= 10000) {
                    rewardsHTML.push(" <span class=moneygold>" + Math.floor(coinValue / 10000) + "</span>");
                    coinValue %= 10000;
                }
                if (coinValue >= 100) {
                    rewardsHTML.push(" <span class=moneysilver>" + Math.floor(coinValue / 100) + "</span>");
                    coinValue %= 100;
                }
                if (coinValue >= 1) {
                    rewardsHTML.push(" <span class=moneycopper>" + coinValue + "</span>");
                }
            }
        }
        rewardsHTML.push("</div>");
    }
    return rewardsHTML.join("");
}
function getCoinsHTML(coinValue) {
    coinHTML = "";
    if (coinValue >= 10000) {
        coinHTML += " <span class=moneygold>" + Math.floor(coinValue / 10000) + "</span>";
        coinValue %= 10000;
    }
    if (coinValue >= 100) {
        coinHTML += " <span class=moneysilver>" + Math.floor(coinValue / 100) + "</span>";
        coinValue %= 100;
    }
    if (coinValue >= 1) {
        coinHTML += " <span class=moneycopper>" + coinValue + "</span>";
    }
    return coinHTML;
}
function addResultsHeaderSpells(itemRow, descriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "spellName_sort";
    itemCell.style.textAlign = "left";
    itemCell.colSpan = 2;
    if (_arrResultsFields[descriptorIndex].indexOf("reagents") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Reagents <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "reagents_Sort";
        itemCell.style.width = "27%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("level") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Level <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "level";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("school") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "School <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "schoolName";
        itemCell.style.width = "12%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("skill") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Skill <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "skill_Sort";
        itemCell.style.width = "14%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("latestDate") >= 0) {
        addLatestHeader(itemRow);
    }
}
function addResultsHeaderComments(entityRow, descriptorIndex) {
    if (!_arrResultsHasData[descriptorIndex]) {
        return;
    }
    headerCell = entityRow.insertCell(entityRow.cells.length);
    headerCell.className = "comment-table-header";
    headerCell.innerHTML = "Sort comments by: <a class=\"selected\" onclick=\"sortResults(event,'comment_date');\">Date</a>, <a onclick=\"sortResults(event,'rating_Sort')\">Rating</a>, <a onclick=\"sortResults(event,'userDisplayName')\">Username</a>";
    return;
}
function addResultsHeaderScreenshots(entityRow, descriptorIndex) {
    if (!_arrResultsHasData[descriptorIndex]) {
        return;
    }
    headerCell = entityRow.insertCell(entityRow.cells.length);
    headerCell.className = "comment-table-header";
    headerCell.colSpan = 4;
    headerCell.innerHTML = "Do you have an interesting screenshot of this? <button class=\"smallButton\" onclick=\"scrollToScreenshotBox();\"><span>Submit a Screenshot</span></button>";
    return;
}
function addResultsHeaderNPC(itemRow, descriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "name";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Level <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "levelRange_Sort";
    itemCell.style.width = "8%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Location <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "locationNames";
    itemCell.style.width = "25%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "React <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "reaction_Sort";
    itemCell.style.width = "9%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Type <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "typeName";
    itemCell.style.width = "9%";
    if (_arrResultsFields[descriptorIndex].indexOf("dropRate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Drop Rate <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "dropRate_Sort";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("wishlistDate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Date Added <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "wishlistDate_Sort";
        itemCell.style.width = "10%";
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[descriptorIndex].indexOf("latestDate") >= 0) {
        addLatestHeader(itemRow);
    }
}
function addResultsHeaderFactions(itemRow, resultsDescriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "name";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Group <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "groupName";
    itemCell.style.width = "30%";
}
function addResultsHeaderObjects(itemRow, resultsDescriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "name";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Location <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "locationNames";
    itemCell.style.width = "30%";
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("skillLevel") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Skill <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "skillLevel";
        itemCell.style.width = "10%";
    }
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Type <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "typeName";
    itemCell.style.width = "12%";
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("dropRate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Drop Rate <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "dropRate_Sort";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestHeader(itemRow);
    }
}
function addResultsHeaderCommentsPreview(oRow, resultsDescriptorIndex) {
    oCell = oRow.insertCell(oRow.cells.length);
    oCell.className = "cellItemListHeader";
    oCell.innerHTML = "Topic <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    oCell.ascColumnID = "name";
    oCell.style.textAlign = "left";
    oCell = oRow.insertCell(oRow.cells.length);
    oCell.className = "cellItemListHeader";
    oCell.innerHTML = "Preview <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    oCell.ascColumnID = "rating_Sort";
    oCell.style.textAlign = "left";
    oCell.style.width = "50%";
    oCell = oRow.insertCell(oRow.cells.length);
    oCell.className = "cellItemListHeader";
    oCell.innerHTML = "Date <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    oCell.ascColumnID = "comment_date";
    oCell.style.width = "16%";
}
function addResultsHeaderItemSets(itemRow, resultsDescriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "itemName_Sort";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Level <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "level";
    itemCell.style.width = "10%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Pieces <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "rewards_Sort";
    itemCell.style.width = "30%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Type <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "typeName";
    itemCell.style.width = "10%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Classes <img  src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "classNames_Sort";
    itemCell.style.width = "15%";
}
function addResultsHeaderQuests(itemRow, resultsDescriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "name";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Level <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "level";
    itemCell.style.width = "5%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Side <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "sideName";
    itemCell.style.width = "8%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Rewards <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "rewards_Sort";
    itemCell.style.width = "26%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Category <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "categoryName_Sort";
    itemCell.style.width = "15%";
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("wishlistDate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Date Added <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "wishlistDate_Sort";
        itemCell.style.width = "10%";
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("factionLevel") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Faction <img id=\"sortBtn_speed\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "factionLevel";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestHeader(itemRow);
    }
}
function addResultsHeaderZones(itemRow) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "name";
    itemCell.style.textAlign = "left";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Level <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "levelRange_Sort";
    itemCell.style.width = "10%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Territory <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "sideName";
    itemCell.style.width = "13%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Instance Type <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "locationType_Sort";
    itemCell.style.width = "22%";
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Category <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "categoryName_Sort";
    itemCell.style.width = "15%";
}
function addResultsHeaderItems(itemRow, resultsDescriptorIndex) {
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Name <img id=\"sortBtn_name\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "itemName_Sort";
    itemCell.style.textAlign = "left";
    itemCell.colSpan = 2;
    itemCell = itemRow.insertCell(itemRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Level <img id=\"sortBtn_level\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "level";
    itemCell.style.width = "8%";
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("dps") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "DPS <img id=\"sortBtn_dps\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "dps";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("speed") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Speed <img id=\"sortBtn_speed\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "speed";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("armor") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Armor <img id=\"sortBtn_armour\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "armour";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("slot") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Slot <img  id=\"sortBtn_slots\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "slots";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("source") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Source <img  id=\"sortBtn_source\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "source";
        itemCell.style.width = "12%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("type") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Type <img  id=\"sortBtn_type\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "type_Sort";
        itemCell.style.width = "14%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("factionLevel") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Standing <img id=\"sortBtn_speed\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "factionLevel";
        itemCell.style.width = "12%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("disenchantChance") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Chance <img  id=\"sortBtn_type\" src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "disenchantChance_Sort";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("dropRate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Drop Rate <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "dropRate_Sort";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("price") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Price <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "price_Sort";
        itemCell.style.width = "10%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("wishlistDate") >= 0) {
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "Date Added <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
        itemCell.ascColumnID = "wishlistDate_Sort";
        itemCell.style.width = "10%";
        itemCell = itemRow.insertCell(itemRow.cells.length);
        itemCell.className = "cellItemListHeader";
        itemCell.innerHTML = "";
        itemCell.style.width = "8%";
    }
    if (_arrResultsFields[resultsDescriptorIndex].indexOf("latestDate") >= 0) {
        addLatestHeader(itemRow);
    }
}
function addLatestHeader(oRow) {
    itemCell = oRow.insertCell(oRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Patch";
    itemCell.style.width = "5%";
    itemCell = oRow.insertCell(oRow.cells.length);
    itemCell.className = "cellItemListHeader";
    itemCell.innerHTML = "Date Added <img src=templates/wowdb/images/sort_asc_new.gif class=sortBtn>";
    itemCell.ascColumnID = "latestDate_Sort";
    itemCell.style.width = "13%";
}
function createResultsTable(resultsDescriptorIndex) {
    if (_$("tableResultsList_" + resultsDescriptorIndex)) {
        return;
    }
    var itemTable;
    var itemRow;
    var itemCell;
    var objContainer = _$("divItemListContainer");
    var pagingDIV;
    var pagingDIVBottom;
    var tableDescriptor = _arrResultsDataDescriptors[resultsDescriptorIndex];
    var myDiv = document.createElement("div");
    myDiv.id = "tab_" + resultsDescriptorIndex;
    itemTable = document.createElement("TABLE");
    itemTable.className = "tableResultsList";
    itemTable.id = "tableResultsList_" + resultsDescriptorIndex;
    itemTable.cellSpacing = "0";
    myDiv.appendChild(itemTable);
    if (resultsDescriptorIndex != _currentDescriptorIndex) {
        itemTable.style.display = "none";
    }
    if (!_arrResultsHasData[resultsDescriptorIndex]) {
        var newRow = itemTable.insertRow( - 1);
        var objCell = newRow.insertCell( - 1);
        objCell.className = "no-data";
        if (_arrResultsNoDataMessage[resultsDescriptorIndex] != null) {
            var noDataMessage = _arrResultsNoDataMessage[resultsDescriptorIndex];
            if (noDataMessage.indexOf("|") > 0) {
                noDataMessage = noDataMessage.split("|");
                if (_userID == "") {
                    objCell.innerHTML = noDataMessage[0];
                } else {
                    objCell.innerHTML = noDataMessage[1];
                }
            } else {
                objCell.innerHTML = noDataMessage;
            }
        }
        objContainer.appendChild(itemTable);
        return;
    }
    var thead = itemTable.createTHead();
    thead.className = "theadResultsList";
    itemRow = thead.insertRow( - 1);
    if (tableDescriptor == "comments") {
        addResultsHeaderComments(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "commentsPreview") {
        addResultsHeaderCommentsPreview(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "screenshots") {
        addResultsHeaderScreenshots(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "npcs") {
        addResultsHeaderNPC(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "locations") {
        addResultsHeaderZones(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "quests") {
        addResultsHeaderQuests(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "items") {
        addResultsHeaderItems(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "itemsets") {
        addResultsHeaderItemSets(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "objects") {
        addResultsHeaderObjects(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "spells") {
        addResultsHeaderSpells(itemRow, resultsDescriptorIndex);
    } else if (tableDescriptor == "factions") {
        addResultsHeaderFactions(itemRow, resultsDescriptorIndex);
    }
    objContainer.appendChild(myDiv);
}
function closeWindow(e, windowObj) {
    var targetObj;
    if (windowObj) {
        targetObj = windowObj;
    } else {
        targetObj = getEventTarget(e);
        targetObj = getTargetWindow(targetObj);
    }
    if (_lastProfileOpened == targetObj) {
        _lastProfileOpened = null;
    }
    var targetParent = targetObj.parentNode;
    targetParent.removeChild(targetObj);
}
function setBrowseDescription() {}
function getBrowseKey() {
    return getQueryStringParam("browse");
}
function getFilterKey() {
    return getQueryStringParam("filters");
}
function toggleFilterForm(objButton) {
    var searchFilterForm = _$("searchFilterForm");
    if (!objButton) {
        objButton = _$("btnToggleFilter");
    }
    if (!searchFilterForm) {
        return;
    }
    if (searchFilterForm.style.display == "none" || !objButton) {
        searchFilterForm.style.display = "block";
        if (objButton) objButton.childNodes[0].innerHTML = "Hide Filter Form";
    } else {
        searchFilterForm.style.display = "none";
        if (objButton) objButton.childNodes[0].innerHTML = "Create a Filter";
    }
}
function saveSearch(browseKey, filterKey, e) {
    var formString = "?search_browse_key=" + browseKey;
    formString += "&search_filter_key=" + filterKey;
    var searchName = _$("save_search_name").value;
    if (searchName == "") {
        return;
    }
    formString += "&search_name=" + searchName;
    formString += "&ajax_action=save_search";
    closeAlert();
    jx.load("ajaxSaveSearch.aspx" + formString, handleSaveSearchPostback, 'text', 'post');
    function handleSaveSearchPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            displayAlert(postResult[1]);
            setUserBookmarks();
        } else {
            displayAlert(postResult[1]);
        }
    }
}
function saveSearch_prompt(btn) {
    var browseKey = getBrowseKey();
    var filterKey = getFilterKey();
    displayAlert("Please enter a name for this bookmark below.", "Bookmark this Search", "Name:^save_search_name^^^20^20^<small>Enter up to 20 characters</small>", "saveSearch('" + browseKey + "','" + filterKey + "',event)^Save|closeAlert()^Cancel", null, null, null, true);
}
function getItemQualityColor(itemID) {
    var myItem = getItem(itemID);
    var rarityID = parseInt(myItem.description.substr(myItem.description.indexOf("class=r") + 7, 1), null);
    switch (rarityID) {
    case 0:
        return "ff9d9d9d";
    case 1:
        return "ffffffff";
    case 2:
        return "ff1eff00";
    case 3:
        return "ff0070dd";
    case 4:
        return "ffa335ee";
    case 5:
        return "ffff8000";
    case 6:
        return "ffe5cc80";
    }
}
function getSharingLink_Item() {
    var linkColor = getItemQualityColor(_detailsEntityID);
    var inGameLink = "/run DEFAULT_CHAT_FRAME:AddMessage(&quot;Shift click this link to put it into the default chat window: \\124c" + linkColor + "\\124Hitem:" + _detailsEntityID + ":0:0:0:0:0:0:0\\124h[" + _detailsEntityName.replace(/"/g, "&quot;") + "]\\124h\\124r&quot;)";
    var wowdbLinkWithIcon = "[item]" + _detailsEntityID + "[/item]";
    var wowdbLinkNoIcon = "[itemlink]" + _detailsEntityID + "[/itemlink]";
    displayAlert("Use the following links to share this item:", "Item Link", "In-Game Link:^item_link^" + inGameLink + "^^42^2048^<small>Copy and paste this command to an in-game chat window.</small>^onfocus='select();' onclick='select();'^style='width:300px;'|WOWDB Tooltip:<br><small>(with icon)</small>^item_link_wowdb_icon^" + wowdbLinkWithIcon + "^^42^42^<small>Copy and paste this to a WOWDB comment box.</small>^onfocus='select();' onclick='select();'^style='width:300px;'|WOWDB Tooltip:<br><small>(without icon)</small>:^item_link_wowdb^" + wowdbLinkNoIcon + "^^42^42^<small>Copy and paste this to a WOWDB comment box.</small>^onfocus='select();' onclick='select();'^style='width:300px;'", null, null, null, null, true, 480);
}
function getSharingLink_Quest() {
    var level = _detailsQuestLevel;
    if (!level) {
        return;
    }
    var inGameLink = "/run DEFAULT_CHAT_FRAME:AddMessage(&quot;Shift click this link to put it into the default chat window: \\124Hquest:" + _detailsEntityID + ":" + level + "\\124h[" + _detailsEntityName.replace(/"/g, "&quot;") + "]\\124h&quot;)";
    var wowdbLink = "[quest]" + _detailsEntityID + "[/quest]";
    displayAlert("Use the following links to share this quest:", "Quest Link", "In-Game Link:^quest_link^" + inGameLink + "^^42^2048^<small>Copy and paste this command to an in-game chat window.</small>^onfocus='select();' onclick='select();'^style='width:300px;'|WOWDB Link:^quest_link_wowdb^" + wowdbLink + "^^42^42^<small>Copy and paste this to a WOWDB comment box.</small>^onfocus='select();' onclick='select();'^style='width:300px;'", null, null, null, null, true, 480);
}
function getSharingLink_Spell() {
    var wowdbLinkWithIcon = "[spell]" + _detailsEntityID + "[/spell]";
    var wowdbLinkNoIcon = "[spelllink]" + _detailsEntityID + "[/spelllink]";
    displayAlert("Use the following links to share this spell:", "Spell Link", "WOWDB Tooltip:<br><small>(with icon)</small>^spell_link_wowdb_icon^" + wowdbLinkWithIcon + "^^42^42^<small>Copy and paste this to a WOWDB comment box.</small>^onfocus='select();' onclick='select();'^style='width:300px;'|WOWDB Tooltip:<br><small>(without icon)</small>:^spell_link_wowdb^" + wowdbLinkNoIcon + "^^42^42^<small>Copy and paste this to a WOWDB comment box.</small>^onfocus='select();' onclick='select();'^style='width:300px;'", null, null, null, null, true, 480);
}
function initFilterForm() {
    var objFilterForm = _$("searchFilterForm");
    if (!objFilterForm) {
        return;
    }
    var objTbl = _$("tableAdditionalFilters");
    var colFc = getElementsByClassName("fc", "DIV", objFilterForm);
    var eventsToHandle = [];
    for (var i = 0; i < colFc.length; i++) {
        if (colFc[i].id) {
            var lookupDef = colFc[i].id.split(".");
            var lookupOnChange = null;
            if (colFc[i].getAttribute("curseonchange")) {
                lookupOnChange = colFc[i].getAttribute("curseonchange");
            }
            var newSelectBox = getLookupSelectBox(lookupDef[1], lookupDef[2], colFc[i], false, lookupOnChange);
            if (lookupOnChange) {
                eventsToHandle.push(newSelectBox);
            }
        }
    }
    var filtersParam = getQueryStringParam("filters");
    var baseFilterHTML = "";
    if (filtersParam != "") {
        baseFilterHTML = "<button onclick=\"hideTooltip();saveSearch_prompt(this)\" onmouseover=\"showTooltip('Click here to bookmark this search for later retrieval.',event,'r');\" onmouseout=\"hideTooltip();\"><span>Bookmark this Search</span></button>&nbsp";
    }
    if (filtersParam == "") {
        _$("searchFilterForm").style.display = "none";
        _$("divBrowsePathRight").innerHTML = baseFilterHTML + "<button id=\"btnToggleFilter\" onclick=\"hideTooltip();toggleFilterForm(this)\" class=\"smallButton\" onmouseover=\"showTooltip('Click here to provide additional criteria for your search.',event,'r');\" onmouseout=\"hideTooltip();\"><span>Create a Filter</span></button>";
        return;
    }
    _$("searchFilterForm").style.display = "block";
    _$("divBrowsePathRight").innerHTML = baseFilterHTML + "<button id=\"btnToggleFilter\" onclick=\"toggleFilterForm(this)\" class=\"smallButton\"><span>Hide Filter Form</span></button>";
    resetAdditionalFilters();
    var filterArray = filtersParam.split(";");
    for (var i = 0; i < filterArray.length; i++) {
        var currentFilter = filterArray[i].split("=");
        var currentFilterName = currentFilter[0];
        var currentFilterValue = currentFilter[1];
        setFilterElement(currentFilterName, currentFilterValue);
    }
    for (var i = 0; i < eventsToHandle.length; i++) {
        eventsToHandle[i].onchange();
    }
}
function resetAdditionalFilters() {
    var objTbl = _$("tableAdditionalFilters");
    if (!objTbl) {
        return;
    }
    var oSel = objTbl.getElementsByTagName("select")[0];
    oSel.selectedIndex = 0;
}
function setFilterElement(name, values) {
    var objFilterForm = _$("searchFilterForm");
    var objElements = objFilterForm.elements;
    var elementFound = false;
    for (var i = 0; i < objElements.length; i++) {
        if (objElements[i].name == name) {
            setFilterElementValue(objElements[i], values);
            elementFound = true;
            break;
        }
    }
    if (elementFound) {
        return;
    }
    var objTbl = _$("tableAdditionalFilters");
    var colSel = getElementsByClassName("filters-add", "select", objTbl);
    var oLastSel = colSel[colSel.length - 1];
    if (oLastSel.selectedIndex > 0) {
        addSearchFilter();
        colSel = getElementsByClassName("filters-add", "select", objTbl);
        oLastSel = colSel[colSel.length - 1];
    }
    var colOptions = oLastSel.options;
    for (var i = 0; i < colOptions.length; i++) {
        var filterName = colOptions[i].value;
        var filterNameArray = filterName.split(",");
        if (filterNameArray[0] == name || filterNameArray[1] == name) {
            var index = 0;
            if (filterNameArray[1] == name) {
                index = 1;
            }
            oLastSel.selectedIndex = i;
            setAdditionalFilterValue(oLastSel, values, index);
            break;
        }
    }
}
function setAdditionalFilterValue(parentSel, values, index) {
    addSearchFilterInput(parentSel);
    var inputContainer = parentSel.parentNode.parentNode.cells[1];
    if (inputContainer.childNodes[0].tagName == "SELECT") {
        setFilterElementValue(inputContainer.childNodes[0], values);
        return;
    }
    var input = inputContainer.getElementsByTagName("input")[index];
    input.value = values;
}
function setFilterElementValue(objElement, values) {
    if (objElement.tagName == "INPUT") {
        if (objElement.type == "checkbox") {
            if (objElement.value == values) {
                objElement.checked = true;
            }
        } else {
            objElement.value = values;
        }
    } else if (objElement.tagName == "SELECT") {
        var valuesArray = values.split(",");
        var objOptions = objElement.options;
        for (var i = 0; i < valuesArray.length; i++) {
            for (var j = 0; j < objOptions.length; j++) {
                if (objOptions[j].value == valuesArray[i]) {
                    objOptions[j].selected = true;
                    break;
                }
            }
        }
    }
}
function handleResultsLoading() {
    initFilterForm();
    setModMenu();
    var userPerPage = getCookie("_resultsPerPage");
    var inputSearchPerPage;
    var i;
    _currentDescriptorIndex = 0;
    var initialIndex = 0;
    if (self.location.hash != "") {
        initialIndex = getViewState(0);
        if (!initialIndex || !IsNumeric(initialIndex)) {
            initialIndex = 0;
        }
    }
    if (IsNumeric(initialIndex) && initialIndex < _arrResultsDataDescriptors.length) {
        _currentDescriptorIndex = parseInt(initialIndex, null);
    }
    if (_arrResultsDataDescriptors.length == 0 && _$("searchResultsSummary")) {
        var noResultsDiv = document.createElement("div");
        var noResultsMessage = "";
        if (_searchText) {
            noResultsMessage = "No search results were found that match <i>" + _searchText + "</i>";
        } else {
            noResultsMessage = "No search results were found";
        }
        noResultsDiv.innerHTML = "<h1>" + noResultsMessage + "</h1>Here are some suggestions for finding what you are looking for: <ul><li><div>Make sure all words are spelled correctly.</div></li><li><div>Try different keywords.</div></li><li><div>Try more general keywords.</div></li><li><div>Try fewer keywords.</div></li></ul>";
        noResultsDiv.className = "text";
        _$("divMainContents").appendChild(noResultsDiv);
        if (_browseDescription != null && _browseDescription != "") {}
        return;
    }
    if (_searchDescription != null && _searchDescription != "") {
        _$("searchResultsSummary").innerHTML = _searchDescription;
        _$("searchResultsSummary").style.display = "block";
    }
    if (_browseDescription != null && _browseDescription != "") {} else {
        _$("divBrowsePathLeft").style.display = "none";
        if (_$("divFilterButtonContainer")) {
            _$("divFilterButtonContainer").style.display = "none";
        }
    }
    if (_searchText != null) {
        _$("titleSearchInput").value = _searchText;
    }
    _$("divItemListContainer").style.display = "block";
    if (_$("resultsContentPanel")) {
        _$("resultsContentPanel").style.display = "block";
    }
    if (_$("divFilterButtonContainer")) {
        _$("divFilterButtonContainer").innerHTML = "<button id=\"filterButton\"></button>";
    }
    if (userPerPage) {
        _resultsPerPage = userPerPage;
        inputSearchPerPage = _$("inputSearchPerPage");
        for (i = 0; i < inputSearchPerPage.options.length; i++) {
            if (inputSearchPerPage.options[i].value == _resultsPerPage) {
                inputSearchPerPage.selectedIndex = i;
                break;
            }
        }
    }
    createResultsTabs(_currentDescriptorIndex);
    for (i = 0; i < _arrResultsDataDescriptors.length; i++) {
        createResultsTable(i);
    }
    objContainer = _$("divItemListContainer");
    pagingDIVBottom = document.createElement("DIV");
    pagingDIVBottom.id = "divItemListPagingBottom";
    pagingDIVBottom.innerHTML = "&nbsp;";
    objContainer.appendChild(pagingDIVBottom);
    if (_arrResultsDataDescriptors[_currentDescriptorIndex] && _arrResultsDataDescriptors[_currentDescriptorIndex].length > 100) {
        setTimeout(handleInitialLoad, 0);
    } else if (_arrResultsDataDescriptors[_currentDescriptorIndex]) {
        handleInitialLoad();
    }
    _addEventListener(document, "click", handleResultsClick);
}
function toggleSearchFilters() {
    if (_arrResultsHasData[_currentDescriptorIndex]) {
        _$("divSearchResultsCount").style.display = "block";
        if (_arrResultsDataCollection[_currentDescriptorIndex] && _arrResultsDataCollection[_currentDescriptorIndex].length > 1) {
            _$("divItemListPagingBottom").style.display = "block";
            _$("divSearchFilters").style.display = "block";
            _$("divRefineContainer").style.visibility = "visible";
            if (_arrResultsDataCollection[_currentDescriptorIndex][0].name) {} else {
                _$("divRefineContainer").style.visibility = "hidden";
            }
        } else {}
    } else {
        _$("divItemListPaging").innerHTML = "";
        _$("divItemListPagingBottom").innerHTML = "";
        _$("divSearchResultsCount").style.display = "none";
        _$("divItemListPagingBottom").style.display = "none";
        _$("divSearchFilters").style.display = "none";
    }
}
function handleInitialLoad() {
    if (processViewState()) {
        return;
    }
    _arrFilteredResultsData = _arrResultsDataCollection[_currentDescriptorIndex];
    _arrOriginalResultsData = _arrResultsDataCollection[_currentDescriptorIndex];
    if (_arrResultsDefaultSort[_currentDescriptorIndex]) {
        sortResults(null, _arrResultsDefaultSort[_currentDescriptorIndex]);
    } else if (_arrCurrentSortStore[_currentDescriptorIndex]) {
        sortResults(null, _arrCurrentSortStore[_currentDescriptorIndex]);
    } else {
        sortResults(null, "name");
    }
}
function setCurrentContributeTab(newTabIndex) {
    setCurrentTab(_$("tabGroupContribute"), newTabIndex);
    _currentDescriptorIndex = newTabIndex;
}
function setCurrentResultsTab(newTabIndex) {
    setCurrentTab(_$("tabsContainer"), newTabIndex);
    _currentDescriptorIndex = newTabIndex;
}
function setCurrentTab(tabsContainer, newTabIndex) {
    var tabsCollection = tabsContainer.getElementsByTagName("A");
    if (tabsCollection.length == 0) {
        return;
    }
    for (var i = 0; i < tabsCollection.length; i++) {
        if (i == newTabIndex) {
            tabsCollection[i].className = "selected";
            if (tabsCollection[i].ascRelatedContainerID && _$(tabsCollection[i].ascRelatedContainerID)) {
                showElement(tabsCollection[i].ascRelatedContainerID);
            }
        } else {
            tabsCollection[i].className = "";
            if (tabsCollection[i].ascRelatedContainerID && _$(tabsCollection[i].ascRelatedContainerID)) {
                _$(tabsCollection[i].ascRelatedContainerID).style.display = "none";
            }
        }
    }
}
function showElement(elementID) {
    var element = _$(elementID);
    if (element.tagName == "TD") {
        if (isIE) {
            element.style.display = "inline-block";
        } else {
            element.style.display = "table-cell";
        }
    } else if (element.tagName == "TABLE") {
        if (isIE) {
            element.style.display = "inline-block";
            if (isIE6) {
                element.style.width = "100%";
            }
        } else {
            element.style.display = "table";
        }
    } else {
        element.style.display = "block";
    }
}
function toggleTab(objTab, tabIndex) {
    var tabContainer = objTab.parentNode;
    while (tabContainer.className != "tabs-container") {
        tabContainer = tabContainer.parentNode;
    }
    setCurrentTab(tabContainer, tabIndex);
}
function createLinkGroup(objTabContainer, tabGroupName, tabLabels, tabValues, tabSelectedIndex, onClick) {
    var containerDiv = document.createElement("DIV");
    var elClassName = "selected";
    var resultsDescriptor;
    var resultsDescriptorLabel;
    var resultsCount;
    var arrTabLabels = tabLabels.split("|");
    var arrTabValues = tabValues.split("|");
    for (var i = 0; i < arrTabLabels.length; i++) {
        if (i == tabSelectedIndex) {
            elClassName = "selected";
        } else {
            elClassName = "";
        }
        var currentTabHTML = "";
        if (i > 0) {
            currentTabHTML = " | ";
        }
        currentTabHTML += "<a ";
        if (onClick) {
            currentTabHTML += "onclick=\"" + onClick + "(this," + i + ",'" + arrTabValues[i] + "')\"";
        }
        currentTabHTML += " class=\"" + elClassName + "\">" + arrTabLabels[i] + "</a>";
        containerDiv.innerHTML += currentTabHTML;
    }
    objTabContainer.appendChild(containerDiv);
    objTabContainer.style.display = "inline";
}
function createTabGroup(objTabContainer, tabGroupName, tabLabels, tabValues, tabSelectedIndex, onClick) {
    var containerDiv = document.createElement("DIV");
    var elUL = document.createElement("UL");
    elUL.className = "tabs";
    var elClassName = "selected";
    var resultsDescriptor;
    var resultsDescriptorLabel;
    var resultsCount;
    var arrTabLabels = tabLabels.split("|");
    var arrTabValues = tabValues.split("|");
    for (var i = 0; i < arrTabLabels.length; i++) {
        if (i == tabSelectedIndex) {
            elClassName = "selected";
        } else {
            elClassName = "";
        }
        var eLI = document.createElement("LI");
        var currentTabHTML = "";
        currentTabHTML = "<a ";
        if (onClick) {
            currentTabHTML += "onclick=\"" + onClick + "(this," + i + ",'" + arrTabValues[i] + "')\"";
        }
        currentTabHTML += " class=\"" + elClassName + "\"><div id=\"" + tabGroupName + "_" + i + "\">" + arrTabLabels[i] + "</div><b>" + arrTabLabels[i] + "</b></a></li>";
        eLI.innerHTML = currentTabHTML;
        eLI.firstChild.ascRelatedContainerID = arrTabValues[i];
        elUL.appendChild(eLI);
    }
    containerDiv.appendChild(elUL);
    objTabContainer.appendChild(containerDiv);
    objTabContainer.style.display = "block";
}
function createResultsTabs() {
    if (_arrResultsDataDescriptors.length == 1 && _searchText == null && !_showTabs) {}
    var tabValues = "";
    var tabLabels = "";
    for (var i = 0; i < _arrResultsDataDescriptors.length; i++) {
        resultsDescriptorLabel = _arrResultsDataDescriptorLabels[i];
        resultsCount = _arrResultsDataCollection[i].length;
        tabLabels = tabLabels + "|" + resultsDescriptorLabel;
        if (resultsCount >= 1) {
            tabLabels += " (" + resultsCount + ")";
        }
        tabValues = tabValues + "|" + "tableResultsList_" + i;
    }
    tabValues = tabValues.substring(1);
    tabLabels = tabLabels.substring(1);
    createTabGroup(_$("tabsContainer"), "resultsTab", tabLabels, tabValues, _currentDescriptorIndex);
}
function handleResultsChange(e) {
    var myEvent = getEventObject(e);
    var targetObj = getEventTarget(myEvent);
    if (targetObj.id == "inputSearchPerPage") {
        if (targetObj.options[targetObj.selectedIndex].value != _resultsPerPage) {
            _resultsPerPage = targetObj.options[targetObj.selectedIndex].value;
            setCookie("_resultsPerPage", _resultsPerPage, 1);
            renderResults(0);
            return true;
        }
    }
    return true;
}
function handleGlobalMouseUp(e) {
    _isDragging = false;
    _selectedObj = null;
}
function handleGlobalMouseDown(e) {
    var targetObj = getEventTarget(e);
    if (targetObj.className.indexOf("moveWidget") == 0) {
        _isDragging = true;
        var targetWindow = getTargetWindow(targetObj);
        offsetx = isIE ? event.clientX: e.clientX;
        offsety = isIE ? event.clientY: e.clientY;
        nowX = parseInt(targetWindow.offsetLeft, null);
        nowY = parseInt(targetWindow.offsetTop, null);
        document.body.style.cursor = targetObj.style.cursor;
        bringWindowToFront(targetWindow);
        _selectedObj = targetObj;
        return true;
    }
    targetObj = null;
    _selectedObj = null;
    return true;
}
function handleGlobalClick(e) {
    var characterID;
    var searchText;
    var charcterProfile;
    var viewportHeight;
    var viewportWidth;
    var targetObj = getEventTarget(e);
    var myEvent = getEventObject(e);
    if (myEvent.which && e.which != 1) {
        return;
    }
    if (myEvent.button && myEvent.button != 0) {
        return;
    }
    if (!targetObj.className && !targetObj.id) {
        return true;
    }
    if (targetObj.className == "imageButtonWindowClose") {
        closeWindow(e);
        return false;
    }
    if (targetObj.id == "itemBrowserIconHighlight") {
        self.location = "?item=" + currentItem.id.split("_")[1];
        return;
    }
    if (targetObj.id.indexOf("characterLink_") >= 0) {
        characterID = targetObj.id.replace("characterLink_", "");
        openProfile(characterID);
        charcterProfile = _$("profile" + characterID);
        if (_lastProfileOpened) {
            var lastPosition = getPosition(_lastProfileOpened);
            charcterProfile.style.left = lastPosition.x + 20 + "px";
            charcterProfile.style.top = lastPosition.y + 20 + "px";
        } else {
            viewportHeight = Client.viewportHeight();
            viewportWidth = Client.viewportWidth();
            charcterProfile.style.left = ((viewportWidth / 2) - (charcterProfile.offsetWidth / 2)) + "px";
            charcterProfile.style.top = ((viewportHeight / 2) - (charcterProfile.offsetHeight / 2)) + "px";
        }
        _lastProfileOpened = charcterProfile;
        return false;
    } else if (targetObj.id == "titleSearchButton") {}
    bringWindowToFront(getTargetWindow(targetObj));
    return true;
}
function handleGlobalMouseMove(e) {
    if (_$("itemPanel") && _$("itemPanel").style.display == "block") {
        var num = 1;
        if (_$("itemPanel1").style.display == "block") num++;
        if (_$("itemPanel2").style.display == "block") num++;
        positionItemPanel(e, num);
    }
    if (!_selectedObj) {
        return;
    }
    var mousePos = mouseCoords(e);
    var mouseX = mousePos.x;
    var mouseY = mousePos.y;
    if (_selectedObj === null || _isDragging == false) {
        return false;
    }
    var targetObj = _selectedObj;
    var newTop = isIE ? nowY + event.clientY - offsety: nowY + e.clientY - offsety;
    var newLeft = isIE ? nowX + event.clientX - offsetx: nowX + e.clientX - offsetx;
    if (newTop < 0) {
        newTop = 0;
    }
    if (newLeft < 0) {
        newLeft = 0;
    }
    if (targetObj.className.indexOf("moveWidget") == 0) {
        var targetWindow = getTargetWindow(targetObj);
        targetWindow.style.width = targetWindow.offsetWidth + "px";
        targetWindow.style.height = targetWindow.offsetHeight + "px";
        targetWindow.style.position = "absolute";
        targetWindow.style.left = newLeft + "px";
        targetWindow.style.top = newTop + "px";
        return false;
    } else if (targetObj.className.indexOf("characterItem") == 0) {
        targetObj.style.left = newLeft + "px";
        targetObj.style.top = newTop + "px";
        return false;
    }
    return false;
}
function setPinItemButton() {
    var pincontainer = _$("divPinItemContainer");
    if (!pincontainer) {
        return;
    }
    var it = getItem(_detailsEntityID);
    if (!it.slot) {
        return;
    }
    var slotName = getLookupLabel("item_slot_id", it.slot);
    var buttonEvents = " onclick=\"hideTooltip();pinItem();\" onmouseover=\"showTooltip('Click here to use this item for side-by-side tooltip comparisons in the " + slotName + " slot.',event,'r')\" onmouseout=\"hideTooltip();\"";
    if (getCookie("Settings.PinnedItem.item_" + it.slot) != null) {
        if (getCookie("Settings.PinnedItem.item_" + it.slot).indexOf(_detailsEntityID) != -1) {
            pincontainer.innerHTML = "<button class=\"smallButton\" onclick=\"unpinItem()\"><span>Unpin Item</span></button>";
        } else {
            var txt = "<button class=\"smallButton\"" + buttonEvents + "><span>Pin this Item</span></button>";
            if ((it.slot == 11) || (it.slot == 12) || (it.slot == 13)) txt += "<div style=\"padding-top: 4px;\"><small>in</small>&nbsp;<select id=\"pinSideSelect\"><option value=\"0\">Main hand</option><option value=\"1\">Off hand</option></select></div>";
            pincontainer.innerHTML = txt;
        }
    } else {
        var txt = "<button class=\"smallButton\"" + buttonEvents + "><span>Pin this Item</span></button>";
        if ((it.slot == 11) || (it.slot == 12) || (it.slot == 13)) txt += "<div><small>in</small> &nbsp;<select id=\"pinSideSelect\"><option value=\"0\">Main hand</option><option value=\"1\">Off hand</option></select></div>";
        pincontainer.innerHTML = txt;
    }
}
function unpinItem(itemID) {
    if (!itemID) {
        itemID = _detailsEntityID;
    }
    var it = getItem(itemID);
    setCookie("Settings.PinnedItem.item_" + it.slot, getCookie("Settings.PinnedItem.item_" + it.slot).replace(itemID, "").replace("|", ""));
    if (_detailsEntityID) {
        setPinItemButton();
    }
}
function pinItem() {
    var it = getItem(_detailsEntityID);
    if ((it.slot == 11) || (it.slot == 12) || (it.slot == 13)) {
        var ck = getCookie("Settings.PinnedItem.item_" + it.slot);
        if (ck == null) setCookie("Settings.PinnedItem.item_" + it.slot, _detailsEntityID);
        else {
            var ids = ck.split("|");
            if ((ids[1] == "") || (ids[1] == null)) ids[1] = _detailsEntityID;
            else if (_$("pinSideSelect").value == "0") {
                ids[0] = _detailsEntityID;
            } else if (_$("pinSideSelect").value == "1") {
                ids[1] = _detailsEntityID;
            }
            setCookie("Settings.PinnedItem.item_" + it.slot, ids.join("|"));
        }
    } else setCookie("Settings.PinnedItem.item_" + it.slot, _detailsEntityID);
    var slot = it.slot;
    if (slot == "17") {
        setCookie("Settings.PinnedItem.item_13", null);
        setCookie("Settings.PinnedItem.item_14", null);
        setCookie("Settings.PinnedItem.item_22", null);
        setCookie("Settings.PinnedItem.item_21", null);
        setCookie("Settings.PinnedItem.item_23", null);
    } else if (slot == "21") {
        setCookie("Settings.PinnedItem.item_17", null);
        if (getCookie("Settings.PinnedItem.item_13") != null) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            vals[0] = "";
            if ((vals[1] == null) || (vals[1] == "")) setCookie("Settings.PinnedItem.item_13", null);
            else setCookie("Settings.PinnedItem.item_13", vals.join("|"));
        }
    } else if (slot == "22") {
        setCookie("Settings.PinnedItem.item_23", null);
        setCookie("Settings.PinnedItem.item_14", null);
        if (getCookie("Settings.PinnedItem.item_13") != null) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            vals[1] = "";
            if ((vals[0] == null) || (vals[0] == "")) setCookie("Settings.PinnedItem.item_13", null);
            else setCookie("Settings.PinnedItem.item_13", vals.join("|"));
        }
    } else if (slot == "14") {
        setCookie("Settings.PinnedItem.item_23", null);
        setCookie("Settings.PinnedItem.item_22", null);
        if (getCookie("Settings.PinnedItem.item_13") != null) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            vals[1] = "";
            if ((vals[0] == null) || (vals[0] == "")) setCookie("Settings.PinnedItem.item_13", null);
            else setCookie("Settings.PinnedItem.item_13", vals.join("|"));
        }
    } else if (slot == "23") {
        setCookie("Settings.PinnedItem.item_22", null);
        setCookie("Settings.PinnedItem.item_14", null);
        if (getCookie("Settings.PinnedItem.item_13") != null) {
            var vals = getCookie("Settings.PinnedItem.item_13").split("|");
            vals[1] = "";
            if ((vals[0] == null) || (vals[0] == "")) setCookie("Settings.PinnedItem.item_13", null);
            else setCookie("Settings.PinnedItem.item_13", vals.join("|"));
        }
    } else if (slot == "13") {
        setCookie("Settings.PinnedItem.item_17", null);
        if (_$("pinSideSelect").value == "0") {
            setCookie("Settings.PinnedItem.item_21", null);
        } else {
            setCookie("Settings.PinnedItem.item_22", null);
            setCookie("Settings.PinnedItem.item_14", null);
            setCookie("Settings.PinnedItem.item_23", null);
        }
    } else if (slot == "5") {
        setCookie("Settings.PinnedItem.item_20", null);
    } else if (slot == "20") {
        setCookie("Settings.PinnedItem.item_5", null);
    }
    var pincontainer = _$("divPinItemContainer");
    pincontainer.innerHTML = "<button class=\"smallButton\" onclick=\"unpinItem()\" ><span>Unpin Item</span></button>";
}
function setWishlistButton() {
    var buttonContainer = _$("divWishlistButtonContainer");
    if (!buttonContainer) {
        return;
    }
    if (!_userID) {
        buttonContainer.innerHTML = "<button class=\"smallButton\" onmouseover=\"showTooltip('This feature requires a WOWDB account. Click here to login or register.',event,'r')\" onmouseout=\"hideTooltip();\" onclick=\"navToLogin()\" ><span>Add to Wishlist</span></button>";
        return;
    }
    var currentWishlist = getCookie("Login.Wishlist");
    if (currentWishlist) {
        var arrCurrentWishlist = currentWishlist.split(",");
        if (arrCurrentWishlist.indexOf(_detailsEntityID + "") >= 0) {
            buttonContainer.innerHTML = "<button class=\"smallButton\" onclick=\"navToWishlist();\"><span>In Your Wishlist</span></button>";
            return;
        }
    }
    var entityLabel = "item";
    if (_detailsEntityTypeID == 4) {
        var entityLabel = "quest";
    }
    var tooltipHelp = " onmouseover=\"showTooltip('Click here to add this " + entityLabel + " to your wish list.',event,'r')\" onmouseout=\"hideTooltip();\"";
    buttonContainer.innerHTML = "<button class=\"smallButton\"" + tooltipHelp + "onclick=\"hideTooltip();addToWishlist_prompt(this," + _detailsEntityID + ")\"><span>Add to Wishlist</span></button>";
}
function handleItemDetailsLoading() {
    setWishlistButton();
    setPinItemButton();
    var divCommentsContainer = _$("cellCommentContainer");
    var currentComment;
    if (_userID == "" && divCommentsContainer) {
        var loginDiv = document.createElement("DIV");
        loginDiv.innerHTML = "<small>You are not logged in. Please <a href=\"javascript:navToLogin();\">Login</a> or <a href=\"register.aspx\">Register</a> to post a comment.</small>";
        _$("cellCommentContainer").insertBefore(loginDiv, _$("cellCommentContainer").childNodes[0]);
        var loginDiv = document.createElement("DIV");
        loginDiv.innerHTML = "<small>You are not logged in. Please <a href=\"javascript:navToLogin();\">Login</a> or <a href=\"register.aspx\">Register</a> to submit a screenshot.</small>";
        _$("cellScreenshotContainer").insertBefore(loginDiv, _$("cellScreenshotContainer").childNodes[0]);
    }
    var contributeTabContainer = document.createElement("DIV");
    contributeTabContainer.className = "tabs-container";
    contributeTabContainer.id = "tabGroupContribute";
    createTabGroup(contributeTabContainer, "tabGroupContribute", "Post a Comment|Submit a Screenshot", "cellCommentContainer|cellScreenshotContainer", 0, "toggleTab");
    var oContribute = _$("divContributeContainer");
    if (oContribute) {
        var contributeContainer = oContribute.parentNode;
        contributeContainer.insertBefore(contributeTabContainer, _$("divContributeContainer"));
        var formDisabled = _userID == "";
        var editorDiv = getTextEditor("postComment.aspx", "formCommentEditor", "ajax_action=add_comment,comment_entity_type_id=" + _detailsEntityTypeID + ",comment_entity_id=" + _detailsEntityID, "comment_body", null, "handleCommentFocus(event)", null, 8, "8,000", true, "handleCommentSubmit(event)^Add Comment", null, formDisabled);
        editorDiv.className = "editor";
        var commentContainer = _$("cellCommentContainer");
        commentContainer.appendChild(editorDiv);
    }
    var screenshotContainer = _$("cellScreenshotContainer");
    if (screenshotContainer) {
        if (_userID) {
            if (screenshotContainer) {
                document.forms.formScreenshot.viewport_height.value = Client.viewportHeight();
                document.forms.formScreenshot.viewport_width.value = Client.viewportWidth();
                document.forms.formScreenshot.entity_id.value = _detailsEntityID;
                document.forms.formScreenshot.entity_type_id.value = _detailsEntityTypeID;
            }
        } else {
            document.forms.formScreenshot.screenshot_file.disabled = true;
        }
    }
    if (_detailsHasPreview) {
        if (_detailsPreviewIDs) {
            setTimeout("loadPreviewThumbnail('" + _detailsPreviewIDs + "')", 200);
        } else {
            setTimeout("loadPreviewThumbnail('" + _detailsEntityID + "')", 200);
        }
    }
    if (!_detailsArticle) {
        processArticle();
    }
    document.body.focus();
}
function handleCommentFocus(e) {
    var objCommentPoster = _$("divCommentPoster");
    if (objCommentPoster.style.display == "block") {
        return;
    }
    var verificationArray = ["Adamar", "Curse", "Aragorn", "Gandalf", "Frodo", "Samwise", "Meriadoc", "Peregrin", "Boromir", "Gimli", "Legolas"];
    var i = Math.round((verificationArray.length - 1) * Math.random());
    _verificationText = verificationArray[i];
    var objVerificationImage = _$("verificationImage");
    objVerificationImage.src = "getVerificationImage.aspx?v=" + _verificationText;
    objCommentPoster.style.display = "block";
}
function getEditorElement(elementName, insText, elementArguments) {
    var editorElement = "";
    if (elementName == "ul" || elementName == "ol") {
        if (insText.indexOf("[ul]") >= 0 || insText.indexOf("[ol]") >= 0) {
            return "";
        }
        editorElement = "[" + elementName + "]\n";
        insTextArray = insText.split("\n");
        var linesInserted = 0;
        for (var i = 0; i < insTextArray.length; i++) {
            if (insTextArray[i] != "") {
                linesInserted += 1;
                editorElement += "[li]" + insTextArray[i] + "[/li]\n";
            }
        }
        if (linesInserted == 0) {
            editorElement += "[li][/li]\n";
        }
        editorElement += "[/" + elementName + "]";
        return editorElement;
    }
    if (elementName == "url") {
        if (!elementArguments) {
            return;
        }
        if (insText.indexOf("[url]") >= 0) {
            return "";
        }
        urlLink = elementArguments.url;
        if (elementArguments.label != "") {
            urlLabel = elementArguments.label;
        } else {
            urlLabel = urlLink;
        }
        editorElement = "[url=";
        editorElement += urlLink;
        editorElement += "]" + urlLabel + "[/url]";
        return editorElement;
    }
    if (elementName == "item" || elementName == "itemlink" || elementName == "spell" || elementName == "spelllink") {
        if (!elementArguments) {
            return;
        }
        if (insText.indexOf("[item]") >= 0 || insText.indexOf("[itemlink]") >= 0) {
            return "";
        }
        tooltipID = elementArguments.tooltipID;
        editorElement = "[" + elementName + "]";
        editorElement += tooltipID;
        editorElement += "[/" + elementName + "]";
        return editorElement;
    }
}
function editorApplyElement(source, elementName, elementArguments) {
    var form = null;
    if (source.className == "editor") {
        form = source.getElementsByTagName("form")[0];
    } else {
        form = source.parentNode.parentNode.getElementsByTagName("form")[0];
    }
    var input = form.getElementsByTagName("textarea")[0];
    input.focus();
    if (typeof document.selection != 'undefined') {
        var range = null;
        if (_currentEditorRange) {
            range = _currentEditorRange;
        } else {
            range = document.selection.createRange();
        }
        var insText = range.text;
        newText = getEditorElement(elementName, insText, elementArguments);
        if (newText == "") {
            return;
        }
        range.text = newText;
        range.select();
    } else if (typeof input.selectionStart != 'undefined') {
        var start = input.selectionStart;
        var end = input.selectionEnd;
        var insText = input.value.substring(start, end);
        newText = getEditorElement(elementName, insText, elementArguments);
        if (newText == "") {
            return;
        }
        input.value = input.value.substr(0, start) + newText + input.value.substr(end);
        input.selectionStart = end + (newText.length - insText.length);
    }
    resizeTextArea(input);
}
function getSelectedText(input) {
    if (typeof document.selection != 'undefined') {
        input.focus();
        var range = document.selection.createRange();
        _currentEditorRange = range;
        return range.text;
    } else if (typeof input.selectionStart != 'undefined') {
        var start = input.selectionStart;
        var end = input.selectionEnd;
        return input.value.substring(start, end);
    }
}
function editorApplyTag(button, tag) {
    var form = button.parentNode.parentNode.getElementsByTagName("form")[0];
    var input = form.getElementsByTagName("textarea")[0];
    var aTag = '[' + tag + ']';
    var eTag = '[/' + tag + ']';
    input.focus();
    if (typeof document.selection != 'undefined') {
        var range = document.selection.createRange();
        var insText = range.text;
        if (insText.substr(0, aTag.length).toLowerCase() == aTag && insText.substr(insText.length - eTag.length).toLowerCase() == eTag) {
            range.text = insText.substring(aTag.length, insText.length - eTag.length);
            return;
        }
        range.text = aTag + insText + eTag;
        range = document.selection.createRange();
        if (insText.length == 0) {
            range.move('character', -eTag.length);
        } else {
            range.moveStart('character', aTag.length + insText.length + eTag.length);
        }
        range.select();
    } else {
        if (typeof input.selectionStart != 'undefined') {
            var start = input.selectionStart;
            var end = input.selectionEnd;
            var insText = input.value.substring(start, end);
            if (insText.toLowerCase().indexOf(aTag) >= 0 && insText.toLowerCase().indexOf(eTag) >= 0) {
                var finished = insText.toLowerCase().indexOf(aTag) == 0;
                var startRegExp = eval("/\\[" + tag + "\\]/g");
                var endRegExp = eval("/\\[\\/" + tag + "\\]/g");
                insText = insText.replace(startRegExp, "");
                insText = insText.replace(endRegExp, "");
                if (finished) {
                    input.value = input.value.substr(0, start) + insText + input.value.substr(end);
                    input.selectionStart = end - eTag.length - aTag.length;
                    input.selectionEnd = input.selectionStart;
                    return;
                } else {
                    input.value = input.value.substr(0, start) + aTag + insText + eTag + input.value.substr(end);
                    input.selectionStart = end;
                    input.selectionEnd = end;
                    return;
                }
            }
            input.value = input.value.substr(0, start) + aTag + insText + eTag + input.value.substr(end);
            var pos;
            if (insText.length == 0) {
                pos = start + aTag.length + eTag.length;
            } else {
                pos = start + aTag.length + insText.length + eTag.length;
            }
            input.selectionStart = pos;
            input.selectionEnd = pos;
        }
    }
}
function resizeTextArea(obj, e) {
    var myEvent = getEventObject(e);
    if (e && getEventTarget(e) != null) {
        obj = getEventTarget(myEvent);
    }
    if (myEvent && myEvent.type == "keydown" && myEvent.keyCode == 13) {
        lineCount = (obj.value + "\n").split("\n").length;
    } else {
        lineCount = obj.value.split("\n").length;
    }
    if (lineCount > 8) {
        obj.rows = lineCount;
    } else {
        obj.rows = 8;
    }
    if (isIE) {
        obj.rows += 1;
    }
}
function processURLPrompt(editorID, e) {
    _cancelBubbling(e);
    var myAlert = _$("alert");
    var promptForm = myAlert.getElementsByTagName("form")[0];
    var urlLink = promptForm.url.value;
    var urlLabel = promptForm.label.value;
    var editor = _$(editorID);
    closeAlert();
    editorApplyElement(editor, "url", {
        "url": urlLink,
        "label": urlLabel
    });
}
var _currentEditorRange;
function processTooltipLinkPrompt(editorID, tag, e) {
    _cancelBubbling(e);
    var myAlert = _$("alert");
    var promptForm = myAlert.getElementsByTagName("form")[0];
    var tooltipID = promptForm.ID.value;
    var editor = _$(editorID);
    closeAlert();
    editorApplyElement(editor, tag, {
        "tooltipID": tooltipID
    });
}
function displayTooltipLinkPrompt(e, editorID, tag, tooltipDescription, inputLabel) {
    var myEvent = getEventObject(e);
    var src = getEventTarget(e);
    var srcPosition = getPosition(src);
    var form = src.parentNode.parentNode.getElementsByTagName("form")[0];
    var input = form.getElementsByTagName("textarea")[0];
    var defaultID = getSelectedText(input);
    displayAlert("Please enter the ID of the tooltip you wish to embed below.", "Create " + tooltipDescription + " tooltip", inputLabel + "^ID^" + defaultID, "processTooltipLinkPrompt('" + editorID + "','" + tag + "',event)^OK|closeAlert()^Cancel", src, srcPosition.y, srcPosition.x + src.offsetWidth + 2);
}
function displayURLPrompt(e, editorID) {
    var myEvent = getEventObject(e);
    var src = getEventTarget(e);
    var srcPosition = getPosition(src);
    var form = src.parentNode.parentNode.getElementsByTagName("form")[0];
    var input = form.getElementsByTagName("textarea")[0];
    var defaultLink = getSelectedText(input);
    displayAlert("Please enter the hyperlink details below.", "Create a Hyperlink", "URL^url^" + defaultLink + "^^^^<small>Enter a URL, starting with http://</small>|Label^label", "processURLPrompt('" + editorID + "',event)^OK|closeAlert()^Cancel", src, srcPosition.y, srcPosition.x + src.offsetWidth + 2);
}
var _editorCreationCount = 0;
function getTextEditor(formAction, formID, formHiddenFields, formBodyID, formBodyValue, formBodyFocusEvent, formBodyCols, formBodyRows, formEntryLimit, formVerification, formButtons, formAdditionalInput, formInputDisabled) {
    _editorCreationCount += 1;
    var editorDiv = document.createElement("div");
    editorDiv.id = "editor_" + _editorCreationCount;
    editorDiv.className = "editor";
    if (formAction == null || formAction == "") {
        formAction = "postComment.aspx";
    }
    if (formID == null || formID == "") {
        formID = "formComment";
    }
    var lineCount = 0;
    if (formBodyValue != null && formBodyValue != "") {
        lineCount = formBodyValue.split("\n").length;
    }
    if (lineCount > formBodyRows) {
        formBodyRows = lineCount;
    }
    var textEditorHTML = "";
    if (!formInputDisabled) {
        textEditorHTML += "<div class=\"editor-mode-links\">Mode:<a class=\"selected\" onclick=\"toggleEditorMode(this,'edit');\">Edit</a>|<a onclick=\"toggleEditorMode(this,'preview');\">Preview</a></div><div class=\"editor-preview\"></div>";
    }
    textEditorHTML += "<div";
    textEditorHTML += " class=\"editor-formatting-buttons\"><button style=\"margin-left: 3px;\" onclick=\"editorApplyTag(this,'b');\">B</button><button onclick=\"editorApplyTag(this,'i');\"><i>I</i></button><button onclick=\"editorApplyTag(this,'u');\"><u>U</u></button><button onclick=\"editorApplyTag(this,'s');\"><s>Strike</s></button><button onclick=\"editorApplyTag(this,'small');\"><small>Small</small></button><button style=\"background: url(images/button_quote.gif) no-repeat; background-position: center;width: 22px;\"  onclick=\"editorApplyTag(this,'quote');\">&nbsp;</button><button onclick=\"editorApplyTag(this,'h');\">Header</button><button onclick=\"editorApplyElement(this,'ul');\" style=\"background: url(images/button_ul.gif) no-repeat; background-position: center;width: 23px;\">&nbsp;</button><button onclick=\"editorApplyElement(this,'ol');\" style=\"background: url(images/button_ol.gif) no-repeat; background-position: center;width: 23px;\">&nbsp;</button><button onclick=\"displayURLPrompt(event,'" + editorDiv.id + "');\" style=\"background: url(images/button_hyperlink.gif) no-repeat; background-position: center;width: 23px;\">&nbsp;</button><button onclick=\"displayTooltipLinkPrompt(event,'" + editorDiv.id + "','item','an Item','Item ID');\" style=\"background: url(images/button_item.gif) no-repeat; background-position: center;width: 24px;\" title=\"Add Item Tooltip (with Icon)\">&nbsp;</button><button onclick=\"displayTooltipLinkPrompt(event,'" + editorDiv.id + "','itemlink','an Item','Item ID');\" style=\"background: url(images/button_itemlink.gif) no-repeat; background-position: center;width: 23px;\" title=\"Add Item Tooltip (without icon)\">&nbsp;</button><button onclick=\"displayTooltipLinkPrompt(event,'" + editorDiv.id + "','spell','a Spell','Spell ID');\" style=\"background: url(images/button_spell.gif) no-repeat; background-position: center;width: 24px;\" title=\"Add Spell Tooltip (with Icon)\">&nbsp;</button><button onclick=\"displayTooltipLinkPrompt(event,'" + editorDiv.id + "','spelllink','a Spell','Spell ID');\" style=\"background: url(images/button_spelllink.gif) no-repeat; background-position: center;width: 29px;\" title=\"Add Spell Tooltip (without icon)\">&nbsp;</button></div>";
    if (formInputDisabled) {
        textEditorHTML = textEditorHTML.replace(/\<button/g, "<button disabled");
    }
    textEditorHTML += "<form onsubmit=\"return false;\" method=\"post\" action=\"" + formAction + "\" name=\"" + formID + "\" id=\"" + formID + "\">";
    formHiddenFields = formHiddenFields.split(",");
    for (var i = 0; i < formHiddenFields.length; i++) {
        var currentField = formHiddenFields[i].split("=");
        textEditorHTML += "<input type=\"hidden\" name=\"" + currentField[0] + "\" id=\"" + currentField[0] + "\" value=\"" + currentField[1] + "\"/>";
    }
    textEditorHTML += "<textarea ";
    if (formInputDisabled) {
        textEditorHTML += "disabled ";
    }
    if (isIE) {
        textEditorHTML += "onpaste=\"resizeTextArea(this);\" onchange=\"resizeTextArea(this);\" onkeydown=\"resizeTextArea(this, event)\" onkeyup=\"resizeTextArea(this)\"";
    } else {
        textEditorHTML += "oninput=\"resizeTextArea(this,event)\"";
    }
    if (formBodyFocusEvent != null && formBodyFocusEvent != "") {
        textEditorHTML += " onfocus=\"" + formBodyFocusEvent + "\"";
    }
    textEditorHTML += " name=\"" + formBodyID + "\" id=\"" + formBodyID + "\"";
    if (formBodyCols != null) {
        textEditorHTML += " cols=\"" + formBodyCols + "\"";
    }
    if (formBodyRows != null) {
        textEditorHTML += " rows=\"" + formBodyRows + "\"";
    }
    textEditorHTML += " style=\"overflow: visible;\">";
    if (formBodyValue != null && formBodyValue != "") {
        textEditorHTML += formBodyValue;
    }
    textEditorHTML += "</textarea>";
    if (formEntryLimit != null && formEntryLimit != "") {
        textEditorHTML += "<br/><small>Note: You may enter up to " + formEntryLimit + " characters.</small>";
    }
    if (formAdditionalInput) {
        textEditorHTML += "<div class=\"editor-input\">";
        textEditorHTML += formAdditionalInput;
        textEditorHTML += "</div>";
    }
    textEditorHTML += "</form>";
    if (formVerification) {
        textEditorHTML += "<div id=\"divCommentPoster\"><img id=\"verificationImage\" src=\"templates/wowdb/images/clear.gif\" />For verification, please enter the text you see in the box: <input maxlength=20 id=\"inputVerificationText\" type=\"text\" onkeypress=\"handleItemDetailsKeyPress(event)\" /></div>";
    }
    textEditorHTML += "<div class=\"editor-buttons\">";
    if (!formButtons) {
        textEditorHTML += "<button class=\"smallButton\" onclick=\"saveEditCommentForm(this)\"><span>Save</span></button>&nbsp;<button class=\"smallButton\" onclick=\"cancelEditComment(this)\"><span>Cancel</span></button>";
    } else {
        formButtons = formButtons.split("|");
        for (var i = 0; i < formButtons.length; i++) {
            var currentButton = formButtons[i].split("^");
            textEditorHTML += "<button class=\"smallButton";
            if (formInputDisabled) {
                textEditorHTML += "Disabled\"";
            } else {
                textEditorHTML += "\" onclick=\"" + currentButton[0] + "\"";
            }
            textEditorHTML += "><span>" + currentButton[1] + "</span></button>";
        }
    }
    textEditorHTML += "</div>";
    editorDiv.innerHTML = textEditorHTML;
    return editorDiv;
}
function cancelEditComment(cancelButton) {
    removeEditCommentForm(cancelButton.parentNode.parentNode);
}
function removeEditCommentForm(editorContainer) {
    var commentContainer = editorContainer.parentNode;
    var commentBody = getElementsByClassName("comment-body", "div", commentContainer, true);
    var editButtonContainer = getElementsByClassName("comment-links", "div", commentContainer, true).firstChild;
    commentContainer.removeChild(editorContainer);
    if (editorContainer.ascBodyContainer) {
        editorContainer.ascBodyContainer.style.display = "inline";
        editorContainer.ascEditButtonContainer.style.display = "inline";
        if (editorContainer.ascHiddenElements && editorContainer.ascHiddenElements.length > 0) {
            for (var i = 0; i < editorContainer.ascHiddenElements.length; i++) {
                editorContainer.ascHiddenElements[i].style.display = "block";
            }
        }
    } else {
        editButtonContainer.style.display = "inline";
        commentBody.style.display = "block";
    }
}
function saveEditArticle(saveButton) {
    var editorContainer = saveButton.parentNode.parentNode;
    var form = editorContainer.getElementsByTagName("form")[0];
    var articleID = form.article_id.value;
    jx.load(getFormAsString(form), handleSaveEditArticlePostback, 'text', 'post');
    function handleSaveEditArticlePostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            if (!_detailsArticle || form.article_body.value.indexOf("[item") >= 0 || form.article_body.value.indexOf("[spell") >= 0) {
                window.location.reload(false);
            }
            _detailsArticle.body = form.article_body.value;
            _detailsArticle.dateModified = getFriendlyDate(new Date());
            _detailsArticle.modifiedUserId = _userID;
            _detailsArticle.modifiedUserName = _userDisplayName;
            _detailsArticle.link = form.article_link.value;
            processArticle();
            removeEditCommentForm(editorContainer);
        } else {
            displayAlert(postResult[1]);
        }
    }
}
function saveEditCommentForm(saveButton) {
    var editorContainer = saveButton.parentNode.parentNode;
    var commentForm = editorContainer.getElementsByTagName("form")[0];
    var commentID = commentForm.comment_id.value;
    jx.load(getFormAsString(commentForm), handleEditCommentPostback, 'text', 'post');
    function handleEditCommentPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            if (commentForm.comment_body.value.indexOf("[item") >= 0 || commentForm.comment_body.value.indexOf("[spell") >= 0) {
                window.location.reload(false);
            }
            var commentDescriptorIndex = getDescriptorIndexByName("comments");
            var commentObject = getEntityRecord(commentDescriptorIndex, commentID);
            commentObject.name = commentForm.comment_body.value;
            commentObject.lastEditDate = getFriendlyDate(new Date());
            commentObject.lastEditByID = _userID;
            commentObject.lastEditByName = _userDisplayName;
            renderResults(_arrResultsStartingIndex[commentDescriptorIndex]);
        } else {
            displayAlert(postResult[1]);
        }
    }
}
function toggleEditorMode(toggleButton, mode) {
    var buttonContainer = toggleButton.parentNode;
    var formContainer = buttonContainer.parentNode;
    var formattingButtonsContainer = getElementsByClassName("editor-formatting-buttons", "div", formContainer, true);
    var formPreviewContainer = getElementsByClassName("editor-preview", "div", formContainer, true);
    var form = formContainer.getElementsByTagName("FORM")[0];
    var buttons = buttonContainer.getElementsByTagName("A");
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].className == "selected") {
            buttons[i].className = "";
        }
    }
    toggleButton.className = "selected";
    if (mode == "preview") {
        formattingButtonsContainer.style.display = "none";
        form.style.display = "none";
        var formBody = form.getElementsByTagName("textarea")[0];
        formPreviewContainer.innerHTML = getFormattedText(formBody.value, true);
        formPreviewContainer.style.display = "block";
    } else {
        formattingButtonsContainer.style.display = "block";
        formPreviewContainer.style.display = "none";
        form.style.display = "block";
    }
}
function setFormattedText(containerID, text) {
    _$(containerID).innerHTML = getFormattedText(text, false, false);
}
function editArticle(anchor, articleID) {
    var articleBodyContainer = _$("detailsArticle");
    var articleContainer = articleBodyContainer.parentNode;
    articleBodyContainer.style.display = "none";
    anchor.parentNode.style.display = "none";
    var infoBox = getElementsByClassName("infobox", "table", document.body, true);
    infoBox.style.display = "none";
    var currentArticle = _detailsArticle;
    if (!currentArticle) {
        articleID = 0;
        articleBody = "";
        articleLink = "http://www.wowwiki.com/" + _detailsEntityName.replace(/\ /g, "_");
    } else {
        articleLink = currentArticle.link;
        articleBody = currentArticle.body;
    }
    var articleCustomInput = "<a target=\"_blank\" href=\"" + articleLink + "\">Potential WowWiki Article</a> Article Source: <select name=\"article_source_id\"><option value=\"1\">WoWWiki</option></select>&nbsp;&nbsp;Article Link: <input name=\"article_link\" size=\"50\" value=\"" + articleLink + "\" />";
    var editorDiv = getTextEditor("postComment.aspx", "formArticleEditor", "ajax_action=edit_article,article_entity_type_id=" + _detailsEntityTypeID + ",article_entity_id=" + _detailsEntityID + ",article_id=" + articleID, "article_body", articleBody, null, null, 8, "8,000", null, "saveEditArticle(this)^Save|cancelEditComment(this)^Cancel", articleCustomInput);
    editorDiv.ascHiddenElements = [];
    editorDiv.ascHiddenElements.push(infoBox);
    editorDiv.ascBodyContainer = articleBodyContainer;
    editorDiv.ascParentContainer = articleContainer;
    editorDiv.ascEditButtonContainer = anchor.parentNode;
    editorDiv.className = "article editor";
    articleContainer.insertBefore(editorDiv, articleBodyContainer);
}
function removeFromWishList(e, anchor, entityID, entityTypeID, wishlistContainerID) {
    _cancelBubbling(e);
    if (!window.confirm("Are you sure you want to remove this from your wish list?")) {
        return;
    }
    anchor.style.visibility = "hidden";
    var formString = "?wishlist_entity_type_id=" + entityTypeID;
    formString += "&wishlist_entity_id=" + entityID;
    formString += "&wishlist_container_id=" + wishlistContainerID;
    formString += "&ajax_action=remove_wishlist";
    var rowObject = anchor.parentNode;
    while (rowObject.tagName != "TR") {
        rowObject = rowObject.parentNode;
    }
    jx.load("editWishlist.aspx" + formString, handleRemoveFromWishListPostback, 'text', 'post');
    function handleRemoveFromWishListPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            displayAlert(postResult[1]);
            var oContainer = rowObject.parentNode;
            oContainer.removeChild(rowObject);
        } else {
            anchor.style.visibility = "visible";
            displayAlert(postResult[1]);
        }
    }
}
function deleteWishlistContainer(wishlistContainerID) {
    if (!window.confirm("Are you sure you want to delete this wish list?")) {
        return;
    }
    var formString = "?wishlist_container_id=" + wishlistContainerID;
    formString += "&ajax_action=delete_wishlist";
    jx.load("editWishlist.aspx" + formString, handleDeleteWishlistPostback, 'text', 'post');
    function handleDeleteWishlistPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            window.location.reload(false);
        } else {
            displayAlert(postResult[1]);
        }
    }
}
function addToWishlist_prompt(anchor, entityID) {
    displayAlert("Please choose the wish list you would like to add this to, or enter the name of a new one.", "Add to Wish List", "Existing^wishlist_container_id^^WishlistContainers." + _detailsEntityTypeID + "|New List^wishlist_container_name^^^^16^Enter up to 16 characters", "addToWishlist(" + entityID + ")^Add|closeAlert()^Cancel", null, null, null, true);
}
function addToWishlist(entityID) {
    var formString = "?wishlist_entity_type_id=" + _detailsEntityTypeID;
    formString += "&wishlist_entity_id=" + _detailsEntityID;
    var oContainerID = _$("wishlist_container_id");
    var wishlistContainerID = oContainerID.options[oContainerID.selectedIndex].value;
    var wishlistContainerName = _$("wishlist_container_name").value;
    if (wishlistContainerID == "" && wishlistContainerName == "") {
        return;
    }
    anchor = _$("divWishlistButtonContainer").childNodes[0];
    anchor.style.visibility = "hidden";
    formString += "&wishlist_container_id=" + wishlistContainerID;
    formString += "&wishlist_container_name=" + wishlistContainerName;
    formString += "&ajax_action=add_wishlist";
    closeAlert();
    jx.load("editWishlist.aspx" + formString, handleAddToWishlistPostback, 'text', 'post');
    function handleAddToWishlistPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            displayAlert(postResult[1]);
            var oContainer = anchor.parentNode;
            oContainer.innerHTML = "<button class=\"smallButtonDisabled\"><span>In Your Wishlist</span></button>";
        } else {
            anchor.style.visibility = "visible";
            displayAlert(postResult[1]);
        }
    }
}
function editComment(anchor, commentID) {
    var commentContainer = _$("comment_" + commentID).firstChild;
    var commentBodyContainer = getElementsByClassName("comment-body", "div", commentContainer, true);
    commentBodyContainer.style.display = "none";
    anchor.parentNode.style.display = "none";
    var currentComment = getEntityRecord(getDescriptorIndexByName("comments"), commentID);
    if (!currentComment) {
        return;
    }
    var editorDiv = getTextEditor("postComment.aspx", "formCommentEditor", "ajax_action=edit_comment,comment_entity_type_id=" + _detailsEntityTypeID + ",comment_entity_id=" + _detailsEntityID + ",comment_id=" + commentID, "comment_body", currentComment.name, null, null, 8, "8,000");
    editorDiv.className = "editor";
    commentContainer.insertBefore(editorDiv, commentBodyContainer);
}
function deleteArticle(anchor, articleID) {
    if (!window.confirm("Are you sure you want to delete this article?")) {
        return;
    }
    var formString = "?ajax_action=delete_article";
    formString += "&article_entity_type_id=" + _detailsEntityTypeID;
    formString += "&article_entity_id=" + _detailsEntityID;
    formString += "&article_id=" + articleID;
    jx.load("postComment.aspx" + formString, handleDeleteArticlePostback, 'text', 'post');
    function handleDeleteArticlePostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            _detailsArticle = null;
            processArticle();
            displayAlert(postResult[1]);
        } else {
            ratingButtonContainer.style.display = "inline";
            displayAlert(postResult[1]);
        }
    }
}
function deleteComment(anchor, commentID) {
    if (!window.confirm("Are you sure you want to delete this comment?")) {
        return;
    }
    var formString = "?comment_entity_type_id=" + _detailsEntityTypeID;
    formString += "&comment_entity_id=" + _detailsEntityID;
    formString += "&delete_comment_id=" + commentID;
    var commentObject = anchor.parentNode;
    while (commentObject.tagName != "TR") {
        commentObject = commentObject.parentNode;
    }
    jx.load("postComment.aspx" + formString, handleDeleteCommentPostback, 'text', 'post');
    function handleDeleteCommentPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            commentObject.parentNode.removeChild(commentObject);
            displayAlert(postResult[1]);
        } else {
            ratingButtonContainer.style.display = "inline";
            displayAlert(postResult[1]);
        }
    }
}
function validatePostScreenshot() {
    var screenshotFile = _$("inputScreenshotFile").value.toLowerCase();
    if (screenshotFile == "") {
        displayAlert("You must select a valid screenshot file.", "Submit a Screenshot");
        return false;
    }
    if (!endsWith(screenshotFile, ".jpg") && !endsWith(screenshotFile, ".png")) {
        displayAlert("The file you selected is not a supported type.", "Submit a Screenshot");
        return false;
    }
    return true;
}
function modScreenshot(e, approved, anchor, screenshotID) {
    _cancelBubbling(e);
    var formString = "?ajax_action=mod_screenshot&screenshot_entity_type_id=" + _detailsEntityTypeID;
    formString += "&screenshot_entity_id=" + _detailsEntityID;
    formString += "&screenshot_id=" + screenshotID;
    formString += "&screenshot_approved=" + approved;
    var modContainer = anchor.parentNode.parentNode;
    modContainer.style.visibility = "hidden";
    jx.load("postComment.aspx" + formString, handleModScreenshotPostback, 'text', 'post');
    function handleModScreenshotPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            displayAlert(postResult[1]);
        } else {
            modContainer.style.visibility = "visible";
            displayAlert(postResult[1]);
        }
    }
}
function rateComment(anchor, commentID, rating) {
    if (_userID == "") {
        navToLogin();
    }
    var commentRecord = getEntityRecord(getDescriptorIndexByName("comments"), commentID);
    var newRating = parseInt(commentRecord.rating, null) + parseInt(rating, null);
    if (newRating > 0) {
        newRating = "+" + newRating;
    } else {
        newRating = newRating;
    }
    var formString = "?comment_entity_type_id=" + _detailsEntityTypeID;
    formString += "&comment_entity_id=" + _detailsEntityID;
    formString += "&comment_id=" + commentID;
    formString += "&comment_rating=" + rating;
    var ratingContainer = anchor.parentNode.parentNode.getElementsByTagName("span")[0];
    var ratingButtonContainer = anchor.parentNode;
    ratingButtonContainer.style.display = "none";
    jx.load("postComment.aspx" + formString, handleCommentRatingPostback, 'text', 'post');
    function handleCommentRatingPostback(data) {
        var postResult = data.split("|");
        if (postResult[0] == 0) {
            commentRecord.rating = newRating;
            commentRecord.rating_Sort = -1 * parseInt(newRating);
            ratingContainer.innerHTML = newRating;
        } else {
            ratingButtonContainer.style.display = "inline";
            displayAlert(postResult[1]);
        }
    }
}
function closeAlert() {
    var alert = _$("alert");
    if (alert) {
        document.body.removeChild(alert);
    }
    var alertOverlay = _$("alert_overlay");
    if (alertOverlay) {
        document.body.removeChild(alertOverlay);
    }
}
function displayAlert(alertMessage, alertTitle, alertInput, alertButtons, alertOpener, alertTop, alertLeft, showAlertOverlay, alertWidth) {
    var oAlert = _$("alert");
    if (oAlert) {
        document.body.removeChild(oAlert);
    }
    var alertOverlay = _$("alert_overlay");
    if (alertOverlay) {
        document.body.removeChild(alertOverlay);
    }
    if (showAlertOverlay) {
        var alertOverlayDiv = document.createElement("DIV");
        alertOverlayDiv.innerHTML = "&nbsp;";
        alertOverlayDiv.id = "alert_overlay";
        document.body.appendChild(alertOverlayDiv);
    }
    var alertDiv = document.createElement("DIV");
    alertDiv.className = "content-panel alert";
    alertDiv.id = "alert";
    alertDiv.style.visibility = "hidden";
    if (alertWidth) {
        alertDiv.style.width = alertWidth + "px";
    }
    if (alertTitle == null) {
        alertTitle = "System Message";
    }
    var alertContainerHTML = "<div class=\"content-panel-bg\"></div><div class=\"content-panel-r\"></div><div class=\"content-panel-b\"></div><div class=\"content-panel-br\"></div><div class=\"content-panel-t\"></div><div class=\"content-panel-tr\"></div><div class=\"content-panel-wrapper\">";
    var alertHTML = alertContainerHTML + "<h2>" + alertTitle + "</h2><div>" + alertMessage + "</div>";
    var primaryButtonCallback = null;
    var secondaryButtonCallback = null;
    var alertButtonsArray = null;
    if (alertButtons) {
        alertButtonsArray = alertButtons.split("|");
        primaryButtonCallback = alertButtonsArray[0].split("^")[0];
        secondaryButtonCallback = alertButtonsArray[1].split("^")[0];
    }
    if (alertInput) {
        alertInputArray = alertInput.split("|");
        alertHTML += "<div>&nbsp;</div><form onsubmit=\"return false;\">";
        alertHTML += "<table>";
        for (var i = 0; i < alertInputArray.length; i++) {
            alertHTML += "<tr>";
            var currentInput = alertInputArray[i].split("^");
            var defaultValue = "";
            var lookupName = "";
            var maxLengthHTML = "";
            var sizeHTML = "";
            var inputLabel = currentInput[0];
            var inputDescription = "";
            var scriptActions = "";
            var inputStyle = "";
            if (inputLabel.indexOf(":") == -1) {
                inputLabel += ":";
            }
            if (currentInput.length >= 3) {
                defaultValue = currentInput[2];
            }
            if (currentInput.length >= 4) {
                lookupName = currentInput[3];
            }
            if (currentInput.length >= 5 && currentInput[4] != "") {
                sizeHTML = "  size='" + currentInput[4] + "'";
            }
            if (currentInput.length >= 6 && currentInput[5] != "") {
                maxLengthHTML = " maxlength=" + currentInput[5];
            }
            if (currentInput.length >= 7) {
                inputDescription = "<div class=desc>" + currentInput[6] + "</div>";
            }
            if (currentInput.length >= 8) {
                scriptActions = currentInput[7];
            }
            if (currentInput.length >= 9) {
                inputStyle = currentInput[8];
            }
            alertHTML += "<td class=\"label\">" + inputLabel + "</td>";
            alertHTML += "<td class=\"alertInput\">";
            if (lookupName != "") {
                var lookupSelect = getLookupSelectBox(lookupName, "", null, false, null, true);
                alertHTML += "<select id=\"" + currentInput[1] + "\" name=\"" + currentInput[1] + "\">" + lookupSelect.innerHTML + "</select>";
            } else {
                alertHTML += "<input id=\"" + currentInput[1] + "\" name=\"" + currentInput[1] + "\" onKeyPress=\"if((event.keyCode==10)||(event.keyCode==13)) " + primaryButtonCallback + "; if (event.keyCode==27) " + secondaryButtonCallback + ";\" type=\"text\" value=\"" + defaultValue + "\"" + maxLengthHTML + sizeHTML + scriptActions + inputStyle + "/>" + inputDescription;
            }
            alertHTML += "</td>";
            alertHTML += "</tr>";
        }
        alertHTML += "</table>";
        alertHTML += "</form>";
    }
    alertHTML += "<div class=\"buttons\">";
    if (alertButtonsArray) {
        for (var i = 0; i < alertButtonsArray.length; i++) {
            var currentButton = alertButtonsArray[i].split("^");
            alertHTML += "&nbsp;<button class=\"smallButton\" onclick=\"" + currentButton[0] + "\"><span>" + currentButton[1] + "</span></button>";
        }
    } else {
        alertHTML += "<button class=\"smallButton\" onclick=\"closeAlert();\"><span>Close</span></button>";
    }
    alertHTML += "</div>";
    alertHTML += "</div>";
    alertDiv.innerHTML = alertHTML;
    document.body.appendChild(alertDiv);
    if (!alertTop) {
        centerElement(alertDiv);
    } else {
        alertDiv.style.top = alertTop + "px";
        alertDiv.style.left = alertLeft + "px";
    }
    alertDiv.style.visibility = "visible";
    if (alertInput) {
        alertDiv.getElementsByTagName("input")[0].focus();
    }
}
function handleCommentSubmit(e) {
    var myEvent = getEventObject(e);
    var targetObj = getEventTarget(myEvent);
    var editorContainer = targetObj;
    while (editorContainer.className != "editor") {
        editorContainer = editorContainer.parentNode;
    }
    var submitButton = getElementsByClassName("smallButton", "button", editorContainer, true);
    var commentForm = editorContainer.getElementsByTagName("form")[0];
    if (commentForm.comment_body.value.length < 10) {
        displayAlert("You must enter at least 10 characters.");
        return;
    }
    var verificationText = _$("inputVerificationText").value;
    if (_verificationText == "" || verificationText.toLowerCase() != _verificationText.toLowerCase()) {
        displayAlert("The text you entered does not match the text in the image.");
        return;
    }
    jx.load(getFormAsString(commentForm), handleCommentSubmitPostback, 'text', 'post');
    submitButton.disabled = true;
    function handleCommentSubmitPostback(data) {
        var postResult = data.split("|");
        submitButton.disabled = false;
        if (postResult[0] == 0) {
            var commentRecord = postResult[1].split("^");
            if (commentRecord[4].indexOf("[item") >= 0 || commentRecord[4].indexOf("[spell") >= 0) {
                window.location.reload(false);
            }
            _$("divPostConfirmation").innerHTML = "Your comment has been posted.";
            _$("divPostConfirmation").style.display = "block";
            _$("inputVerificationText").value = "";
            commentForm.reset();
            commentRecord[4] = eval("\"" + commentRecord[4] + "\"");
            var newComment = new AscComment(commentRecord[0], commentRecord[1], commentRecord[2], commentRecord[3], commentRecord[4], commentRecord[5], commentRecord[6], commentRecord[7]);
            var commentTabIndex = getDescriptorIndexByName("comments");
            _arrResultsDataCollection[commentTabIndex].push(newComment);
            _arrResultsCounts[commentTabIndex] += 1;
            if (!_arrResultsHasData[commentTabIndex]) {
                _arrResultsHasData[commentTabIndex] = true;
                _$("divItemListContainer").removeChild(_$("tableResultsList_" + commentTabIndex));
                createResultsTable(commentTabIndex);
            }
            setViewState("0,1", commentTabIndex + "," + getLastPageIndex());
            setCurrentResultsTab(commentTabIndex);
            navPage(getLastPageIndex());
        } else {
            displayAlert(postResult[1]);
        }
    }
}
function showHiddenComment(anchor) {
    anchor.style.display = "none";
    anchor.parentNode.parentNode.parentNode.childNodes[1].style.display = "block";
    anchor.parentNode.parentNode.parentNode.childNodes[2].style.display = "block";
}
function getFormAsString(formObject) {
    returnString = formObject.action;
    formElements = formObject.elements;
    for (var i = formElements.length - 1; i >= 0; --i) {
        if (i == formElements.length - 1) {
            returnString = returnString + "?";
        } else {
            returnString = returnString + "&";
        }
        returnString = returnString + encodeURI(formElements[i].name) + "=" + encodeURIComponent(formElements[i].value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
    }
    return returnString;
}
function getLastPageIndex() {
    var pageCount = Math.ceil(_arrFilteredResultsData.length / _resultsPerPage);
    var startingIndex = ((pageCount - 1) * _resultsPerPage);
    return startingIndex;
}
function getDescriptorIndexByName(descriptorName) {
    var descriptorIndex = "";
    for (var i = 0; i < _arrResultsDataDescriptors.length; i++) {
        if (_arrResultsDataDescriptors[i] == descriptorName) {
            descriptorIndex += "," + i;
        }
    }
    if (descriptorIndex == "") {
        return null;
    }
    descriptorIndex = descriptorIndex.substring(1);
    var descriptorIndexArray = descriptorIndex.split(",");
    if (descriptorIndexArray.count == 1) {
        return descriptorIndexArray[0];
    } else {
        return descriptorIndexArray;
    }
}
function handleItemDetailsKeyPress(e) {
    var myEvent = getEventObject(e);
    if (myEvent.keyCode == 13) {
        handleCommentSubmit(e);
        return;
    }
}
function getItemIDFromURL() {
    var itemID = self.location.href.substring(self.location.href.indexOf("?id=") + 4);
    if (itemID.indexOf("&") >= 0) {
        itemID = itemID.substring(0, itemID.indexOf("&"));
    }
    return itemID;
}
function scrollToCommentBox() {
    setCurrentContributeTab(0);
    var objCommentBox = _$("tabGroupContribute");
    var objContributeContainer = _$("divContributeContainer");
    var objCommentInput = objContributeContainer.getElementsByTagName("textarea")[0];
    objCommentBox.scrollIntoView();
    objCommentInput.focus();
}
function scrollToScreenshotBox() {
    setCurrentContributeTab(1);
    var objScreenshotBox = _$("tabGroupContribute");
    objScreenshotBox.scrollIntoView();
}
function setAds() {
    var adRight = _$("iframeAdRight");
    if (adRight) {}
    var adComments = _$("iframeAdComments");
    if (adComments) {}
}
function refreshAds() {
    var adRight = _$("iframeAdRight");
    if (adRight) {}
}
function setModMenu() {
    if (!_userIsMod) {
        return;
    }
}
function handleSearchInputFocus(inputbox) {
    if (inputbox.value == "Search the database...") {
        inputbox.value = "";
    }
}
function handleSearchInputBlur(inputbox) {
    if (inputbox.value == "") {
        inputbox.value = "Search the database...";
    }
}
function setUser() {
    _userInitialized = true;
    var userID = getCookie("Login.UserID");
    if (userID != null) {
        _userID = userID;
    }
    var userDisplayName = getCookie("Login.UserDisplayName");
    if (userDisplayName != null) {
        _userDisplayName = userDisplayName;
    }
    var userIsMod = getCookie("Login.UserIsMod");
    if (userIsMod != null && userIsMod == "1") {
        _userIsMod = true;
    }
    if (userDisplayName != null && userDisplayName != "") {
        mn_User_1 = [[1, "Account Settings", "account.aspx"], [2, "Wish Lists", "javascript:navToWishlist();"], [3, "Characters", "characters.aspx"], [4, "Logout", "javascript:navToLogout();"]];
        if (_userIsMod) {
            mn_User_1.push([3, "Approve Map Locations", "adminApproveLocations.aspx"]);
            mn_User_1.push([4, "Approve Talent Builds", "adminApproveBuilds.aspx"]);
        }
        mn_User = [[1, "Welcome <b>" + userDisplayName + "</b>", "$nonav$", null, "menuWelcomeMessage"], [2, "My Account", "account.aspx", mn_User_1]];
        _$("divLoginOptions").style.display = "none";
    } else {
        _$("divLoginOptions").style.display = "block";
    }
    setUserBookmarks();
    _$("spanUserMenu").innerHTML = "";
    addMainMenu("spanUserMenu", mn_User);
}
function handleBookmarkChange(e) {
    var savedSearchSel = _$("fi_selBookmarks");
    var savedSearchValue = savedSearchSel.value;
    if (savedSearchValue == "") {
        return;
    }
    savedSearchValue = savedSearchValue.split("@@");
    var browseKey = savedSearchValue[0];
    var filterKey = savedSearchValue[1].replace(/\!\!/g, ";");
    setTimeout("self.location = 'search.aspx?browse=" + browseKey + "&filters=" + filterKey + "'", 1);
}
function setUserBookmarks() {
    if (!_$("spanBookmarks")) {
        return;
    }
    if (getCookie("Login.SavedSearches.1") == "" || getCookie("Login.SavedSearches.1") == null) {
        if (!_userID) {
            _$("spanBookmarks").innerHTML = "<a href=\"register.aspx\">Register</a> to <span class=\"tip\" onmouseout=\"hideTooltip();\" onmouseover=\"showTooltip('With a WOWDB account you can save searches as Bookmarks for later retrieval.', event,'r')\">Create Bookmarks</span>";
        } else {
            _$("spanBookmarks").innerHTML = "Your <span class=\"tip\" onmouseout=\"hideTooltip();\" onmouseover=\"showTooltip('To easily retrieve a search you frequently perform, simply click the &quot;Bookmark this Search&quot; button after filtering results.', event,'r')\">Bookmarks</span> will appear here.";
        }
        return;
    }
    var savedSearchSel = getLookupSelectBox("SavedSearches.1", "selBookmarks", null, false, "handleBookmarkChange()", true, "~~", "||");
    var currentBookmark = getBrowseKey() + "@@" + getFilterKey().replace(/;/g, "!!");
    savedSearchSel.value = currentBookmark;
    for (var i = 0; i < savedSearchSel.options.length; i++) {}
    _$("spanBookmarks").innerHTML = "<small>Bookmarks:</small>&nbsp;";
    _$("spanBookmarks").appendChild(savedSearchSel);
}
var _searchAssistLastTerm = null;
var _searchAssistInProgress = false;
var _searchAssistInitialized = false;
var _searchAssistIndex = -1;
var _searchAssistInputField = null;
function getEntityLabelPlural(categoryID) {
    for (var i = 0; i < mn_Main.length; i++) {
        if (mn_Main[i][0] == categoryID) {
            return mn_Main[i][1];
        }
    }
}
function getEntityLabel(entityTypeID) {
    return getLookupLabel("entity_label", entityTypeID);
}
function setSearchAssistIndex(newIndex) {
    var oSearchAssistant = _$("completeTable");
    if (!oSearchAssistant) {
        return;
    }
    if (newIndex < -1) {
        return;
    }
    if (newIndex >= oSearchAssistant.rows.length) {
        newIndex = 0;
    } else if (newIndex == -1) {
        newIndex = oSearchAssistant.rows.length - 1;
    }
    if (_searchAssistIndex >= 0 && _searchAssistIndex < oSearchAssistant.rows.length) {
        oSearchAssistant.rows[_searchAssistIndex].className = "wowdb-ac-a";
    }
    _searchAssistIndex = newIndex;
    if (_searchAssistIndex >= 0 && _searchAssistIndex < oSearchAssistant.rows.length) {
        oSearchAssistant.rows[_searchAssistIndex].className = "wowdb-ac-b";
        _searchAssistInputField.value = getTextValue(oSearchAssistant.rows[_searchAssistIndex].cells[1]);
    }
}
function handleHomeKeyPress(e) {
    var oSearchAssistant = _$("completeTable");
    if (!oSearchAssistant) {
        return;
    }
    e = getEventObject(e);
    if (e.keyCode == 38) {
        if (oSearchAssistant.style.visibility == "hidden") {
            handleHomeSearchInput();
            return;
        }
        setSearchAssistIndex(_searchAssistIndex - 1);
        _cancelBubbling(e);
        return false;
    } else if (e.keyCode == 40) {
        if (oSearchAssistant.style.visibility == "hidden") {
            handleHomeSearchInput();
            return;
        }
        setSearchAssistIndex(_searchAssistIndex + 1);
        _cancelBubbling(e);
        return false;
    } else if (e.keyCode == 27) {
        hideSearchAssistant();
        _cancelBubbling(e);
        return false;
    } else if (e.keyCode == 13) {
        _$("searchAssistant").style.visibility = "hidden";
        setTimeout("processSearchAssist(null);", 1);
        _cancelBubbling(e);
        return false;
    }
}
function processSearchAssist(target) {
    var oSearchAssistant = _$("completeTable");
    if (!oSearchAssistant || oSearchAssistant.style.visbility == "hidden") {
        return;
    }
    if (_searchAssistIndex < -1) {
        return;
    }
    if (_searchAssistIndex > oSearchAssistant.rows.length) {
        return;
    }
    if (target && target != oSearchAssistant && !oSearchAssistant.contains(target)) {
        return;
    }
    var searchValue;
    if (_searchAssistIndex == -1) {
        var searchValue = _searchAssistInputField.value;
    } else {
        var searchValue = getTextValue(oSearchAssistant.rows[_searchAssistIndex].cells[1]);
    }
    hideSearchAssistant();
    _searchAssistInputField.value = searchValue;
    var oSearchForm = _searchAssistInputField;
    while (oSearchForm.tagName != "FORM") {
        oSearchForm = oSearchForm.parentNode;
    }
    oSearchForm.submit();
}
function handleSearchAssistKeyDown(e) {
    if (e.keyCode == 38 || e.keyCode == 40) {
        _cancelBubbling(e);
        return false;
    }
}
function handleHomeSearchInput(e) {
    if (!_searchAssistInitialized) {
        return;
    }
    e = getEventObject(e);
    if (_searchAssistInProgress) {
        return;
    }
    if (_searchAssistLastTerm && e && (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 27)) {
        return false;
    }
    var newTerm = _searchAssistInputField.value.toLowerCase();
    if (newTerm == _searchAssistLastTerm) {
        if (_$("completeTable").rows.length > 0) {
            showSearchAssistant();
        }
        return;
    }
    if (newTerm.length < 3) {
        _searchAssistLastTerm = null;
        hideSearchAssistant();
        clearSearchAssistant();
        return;
    }
    _searchAssistInProgress = true;
    _searchAssistLastTerm = newTerm;
    if (_searchAssistInputField.id == "home-search-input") {
        _searchAssistInputField.style.backgroundImage = "url(images/spinner.gif)";
    }
    jx.load("ajaxSearchAssistant.aspx?s=" + newTerm, handleHomeSearchInputPostback, "text", "get", true);
    function handleHomeSearchInputPostback(data) {
        if (_searchAssistInputField.id == "home-search-input") {
            _searchAssistInputField.style.backgroundImage = "none";
        }
        populateSearchAssistant(data);
        _searchAssistInProgress = false;
    }
}
function showSearchAssistant() {
    var oSearchAssistant = _$("searchAssistant");
    if (!oSearchAssistant) {
        return;
    }
    oSearchAssistant.style.visibility = "visible";
}
function hideSearchAssistant() {
    var oSearchAssistant = _$("completeTable");
    if (!oSearchAssistant) {
        return;
    }
    if (_searchAssistIndex >= 0 && _searchAssistIndex < oSearchAssistant.rows.length) {
        oSearchAssistant.rows[_searchAssistIndex].className = "wowdb-ac-a";
    }
    _searchAssistIndex = -1;
    _$("searchAssistant").style.visibility = "hidden";
}
function clearSearchAssistant() {
    _searchAssistIndex = -1;
    var oSearchAssistant = _$("completeTable");
    if (!oSearchAssistant) {
        return;
    }
    var rowCount = oSearchAssistant.rows.length;
    var oSearchAssistantBody = oSearchAssistant.getElementsByTagName("tbody")[0];
    for (var i = 0; i < rowCount; i++) {
        oSearchAssistantBody.removeChild(oSearchAssistant.rows[0]);
    }
}
function populateSearchAssistant(matches) {
    clearSearchAssistant();
    if (matches == "") {
        hideSearchAssistant();
        return;
    }
    var oSearchAssistant = _$("searchAssistant");
    var oSearchAssistantTable = _$("completeTable");
    var searchTerm = _searchAssistInputField.value;
    var arrMatches = matches.split("^");
    for (i = 0; i < arrMatches.length; i++) {
        var arrCurrentMatch = arrMatches[i].split("|");
        var matchName = arrCurrentMatch[0];
        var iconID = arrCurrentMatch[1];
        var arrMatchCount = arrCurrentMatch[2].split(",");
        var oNewRow = oSearchAssistantTable.insertRow( - 1);
        oNewRow.onmouseover = handleSearchAssistMouseOver;
        oNewRow.onmouseout = handleSearchAssistMouseOut;
        oNewRow.className = "wowdb-ac-a";
        var oIconCell = oNewRow.insertCell(0);
        oIconCell.className = "wowdb-ac-icon";
        var iconHTML = "";
        if (iconID != -1) {
            iconHTML = "<div class=\"iconsmall\" style=\"background-image:url(icons/s/" + iconID + ".gif);\">";
            iconHTML += "<div class=\"tile\"></div></div>";
        }
        oIconCell.innerHTML = iconHTML;
        var oNameCell = oNewRow.insertCell(1);
        oNameCell.className = "wowdb-ac-c";
        oNameCell.innerHTML = getHighlightedName(searchTerm, matchName);
        var oCountCell = oNewRow.insertCell(2);
        oCountCell.className = "wowdb-ac-d";
        var matchCountHTML = "";
        for (j = 0; j < arrMatchCount.length; j += 2) {
            var entityLabel = getEntityLabelPlural(arrMatchCount[j]);
            var entityCount = arrMatchCount[j + 1];
            matchCountHTML += ", " + entityLabel + ": " + entityCount;
        }
        oCountCell.innerHTML = matchCountHTML.substring(2);
    }
    positionSearchAssistant();
    showSearchAssistant();
}
function getTextValue(obj) {
    if (isIE) {
        return obj.innerText;
    } else {
        return obj.textContent;
    }
}
function getHighlightedName(searchTerm, matchingName) {
    searchTerm = searchTerm.toLowerCase();
    if (searchTerm.length > matchingName.length) {
        return matchingName;
    }
    if (searchTerm.toLowerCase() == matchingName) {
        return "<b>" + matchingName + "</b>";
    }
    for (var i = 0; i < searchTerm.length; i++) {
        if (searchTerm[i] != matchingName[i]) {
            break;
        }
    }
    var highlightedName = new String();
    return "<b>" + matchingName.substring(0, i) + "</b>" + matchingName.substring(i);
}
function handleSearchAssistMouseOver(e) {
    e = getEventObject(e);
    var targetObj = getEventTarget(e);
    while (targetObj.tagName != "TR") {
        targetObj = targetObj.parentNode;
    }
    targetObj.className = "wowdb-ac-b";
    _searchAssistIndex = targetObj.rowIndex;
}
function handleSearchAssistMouseOut(e) {
    e = getEventObject(e);
    var targetObj = getEventTarget(e);
    while (targetObj.tagName != "TR") {
        targetObj = targetObj.parentNode;
    }
    targetObj.className = "wowdb-ac-a";
}
function handleSearchAssistClick(e) {
    e = getEventObject(e);
    var targetObj = getEventTarget(e);
    processSearchAssist(targetObj);
    if (targetObj.id && targetObj == _searchAssistInputField) {
        var oSearchAssistantTable = _$("completeTable");
        if (oSearchAssistantTable.rows.length > 0) {
            showSearchAssistant();
        }
    } else {
        hideSearchAssistant();
    }
}
function initSearchAssistant(inputFieldID) {
    _searchAssistInputField = _$(inputFieldID);
    _addEventListener(_searchAssistInputField, "keypress", handleHomeKeyPress);
    _addEventListener(_searchAssistInputField, "keyup", handleHomeSearchInput);
    _addEventListener(document, "click", handleSearchAssistClick);
    var oNewDIV = document.createElement("div");
    oNewDIV.id = "searchAssistant";
    oNewDIV.className = "wowdb-ac-container";
    var oSearchAssistantTable = _$("completeTable");
    var oNewTable = document.createElement("table");
    oNewTable.id = "completeTable";
    oNewTable.className = "wowdb-ac-m";
    oNewDIV.appendChild(oNewTable);
    var oDIV = document.createElement("div");
    oDIV.className = "wowdb-ac-bg";
    oDIV.id = "searchAssistantBG";
    oNewDIV.appendChild(oDIV);
    var oDIV = document.createElement("em");
    oNewDIV.appendChild(oDIV);
    var oDIV = document.createElement("var");
    oNewDIV.appendChild(oDIV);
    var oDIV = document.createElement("strong");
    oNewDIV.appendChild(oDIV);
    document.body.appendChild(oNewDIV);
    oSearchAssistant = _$("searchAssistant");
    _searchAssistInitialized = true;
}
function positionSearchAssistant() {
    var oSearchAssistant = _$("searchAssistant");
    if (!oSearchAssistant) {
        return;
    }
    var oSearchAssistantTable = _$("completeTable");
    var oSearchAssistantBG = _$("searchAssistantBG");
    var oSearchInputPosition = getPosition(_searchAssistInputField);
    oSearchAssistant.style.top = oSearchInputPosition.y + 24 + "px";
    oSearchAssistant.style.left = oSearchInputPosition.x + 1 + "px";
    if (oSearchAssistantTable.offsetWidth < _searchAssistInputField.offsetWidth) {
        oSearchAssistantTable.style.width = _searchAssistInputField.offsetWidth - 1 + "px";
    } else {
        oSearchAssistantTable.style.width = "auto";
    }
}
function handleGlobalLoad(e) {
    _addEventListener(document, "mouseover", handleGlobalMouseOver);
    _addEventListener(document, "mouseout", handleGlobalMouseOut);
    _addEventListener(document, "mousedown", handleGlobalMouseDown);
    _addEventListener(document, "mouseup", handleGlobalMouseUp);
    _addEventListener(document, "click", handleGlobalClick);
    _addEventListener(document, "keyup", handleGlobalKeyPress);
    _addEventListener(window, "scroll", handleWindowScroll);
    if (!_userInitialized) {
        setUser();
    }
    setAds();
    setFirstVisit();
}
var lastScrollTop = 0;
function handleWindowScroll(e) {
    if (_$("previewPanel") && _$("previewPanel").style.display == "block") {
        var scrollDelta = (_scrollTop() - lastScrollTop);
        _$("previewPanel").style.top = (_$("previewPanel").offsetTop + scrollDelta) + "px";
    }
    lastScrollTop = _scrollTop();
}
function addMainMenu(containerID, menuArray) {
    var currentBrowseCategory = getQueryStringParam("browse").split(".")[0];
    var currentFilterString = getQueryStringParam("filters");
    if (currentFilterString != "") {
        currentFilterString = "&filters=" + currentFilterString;
    }
    if (!menuArray) return;
    for (var i = 0; i < menuArray.length; i++) {
        var currentMenu = menuArray[i];
        var anchor = document.createElement("A");
        if (i == menuArray.length - 1) {
            anchor.style.background = "none";
        }
        anchor.className = "spanMenuLink";
        anchor.style.cursor = "default";
        if (currentMenu[2] != null) {
            if (currentMenu[2] != "$nonav$") {
                anchor.href = currentMenu[2];
                anchor.style.cursor = "pointer";
            }
        } else {
            if (currentFilterString != "" && currentMenu[0] == currentBrowseCategory) {
                anchor.href = "search.aspx?browse=" + currentMenu[0] + currentFilterString;
            } else {
                anchor.href = "search.aspx?browse=" + currentMenu[0];
            }
            anchor.style.cursor = "pointer";
            anchor._baseLink = currentMenu[0];
        }
        anchor.style.textDecoration = "none";
        var span = document.createElement("SPAN");
        if (currentMenu.length >= 4) {
            anchor._subMenuArray = currentMenu[3];
        }
        if (currentMenu.length >= 5) {
            anchor.className = currentMenu[4];
        }
        span.innerHTML = currentMenu[1];
        anchor.appendChild(span);
        _addEventListener(anchor, "mouseover", handleMainMenuMouseOver);
        _addEventListener(anchor, "mouseout", handleMainMenuMouseOut);
        _addEventListener(anchor, "click", closeMainMenu);
        _$(containerID).appendChild(anchor);
    }
}
function addBrowseMenu(containerID, menuArray) {
    if (!menuArray) {
        return;
    }
    var currentFilterString = getQueryStringParam("filters");
    if (currentFilterString != "") {
        currentFilterString = "&filters=" + currentFilterString;
    }
    var currentBrowsePath;
    var span = document.createElement("SPAN");
    span.className = "spanMenuArrow";
    var anchor = document.createElement("A");
    anchor.style.textDecoration = "none";
    anchor.style.color = "#ffffff";
    anchor.href = "./";
    anchor.innerHTML = "Home";
    span.appendChild(anchor);
    _$(containerID).appendChild(span);
    var currentMenu;
    currentBrowsePath = "";
    for (var i = 0; i < menuArray.length; i++) {
        currentMenu = menuArray[i];
        span = document.createElement("SPAN");
        anchor = document.createElement("A");
        anchor.style.cursor = "default";
        if (i < menuArray.length - 1) {
            span.className = "spanMenuArrow";
        }
        if (i > 0) {
            currentBrowsePath += "." + currentMenu[0];
        } else {
            currentBrowsePath = currentMenu[0] + "";
        }
        anchor.className = "spanBrowseMenuLink";
        if (currentMenu[2] != null) {
            if (currentMenu[2] != "$nonav$") {
                anchor.href = currentMenu[2];
                anchor.style.cursor = "pointer";
            }
            currentLink = "";
        } else {
            anchor.href = "search.aspx?browse=" + currentBrowsePath + currentFilterString;
            anchor.style.cursor = "pointer";
        }
        anchor.innerHTML = currentMenu[1];
        anchor._baseLink = currentBrowsePath;
        var arrayKey = "mn_" + currentBrowsePath.replace(/\./g, "_").replace(/\-/g, "$");
        var subMenuArray = null;
        try {
            subMenuArray = eval(arrayKey);
        } catch(ex) {}
        if (subMenuArray) {
            anchor._subMenuArray = subMenuArray;
        }
        span.appendChild(anchor);
        _addEventListener(anchor, "mouseover", handleMainMenuMouseOver);
        _addEventListener(anchor, "mouseout", handleMainMenuMouseOut);
        _$(containerID).appendChild(span);
    }
    _$("divMainPrecontents").style.display = "block";
}
function handleMainMenuMouseOver(e) {
    if (typeof getEventTarget == "undefined") {
        return;
    }
    var targetObj = getEventTarget(e);
    if (targetObj.tagName != "A" && targetObj.tagName != "SPAN") {
        return;
    }
    if (_$("inputSearchFilterName")) {
        _$("inputSearchFilterName").blur();
    }
    if (targetObj != _menuTimeoutPointer) {
        hideSubMenu();
        hideMainMenu();
    }
    while (targetObj.tagName != "A") {
        targetObj = targetObj.parentNode;
    }
    if (targetObj.className == "spanMenuLink") {}
    if (targetObj._subMenu && targetObj._subMenu.style.display == "block") {
        return;
    }
    var viewportHeight = Client.viewportHeight();
    if (targetObj._subMenu && targetObj._subMenuArray) {
        if (targetObj._viewportHeight && targetObj._viewportHeight != viewportHeight) {
            var menuParent = targetObj._subMenu.parentNode;
            menuParent.removeChild(targetObj._subMenu);
            targetObj._subMenu = null;
        }
    }
    if (!targetObj._subMenu && targetObj._subMenuArray) {
        var subMenu = addMenu("layers", targetObj._subMenuArray, targetObj, targetObj._baseLink);
        targetObj._subMenu = subMenu;
        targetObj._viewportHeight = viewportHeight;
    }
    if (targetObj._subMenu) {
        positionSubMenu(targetObj);
    }
}
function closeMainMenu(e) {
    var targetObj = getEventTarget(e);
    _menuTimeoutPointer = targetObj;
    hideMainMenu();
}
function handleMainMenuMouseOut(e) {
    if (typeof getEventTarget == "undefined") {
        return;
    }
    var targetObj = getEventTarget(e);
    while (targetObj.tagName != "A") {
        targetObj = targetObj.parentNode;
    }
    var relatedTarget = getEventRelatedTarget(e);
    if (relatedTarget && targetObj._subMenu && targetObj._subMenu.contains(relatedTarget)) {
        return;
    }
    if (relatedTarget && targetObj && targetObj.contains(relatedTarget)) {
        return;
    }
    if (_menuTimeout == null) {
        _menuTimeoutPointer = targetObj;
        _menuTimeout = setTimeout(hideMainMenu, 300);
    }
}
function hideMainMenu() {
    if (!_menuTimeoutPointer || !_menuTimeoutPointer.style) {
        return;
    }
    _menuTimeoutPointer.style.backgroundColor = "";
    _menuTimeoutPointer.style.borderColor = "";
    var menuCollection = getElementsByClassName("menu", "DIV", _$("layers"));
    for (var i = 0; i < menuCollection.length; i++) {
        menuCollection[i].style.display = "none";
    }
    _menuTimeout = null;
    _menuTimeoutPointer = null;
}
function addMenu(containerID, menuArray, objParentMenu, baseID) {
    var arrBrowseArray = _searchBrowseString.split(".");
    var parentCategory = arrBrowseArray[0];
    var filterString = getQueryStringParam("filters");
    if (filterString != "") {
        filterString = "&filters=" + filterString;
    }
    var viewportHeight = Client.viewportHeight();
    var heightPerMenuItem;
    var parentMenuItemPosition = getPosition(objParentMenu);
    var newMenu = document.createElement("div");
    _addEventListener(newMenu, "mouseover", expandMenu);
    _addEventListener(newMenu, "mouseout", contractMenu);
    _addEventListener(newMenu, "click", contractMenu);
    newMenu.className = "menu";
    if (objParentMenu) {
        newMenu._parentMenu = objParentMenu;
    }
    if (objParentMenu._parentMenu && objParentMenu._parentMenu._subMenuArray) {
        heightPerMenuItem = Math.ceil(objParentMenu.offsetHeight / objParentMenu._parentMenu._subMenuArray.length);
    }
    heightPerMenuItem = 27;
    var menuContainerHeight;
    if (objParentMenu.className == "spanBrowseMenuLink") {
        var parentPosition = getPosition(objParentMenu);
        menuContainerHeight = parentPosition.y + objParentMenu.offsetHeight + (heightPerMenuItem * menuArray.length);
    } else {
        menuContainerHeight = (heightPerMenuItem * menuArray.length);
    }
    var itemsPerContainer;
    if (menuContainerHeight > viewportHeight) {
        menuGroupCount = Math.ceil(menuContainerHeight / viewportHeight);
        itemsPerContainer = Math.ceil(menuArray.length / menuGroupCount);
    } else {
        newMenu.appendChild(document.createElement("em"));
        newMenu.appendChild(document.createElement("var"));
        newMenu.appendChild(document.createElement("strong"));
        menuGroupCount = 1;
        itemsPerContainer = menuArray.length;
    }
    var mainTable = document.createElement("table");
    newMenu.appendChild(mainTable);
    var tbody;
    if (mainTable.tBodies && mainTable.tBodies.length > 0) {
        tbody = mainTable.tBodies[0];
    } else {
        tbody = document.createElement("tbody");
        mainTable.appendChild(tbody);
    }
    var mainRow = tbody.insertRow(tbody.rows.length);
    for (var k = 0; k < menuGroupCount; k++) {
        var mainCell = mainRow.insertCell(mainRow.cells.length);
        var itemsStart;
        var mainDiv = null;
        if (menuGroupCount > 1) {
            var mainParagraph = document.createElement("p");
            mainCell.appendChild(mainParagraph);
            mainParagraph.appendChild(document.createElement("em"));
            mainParagraph.appendChild(document.createElement("var"));
            mainParagraph.appendChild(document.createElement("strong"));
            mainDiv = document.createElement("div");
            mainParagraph.appendChild(mainDiv);
            itemsStart = k * itemsPerContainer;
        } else {
            itemsStart = 0;
            mainDiv = document.createElement("div");
            mainCell.appendChild(mainDiv);
        }
        var secondDiv = document.createElement("div");
        mainDiv.appendChild(secondDiv);
        for (var i = 0; i < itemsPerContainer; i++) {
            var currentMenuIndex;
            currentMenuIndex = i;
            if (k > 0) {
                currentMenuIndex = i + (itemsPerContainer * k);
            }
            if (currentMenuIndex >= menuArray.length) {
                break;
            }
            var currentMenu = menuArray[currentMenuIndex];
            if (currentMenu) {
                var currentLink = "";
                var hasChildren = false;
                if (isArray(currentMenu)) {
                    if (currentMenu.length == 4) {
                        hasChildren = true;
                    }
                    currentLink = currentMenu[0] + "";
                } else {
                    currentLink = i + "";
                }
                var anchor = document.createElement("a");
                if (currentMenu[2] != null) {
                    if (currentMenu[2] != "$nonav$") {
                        anchor.href = currentMenu[2].replace(/\$userid\$/, _userID);;
                    }
                    currentLink = "";
                } else {
                    if (baseID != null) {
                        currentLink = baseID + "." + currentLink;
                    }
                    if (filterString != "" && currentLink.split(".")[0] == parentCategory) {
                        anchor.href = "search.aspx?browse=" + currentLink + filterString;
                    } else {
                        anchor.href = "search.aspx?browse=" + currentLink;
                    }
                }
                anchor._parentElement = newMenu;
                secondDiv.appendChild(anchor);
                var mainSpan = document.createElement("span");
                if (hasChildren) {
                    mainSpan.className = "menusub";
                }
                anchor.appendChild(mainSpan);
                var secondSpan = document.createElement("span");
                if (!isArray(currentMenu)) {
                    secondSpan.innerHTML = currentMenu;
                } else {
                    secondSpan.innerHTML = currentMenu[1];
                }
                if (currentLink != "") {
                    var arrCurrentLink;
                    arrCurrentLink = currentLink.split(".");
                    var blnMenuChecked = true;
                    for (var j = 0; j < arrCurrentLink.length; j++) {
                        if (arrCurrentLink[j] != arrBrowseArray[j]) {
                            blnMenuChecked = false;
                        }
                    }
                    if (blnMenuChecked) {
                        secondSpan.className = "menucheck";
                    }
                }
                anchor._baseLink = currentLink;
                mainSpan.appendChild(secondSpan);
                if (hasChildren) {
                    anchor._subMenuArray = currentMenu[3];
                } else {}
            }
        }
    }
    _$(containerID).appendChild(newMenu);
    return newMenu;
}
function expandMenu(e) {
    var viewportHeight = Client.viewportHeight();
    var targetObj = getEventTarget(e);
    if (targetObj.tagName != "A" && targetObj.tagName != "SPAN") {
        return;
    }
    while (targetObj.tagName != "A") {
        targetObj = targetObj.parentNode;
    }
    var menuObj = targetObj;
    while (menuObj.className != "menu") {
        menuObj = menuObj.parentNode;
    }
    if (_menuTimeoutPointer && _menuTimeoutPointer._subMenu == menuObj) {
        window.clearTimeout(_menuTimeout);
        _menuTimeout = null;
    }
    if (_subMenuTimeoutPointer && _subMenuTimeoutPointer == menuObj) {
        window.clearTimeout(_subMenuTimeout);
        _subMenuTimeout = null;
    }
    if (targetObj._subMenu && targetObj._subMenu.style.display == "block") {
        return;
    }
    if (targetObj._subMenu && targetObj._subMenuArray) {
        if (targetObj._viewportHeight && targetObj._viewportHeight != viewportHeight) {
            var menuParent = targetObj._subMenu.parentNode;
            menuParent.removeChild(targetObj._subMenu);
            targetObj._subMenu = null;
        }
    }
    if (!targetObj._subMenu) {
        if (targetObj._subMenuArray) {
            var subMenu = addMenu("layers", targetObj._subMenuArray, menuObj, targetObj._baseLink);
            targetObj._subMenu = subMenu;
            targetObj._viewportHeight = viewportHeight;
        }
    }
    if (targetObj._subMenu) {
        positionSubMenu(targetObj);
    }
}
function positionSubMenu(objParentMenu) {
    var objSubMenu = objParentMenu._subMenu;
    var parentMenuItemPosition = getPosition(objParentMenu);
    var parentTop = parentMenuItemPosition.y - 2;
    var viewportHeight = Client.viewportHeight();
    if (objParentMenu.className == "spanMenuLink" || objParentMenu.className == "spanBrowseMenuLink") {
        objSubMenu.style.visibility = "hidden";
        objSubMenu.style.display = "block";
        if (objSubMenu.offsetHeight + parentTop > viewportHeight) {
            objSubMenu.style.top = (parentTop - objSubMenu.offsetHeight - objParentMenu.offsetHeight + 10) + "px";
        } else {
            objSubMenu.style.top = parentMenuItemPosition.y + objParentMenu.offsetHeight + 2 + "px";
        }
        objSubMenu.style.left = parentMenuItemPosition.x + "px";
        objSubMenu.style.visibility = "visible";
    } else if (parentMenuItemPosition.y > 0) {
        objSubMenu.style.visibility = "hidden";
        objSubMenu.style.display = "block";
        var menuPad = 10;
        if (isIE) {
            menuPad = 20;
        }
        if (menuPad + objSubMenu.offsetHeight + parentTop > viewportHeight) {
            objSubMenu.style.top = (viewportHeight - objSubMenu.offsetHeight - menuPad) + "px";
        } else {
            objSubMenu.style.top = parentTop + "px";
        }
        objSubMenu.style.left = parentMenuItemPosition.x + objParentMenu.offsetWidth - 2 + "px";
        objSubMenu.style.visibility = "visible";
    }
}
function handleMainMenuMouseOut(e) {
    if (typeof getEventTarget == "undefined") {
        return;
    }
    var targetObj = getEventTarget(e);
    while (targetObj.tagName != "A") {
        targetObj = targetObj.parentNode;
    }
    var relatedTarget = getEventRelatedTarget(e);
    if (relatedTarget && targetObj._subMenu && targetObj._subMenu.contains(relatedTarget)) {
        return;
    }
    if (relatedTarget && targetObj && targetObj.contains(relatedTarget)) {
        return;
    }
    if (_menuTimeout == null) {
        _menuTimeoutPointer = targetObj;
        _menuTimeout = setTimeout(hideMainMenu, 300);
    }
}
var _subMenuTimeoutPointer = null;
var _subMenuTimeout = null;
function hideSubMenu() {
    if (!_subMenuTimeoutPointer) {
        return;
    }
    _subMenuTimeoutPointer.style.display = "none";
    if (_subMenuTimeoutPointer._parentMenu) {
        hideAllParents(_subMenuTimeoutPointer._parentMenu);
    }
    _subMenuTimeoutPointer = null;
    _subMenuTimeout = null;
}
function contractMenu(e) {
    var targetObj = getEventTarget(e);
    var relatedTarget = getEventRelatedTarget(e);
    if (targetObj.tagName == "SPAN") {
        while (targetObj.tagName != "A") {
            targetObj = targetObj.parentNode;
        }
    }
    if (targetObj.tagName == "A") {
        if (targetObj._subMenu) {
            if (relatedTarget == targetObj._subMenu || targetObj._subMenu.contains(relatedTarget)) {
                return;
            } else {
                targetObj._subMenu.style.display = "none";
            }
        }
    }
    loopCounter = 0;
    while (targetObj.className != "menu") {
        loopCounter += 1;
        targetObj = targetObj.parentNode;
    }
    if (!relatedTarget) {
        targetObj.style.display = "none";
        if (targetObj._parentMenu) {
            hideAllParents(targetObj._parentMenu);
        }
        return;
    }
    if (targetObj.contains(relatedTarget)) {
        return;
    }
    if (relatedTarget._subMenu && relatedTarget.subMenu == targetObj) {
        return;
    }
    if (targetObj._parentMenu && targetObj._parentMenu.contains(relatedTarget)) {
        targetObj.style.display = "none";
        return;
    }
    if (_subMenuTimeout == null) {
        _subMenuTimeoutPointer = targetObj;
        _subMenuTimeout = setTimeout(hideSubMenu, 300);
    }
    return;
}
function hideAllParents(targetObj) {
    if (!targetObj) {
        return;
    }
    if (!targetObj._parentMenu) {
        if (targetObj.className == "spanMenuLink") {
            targetObj.style.backgroundColor = "";
            targetObj.style.borderColor = "";
        }
        return;
    }
    targetObj.style.display = "none";
    hideAllParents(targetObj._parentMenu);
}
function submitSearch(srcObject) {
    if (_$("titleSearchInput").value.trim().length < 3) {
        displayAlert('To perform a full text search, you must enter at least 3 letters.');
        return false;
    }
    if (srcObject) {
        srcObject.parentNode.submit();
    }
    return true;
}
function showItemComparisonTable(classid) {
    if (classid == -1) classid = document.getElementById("compareTypeSelect").value;
    var compTable = document.getElementById("itemCompareTable");
    while (compTable.childNodes[1].childNodes[1] != null) compTable.childNodes[1].removeChild(compTable.childNodes[1].childNodes[1]);
    for (var i = 0; i < compdata[classid].length; i++) {
        var row = document.createElement('tr');
        row.onmouseout = "handleRowMouseOut(event);";
        row.onmouseover = "handleRowMouseOver(event);";
        for (var j = 0; j < compdata[classid][i].length; j++) {
            var cell = document.createElement('td');
            cell.align = "center";
            var txt = document.createTextNode(compdata[classid][i][j]);
            cell.appendChild(txt);
            row.appendChild(cell);
        }
        compTable.childNodes[1].appendChild(row);
    }
}
function fixIE6() {
    var objWrapper = _$("divWrapper");
    if (!objWrapper) {
        return;
    }
    objWrapper.style.width = _$("main-layout-center").offsetWidth - 10 + "px";
    var colContentPanels = getElementsByClassName("content-panel", "DIV");
    for (var i = 0; i < colContentPanels.length; i++) {
        var objContentPanel = colContentPanels[i];
        var objContentPanelBG = getElementsByClassName("content-panel-bg", "DIV", objContentPanel, true);
        if (objContentPanel.offsetHeight > 0) {
            objContentPanelBG.style.height = objContentPanel.offsetHeight - 15 + "px";
            var objContentPanelRight = getElementsByClassName("content-panel-r", "DIV", objContentPanel, true);
            objContentPanelRight.style.height = objContentPanel.offsetHeight - 15 + "px";
        }
    }
}
function buildFieldSelects() {
    var labelSelect = _$("fieldIDSelect");
    for (var i = 0; i < labeldata.length; i++) {
        var opt = document.createElement("option");
        opt.value = i;
        opt.innerHTML = labeldata[i].key;
        labelSelect.appendChild(opt);
    }
}
function showFieldLabel() {
    var lang = _$("langSelect").value;
    var ind = _$("fieldIDSelect").selectedIndex - 1;
    var label = labeldata[ind];
    _$("engTxt").innerHTML = label.txt_en;
    _$("transText").value = label["txt_" + lang];
}
function submitLabelEdit() {
    var lang = _$("langSelect").value;
    var ind = _$("fieldIDSelect").selectedIndex - 1;
    var label = labeldata[ind];
    jx.load("ajaxEditLabel.aspx?lang=" + lang + "&key=" + label.key + "&label=" + _$("transText").value,
    function(data) {
        handleLabelEdit(data, lang, _$("transText").value, ind);
    },
    'text', 'post');
    _$("editFieldButton").value = "Submitting...";
}
function handleLabelEdit(data, lang, val, ind) {
	if (data == "1") {
		labeldata[ind]["txt_" + lang] = val;
		displayAlert("Label Edit Successful");
	} else displayAlert("Error Editing Label");
		_$("editFieldButton").value = "Submit";
}
function sendBuildAction(bid, action) {
    jx.load("ajaxBuildSubmit.aspx?buildid=" + bid + "&action=" + action,
    function(data) {
        handleBuildAction(data, bid, action);
    },
    "text", "post");
}
function handleBuildAction(data, bid, action) {
    if (data == "0") {
        displayAlert("Error Approving Build");
    } else {
        var tbl = _$("talentBuildTable");
        for (var i = 0; i < tbl.rows.length; i++) {
            if (tbl.rows[i].id == "buildRow" + bid) {
                tbl.tBodies[0].deleteRow(i);
                break;
            }
        }
        if (action == 'approve') displayAlert("Build Approved");
        else displayAlert("Build Denied");
    }
}
function loadSearchAssistant() {
    if (_$("home-search-input")) {
        initSearchAssistant("home-search-input");
    } else if (_$("titleSearchInput")) {
        initSearchAssistant("titleSearchInput");
    }
}
if (isIE6) {
    _addEventListener(window, "load", fixIE6);
    _addEventListener(window, "resize", fixIE6);
}
loadSearchAssistant();
_addEventListener(window, "load", handleGlobalLoad);
_addEventListener(document, "mousemove", handleGlobalMouseMove);
function submitFilter() {
    var filterString = "";
    var objFilterForm = document.getElementById("searchFilterForm");
    var searchFilterCollection = objFilterForm.elements;
    var filtersProcessed = [];
    for (var i = 0; i < searchFilterCollection.length; i++) {
        if (searchFilterCollection[i].name == "") {
            continue;
        }
        if (filtersProcessed.indexOf(searchFilterCollection[i].name) >= 0) {
            continue;
        }
        if (searchFilterCollection[i].multiple) {
            var multiSelectValues = "";
            for (var j = 0; j < searchFilterCollection[i].options.length; j++) {
                if (searchFilterCollection[i].options[j].selected && searchFilterCollection[i].options[j].value != "") {
                    multiSelectValues += "," + searchFilterCollection[i].options[j].value;
                }
            }
            if (multiSelectValues != "") {
                multiSelectValues = multiSelectValues.substring(1);
                filterString += ";" + searchFilterCollection[i].name + "=" + multiSelectValues;
            }
        } else {
            if ((searchFilterCollection[i].type == "checkbox" && searchFilterCollection[i].checked) || (searchFilterCollection[i].type != "checkbox" && searchFilterCollection[i].value != "")) {
                filterString += ";" + searchFilterCollection[i].name + "=" + searchFilterCollection[i].value;
            }
        }
        filtersProcessed.push(searchFilterCollection[i].name);
    }
    filterString = filterString.substring(1);
    if (filterString != "") {
        filterString = "&filters=" + filterString;
    }
    var browseKey = getBrowseKey();
    if (!browseKey) {
        return;
    }
    self.location = "search.aspx?browse=" + browseKey + filterString;
}
function removeFilter() {
    var browseKey = getBrowseKey();
    if (!browseKey) {
        return;
    }
    self.location = "search.aspx?browse=" + browseKey;
}
function addSearchFilterInput(selectObj) {
    var selectedOption = selectObj.options[selectObj.selectedIndex];
    var searchFilterID = selectedOption.value;
    var cellInputContainer = selectObj.parentNode.parentNode.cells[1];
    var cellLinkContainer = selectObj.parentNode.parentNode.cells[2];
    var numChildNodes = cellInputContainer.childNodes.length;
    for (var i = 0; i < numChildNodes; i++) {
        cellInputContainer.removeChild(cellInputContainer.childNodes[0]);
    }
    if (selectObj.selectedIndex == 0) {
        cellLinkContainer.innerHTML = "";
        return;
    }
    if (selectedOption.className == "inputRange") {
        var minRangeID = searchFilterID.split(",")[0];
        var maxRangeID = searchFilterID.split(",")[1];
        var minRangeInput = document.createElement("input");
        minRangeInput.type = "text";
        minRangeInput.name = minRangeID;
        minRangeInput.size = "4";
        minRangeInput.style.textAlign = "center";
        minRangeInput.value = "1";
        cellInputContainer.appendChild(minRangeInput);
        var rangeLabel = document.createElement("span");
        rangeLabel.innerHTML = " to ";
        cellInputContainer.appendChild(rangeLabel);
        var maxRangeInput = document.createElement("input");
        maxRangeInput.type = "text";
        maxRangeInput.name = maxRangeID;
        maxRangeInput.size = "4";
        maxRangeInput.style.textAlign = "center";
        cellInputContainer.appendChild(maxRangeInput);
    } else if (selectedOption.className == "inputLookup") {
        cellInputContainer.appendChild(getLookupSelectBox(selectedOption.id, selectedOption.value, null, true));
    } else if (selectedOption.className == "inputText") {
        var textInput = document.createElement("input");
        textInput.type = "text";
        textInput.name = searchFilterID;
        textInput.size = "20";
        cellInputContainer.appendChild(textInput);
    }
    cellLinkContainer.innerHTML = "<button class=\"smallButton\" onclick=\"removeSearchFilter(this);\"><span>Remove</span></button>";
}
function toggleModeBox(locationBox, modeBoxID) {
    var lookupList = _objLookups.LookupList("location_has_modes");
    var currentLocationID = locationBox.options[locationBox.selectedIndex].value;
    var locationFound = false;
    for (var i = 0; i < lookupList.length; i++) {
        if (lookupList[i].indexOf(currentLocationID) >= 0) {
            locationFound = true;
            break;
        }
    }
    if (!locationFound && currentLocationID != "") {
        _$("fi_" + modeBoxID).selectedIndex = -1;
        _$("fi_" + modeBoxID).disabled = true;
    } else {
        _$("fi_" + modeBoxID).style.display = "inline";
        _$("fi_" + modeBoxID).disabled = false;
    }
}
function addSearchFilter() {
    var objFilterTable = document.getElementById("tableAdditionalFilters");
    var cloneFilterRow = objFilterTable.rows[0].cloneNode(true);
    objFilterTable.tBodies[0].appendChild(cloneFilterRow);
    cloneFilterRow.cells[1].innerHTML = "";
    return cloneFilterRow.cells[0];
}
function removeSearchFilter(removeLink) {
    var objFilterTable = document.getElementById("tableAdditionalFilters");
    if (objFilterTable.rows.length == 1) {
        objFilterTable.rows[0].cells[0].childNodes[0].selectedIndex = 0;
        addSearchFilterInput(objFilterTable.rows[0].cells[0].childNodes[0]);
        return;
    } else {
        var parentRow = removeLink.parentNode.parentNode;
        objFilterTable.deleteRow(parentRow.rowIndex);
    }
}
