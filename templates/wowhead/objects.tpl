{config_load file="$conf_file"}

{include file='header.tpl'}

		<div id="main">
			<div id="main-precontents"></div>
			<div id="main-contents" class="main-contents">
				<script type="text/javascript">
				g_initPath({$page.path});
				</script>

				<div id="lv-objects" class="listview"></div>

				<script type="text/javascript">
					{* Перебор всех данных из массива data, переданного шаблонизатору для составления списка объектов *}
					{include file='bricks/object_table.tpl' id='objects' data=$data}
				</script>

				<div class="listview-void">
					{* Ещё один перебор для чего - хз *}
					{section name=i loop=$data}
						<a href="/?object={$data[i].entry}">{$data[i].name}</a>
					{/section}
				</div>

				<div class="clear"></div>
			</div>
		</div>

{include file='footer.tpl'}
