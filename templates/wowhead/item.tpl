{include file='header.tpl'}

		<div id="main">

			<div id="main-precontents"></div>
			<div id="main-contents" class="main-contents">

				<script type="text/javascript">
					{include file='bricks/allcomments.tpl'}
					var g_pageInfo = {ldelim}type: {$page.type}, typeId: {$page.typeid}, name: '{$item.name|escape:"quotes"}'{rdelim};
					g_initPath({$page.path});
				</script>

				<table class="infobox">
					<tr><th>{#facts#}</th></tr>
					<tr><td>
						<div class="infobox-spacer"></div>
						<ul>
							{* Уровень вещи *}
							<li><div>{#level#}: {$item.level}</div></li>
							{* Стоимость вещи *}
							{if $item.buygold or $item.buysilver or $item.buycopper}
								<li><div>
									{#Buy_for#}:
									{if $item.buygold}<span class="moneygold">{$item.buygold}</span>{/if}
									{if $item.buysilver}<span class="moneysilver">{$item.buysilver}</span>{/if}
									{if $item.buycopper}<span class="moneycopper">{$item.buycopper}</span>{/if}
								</div></li>
							{/if}
							{if $item.sellgold or $item.sellsilver or $item.sellcopper}
								<li><div>
									{#Sells_for#}:
									{if $item.sellgold}<span class="moneygold">{$item.sellgold}</span>{/if}
									{if $item.sellsilver}<span class="moneysilver">{$item.sellsilver}</span>{/if}
									{if $item.sellcopper}<span class="moneycopper">{$item.sellcopper}</span>{/if}
								</div></li>
							{/if}
							{if $item.disenchantskill}<li><div>{#Disenchantable#} (<span class="tip" onmouseover="Tooltip.showAtCursor(event, '{#Required_enchanting_skill#}', 0, 0, 'q')" onmousemove="Tooltip.cursorUpdate(event)" onmouseout="Tooltip.hide()">{$item.disenchantskill}</span>)</div></li>{/if}
							{if $item.key}<li><div>{#Can_be_placed_in_the_keyring#}</div></li>{/if}
						</ul>
					</td></tr>
				</table>

				<div class="text">

					<h1>{$item.name}</h1>

					<div id="ic{$item.id}" style="float: left"></div>
					<div id="tt{$item.id}" class="tooltip" style="float: left; padding-top: 1px">
					<table><tr><td>{$item.info}</td><th style="background-position: top right"></th></tr><tr><th style="background-position: bottom left"></th><th style="background-position: bottom right"></th></tr></table>
					</div>

					<div style="clear: left"></div>

					<script type="text/javascript">
						ge('ic{$item.id}').appendChild(Icon.create('{$item.iconname}', 2, 0, 0, {$item.stackable}));
						Tooltip.fix(ge('tt{$item.id}'), 1, 1);
					</script>

					<h2>{#Related#}</h2>

				</div>

				<div id="jkbfksdbl4"></div>
				<div id="lkljbjkb574" class="listview"></div>
				<script type="text/javascript">

					{* Иконки, подсказки для вещей *}
					{if $allitems}
						{include file='bricks/allitems_table.tpl' data=$allitems}
					{/if}

					{* Иконки, подсказки для спеллов *}
					{if $allspells}
						{include file='bricks/allspells_table.tpl' data=$allspells}
					{/if}

					var tabsRelated = new Tabs({ldelim}parent: ge('jkbfksdbl4'){rdelim});

					{* Список локаций где ловится *}
					{if $item.fishedin}
						{include file='bricks/fished-in.tpl' data=$item.fishedin name=#Fished_in#}
					{/if}

					{* Список мобов с которых падает вещь *}
					{if $droppedby}
						{include file='bricks/creature_table.tpl' id='dropped-by' tabsid='tabsRelated' data=$droppedby name=#droppedby#}
					{/if}

					{* Список мобов, которые продают вещь *}
					{if $soldby}
						{include file='bricks/creature_table.tpl' id='sold-by' tabsid='tabsRelated' data=$soldby name=#soldby#}
					{/if}

					{* Обучает *}
					{if $item.teaches}
						{include file='bricks/spell_table.tpl' id='teaches-recipe' tabsid='tabsRelated' data=$item.teaches name=#Teaches#}
					{/if}

					{* Список игровых объектов в которых содержится вещь*}
					{if $containedinobject}
						{include file='bricks/object_table.tpl' id='contained-in-object' tabsid='tabsRelated' data=$containedinobject name=#containedinobject#}
					{/if}

					{* MineralVeins *}
					{if $minedfromobject}
						{include file='bricks/object_table.tpl' id='mined-from-object' tabsid='tabsRelated' data=$minedfromobject name=#minedfromobject#}
					{/if}

					{* Herbs *}
					{if $gatheredfromobject}
						{include file='bricks/object_table.tpl' id='gathered-from-object' tabsid='tabsRelated' data=$gatheredfromobject name=#gatheredfromobject#}
					{/if}

					{* Список вещей в которых содержится вещь*}
					{if $containedinitem}
						{include file='bricks/item_table.tpl' id='contained-in-item' tabsid='tabsRelated' data=$containedinitem name=#containedinitem#}
					{/if}

					{* Список вещей, которые содержатся в ней *}
					{if $item.contains}
						{include file='bricks/item_table.tpl' id='contains' tabsid='tabsRelated' data=$item.contains name=#Contains#}
					{/if}

					{* Список созданий у которых воруется вещь*}
					{if $pickpocketingloot}
						{include file='bricks/creature_table.tpl' id='pick-pocketed-from' tabsid='tabsRelated' data=$pickpocketingloot name=#pickpocketingloot#}
					{/if}

					{* Список созданий с которых сдирается эта шкура*}
					{if $skinnedfrom}
						{include file='bricks/creature_table.tpl' id='skinned-from' tabsid='tabsRelated' data=$skinnedfrom name=#skinnedfrom#}
					{/if}

					{* Список вещей из которых перерабатывается вещь *}
					{if $prospectingloot}
						{include file='bricks/item_table.tpl' id='prospected-from' tabsid='tabsRelated' data=$prospectingloot name=#prospectedfrom#}
					{/if}

					{* Список вещей в которые можно положить эту вещь *}
					{if $canbeplacedin}
						{include file='bricks/item_table.tpl' id='can-be-placed-in' tabsid='tabsRelated' data=$canbeplacedin name=#canbeplacedin#}
					{/if}

					{* Квесты, которым нужен этот предмет *}
					{if $objectiveof}
						{include file='bricks/quest_table.tpl' id='objective-of' tabsid='tabsRelated' data=$objectiveof name=#objectiveof#}
					{/if}

					{if $starts}
						{include file='bricks/quest_table.tpl' id='starts' tabsid='tabsRelated' data=$starts name=#starts#}
					{/if}

					{* Квесты, наградой за которые, является этот предмет *}
					{if $rewardof}
					 	{include file='bricks/quest_table.tpl' id='reward-of' tabsid='tabsRelated' data=$rewardof name=#rewardof#}
					{/if}

					{* Реагентом для чего является *}
					{if $reagentfor}
					 	{include file='bricks/spell_table.tpl' id='reagent-for' tabsid='tabsRelated' data=$reagentfor name=#reagentfor#}
					{/if}

					{* Создается из *}
					{if $createdfrom}
					 	{include file='bricks/created-by.tpl' data=$createdfrom}
					{/if}

					{* Дизэнчант *}
					{* Список вещей в которые перерабатывается вещь *}
					{if $item.disenchanting}
						{include file='bricks/item_table.tpl' id='disenchanting' tabsid='tabsRelated' data=$item.disenchanting name=#disenchanting#}
					{/if}
					{* Список вещей, из которых, перерабатывается вещь *}
					{if $item.disenchantedfrom}
						{include file='bricks/item_table.tpl' id='disenchanting' tabsid='tabsRelated' data=$item.disenchantedfrom name=#Disenchanted_from#}
					{/if}
					new Listview({ldelim}template: 'comment', id: 'comments', name: '{#Comments#}', tabs: tabsRelated, parent: 'lkljbjkb574', data: lv_comments{rdelim});
					tabsRelated.flush();
				</script>

				{include file='bricks/contribute.tpl'}

			</div>
		</div>
		
{include file='footer.tpl'}