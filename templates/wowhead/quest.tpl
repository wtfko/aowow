{config_load file="$conf_file"}

{include file='header.tpl'}

		<div id="main">

			<div id="main-precontents"></div>

			<div id="main-contents" class="main-contents">
				<script type="text/javascript">
					{include file='bricks/allcomments.tpl'}
					var g_pageInfo = {ldelim}type: 5, typeId: {$quest.id}, name: '{$quest.name|escape:"quotes"}'{rdelim};
					g_initPath([0,3,{$quest.type},{$quest.category}]);
				</script>

				{if $allitems}
				<script type="text/javascript">
					{include file='bricks/allitems_table.tpl' data=$allitems}
				</script>
				{/if}

				<table class="infobox">
					<tr><th>{#Quick_Facts#}</th></tr>
					<tr><td>
						<div class="infobox-spacer"></div>
						<ul>
							<li><div>{#Level#}: {$quest.level}</div></li>
							<li><div>{#Requires_level#}: {$quest.reqlevel}</div></li>
							{if $quest.typename}<li><div>{#Type#}: {$quest.typename}</div></li>{/if}
							{if $quest.sidename}<li><div>{#Side#}: {$quest.sidename}</div></li>{/if}
							<li><div>{#Start#}: <a href="?{$quest.start.type}={$quest.start.id}">{$quest.start.name}</a></div></li>
							<li><div>{#End#}: <a href="?{$quest.end.type}={$quest.end.id}">{$quest.end.name}</a></div></li>
							{* <li><div>{#Sharable#}</div></li> *}
						</ul>
					</td></tr>

					{if $quest.series}
					<tr><th>{#Series#}</th></tr>
					<tr>
						<td>
							<div class="infobox-spacer"></div>
							<table class="series">
							{section name=i loop=$quest.series}
								<tr>
									<th>{$smarty.section.i.index+1}.</th>
									<td>
										{if ($quest.series[i].id==$quest.id)}
											<b>{$quest.series[i].name}</b>
										{else}
											<div><a href="?quest={$quest.series[i].id}">{$quest.series[i].name}</a></div>
										{/if}
									</td>
								</tr>
							{/section}
							</table>
						</td>
					</tr>
					{/if}
				</table>
				<script type="text/javascript">ss_appendSticky()</script>

				<div class="text">

					<h1>{$quest.name}</h1>

					{$quest.Objectives}

					{if ($quest.itemreqs) or (($quest.creaturereqs)) or ($quest.objectreqs)}
						<table class="iconlist">
						{foreach from=$quest.creaturereqs key=i item=creature}
							<tr>
								<th><ul><li><var>&nbsp;</var></li></ul></th>
								<td>
									<a href="?npc={$creature.id}">
									{if $quest.objectivetext[$i]}{$quest.objectivetext[$i]}{else}{$creature.name}{/if}
									</a>
									{if !($quest.objectivetext[$i])}{#slain#}{/if}
									{if $creature.count>1}({$creature.count}){/if}
								</td>
							</tr>
						{/foreach}
						{foreach from=$quest.itemreqs key=i item=item}
							<tr>
								<th align="right" id="iconlist-icon{$i}"></th>
								<td>
									<span class="q{$item.quality}">
										<a href="?item={$item.id}">
											{$item.name}
										</a>
									</span>
									{if $item.count>1}
										({$item.count})
									{/if}
								</td>
							</tr>
						{/foreach}
						{foreach from=$quest.objectreqs key=i item=object}
							<tr>
								<th><ul><li><var>&nbsp;</var></li></ul></th>
								<td>
									<a href="?object={$object.id}">
										{if $quest.objectivetext[$i]}{$quest.objectivetext[$i]}{else}{$object.name}{/if}
									</a>
									{if $object.count>1}({$object.count}){/if}
								</td>
							</tr>
						{/foreach}
						</table>
						{if ($quest.itemreqs)}
							<script type="text/javascript">
							{foreach from=$quest.itemreqs key=i item=item}
								ge('iconlist-icon{$i}').appendChild(g_items.createIcon({$item.id}, 0, {$item.count}));
							{/foreach}
							</script>
						{/if}
					{/if}

					{if $quest.Details}
						<h3>{#Description#}</h3>
						{$quest.Details}
					{/if}

					{if (($quest.itemchoices) or ($quest.money) or ($quest.itemrewards))}
					<h3>{#Rewards#}</h3>

					{if ($quest.itemchoices)}
						{#You_will_be_able_to_choose_one_of_these_rewards#}:
						<div class="pad"></div>
						<table class="icontab">
						<tr>
						{section name=j loop=$quest.itemchoices}
								<th id="icontab-icon{$smarty.section.j.index+1}"></th>
								<td>
									<span class="q{$quest.itemchoices[j].quality}">
										<a href="?item={$quest.itemchoices[j].id}">
											{$quest.itemchoices[j].name}
										</a>
									</span>
								</td>
						{/section}
						</tr>
						</table>
						<script type="text/javascript">
						{section name=j loop=$quest.itemchoices}
							ge('icontab-icon{$smarty.section.j.index+1}').appendChild(g_items.createIcon({$quest.itemchoices[j].id}, 1, {$quest.itemchoices[j].count}));
						{/section}
						</script>
					{/if}

					{if ($quest.itemrewards)}
						{#You_will_receive#}:
						<div class="pad"></div>
						<table class="icontab">
						<tr>
						{section name=j loop=$quest.itemrewards}
								<th id="icontab-icon{$smarty.section.j.index+1}"></th>
								<td>
									<span class="q{$quest.itemrewards[j].quality}">
										<a href="?item={$quest.itemrewards[j].id}">
											{$quest.itemrewards[j].name}
										</a>
									</span>
								</td>
						{/section}
						</tr>
						</table>
						<script type="text/javascript">
						{section name=j loop=$quest.itemrewards}
							ge('icontab-icon{$smarty.section.j.index+1}').appendChild(g_items.createIcon({$quest.itemrewards[j].id}, 1, {$quest.itemrewards[j].count}));
						{/section}
						</script>
					{/if}

					{if ($quest.money)}
						{#You_will_also_receive#}:
						{if ($quest.moneysilver>0)}
							{if ($quest.moneygold>0)}
								<span class="moneygold">{$quest.moneygold}</span>
							{/if}
							<span class="moneysilver">{$quest.moneysilver}</span>
						{/if}
						<span class="moneycopper">{$quest.moneycopper}</span>
					{/if}
					{/if}

					{if $quest.RequestItemsText}
						<h3>{#Progress#}</h3>
						{$quest.RequestItemsText}
					{/if}

					{if $quest.OfferRewardText}
						<h3>{#Completion#}</h3>
						{$quest.OfferRewardText}
					{/if}

	{if ($quest.xp or $quest.reprewards)}
					<h3>{#Gains#}</h3>
					{#Upon_completion_of_this_quest_you_will_gain#}:
					<ul>
						{if $quest.xp}
						<li><div>{$quest.xp} {#experience#} </div></li>
						{/if}

						{if ($quest.reprewards)}
						{section name=j loop=$quest.reprewards}
						<li><div>{$quest.reprewards[j].value} {#reputationwith#} <a href="?faction={$quest.reprewards[j].id}">{$quest.reprewards[j].name}</a></div></li>
						{/section}
						{/if}
					</ul>
	{/if}

				<h2>{#Related#}</h2>

			</div>

			<div id="jkbfksdbl4"></div>
			<div id="lkljbjkb574" class="listview"></div>
			<script type="text/javascript">
				var tabsRelated = new Tabs({ldelim}parent: ge('jkbfksdbl4'){rdelim});
				new Listview({ldelim}template: 'comment', id: 'comments', name: '{#Comments#}', tabs: tabsRelated, parent: 'lkljbjkb574', data: lv_comments{rdelim});
				tabsRelated.flush();
			</script>

			{include file='bricks/contribute.tpl'}

			</div>
		</div>
	</div>
{include file='footer.tpl'}