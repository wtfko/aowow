{include file='header.tpl'}
	
	<div id="main">

		<div id="main-precontents"></div>
		<div id="main-contents" class="main-contents">

			<script type="text/javascript">
				{include file='bricks/allcomments.tpl'}
				var g_pageInfo = {ldelim}type: 1, typeId: {$npc.id}, name: '{$npc.name|escape:"quotes"}'{rdelim};
				g_initPath([0,4,{$npc.type}]);
			</script>

			<table class="infobox">
				<tr><th>{#Quick_Facts#}</th></tr>
				<tr><td><div class="infobox-spacer"></div>
					<ul>
						<li><div>{#Level#}: {if $npc.minlevel<>$npc.maxlevel}{$npc.minlevel}-{/if}{$npc.maxlevel}</div></li>
						<li><div>{#Classification#}: {$npc.rank}</div></li>
						<li><div>{#React#}: <span class="q{if $npc.A==-1}7{elseif $npc.A==1}2{else}6{/if}">A</span> <span class="q{if $npc.H==-1}7{elseif $npc.H==1}2{else}6{/if}">H</span></div></li>
						<li><div>{#Faction#}: <a href="?faction={$npc.faction_num}">{$npc.faction}</a></div></li>
						<li><div>Armor: {$npc.armor}</div></li>
						<li><div>Damage: {$npc.mindmg} - {$npc.maxdmg}</div></li>
						{if ($npc.sptime>0)}<li><div>Respawn time: {$npc.sptime}secs</div></li>{/if}
						<li><div>{#Health#}: {if $npc.minhealth<>$npc.maxhealth}{$npc.minhealth}-{/if}{$npc.maxhealth}</div></li>
						{if ($npc.minmana or $npc.maxmana)}
							<li><div>{#Mana#}: {if $npc.minmana<>$npc.maxmana}{$npc.minmana}-{/if}{$npc.maxmana}</div></li>
						{/if}
						<li><div>{#Wealth#}:
							{if ($npc.moneysilver>0)}
								{if ($npc.moneygold>0)}
									<span class="moneygold">{$npc.moneygold}</span>
								{/if}
								<span class="moneysilver">{$npc.moneysilver}</span>
							{/if}
							<span class="moneycopper">{$npc.moneycopper}</span>
						</div></li>
					</ul>
				</td></tr>
			</table>

			<div class="text">
	  			<h1>{$npc.name}{if $npc.subname} &lt;{$npc.subname}&gt;{/if}</h1>

{if $exdata and $zonedata}
				{#This_NPC_can_be_found_in#} <span id="locations">
					{section name=i loop=$zonedata}
					{if ($exdata[i][0].x) or ($exdata[i][0].y)}
						<a href="javascript:;" onclick="myMapper.update({ldelim}
							{assign var=mapp value=true}
							zone: {$zonedata[i].zone}, coords:
								[{section name=j loop=$exdata[i]}
									[{$exdata[i][j].x},{$exdata[i][j].y}]
									{if $smarty.section.j.last}
									{else}
										,
									{/if}
								{/section}]{rdelim});
							g_setSelectedLink(this, 'mapper'); return false" onmousedown="return false">
					{else}
						<a href="?zone={$zonedata[i].zone}">
					{/if}
							{$zonedata[i].name}</a>{if $zonedata[i].count>1}&nbsp;({$zonedata[i].count}){else}{/if}
{if $smarty.section.i.last}.{else},{/if}
					{/section}
				</span>
{else}
				{#This_NPC_cant_be_found#}.
{/if}

				<div id="k6b43j6b"></div>
				<div class="clear"></div>

				{if $mapp}{literal}
				<script type="text/javascript">
				var myMapper = new Mapper({parent: 'k6b43j6b'});
				gE(ge('locations'), 'a')[0].onclick();
				</script>
				{/literal}{/if}

				<h2>{#Related#}</h2>

			</div>

			<div id="jkbfksdbl4"></div>

			<div id="lkljbjkb574" class="listview"></div>
			<script type="text/javascript">

				{if $allitems}
					{include file='bricks/allitems_table.tpl' data=$allitems}
				{/if}

				{if $allspells}
					{include file='bricks/allspells_table.tpl' data=$allspells}
				{/if}

					var tabsRelated = new Tabs({ldelim}parent: ge('jkbfksdbl4'){rdelim});

				{if $npc.sells}
					{include file='bricks/item_table.tpl' id='sells' name=#sells# tabsid='tabsRelated' data=$npc.sells}
				{/if}

				{if $npc.drop}
					{include file='bricks/item_table.tpl' id='drop' name=#drop# tabsid='tabsRelated' data=$npc.drop}
				{/if}

				{if $npc.pickpocketing}
					{include file='bricks/item_table.tpl' id='pick-pocketing' name=#pickpocketing# tabsid='tabsRelated' data=$npc.pickpocketing}
				{/if}

				{if $npc.skinning}
					{include file='bricks/item_table.tpl' id='skinning' name=#skinning# tabsid='tabsRelated' data=$npc.skinning}
				{/if}

				{if $npc.starts}
					{include file='bricks/quest_table.tpl' id='starts' name=#starts# tabsid='tabsRelated' data=$npc.starts}
				{/if}

				{if $npc.ends}
					{include file='bricks/quest_table.tpl' id='ends' name=#ends# tabsid='tabsRelated' data=$npc.ends}
				{/if}

				{if $npc.abilities}
					{include file='bricks/spell_table.tpl' id='abilities' name=#Abilities# tabsid='tabsRelated' data=$npc.abilities}
				{/if}

				{if $npc.objectiveof}
					{include file='bricks/quest_table.tpl' id='objective-of' name=#objectiveof# tabsid='tabsRelated' data=$npc.objectiveof}
				{/if}

				{if $npc.teaches}
					{include file='bricks/spell_table.tpl' id='teaches-ability' name=#Teaches# tabsid='tabsRelated' data=$npc.teaches}
				{/if}

				new Listview({ldelim}template: 'comment', id: 'comments', name: '{#Comments#}', tabs: tabsRelated, parent: 'lkljbjkb574', data: lv_comments{rdelim});

				tabsRelated.flush();
			</script>

			{include file='bricks/contribute.tpl'}

			<div class="clear"></div>
		</div>
	</div>

{include file='footer.tpl'}