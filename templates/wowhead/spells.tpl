{config_load file="$conf_file"}

{include file='header.tpl'}

	<div id="main">
		<div id="main-precontents"></div>
		<div id="main-contents" class="main-contents">
			<script type="text/javascript">
				g_initPath({$page.path});
			</script>

			<div id="lv-spells" class="listview"></div>

			<script type="text/javascript">
				{* Иконки, подсказки для вещей *}
				{if $allitems}
					{include file='bricks/allitems_table.tpl' data=$allitems}
				{/if}
				{if $allspells}
					{include file='bricks/allspells_table.tpl' data=$allspells}
				{/if}
				{* Список найденных спеллов: *}
				{if $spells}
					{include file='bricks/spells.tpl' data=$spells}
				{/if}
			</script>

			<div class="listview-void">
				{* Ещё один перебор для чего - хз *}
				{section name=i loop=$items}
					<a href="/?item={$data[i].entry}">{$items[i].name}</a>
				{/section}
			</div>

			<div class="clear"></div>
		</div>
	</div>

{include file='footer.tpl'}
