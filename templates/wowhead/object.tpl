{config_load file="$conf_file"}

{include file='header.tpl'}

	<div id="main">

		<div id="main-precontents"></div>
		<div id="main-contents" class="main-contents">

			<script type="text/javascript">
				{include file='bricks/allcomments.tpl'}
				var g_pageInfo = {ldelim}type: {$page.type}, typeId: {$page.typeid}, name: '{$object.name|escape:"quotes"}'{rdelim};
				g_initPath({$page.path});
			</script>

			<table class="infobox">
				<tr><th>{#Quick_Facts#}</th></tr>
				<tr><td><div class="infobox-spacer"></div>
				<ul>
					{if $object.key}<li><div>{#Key#}: <a class="q{$object.key.quality}" href="?item={$object.key.id}">[{$object.key.name}]</a></div></li>{/if}
					{if $object.lockpicking}<li><div>{#Lockpickable#} (<span class="tip" onmouseover="Tooltip.showAtCursor(event, '{#Required_lockpicking_skill#}', 0, 0, 'q')" onmousemove="Tooltip.cursorUpdate(event)" onmouseout="Tooltip.hide()">{$object.lockpicking}</span>)</div></li>{/if}
					{if $object.mining}<li><div>{#Mining#} (<span class="tip" onmouseover="Tooltip.showAtCursor(event, '{#Required_mining_skill#}', 0, 0, 'q')" onmousemove="Tooltip.cursorUpdate(event)" onmouseout="Tooltip.hide()">{$object.mining}</span>)</div></li>{/if}
					{if $object.herbalism}<li><div>{#Herb#} (<span class="tip" onmouseover="Tooltip.showAtCursor(event, '{#Required_herb_skill#}', 0, 0, 'q')" onmousemove="Tooltip.cursorUpdate(event)" onmouseout="Tooltip.hide()">{$object.herbalism}</span>)</div></li>{/if}
				</ul>
				</td></tr>
			</table>

			<div class="text">

				<h1>{$object.name}</h1>

{if $exdata and $zonedata}
				{#This_Object_can_be_found_in#} <span id="locations">
					{section name=i loop=$zonedata}
					{if ($exdata[i][0].x) or ($exdata[i][0].y)}
						<a href="javascript:;" onclick="myMapper.update({ldelim}
							{assign var=mapp value=true}
							zone: {$zonedata[i].zone}, coords:
								[{section name=j loop=$exdata[i]}
									[{$exdata[i][j].x},{$exdata[i][j].y}]
									{if $smarty.section.j.last}
									{else}
										,
									{/if}
								{/section}]{rdelim});
							g_setSelectedLink(this, 'mapper'); return false" onmousedown="return false">
					{else}
						<a href="?zone={$zonedata[i].zone}">
					{/if}
							{$zonedata[i].name}</a>{if $zonedata[i].count>1}&nbsp;({$zonedata[i].count}){else}{/if}
{if $smarty.section.i.last}.{else},{/if}
					{/section}
				</span>
{else}
				{#This_Object_cant_be_found#}.
{/if}

				<div id="k6b43j6b"></div>
				<div class="clear"></div>

{if $exdata and $zonedata}
				{literal}
				<script type="text/javascript">
					var myMapper = new Mapper({parent: 'k6b43j6b'});
					gE(ge('locations'), 'a')[0].onclick();
				</script>
				{/literal}
{/if}
				<h2>{#Related#}</h2>

			</div>

			<div id="jkbfksdbl4"></div>
			<div id="lkljbjkb574" class="listview"></div>
			<script type="text/javascript">

				{if $allitems}
					{include file='bricks/allitems_table.tpl' data=$allitems}
				{/if}

				var tabsRelated = new Tabs({ldelim}parent: ge('jkbfksdbl4'){rdelim});

				{if $object.drop}
					{include file='bricks/item_table.tpl' id='drop' name=#contain# tabsid='tabsRelated' data=$object.drop}
				{/if}

				{if $object.starts}
					{include file='bricks/quest_table.tpl' id='starts' name=#starts# tabsid='tabsRelated' data=$object.starts}
				{/if}

				{if $object.ends}
					{include file='bricks/quest_table.tpl' id='ends' name=#ends# tabsid='tabsRelated' data=$object.ends}
				{/if}

				new Listview({ldelim}template: 'comment', id: 'comments', name: '{#Comments#}', tabs: tabsRelated, parent: 'lkljbjkb574', data: lv_comments{rdelim});

				tabsRelated.flush();
			</script>

			{include file='bricks/contribute.tpl'}

			</div>
		</div>
	</div>

{include file='footer.tpl'}