{include file='header.tpl'}

		<div id="main">
			<div id=main-precontents></div>
			<div class=main-contents id=main-contents>
	{if $items or $npcs or $objects or $quests}
				<div class=text>
					<h1>{#Search_results_for#} <i>{$search}</i></h1>
				</div>
				<div id=jkbfksdbl4></div>
				<div class=listview id=lkljbjkb574></div>
				<script type=text/javascript>
	{* Иконки, подсказки для вещей *}
	{if $allitems}
					{include file='bricks/allitems_table.tpl' data=$allitems}
	{/if}
	{if $allspells}
					{include file='bricks/allspells_table.tpl' data=$allspells}
	{/if}
					var myTabs = new Tabs({ldelim}parent: ge('jkbfksdbl4'){rdelim});
	{* Список найденных вещей: *}
	{if $items}
	{include file='bricks/item_table.tpl' id='items' name=#items# tabsid='myTabs' data=$items}
	{/if}
	{* Список найденных NPC: *}
	{if $npcs}
	{include file='bricks/creature_table.tpl' id='npcs' name=#npcs# tabsid='myTabs' data=$npcs}
	{/if}
	{* Список найденных ГО: *}
	{if $objects}
	{include file='bricks/object_table.tpl' id='objects' name=#objects# tabsid='myTabs' data=$objects}
	{/if}
	{* Список найденных квестов: *}
	{if $quests}
	{include file='bricks/quest_table.tpl' id='quests' name=#quests# tabsid='myTabs' data=$quests}
	{/if}
	{if $itemsets}
	{include file='bricks/itemset_table.tpl' id='itemsets' name=#itemsets# tabsid='myTabs' data=$itemsets}
	{/if}
	{if $spells}
	{include file='bricks/spell_table.tpl' id='spells' name=#Spells# tabsid='myTabs' data=$spells}
	{/if}

					myTabs.flush();
				</script>
	
				<div class=clear></div>
	{else}
				<div class="text">
					<h1>{#No_results_for#} <i>{$search}</i></h1>
					{#Please_try_some_different_keywords_or_check_your_spelling#}.
				</div>
	{/if}
			</div>
		</div>

{include file='footer.tpl'}
