{*
		ШАБЛОН ТАБЛИЦЫ НАБОРОВ ВЕЩЕЙ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{ldelim}include file='bricks/item_table.tpl' id='drop' tabsid='tabsRelated' data=$npc.drop}
		Без вкладок:
	{ldelim}include file='bricks/item_table.tpl' id='items' data=$items}
*}
	new Listview({ldelim}template: 'itemset', id: 'itemsets',name:'{$name}',{if ($tabsid)}tabs:{$tabsid},parent:'lkljbjkb574',{/if} data: [
		{section name=i loop=$data}
			{ldelim}
				name: '{$data[i].quality2}{$data[i].name|escape:"quotes"}',
				{if $data[i].minlevel}minlevel: {$data[i].minlevel},{/if}
				{if $data[i].maxlevel}maxlevel: {$data[i].maxlevel},{/if}
				{if $data[i].pieces}pieces:[{section name=j loop=$data[i].pieces}{$data[i].pieces[j]}{if $smarty.section.j.last}{else},{/if}{/section}],{/if}
				{if $data[i].type}type: {$data[i].type},{/if}
				id: {$data[i].id}
			{rdelim}
			{if $smarty.section.i.last}{else},{/if}
		{/section}
	]{rdelim});
