{strip}
	new Listview({ldelim}
		template:'zone',
		id:'fished-in',
		name:'{#Fished_in#}',
		tabs:tabsRelated,
		parent:'lkljbjkb574',
		hiddenCols: ['instancetype', 'level', 'territory', 'category'],
		extraCols: [Listview.extraCols.percent],
		sort:['-percent', 'name'],
		data:[
{section name=i loop=$data}
			{ldelim}
{* Номер локации, обязательно *}
			id: '{$data[i].id}',
{* Название локации, обязательно *}
			name: '{$data[i].name|escape:"quotes"}',
{* Шанс дропа *}
			percent: {$data[i].percent}
			{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});{/strip}
