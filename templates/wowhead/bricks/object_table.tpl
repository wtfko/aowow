{*
		ШАБЛОН ТАБЛИЦЫ ОБЪЕКТОВ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{include file='bricks/object_table.tpl' id='contained-in-object' tabsid='tabsRelated' data=$containedinobject name=#containedinobject#}
		Без вкладок:
	{include file='bricks/creature_table.tpl' id='items' data=$items}
*}
				new Listview({ldelim}template:'object',id:'{$id}',name:'{$name}',{if ($tabsid)}tabs:{$tabsid},parent:'lkljbjkb574',{/if}{if $data[0].percent}extraCols:[Listview.extraCols.percent],{/if}{if isset($data[0].skill)}visibleCols:['skill'],{/if}hiddenCols:['location'],data:[
{section name=i loop=$data}
				{ldelim}
{* Название обекта, обязательно *}
					name: '{$data[i].name|escape:"quotes"}',
{* Тип обекта, обязательно *}
					type: {$data[i].type},
{* Процент дропа *}
{if $data[0].percent>0}
					percent: {$data[i].percent},
{/if}
{* Необходимый уровень скилла *}
{if isset($data[0].skill)}
					skill: {$data[i].skill},
{/if}
{* Номер создания, обязателен *}
					id: {$data[i].id}
				{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});
