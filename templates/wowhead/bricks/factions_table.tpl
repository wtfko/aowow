new Listview({ldelim}template: 'faction', id: 'factions', data: [
{section name=i loop=$data}
{ldelim}
	id: {$data[i].id},
	name: '{$data[i].name|escape:"quotes"}'
	{if $data[i].group},group: '{$data[i].group|escape:"quotes"}'{/if}
	{if $data[i].side},side: '{$data[i].side|escape:"quotes"}'{/if}
{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
]{rdelim});
