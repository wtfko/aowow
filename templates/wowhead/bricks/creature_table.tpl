{*
		ШАБЛОН ТАБЛИЦЫ СОЗДАНИЙ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{include file='bricks/creature_table.tpl' id='dropped-by' tabsid='tabsRelated' data=$droppedby name=#droppedby#}
		Без вкладок:
	{include file='bricks/creature_table.tpl' id='items' data=$items}
*}
{strip}

{assign var="cost" value=false}
{assign var="percent" value=false}

{foreach from=$data item=curr}
	{if ($curr.cost)}{assign var="cost" value=true}{/if}
	{if ($curr.percent)}{assign var="percent" value=true}{/if}
{/foreach}

new Listview(
	{ldelim}template:'npc',
		id:'{$id}',
		name:'{$name}',
		{if ($tabsid)}tabs:{$tabsid},parent: 'lkljbjkb574',{/if}
		{if $percent}extraCols:[Listview.extraCols.percent],
		{elseif $cost}extraCols:[Listview.extraCols.stock, Listview.funcBox.createSimpleCol('stack', 'stack', '10%', 'stack'), Listview.extraCols.cost],{/if}
		hiddenCols:[{if $cost}'type'{else}'location'{/if}],
		data:[
{section name=i loop=$data}
				{ldelim}
{* Название создания, обязательно *}
					name: '{$data[i].name|escape:"quotes"}',
{if $data[i].subname}
					tag: '{$data[i].subname|escape:"quotes"}',
{/if}
{* Минимальный уровень содания *}
					minlevel: {$data[i].minlevel},
{* Максимальный уровень создания *}
					maxlevel: {$data[i].maxlevel},
{* Тип создания, обязательно *}
					type: {$data[i].type},
{* Класс создания, обязательно *}
					classification: {$data[i].classification},
{* Отношения с фракциями *}
					react: [{$data[i].react}],
{* Процент дропа *}
{if $data[0].percent>0}
					percent: {$data[i].percent},
{/if}
{* Стоимость *}
{if $cost}
	cost: [
		{$data[i].cost.money}
		{if $data[i].cost.honor or $data[i].cost.arena or $data[i].cost.items}
			,{$data[i].cost.honor}
			{if $data[i].cost.arena or $data[i].cost.items}
				,{$data[i].cost.arena}
				{if $data[i].cost.items}
					,[
					{foreach from=$data[i].cost.items item=curitem}
						[{$curitem.item},{$curitem.count}],
					{/foreach}
					]
				{/if}
			{/if}
		{/if}
		],
		stock: 1,
		stack: 1,
{/if}
{* Номер создания, обязателен *}
					id: {$data[i].id}
				{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});
{/strip}
