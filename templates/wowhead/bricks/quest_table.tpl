{*
		ШАБЛОН ТАБЛИЦЫ КВЕСТОВ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{include file='bricks/object_table.tpl' id='dropped-by' tabsid='tabsRelated' data=$droppedby name=#droppedby#}
		Без вкладок:
	{include file='bricks/creature_table.tpl' id='items' data=$items}
*}
				new Listview({ldelim}template:'quest',id:'{$id}',{if $name}name:'{$name}',{/if}{if ($tabsid)}tabs:{$tabsid},parent:'lkljbjkb574',{/if}data:[
{section name=i loop=$data}
				{ldelim}
{* Номер квеста, обязательно *}
					id: '{$data[i].id}',
{* Название квеста, обязательно *}
					name: '{$data[i].name|escape:"quotes"}',
{* Уровень квеста, обязательно *}
					level: '{$data[i].level}',
{* Уровень, необходимый для получения квеста *}
{if ($data[i].reqlevel)}
					reqlevel:{$data[i].reqlevel},
{/if}
{* Для кого квест? *}
					side: '{$data[i].side}'
{* Награды за квест *}
{if ($data[i].itemrewards)}
					,itemrewards:[{section name=j loop=$data[i].itemrewards}[{$data[i].itemrewards[j].id},{$data[i].itemrewards[j].count}]{if $smarty.section.j.last}{else},{/if}{/section}]
{/if}
{if ($data[i].itemchoices)}
					,itemchoices:[{section name=j loop=$data[i].itemchoices}[{$data[i].itemchoices[j].id},{$data[i].itemchoices[j].count}]{if $smarty.section.j.last}{else},{/if}{/section}]
{/if}
{if ($data[i].xp)}
					,xp:{$data[i].xp}
{/if}
{if ($data[i].money)}
					,money:{$data[i].money}
{/if}
{if ($data[i].category)}
					,category:{$data[i].category}
{/if}
{if ($data[i].category2)}
					,category2:{$data[i].category2}
{/if}
{if ($data[i].type)}
					,type:{$data[i].type}
{/if}
{if ($data[i].daily)}
					,daily:{$data[i].daily}
{/if}
				{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});
