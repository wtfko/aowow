{*
		ШАБЛОН ТАБЛИЦЫ ВЕЩЕЙ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{include file='bricks/item_table.tpl' id='drop' tabsid='tabsRelated' data=$npc.drop}
		Без вкладок:
	{include file='bricks/item_table.tpl' id='items' data=$items}
*}
{strip}

{assign var="cost" value=true}
{assign var="percent" value=true}
{assign var="classs1" value=true}
{assign var="classs2" value=true}
{assign var="classs4" value=true}

{foreach from=$data item=curr}
	{if !($curr.cost)}{assign var="cost" value=false}{/if}
	{if !($curr.percent)}{assign var="percent" value=false}{/if}
	{if !($curr.classs==1)}{assign var="classs1" value=false}{/if}
	{if !($curr.classs==2)}{assign var="classs2" value=false}{/if}
	{if !($curr.classs==4)}{assign var="classs4" value=false}{/if}
{/foreach}

new Listview(
	{ldelim}template:'item',
	id:'{$id}',
	name:'{$name}',
	{if ($tabsid)}tabs:{$tabsid},parent:'lkljbjkb574',{/if}
	{if     $percent}extraCols:[Listview.extraCols.percent],
	{elseif $cost}extraCols:[Listview.extraCols.cost],
	{elseif $classs1}visibleCols: ['slots'],
	{elseif $classs2}visibleCols: ['dps', 'speed'],
	{elseif $classs4}visibleCols: ['armor', 'slot'],{/if}
	hiddenCols:['source'],
	data: [
	{section name=i loop=$data}
		{ldelim}
		{* Название/качество вещи, обязательно *}
		name: '{$data[i].quality2}{$data[i].name|escape:"quotes"}',
		{* Уровень вещи *}
		{if $data[i].level}
			level: {$data[i].level},
		{/if}
		{* Требуемый уровень вещи *}
		{if $data[i].reqlevel}
			reqlevel: {$data[i].reqlevel},
		{/if}
		{* Класс вещи, обязательно *}
			classs: {$data[i].classs},
		{* Подкласс вещи, обязательно *}
			subclass: {$data[i].subclass},
		{* Кол-во вещей при дропе *}
		{if $data[i].maxcount>1}
			stack:[{$data[i].mincount},{$data[i].maxcount}],
		{/if}
		{* Процент дропа *}
		{if $percent}
			percent: {$data[i].percent},
		{/if}
		{* Стоимость *}
		{if $cost}
			cost: [
				{$data[i].cost.money}
				{if $data[i].cost.honor or $data[i].cost.arena or $data[i].cost.items}
					,{$data[i].cost.honor}
					{if $data[i].cost.arena or $data[i].cost.items}
						,{$data[i].cost.arena}
						{if $data[i].cost.items}
							,[
							{foreach from=$data[i].cost.items item=curitem}
								[{$curitem.item},{$curitem.count}],
							{/foreach}
							]
						{/if}
					{/if}
				{/if}
				],
		{/if}
		{if $classs1==1}
			nslots: {$data[i].slots},
		{/if}
		{if $classs2}
			dps: {$data[i].dps},
			speed: {$data[i].speed},
		{/if}
		{if $classs4}
			armor: {$data[i].armor},
			slot: {$data[i].slot},
		{/if}
		{* Номер вещи, обязателен *}
		id: {$data[i].id}
		{rdelim}{if $smarty.section.i.last}{else},{/if}
	{/section}
	]{rdelim}
);{/strip}
