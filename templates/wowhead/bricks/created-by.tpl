				new Listview({ldelim}template:'spell',id:'created-by',name:'{#Created_by#}', sort: ['name'], tabs:tabsRelated, parent: 'lkljbjkb574', data:[
{section name=i loop=$data}
				{ldelim}
{* Название спелла, обязательно *}
					name: '{$data[i].quality}{$data[i].name|escape:"quotes"}',
{* Ранк спелла *}
{if $data[i].rank}
					rank: '{$data[i].rank|escape:"quotes"}',
{/if}
{* Используемый скилл *}
{if $data[i].skill}
					skill: [{$data[i].skill}],
{/if}
{* Реагенты для спелла *}
{if $data[i].reagents}
					reagents:[{section name=j loop=$data[i].reagents}[{$data[i].reagents[j].id},{$data[i].reagents[j].count}]{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{if $data[i].creates}
					creates:[{section name=j loop=$data[i].creates}{$data[i].creates[j].id},{$data[i].creates[j].count}{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{* Требуемый уровень навыка для обучения *}
{if $data[i].learnedat}
					learnedat: {$data[i].learnedat},
{/if}
{* "Цвета скиллов" *}
{if $data[i].colors}
					colors:[{section name=j loop=$data[i].colors}{$data[i].colors[j]}{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{* Номер спелла, обязателен *}
					id: {$data[i].id}
				{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});
