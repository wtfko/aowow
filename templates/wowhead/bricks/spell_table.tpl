{*
		ШАБЛОН ТАБЛИЦЫ СПЕЛЛОВ
	Переменные, передаваемые шаблону:
	id     - идентификатор/тип табл
	name   - название табл
	tabsid - идентификатор вкладок
	data   - данные для табл

	Пример вставки модуля в текст:
		Со вкладками:
	{include file='bricks/spell_table.tpl' id='dropped-by' tabsid='tabsRelated' data=$droppedby name=#droppedby#}
		Без вкладок:
	{include file='bricks/spell_table.tpl' id='items' data=$items}
*}
				new Listview({ldelim}template:'spell',id:'{$id}',name:'{$name}', visibleCols: ['level', 'school'], sort: ['name'], {if ($tabsid)}tabs:{$tabsid}{/if},parent: 'lkljbjkb574', {$params} data:[
{section name=i loop=$data}
				{ldelim}
{* Название спелла, обязательно *}
					name: '{$data[i].quality}{$data[i].name|escape:"quotes"}',
{* Уровень спелла *}
					level: {$data[i].level},
{* Школа спелла *}
					school: {$data[i].school},
{* Ранк спелла *}
{if $data[i].rank}
					rank: '{$data[i].rank|escape:"quotes"}',
{/if}
{* Используемый скилл *}
{if $data[i].skill}
					skill: [{$data[i].skill}],
{/if}
{* Реагенты для спелла *}
{if $data[i].reagents}
					reagents:[{section name=j loop=$data[i].reagents}[{$data[i].reagents[j].id},{$data[i].reagents[j].count}]{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{if $data[i].creates}
					creates:[{section name=j loop=$data[i].creates}{$data[i].creates[j].id},{$data[i].creates[j].count}{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{* Требуемый уровень навыка для обучения *}
{if $data[i].learnedat}
					learnedat: {$data[i].learnedat},
{/if}
{* "Цвета скиллов" *}
{if $data[i].colors}
					colors:[{section name=j loop=$data[i].colors}{$data[i].colors[j]}{if $smarty.section.j.last}{else},{/if}{/section}],
{/if}
{* Номер спелла, обязателен *}
					id: {$data[i].id}
				{rdelim}{if $smarty.section.i.last}{else},{/if}
{/section}
				]{rdelim});
