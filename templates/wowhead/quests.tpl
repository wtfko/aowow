{config_load file="$conf_file"}

{include file='header.tpl'}

		<div id="main">
			<div id="main-precontents"></div>
			<div id="main-contents" class="main-contents">
				<script type="text/javascript">
				g_initPath({$page.path});
				</script>

				<div id="lv-quests" class="listview"></div>

				<script type="text/javascript">
					{* Иконки, подсказки для вещей *}
					{if $allitems}
						{include file='bricks/allitems_table.tpl' data=$allitems}
					{/if}
					{* Список найденных квестов: *}
					{if $quests}
						{include file='bricks/quest_table.tpl' id='quests' data=$quests}
					{/if}
				</script>

				<div class="listview-void">
					{* Ещё один перебор для чего - хз *}
					{section name=i loop=$quests}
						<a href="?quest={$quests[i].id}">{$quests[i].name}</a>
					{/section}
				</div>

				<div class="clear"></div>
			</div>
		</div>

{include file='footer.tpl'}
