{config_load file="$conf_file"}

{include file='header.tpl'}

		<div id="main">
			<div id="main-precontents"></div>
			<div id="main-contents" class="main-contents">
				<script type="text/javascript">
				g_initPath({$page.path});
				</script>

				<div id="lv-factions" class="listview"></div>

				<script type="text/javascript">
					{if $factions}
						{include file='bricks/factions_table.tpl' id='factions' data=$factions}
					{/if}
				</script>

				<div class="listview-void">
					{section name=i loop=$factions}
						<a href="?faction={$factions[i].id}">{$factions[i].name}</a>
					{/section}
				</div>

				<div class="clear"></div>
			</div>
		</div>

{include file='footer.tpl'}
