<?php

require_once ('includes/game.php');
require_once ('includes/allspells.php');
require_once ('includes/allquests.php');
require_once ('includes/allitems.php');
require_once ('includes/allnpcs.php');
require_once ('includes/allobjects.php');
require_once ('includes/allcomments.php');

// Загружаем файл перевода для smarty
$smarty->config_load($conf_file,'item');

global $DB;
global $allitems;
global $allspells;

global $item_cols;
global $spell_cols;

$id = $podrazdel;

// Информация о вещи...
$item = iteminfo($id, 1);

// Поиск мобов с которых эта вещь лутится
$drops = drop('creature_loot_template',$item['id']);
$droppedby = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			lootid=?d
			AND factiontemplateID=faction_A
		',
		$npc_cols[0],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($droppedby);
		$droppedby[$num] = creatureinfo2($row);
		$droppedby[$num] = array_merge($droppedby[$num], $drop);
	}
}
if (IsSet($droppedby))
	$smarty->assign('droppedby', $droppedby);

// Поиск объектов, из которых лутится эта вещь
$drops = drop('gameobject_loot_template',$item['id']);
$containedinobject = array();
$minedfromobject = array();
$gatheredfromobject = array();
foreach($drops as $lootid => $drop)
{
	// Сундуки
	$rows = $DB->select('
		SELECT g.entry, g.name, g.type, a.lockproperties1
		FROM gameobject_template g, ?_lock a
		WHERE
			g.data1=?d
			AND g.type=?d
			AND a.lockID=g.data0
		',
		$lootid,
		GAMEOBJECT_TYPE_CHEST,
		LOCK_PROPERTIES_HERBALISM,
		LOCK_PROPERTIES_MINING
	);
	foreach ($rows as $numRow=>$row)
	{
		if ($row['lockproperties1'] == LOCK_PROPERTIES_MINING)
		{
			// Залежи руды
			$num = count($minedfromobject);
			$minedfromobject[$num] = array();
			$minedfromobject[$num] = objectinfo2($row);
			$minedfromobject[$num] = array_merge($minedfromobject[$num], $drop);
		} elseif ($row['lockproperties1'] == LOCK_PROPERTIES_HERBALISM)
		{
			// Собирается с трав
			$num = count($gatheredfromobject);
			$gatheredfromobject[$num] = array();
			$gatheredfromobject[$num] = objectinfo2($row);
			$gatheredfromobject[$num] = array_merge($gatheredfromobject[$num], $drop);
		} else {
			// Сундуки
			$num = count($containedinobject);
			$containedinobject[$num] = array();
			$containedinobject[$num] = objectinfo2($row);
			$containedinobject[$num] = array_merge($containedinobject[$num], $drop);
		}
	}
}
if (IsSet($containedinobject))
	$smarty->assign('containedinobject', $containedinobject);
if (IsSet($minedfromobject))
	$smarty->assign('minedfromobject', $minedfromobject);
if (IsSet($gatheredfromobject))
	$smarty->assign('gatheredfromobject', $gatheredfromobject);

// Поиск вендеров, которые эту вещь продают
$rows = $DB->select('
	SELECT ?#, c.entry, v.ExtendedCost
	FROM npc_vendor v, creature_template c, ?_factiontemplate
	WHERE
		v.item=?d
		AND c.entry=v.entry
		AND factiontemplateID=faction_A
	ORDER BY 1 DESC, 2 DESC
	',
	$npc_cols['0'],
	$item['id']
);
foreach ($rows as $numRow=>$row)
{
	$soldby[$numRow] = array();
	$soldby[$numRow] = creatureinfo2($row);
	$soldby[$numRow]['cost'] = array();
	if ($row['ExtendedCost'])
	{
		$extcost = $DB->selectRow('SELECT * FROM ?_item_extended_cost WHERE extendedcostID=?d LIMIT 1', $row['ExtendedCost']);
		if ($extcost['reqhonorpoints']>0)
			$soldby[$numRow]['cost']['honor'] = (($row['A']==1)? 1: -1) * $extcost['reqhonorpoints'];
		if ($extcost['reqarenapoints']>0)
			$soldby[$numRow]['cost']['arena'] = $extcost['reqarenapoints'];
		$soldby[$numRow]['cost']['items'] = array();
		for ($j=1;$j<=5;$j++)
			if (($extcost['reqitem'.$j]>0) and ($extcost['reqitemcount'.$j]>0))
			{
				allitemsinfo($extcost['reqitem'.$j], 0);
				$soldby[$numRow]['cost']['items'][] = array('item' => $extcost['reqitem'.$j], 'count' => $extcost['reqitemcount'.$j]);
			}
	}
}
if (IsSet($soldby))
	$smarty->assign('soldby', $soldby);

// Квесты, которые начинаются с этой вещи
$rows = $DB->select('
	SELECT ?#
	FROM quest_template
	WHERE
		SrcItemId=?d
		
	',
	$quest_cols[2],
	$item['id']
);
foreach ($rows as $numRow=>$row)
{
	$starts[$numRow] = array();
	$starts[$numRow] = questinfo2($row);
}
if (IsSet($starts))
	$smarty->assign('starts', $starts);


// Поиск квестов, для выполнения которых нужен этот предмет
$rows = $DB->select('
	SELECT ?#
	FROM quest_template
	WHERE
		ReqItemId1=?d
		OR ReqItemId2=?d
		OR ReqItemId3=?d
		OR ReqItemId4=?d
	',
	$quest_cols[2],
	$item['id'], $item['id'], $item['id'], $item['id']
);
foreach ($rows as $numRow=>$row)
{
	$objectiveof[$numRow] = array();
	$objectiveof[$numRow] = questinfo2($row);
}
if (IsSet($objectiveof))
	$smarty->assign('objectiveof', $objectiveof);

// Поиск квестов, наградой за выполнение которых, является этот предмет
$rows = $DB->select('
	SELECT ?#
	FROM quest_template
	WHERE
		RewItemId1=?d
		OR RewItemId2=?d
		OR RewItemId3=?d
		OR RewItemId4=?d
		OR RewChoiceItemId1=?d
		OR RewChoiceItemId2=?d
		OR RewChoiceItemId3=?d
		OR RewChoiceItemId4=?d
		OR RewChoiceItemId5=?d
		OR RewChoiceItemId6=?d
	',
	$quest_cols[2],
	$item['id'], $item['id'], $item['id'], $item['id'],
	$item['id'], $item['id'], $item['id'], $item['id'], $item['id'], $item['id']
);
foreach ($rows as $numRow=>$row)
{
	$rewardof[$numRow] = array();
	$rewardof[$numRow] = questinfo2($row);
}
if (IsSet($rewardof))
	$smarty->assign('rewardof', $rewardof);

// Поиск вещей, в которых находятся эти вещи
$drops = drop('item_loot_template',$item['id']);
$containedinitem = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry, maxcount
		FROM item_template, ?_icons
		WHERE
			entry=?d
			AND id=displayid
		',
		$item_cols[2],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($containedinitem);
		$containedinitem[$num] = iteminfo2($row, 0);
		$containedinitem[$num] = array_merge($containedinitem[$num], $drop);
	}
}
if (IsSet($containedinitem))
	$smarty->assign('containedinitem', $containedinitem);

// Какие вещи содержатся в этой вещи
$item['contains'] = array();
$item['contains'] = loot('item_loot_template', $item['id']);

// Поиск созданий, у которых воруется вещь
$drops = drop('pickpocketing_loot_template',$item['id']);
$pickpocketingloot = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			pickpocketloot=?d
			AND factiontemplateID=faction_A
		',
		$npc_cols[0],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($pickpocketingloot);
		$pickpocketingloot[$num] = creatureinfo2($row);
		$pickpocketingloot[$num] = array_merge($pickpocketingloot[$num], $drop);
	}
}
if (IsSet($pickpocketingloot))
	$smarty->assign('pickpocketingloot', $pickpocketingloot);

// Поиск созданий, с которых сдираеццо эта шкура
$drops = drop('skinning_loot_template',$item['id']);
$skinnedfrom = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			skinloot=?d
			AND factiontemplateID=faction_A
		',
		$npc_cols[0],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($skinnedfrom);
		$skinnedfrom[$num] = creatureinfo2($row);
		$skinnedfrom[$num] = array_merge($skinnedfrom[$num], $drop);
	}
}
if (IsSet($skinnedfrom))
	$smarty->assign('skinnedfrom', $skinnedfrom);

// Поиск вещей, из которых перерабатывается эта вещь
$drops = drop('prospecting_loot_template',$item['id']);
$prospectingloot = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry, maxcount
		FROM item_template, ?_icons
		WHERE
			entry=?d
			AND id=displayid
		',
		$item_cols[2],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($prospectingloot);
		$prospectingloot[$num] = iteminfo2($row, 0);
		$prospectingloot[$num] = array_merge($prospectingloot[$num], $drop);
	}
}
if (IsSet($prospectingloot))
	$smarty->assign('prospectingloot', $prospectingloot);

// Дизенчантитcя в:
$item['disenchanting'] = array();
$item['disenchanting'] = loot('disenchant_loot_template', $item['DisenchantID']);

// Получается дизэнчантом из..
$drops = drop('disenchant_loot_template',$item['id']);
$item['disenchantedfrom'] = array();
foreach($drops as $lootid => $drop)
{
	$rows = $DB->select('
		SELECT ?#, entry, maxcount
		FROM item_template, ?_icons
		WHERE
			DisenchantID=?d
			AND id=displayid
		',
		$item_cols[2],
		$lootid
	);
	foreach ($rows as $numRow=>$row)
	{
		$num = count($item['disenchantedfrom']);
		$item['disenchantedfrom'][$num] = iteminfo2($row, 0);
		$item['disenchantedfrom'][$num] = array_merge($item['disenchantedfrom'][$num], $drop);
	}
}

// Поиск сумок в которые эту вещь можно положить
if ($item['BagFamily'] == 256)
{
	// Если это ключ
	$item['key'] = true;
} elseif ($item['BagFamily'] > 0 and $item['ContainerSlots'] == 0) {
	$rows = $DB->select('
		SELECT ?#, entry, maxcount
		FROM item_template, ?_icons
		WHERE
			BagFamily=?d
			AND ContainerSlots>0
			AND id=displayid
		',
		$item_cols[2],
		$item['BagFamily']
	);
	foreach ($rows as $numRow=>$row)
	{
		$canbeplacedin[$numRow] = array();
		$canbeplacedin[$numRow] = iteminfo2($row, 0);
	}
	if (IsSet($canbeplacedin))
		$smarty->assign('canbeplacedin', $canbeplacedin);
}

// Реагент для...
$rows = $DB->select('
	SELECT ?#, spellID
	FROM ?_spell s, ?_spellicons i
	WHERE
		(( reagent1=?d
		OR reagent2=?d
		OR reagent3=?d
		OR reagent4=?d
		OR reagent5=?d
		OR reagent6=?d
		OR reagent7=?d
		OR reagent8=?d
		) AND ( i.id=s.spellicon))
	',
	$spell_cols[2],
	$item['id'], $item['id'], $item['id'], $item['id'], $item['id'], $item['id'], $item['id'], $item['id']
);
$quality = 1;
foreach ($rows as $numRow=>$row)
{
	$reagentfor[$numRow] = array();
	$reagentfor[$numRow]['id'] = $row['spellID'];
	$reagentfor[$numRow]['name'] = $row['spellname'];
	$reagentfor[$numRow]['school'] = $row['resistancesID'];
	$reagentfor[$numRow]['level'] = $row['levelspell'];
	$reagentfor[$numRow]['quality'] = '@';
	for ($j=1;$j<=8;$j++)
		if ($row['reagent'.$j])
		{
			$reagentfor[$numRow]['reagents'][]['id'] = $row['reagent'.$j];
			$reagentfor[$numRow]['reagents'][count($reagentfor[$numRow]['reagents'])-1]['count'] = $row['reagentcount'.$j];
			allitemsinfo($row['reagent'.$j], 0);
		}
	for ($j=1;$j<=3;$j++)
		if ($row['effect'.$j.'itemtype'])
		{
			$reagentfor[$numRow]['creates'][]['id'] = $row['effect'.$j.'itemtype'];
			$reagentfor[$numRow]['creates'][count($reagentfor[$numRow]['creates'])-1]['count'] = 1 + $row['effect'.$j.'BasePoints'];
			allitemsinfo($row['effect'.$j.'itemtype'], 0);
			@$reagentfor[$numRow]['quality'] = 6 - $allitems[$row['effect'.$j.'itemtype']]['quality'];
		}
	// Добавляем в таблицу спеллов
	allspellsinfo2($row);
}
if (IsSet($reagentfor))
	$smarty->assign('reagentfor', $reagentfor);

// Создается из...
$rows = $DB->select('
	SELECT ?#, s.spellID
	FROM ?_spell s, ?_spellicons i
	WHERE
		((s.effect1itemtype=?d
		OR s.effect2itemtype=?d
		OR s.effect3itemtype=?)
		AND (i.id = s.spellicon))
	',
	$spell_cols[2],
	$item['id'], $item['id'], $item['id']
);

foreach ($rows as $numRow=>$row)
{
	$createdfrom[$numRow] = array();
	$skillrow = $DB->selectRow('
		SELECT skillID, min_value, max_value
		FROM ?_skill_line_ability
		WHERE spellID=?d
		LIMIT 1',
		$row['spellID']
	);
	$createdfrom[$numRow] = spellinfo2(array_merge($row, $skillrow));
}

if (IsSet($createdfrom))
	$smarty->assign('createdfrom', $createdfrom);

// Ловится в ...
$drops = drop('fishing_loot_template',$item['id']);
$item['fishedin'] = array();
foreach($drops as $lootid => $drop)
{
	// Обычные локации
	$row = $DB->selectRow('
		SELECT name, areatableID as id
		FROM ?_zones
		WHERE
			areatableID=?d
			AND (x_min!=0 AND x_max!=0 AND y_min!=0 AND y_max!=0)
		LIMIT 1
		',
		$lootid
	);
	if ($row)
	{
		$num = count($item['fishedin']);
		$item['fishedin'][$num] = array();
		$item['fishedin'][$num] = array_merge($row, $drop);
	} else {
		// Инсты
		$row = $DB->selectRow('
			SELECT name, mapID as id
			FROM ?_zones
			WHERE
				areatableID=?d
			LIMIT 1
			',
			$lootid
		);
		$num = count($item['fishedin']);
		$item['fishedin'][$num] = array();
		$item['fishedin'][$num] = array_merge($row, $drop);
	}
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $item['name'].' - '.$smarty->get_config_vars('Items');
// Тип страницы
$page['type'] = 3;
$page['typeid'] = $item['id'];
// Путь
$page['path'] = '[0,0,'.$item['classs'].','.$item['subclass'].']';
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
if (IsSet($allitems))
	$smarty->assign('allitems', $allitems);
if (IsSet($allspells))
	$smarty->assign('allspells', $allspells);
$smarty->assign('item', $item);
$smarty->display('item.tpl');

?>
