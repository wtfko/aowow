<?php

require_once('includes/allnpcs.php');
require_once('includes/allitems.php');
require_once('includes/allquests.php');
require_once('includes/allcomments.php');

global $npc_cols;
global $item_cols;
global $quest_cols;

$smarty->config_load($conf_file,'faction');

// Номер фракции
$id = $podrazdel;

// Подключаемся к ДБ:
global $DB;

$row = $DB->selectRow('
	SELECT factionID, name, description1, description2, team, side
	FROM ?_factions
	WHERE factionID=?d
	LIMIT 1
',$id);
$faction=array();
// Номер фракции
$faction['id'] = $row['factionID'];
// Название фракции
$faction['name'] = $row['name'];
// Описание фракции, из клиента:
$faction['description1'] = $row['description1'];
// Описание фракции, c wowwiki.com, находится в таблице factions.sql:
$faction['description2'] = $row['description2'];
// Команда/Группа фракции
if($row['team']!=0)
	$faction['group'] = $DB->selectCell('SELECT name FROM ?_factions WHERE factionID=?d LIMIT 1', $row['team']);
// Альянс(1)/Орда(2)
if($row['side']!=0)
	$faction['side'] = $row['side'];

// Итемы с requiredreputationfaction
$rows = $DB->select('
	SELECT ?#, entry
	FROM item_template i, ?_icons a
	WHERE
		i.RequiredReputationFaction=?d
		AND a.id=i.displayid
	',
	$item_cols[2],
	$id
);

$items = array();
foreach ($rows as $i=>$row)
{
	$items[$i] = array();
	$items[$i] = iteminfo2($row, 0);
}

// Персонажи, состоящие во фракции
$rows = $DB->select('
	SELECT ?#, entry
	FROM creature_template, ?_factiontemplate
	WHERE
		faction_A IN (SELECT factiontemplateID FROM ?_factiontemplate WHERE factionID=?d)
		AND factiontemplateID=faction_A
	',
	$npc_cols[0],
	$id
);
$npcs = array();
foreach ($rows as $i=>$row)
{
	$npcs[$i] = array();
	$npcs[$i] = creatureinfo2($row);
}

// Квесты для этой фракции
$rows = $DB->select('
	SELECT ?#
	FROM quest_template
	WHERE
		RewRepFaction1=?d
		OR RewRepFaction2=?d
		OR RewRepFaction3=?d
		OR RewRepFaction4=?d
	',
	$quest_cols[2],
	$id, $id, $id, $id
);
$quests = array();
foreach ($rows as $i=>$row)
{
	$quests[$i] = array();
	$quests[$i] = questinfo2($row);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $faction['name'].' - '.$smarty->get_config_vars('Factions');
// Путь к этому разделу
$page['path'] = '[0, 7, 0]';
// Тип страницы
$page['type'] = 8;
$page['typeid'] = $faction['id'];
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// Данные о квесте
$smarty->assign('faction', $faction);
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems',$allitems);
if (isset($npcs))
	$smarty->assign('npcs',$npcs);
if (isset($quests))
	$smarty->assign('quests',$quests);
if (isset($items))
	$smarty->assign('items',$items);
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Загружаем страницу
$smarty->display('faction.tpl');
?>
