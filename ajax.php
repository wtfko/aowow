<?php

function str_normalize($str)
{
	return str_replace("'", "\'", $str);
}

// Настройка БД
global $DB;
require_once('includes/db.php');

// Параметры передаваемые скрипту
@list($what, $id) = explode("=", $_SERVER['QUERY_STRING']);

$x = '';

switch($what):
	case 'item':
		require_once('includes/allitems.php');
		$item = allitemsinfo($id, 1);
		$x .= '$WowheadPower.registerItem('.$id.', 0, {';
		if ($item['name'])
			$x .= 'name: \''.str_normalize($item['name']).'\',';
		if ($item['quality'])
			$x .= 'quality: '.$item['quality'].',';
		if ($item['icon'])
			$x .= 'icon: \''.str_normalize($item['icon']).'\',';
		if ($item['info'])
			$x .= 'tooltip: \''.str_normalize($item['info']).'\'';
		$x .= '});';
		break;
	case 'spell':
		require_once('includes/allspells.php');
		$spell = allspellsinfo($id, 1);
		$x .= '$WowheadPower.registerSpell('.$id.', 0,{';
		if ($spell['name'])
			$x .= 'name: \''.str_normalize($spell['name']).'\',';
		if ($spell['icon'])
			$x .= 'icon: \''.str_normalize($spell['icon']).'\',';
		if ($spell['info'])
			$x .= 'tooltip: \''.str_normalize($spell['info']).'\'';
		$x .= '});';
		break;
	case 'quest':
		require_once('includes/allquests.php');
		$quest = allquestsinfo($id, 1);
		$x .= '$WowheadPower.registerQuest('.$id.', 0,{';
		if ($quest['name'])
			$x .= 'name: \''.str_normalize($quest['name']).'\',';
		if ($quest['info'])
			$x .= 'tooltip: \''.str_normalize($quest['info']).'\'';
		$x .= '});';
		break;
	default:
		break;
endswitch;

echo $x;

?>
