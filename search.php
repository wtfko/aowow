<?php

// Необходима функция iteminfo
require_once('includes/game.php');
require_once('includes/allspells.php');
require_once('includes/allquests.php');
require_once('includes/allitems.php');
require_once('includes/allnpcs.php');
require_once('includes/allobjects.php');

// Настраиваем Smarty ;)
$smarty->config_load($conf_file, 'search');

// Строка поиска:
$search = urldecode($podrazdel);
$nsearch = '%'.str_normalize($search).'%';

// Заголовок страницы
$title = $search . ' - ' . $smarty->get_config_vars('name') . ' ' . $smarty->
	get_config_vars('Search');
$tab = '0';

// Подключаемся к ДБ
global $DB;
global $allitems;
global $allspells;

global $npc_cols;
global $spell_cols;

// Ищем вещи:
$rows = $DB->select('
	SELECT ?#
	FROM item_template i, ?_icons a
	WHERE
		i.name like ?
		AND a.id=i.displayid;
	',
	$item_cols[3],
	$nsearch
);
foreach ($rows as $numRow=>$row)
	$items[] = iteminfo2($row);
if (isset($items))
	$smarty->assign('items', $items);

// Ищем NPC
$rows = $DB->select('
	SELECT ?#, entry
	FROM creature_template, ?_factiontemplate
	WHERE
		(name like ?
		OR subname like ?)
		AND factiontemplateID=faction_A
	',
	$npc_cols[0],
	$nsearch, $nsearch
);
foreach ($rows as $numRow=>$row)
	$npcs[] = creatureinfo2($row);
if (isset($npcs))
	$smarty->assign('npcs', $npcs);

// Ищем объекты
$rows = $DB->select('
	SELECT ?#
	FROM gameobject_template
	WHERE name like ?
	',
	$object_cols[0],
	$nsearch
);
foreach ($rows as $numRow=>$row)
	$objects[] = objectinfo2($row);
if (isset($objects))
	$smarty->assign('objects', $objects);

// Ищем квесты
$rows = $DB->select('
	SELECT *
	FROM quest_template
	WHERE Title like ?
	',
	$nsearch
);
foreach ($rows as $numRow=>$row)
	$quests[] = questinfo2($row);
if (isset($quests))
	$smarty->assign('quests', $quests);

// Ищем наборы вещей
$rows = $DB->select('
	SELECT *
	FROM ?_itemset
	WHERE name like ?
	',
	$nsearch
);
foreach ($rows as $numRow=>$row)
	$itemsets[] = itemsetinfo2($row);
if (isset($itemsets))
	$smarty->assign('itemsets', $itemsets);

// Ищем спеллы
$rows = $DB->select('
	SELECT ?#, spellID
	FROM ?_spell s, ?_spellicons i
	WHERE
		s.spellname like ?
		AND i.id = s.spellicon
	',
	$spell_cols[2],
	$nsearch
);
foreach ($rows as $numRow=>$row)
	$spells[] = spellinfo2($row);
if (isset($spells))
	$smarty->assign('spells', $spells);

// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems', $allitems);
if (isset($allspells))
	$smarty->assign('allspells', $allspells);

$smarty->assign('mysql', $DB->getStatistics());
$smarty->assign('tab', $tab);
$smarty->assign('title', $title);
$smarty->assign('search', $search);
$smarty->display('search.tpl');

?>
