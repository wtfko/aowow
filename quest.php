<?php

// Необходима функция questinfo
require_once('includes/allquests.php');
require_once('includes/allobjects.php');
require_once('includes/allnpcs.php');
require_once('includes/allcomments.php');

$smarty->config_load($conf_file, 'quest');

// Номер квеста
$id = $podrazdel;

// Подключаемся к ДБ:
global $DB;

// Составляем запрос к БД, выполняющий поиск квеста
$row = $DB->selectRow('
	SELECT ?#
	FROM quest_template
	WHERE
		entry=?d
	LIMIT 1
	',
	$quest_cols[3],
	$id
);

// Посылаем запрос БД
$quest = array();
$quest = questinfo2($row, 1);

// Перевод
if ($_SESSION['locale'])
	$locale_row = $DB->selectRow('
		SELECT ?#
		FROM locales_quest
		WHERE
			entry=?d
		LIMIT 1
		',
		$locale_quest_cols,
		$quest['id']
	);

$quest['Objectives'] = QuestReplaceStr(htmlspecialchars((IsSet($locale_row['Objectives_loc'.$_SESSION['locale']])? $locale_row['Objectives_loc'.$_SESSION['locale']] : $row['Objectives'])));
$quest['Details'] = QuestReplaceStr(htmlspecialchars((IsSet($locale_row['Details_loc'.$_SESSION['locale']])? $locale_row['Details_loc'.$_SESSION['locale']] : $row['Details'])));
$quest['RequestItemsText'] = QuestReplaceStr(htmlspecialchars((IsSet($locale_row['RequestItemsText_loc'.$_SESSION['locale']])? $locale_row['RequestItemsText_loc'.$_SESSION['locale']] : $row['RequestItemsText'])));
$quest['OfferRewardText'] = QuestReplaceStr(htmlspecialchars((IsSet($locale_row['OfferRewardText_loc'.$_SESSION['locale']])? $locale_row['OfferRewardText_loc'.$_SESSION['locale']] : $row['OfferRewardText'])));

for ($j=1;$j<=4;$j++)
	if ($row['ObjectiveText'.$j])
		$quest['ObjectiveText'][$j] = QuestReplaceStr(htmlspecialchars((IsSet($locale_row['ObjectiveText'.$j.'_loc'.$_SESSION['locale']])? $locale_row['ObjectiveText'.$j.'_loc'.$_SESSION['locale']] : $row['ObjectiveText'.$j])));

// Денежная награда
if ($quest['money'])
{
	$quest['moneygold'] = floor($quest['money']/10000);
	$quest['moneysilver'] = floor(($quest['money'] - ($quest['moneygold']*10000))/100);
	$quest['moneycopper'] = floor($quest['money'] - ($quest['moneygold']*10000) - ($quest['moneysilver']*100));
}

// Создания, необходимые для квеста
$c=0;$o=0;
$quest['creaturereqs'] = array();
$quest['objectreqs'] = array();
for ($i=1;$i<=4;$i++)
{
	if (($row['ReqCreatureOrGOId'.$i]!=0) and ($row['ReqCreatureOrGOCount'.$i]!=0))
	{
		if ($row['ReqCreatureOrGOId'.$i]>0)
		{
			// Необходимо какое-либо взамодействие с созданием
			$quest['creaturereqs'][$c] = array();
			$quest['creaturereqs'][$c]=creatureinfo($row['ReqCreatureOrGOId'.$i]);
			$quest['creaturereqs'][$c]['count'] = $row['ReqCreatureOrGOCount'.$i];
			$c++;
		} else {
			// необходимо какое-то взаимодействие с объектом
			$quest['objectreqs'][$o] = array();
			$quest['objectreqs'][$o]=objectinfo(abs($row['ReqCreatureOrGOId'.$i]));
			$quest['objectreqs'][$o]['count'] = $row['ReqCreatureOrGOCount'.$i];
			$o++;
		}
	}
}

// Вещи, необходимые для квеста
$k=0;
$quest['itemreqs'] = array();
for ($i=1;$i<=4;$i++)
{
	$item_id = $row['ReqItemId'.$i];
	$item_count = $row['ReqItemCount'.$i];
	if (($item_id!=0) and ($item_count!=0))
	{
		$quest['itemreqs'][$k] = array();
		$quest['itemreqs'][$k]=iteminfo($item_id);
		$quest['itemreqs'][$k]['count'] = $item_count;
		$k++;
	}
}

$tmp=$id;
// Устанавливаем 1 номер квеста в серии
while($tmp)
{
	$row = $DB->selectRow('SELECT entry FROM quest_template WHERE NextQuestInChain=? LIMIT 1', $tmp);
	if ($row)
		$tmp = $row['entry'];
	else
		break;
}

$i=0;
$quest['series'] = array();
$quest['series'][0] = array();
$quest['series'][0]['id']=$tmp;
while(true)
{
	$row = $DB->selectRow('SELECT NextQuestInChain, Title FROM quest_template WHERE entry=? LIMIT 1', $quest['series'][$i]['id']);
	if ($row)
	{
		$quest['series'][$i]['name'] = $row['Title'];
		if ($row['NextQuestInChain']<=0)
			break;
		$quest['series'][$i+1] = array();
		$quest['series'][$i+1]['id'] = $row['NextQuestInChain'];
		$i++;
	}
}
if ($i<=0)
	{ UnSet($quest['series']); }

// Откуда квест и куда =)
if (!IsSet($quest['start']))
{
	// Может из создания?
	$row = $DB->selectRow('SELECT id FROM creature_questrelation WHERE quest=? LIMIT 1', $id);
	// Посылаем запрос БД
	if ($row)
	{
		$quest['start'] = array();
		$quest['start']=creatureinfo($row['id']);
		$quest['start']['type']='npc';
	}
}

// Может из объекта?
if (!IsSet($quest['start']))
{
	$row = $DB->selectRow('SELECT id FROM gameobject_questrelation WHERE quest=? LIMIT 1', $id);
	// Посылаем запрос БД
	if ($row)
	{
		$quest['start'] = array();
		$quest['start']=objectinfo($row['id']);
		$quest['start']['type']='object';
	}
}

// Если квест начинается с вещи - записываем название, номер и кол-во
if (!IsSet($quest['start']))
{
	$row = $DB->selectRow('SELECT name, entry as id FROM item_template, ?_icons WHERE startquest=?d AND id=displayid LIMIT 1', $id);
	if ($row)
	{
		$quest['start'] = array();
		$quest['start'] = $row;
		$quest['start']['type']='item';
	}
}

// Кому сдавать
// Может созданию?
$row = $DB->selectRow('SELECT id FROM creature_involvedrelation WHERE quest=? LIMIT 1', $id);
// Посылаем запрос БД
if ($row)
{
	$quest['end'] = array();
	$quest['end']=creatureinfo($row['id']);
	$quest['end']['type']='npc';
}

if (!IsSet($quest['end']))
{
	// Может объекту?
	$row = $DB->selectRow('SELECT id FROM gameobject_involvedrelation WHERE quest=? LIMIT 1', $id);
	// Посылаем запрос БД
	if ($row)
	{
		$quest['end'] = array();
		$quest['end']=objectinfo($row['id']);
		$quest['end']['type']='object';
	}
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $quest['name'].' - '.$smarty->get_config_vars('Quests');
// Путь к этому разделу
$page['path'] = '[0, 3,'.$quest['type'].' ,'.$quest['category'].']';
// Тип страницы
$page['type'] = 5;
$page['typeid'] = $quest['id'];
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// Данные о квесте
$smarty->assign('quest', $quest);
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems',$allitems);
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Загружаем страницу
$smarty->display('quest.tpl');

?>
