<?php
require_once('includes/game.php');
require_once('includes/allcomments.php');

$smarty->config_load($conf_file, 'itemset');

// БД
global $DB;
$id = $podrazdel;

$row = $DB->selectRow("SELECT * from ?_itemset WHERE itemsetID=? LIMIT 1", $id);
$i=0;
if ($row) {
	$itemset = array();
	$itemset['id'] = $row['itemsetID'];
	$itemset['name'] = $row['name'];
	$itemset['minlevel'] = 255;
	$itemset['maxlevel'] = 0;
	$itemset['count'] = 0;
	$x=0;
	$itemset['pieces'] = array();
	for ($j=1;$j<=10;$j++)
	{
		if ($row['item'.$j])
		{
			$itemset['pieces'][$itemset['count']] = array();
			$itemset['pieces'][$itemset['count']] = iteminfo($row['item'.$j]);
			if ($itemset['pieces'][$itemset['count']]['level'] < $itemset['minlevel']) $itemset['minlevel'] = $itemset['pieces'][$itemset['count']]['level'];
			if ($itemset['pieces'][$itemset['count']]['level'] > $itemset['maxlevel']) $itemset['maxlevel'] = $itemset['pieces'][$itemset['count']]['level'];
			$itemset['count']++;
		}
	}
	$itemset['spells'] = array();
	for ($j=1;$j<=8;$j++)
		if ($row['spell'.$j])
		{
			$itemset['spells'][$x] = array();
			$itemset['spells'][$x]['id'] = $row['spell'.$j];
			$itemset['spells'][$x]['tooltip'] = spell_desc($row['spell'.$j]);
			$itemset['spells'][$x]['bonus'] = $row['bonus'.$j];
			$x++;
		}
	for ($i=0;$i<=$x-1;$i++)
		for ($j=$i;$j<=$x-1;$j++)
			if ($itemset['spells'][$j]['bonus'] < $itemset['spells'][$i]['bonus'])
			{
				UnSet($tmp);
				$tmp = $itemset['spells'][$i];
				$itemset['spells'][$i] = $itemset['spells'][$j];
				$itemset['spells'][$j] = $tmp;
			}
}
$smarty->assign('itemset', $itemset);

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $itemset['name'].' - '.$smarty->get_config_vars('Item_Sets');
// Путь к этому разделу
$page['path'] = '[0, 2]';
// Тип страницы
$page['type'] = 4;
$page['typeid'] = $itemset['id'];
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// --Передаем данные шаблонизатору--
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems',$allitems);
if (isset($allitems))
	$smarty->assign('allspells',$allspells);
// Запускаем шаблонизатор
$smarty->display('itemset.tpl');
?>
