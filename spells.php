<?php

require_once('includes/allspells.php');

$smarty->config_load($conf_file, 'spells');

global $DB;
global $spell_cols;

@list($s1, $s2, $s3) = explode('.', $podrazdel);

if ($s1==7)
{
	// Классы
	if (!isset($s2))
		$classes = array(1, 2, 4, 8, 16, 64, 128, 256, 1024);

	$rows = $DB->select('
		SELECT ?#, s.`spellID`
		FROM ?_spell s, ?_skill_line_ability sla, ?_spellicons i, ?_skill sk
		WHERE
			s.spellID = sla.spellID
			AND i.id=s.spellicon
			{AND sla.classmask = ?d}
			{AND sla.skillID=?d}
			{AND sla.classmask IN (?a)}
			AND sla.skillID=sk.skillID
	',
	$spell_cols[2],
	(isset($s2))? pow(2, ($s2-1)): DBSIMPLE_SKIP,
	(isset($s3))? $s3: DBSIMPLE_SKIP,
	(isset($classes))? $classes: DBSIMPLE_SKIP
	);
} elseif ($s1>0) {
	// Профессии
	$rows = $DB->select('
		SELECT
			?#, `s`.`spellID`,
			sla.skillID, sla.min_value, sla.max_value
		FROM ?_spell s, ?_skill_line_ability sla, ?_spellicons i, ?_skill sk
		WHERE
			s.spellID = sla.spellID
			AND i.id=s.spellicon
			{AND sk.categoryID=?d}
			{AND sla.skillID=?d}
			AND sla.skillID=sk.skillID
	',
	$spell_cols[2],
	$s1,
	(isset($s2))? $s2: DBSIMPLE_SKIP
	);
} elseif ($s1==-3) {
	if (!(isset($s2)))
		$pets = array(270, 653, 210, 211, 213, 209, 214, 212, 763, 215, 654, 764, 655, 217, 767, 236, 768, 203, 218, 251, 766, 656, 208, 761, 189, 188, 205, 204);
	$rows = $DB->select('
		SELECT
			?#, `s`.`spellID`
		FROM ?_spell s, ?_skill_line_ability sla, ?_spellicons i
		WHERE
			s.spellID = sla.spellID
			AND i.id=s.spellicon
			{AND sla.skillID=?d}
			{AND sla.skillID IN (?a)}
	',
	$spell_cols[2],
	(isset($s2))? $s2: DBSIMPLE_SKIP,
	(isset($pets))? $pets: DBSIMPLE_SKIP
	);
} elseif ($s1==-4) {
	// Racial Traits
	$rows = $DB->select('
		SELECT
			?#, `s`.`spellID`
		FROM ?_spell s, ?_spellicons i
		WHERE
			s.spellID IN (SELECT spellID FROM ?_skill_line_ability WHERE racemask>0)
			AND i.id=s.spellicon
		',
		$spell_cols[2]
	);
} elseif ($s1==-2) {
	// Talents

} else {
	// просто спеллы
	$rows = $DB->select('
		SELECT
			?#, `s`.`spellID`
		FROM ?_spell s, ?_spellicons i
		WHERE
			i.id=s.spellicon
		LIMIT 2000;
	',
	$spell_cols[2]
	);
}

$spells = array();
foreach($rows as $i => $row)
{
	$spells[$i] = spellinfo2($row);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('Spells');
// Путь к этому разделу
$page['path'] = "[0, 6, $s1 , $s2, $s3]";
$smarty->assign('page', $page);

// Статистика выполнения mysql запросов
$smarty->assign('mysql', $DB->getStatistics());
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems',$allitems);
if (count($allspells)>=0)
	$smarty->assign('allspells',$allspells);
if (count($spells>=0))
	$smarty->assign('spells',$spells);
// Загружаем страницу
$smarty->display('spells.tpl');

?>
