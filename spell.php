<?php

require_once('includes/allspells.php');
require_once('includes/allnpcs.php');
require_once('includes/allquests.php');
require_once('includes/allcomments.php');

$smarty->config_load($conf_file,'spell');

// Заголовок страницы
$title = 'Spell - AoWoW MangOS Interface';

// номер спелла;
$id = $podrazdel;

// БД
global $DB;
// Таблица спеллов
global $allspells;
// Таблица вещей
global $allitems;

global $npc_cols;

// Данные об спелле:
$row = $DB->selectRow('
	SELECT s.*, i.iconname
	FROM ?_spell s, ?_spellicons i
	WHERE
		s.spellID=?
		AND i.id = s.spellicon
	',
	$id
);
if ($row)
{
	$spell = array();
	// Номер спелла
	$spell['id'] = $id;
	// Имя спелла
	$spell['name'] = $row['spellname'];
	// Иконка спелла
	//$spell['icon'] = $row['iconname'];
	// Затраты маны на сспелл
	if ($row['manacost'])
		$spell['manacost'] = $row['manacost'];
	elseif ($row['manacostpercent'])
		$spell['manacost'] = $row['manacostpercent'].'% '.$smarty->get_config_vars('of_base');
	// Уровень спелла
	$spell['level'] = $row['levelspell'];
	// Дальность
	$RangeRow = $DB->selectRow('SELECT rangeMin, rangeMax, name from ?_spellrange where rangeID=? limit 1', $row['rangeID']);
	$spell['range'] = '';
	if (($RangeRow['rangeMin'] != $RangeRow['rangeMax']) and ($RangeRow['rangeMin'] != 0))
		$spell['range'] = $RangeRow['rangeMin'].'-';
	$spell['range'] .= $RangeRow['rangeMax'];
	$spell['rangename'] = $RangeRow['name'];
	// Время каста
	$casttime = $DB->selectCell('SELECT base from ?_spellcasttimes where id=? limit 1', $row['spellcasttimesID']);
	if ($casttime>0)
		$spell['casttime'] = ($casttime/1000).' '.$smarty->get_config_vars('seconds');
	else
		$spell['casttime'] = 'Channeled/Instant';
	// Cooldown
	if ($row['cooldown']>0)
		$spell['cooldown'] = $row['cooldown'] / 1000;
	// Время действия спелла
	$duration = $DB->selectCell('SELECT durationBase FROM ?_spellduration WHERE durationID=?d LIMIT 1', $row['durationID']);
	if ($duration > 0)
		$spell['duration'] = ($duration/1000).' '.$smarty->get_config_vars('seconds');
	else
		$spell['duration'] ='<span class="q0">n/a</span>';
	// Школа спелла
	$spell['school'] = $DB->selectCell('SELECT name FROM ?_resistances WHERE id=?d LIMIT 1', $row['resistancesID']);
	// Тип диспела
	if ($row['dispeltypeID'])
		$spell['dispel'] = $DB->selectCell('SELECT name FROM ?_spelldispeltype WHERE id=?d LIMIT 1', $row['dispeltypeID']);
	// Механика спелла
	if ($row['mechanicID'])
		$spell['mechanic'] = $DB->selectCell('SELECT name FROM ?_spellmechanic WHERE id=?d LIMIT 1', $row['mechanicID']);

	// Информация о спелле
	$spell['info'] = allspellsinfo2($row, 2);

	// Инструменты
	$spell['tools'] = array();
	$i=0;
	for ($j=1;$j<=2;$j++)
	{
		if ($row['tool'.$j])
		{
			$spell['tools'][$i] = array();
			// Имя инструмента
			$tool_row = $DB->selectRow('SELECT ?#, `name`, `quality` FROM item_template, ?_icons WHERE entry=?d AND id=displayid LIMIT 1', $item_cols[0], $row['tool'.$j]);
			$spell['tools'][$i]['name'] = $tool_row['name'];
			$spell['tools'][$i]['quality'] = $tool_row['quality'];
			// ID инструмента
			$spell['tools'][$i]['id'] = $row['tool'.$j];
			// Добавляем инструмент в таблицу вещей
			allitemsinfo2($tool_row, 0);
			$i++;
		}
	}

	// Реагенты
	$spell['reagents'] = array();
	$i=0;
	for ($j=1;$j<=8;$j++)
	{
		if ($row['reagent'.$j])
		{
			$spell['reagents'][$i] = array();
			// Имя реагента
			$reagentrow = $DB->selectRow('
				SELECT ?#, name
				FROM item_template, ?_icons
				WHERE
					entry=?d
					AND id=displayid
				LIMIT 1
				',
				$item_cols[0],
				$row['reagent'.$j]
			);
			$spell['reagents'][$i]['name'] = $reagentrow ['name'];
			$spell['reagents'][$i]['quality'] = $reagentrow ['quality'];
			// ID реагента
			$spell['reagents'][$i]['id'] = $row['reagent'.$j];
			// Количество реагентов
			$spell['reagents'][$i]['count'] = $row['reagentcount'.$j];
			// Добавляем реагент в таблицу вещей
			allitemsinfo2($reagentrow, 0);
			$i++;
		}
	}

	// Перебираем все эффекты:
	$i=0;
	$spell['effect'] = array();
	// Btt - Buff TollTip
	if ($row['buff'])
		$spell['btt'] = spell_buff_render($row);
	for ($j=1;$j<=3;$j++)
	{
		// Название эффекта
		if ($row['effect'.$j.'id'] > 0)
			$spell['effect'][$i]['name'] = $spell_effect_names[$row['effect'.$j.'id']];
		// Доп информация в имени
		if ($row['effect'.$j.'MiscValue'])
		{
			// Если эффект - создание обекта, создаем информацию о нём
			if (($row['effect'.$j.'id'] == 50) or ($row['effect'.$j.'id'] == 76) or ($row['effect'.$j.'id'] == 86) or ($row['effect'.$j.'id'] == 104) or ($row['effect'.$j.'id'] == 105) or ($row['effect'.$j.'id'] == 106) or ($row['effect'.$j.'id'] == 107))
			{
				$spell['effect'][$i]['object'] = array();
				$spell['effect'][$i]['object']['id'] = $row['effect'.$j.'MiscValue'];
				$spell['effect'][$i]['object']['name'] = $DB->selectCell("SELECT name from gameobject_template where entry=? limit 1", $spell['effect'][$i]['object']['id']);
			}
			if ($row['effect'.$j.'id'] == 118)
			{
				$spell['effect'][$i]['name'] .= ' ('.$DB->selectCell('SELECT name FROM ?_skill WHERE skillID=? LIMIT 1', $row['effect'.$j.'MiscValue']).')';
			} else {
				$spell['effect'][$i]['name'] .= ' ('.$row['effect'.$j.'MiscValue'].')';
			}
		}
		// Если просто урон школой - добавляем подпись школы
		if ($row['effect'.$j.'id'] == 2)
			$spell['effect'][$i]['name'] .= ' ('.$spell['school'].')';
		// Радиус действия эффекта
		if ($row['effect'.$j.'radius'])
			$spell['effect'][$i]['radius'] = $DB->selectCell("SELECT radiusbase from ?_spellradius where radiusID=? limit 1", $row['effect'.$j.'radius']);
		// Значение спелла (урон)
		if (($row['effect'.$j.'BasePoints']) and (!($row['effect'.$j.'itemtype'])))
			$spell['effect'][$i]['value'] = $row['effect'.$j.'BasePoints'] + 1;
		// Интервал действия спелла
		if ($row['effect'.$j.'Amplitude'] > 0)
			$spell['effect'][$i]['interval'] = $row['effect'.$j.'Amplitude'] / 1000;
		// Название ауры:
		if (($row['effect'.$j.'Aura'] > 0) and (IsSet($spell_aura_names[$row['effect'.$j.'Aura']])))
			$spell['effect'][$i]['name'] .= ': '.$spell_aura_names[$row['effect'.$j.'Aura']];
		elseif ($row['effect'.$j.'Aura'] > 0)
			$spell['effect'][$i]['name'] .= ': Unknown_Aura('.$row['effect'.$j.'Aura'].')';
		// Создает вещь:
		if (($row['effect'.$j.'itemtype']) and (!($row['effect'.$j.'Aura'])))
		{
			$spell['effect'][$i]['item'] = array();
			$spell['effect'][$i]['item']['id'] = $row['effect'.$j.'itemtype'];
			$tmpRow = $DB->selectRow('
				SELECT ?#, `name`
				FROM item_template, ?_icons
				WHERE
					entry=?
					AND id=displayID
				LIMIT 1
				',
				$item_cols[0],
				$spell['effect'][$i]['item']['id']
			);
			$spell['effect'][$i]['item']['name'] = $tmpRow['name'];
			$spell['effect'][$i]['item']['quality'] = $tmpRow['quality'];
			$spell['effect'][$i]['item']['count'] = $row['effect'.$j.'BasePoints'] + 1;
			// Иконка итема, если спелл создает этот итем
			if(!IsSet($spell['icon']))
				$spell['icon'] = $tmpRow['iconname'];
			allitemsinfo2($tmpRow, 0);
		}
		// Создает спелл
		if ($row['effect'.$j.'triggerspell'] > 0)
		{
			$spell['effect'][$i]['spell'] = array();
			$spell['effect'][$i]['spell']['id'] = $row['effect'.$j.'triggerspell'];
			$spell['effect'][$i]['spell']['name'] = $DB->selectCell('SELECT spellname FROM ?_spell WHERE spellID=?d LIMIT 1', $spell['effect'][$i]['spell']['id']);
			allspellsinfo($spell['effect'][$i]['spell']['id']);
		}
		$i++;
	}

	if(!$spell['icon'])
		$spell['icon'] = $row['iconname'];


	// Спеллы с таким же названием
	$spell['seealso'] = array();
	$rows = $DB->select('
		SELECT s.*, i.iconname
		FROM ?_spell s, ?_spellicons i
		WHERE
			s.spellname=?
			AND s.spellID!=?d
			AND (
						(s.effect1id=?d AND s.effect1id!=0)
						OR (s.effect2id=?d AND s.effect2id!=0)
						OR (s.effect3id=?d AND s.effect3id!=0)
					)
			AND i.id=s.spellicon
		',
		$spell['name'],
		$spell['id'],
		$row['effect1id'],
		$row['effect2id'],
		$row['effect3id']
	);
	foreach($rows as $i => $row)
	{
		$spell['seealso'][$i] = array();
		$spell['seealso'][$i] = spellinfo2($row);
	}

	// Кто обучает этому спеллу
	$spell['taughtbynpc'] = array();
	// Список тренеров, обучающих нужному спеллу
	$taughtbytrainers = $DB->select('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			entry IN (SELECT entry FROM npc_trainer WHERE spell=?d)
			AND factiontemplateID=faction_A
		',
		$npc_cols[0],
		$spell['id']
	);

	foreach($taughtbytrainers as $i=>$npcrow)
	{
		$num = count($spell['taughtbynpc']);
		$spell['taughtbynpc'][$num] = array();
		$spell['taughtbynpc'][$num] = creatureinfo2($npcrow);
	}

	// Список книг, кастующих спелл, обучающий нужному спеллу
	$spell['taughtbyitem'] = array();
	$taughtbyitem = $DB->select('
		SELECT ?#, entry
		FROM item_template, ?_icons
		WHERE
			((spellid_2=?d)
			AND (spelltrigger_2=6))
			AND id=displayid
		',
		$item_cols[2],
		$spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id']
	);
	foreach($taughtbyitem as $i=>$itemrow)
	{
		$num = count($spell['taughtbyitem']);
		$spell['taughtbyitem'][$num] = array();
		$spell['taughtbyitem'][$num] = iteminfo2($itemrow, 0);
	}

	// Список спеллов, обучающих этому спеллу:
	$taughtbyspells = $DB->selectCol('
		SELECT spellID
		FROM ?_spell
		WHERE
			(effect1triggerspell=?d AND (effect1id=57 OR effect1id=36))
			OR (effect2triggerspell=?d AND (effect2id=57 OR effect2id=36))
			OR (effect3triggerspell=?d AND (effect3id=57 OR effect3id=36))
		',
		$spell['id'], $spell['id'], $spell['id']
	);

	if ($taughtbyspells)
	{
		// Список петов, кастующих спелл, обучающий нужному спеллу
		$taughtbypets = $DB->select('
			SELECT ?#, entry
			FROM creature_template, ?_factiontemplate
			WHERE
				entry IN (SELECT entry FROM petcreateinfo_spell WHERE (Spell1 IN (?a)) OR (Spell2 IN (?a)) OR (Spell3 IN (?a)) OR (Spell4 IN (?a)))
				AND factiontemplateID=faction_A
			',
			$npc_cols[0],
			$taughtbyspells, $taughtbyspells, $taughtbyspells, $taughtbyspells
		);
		// Перебираем этих петов
		foreach($taughtbypets as $i=>$petrow)
		{
			$num = count($spell['taughtbynpc']);
			$spell['taughtbynpc'][$num] = array();
			$spell['taughtbynpc'][$num] = creatureinfo2($petrow);
		}

		// Список квестов, наградой за которые является спелл, обучающий нужному спеллу
		$spell['taughtbyquest'] = array();
		$taughtbyquest = $DB->select('
			SELECT ?#
			FROM quest_template
			WHERE
				RewSpell IN (?a)
			',
			$quest_cols[2],
			$taughtbyspells
		);
		foreach($taughtbyquest as $i=>$questrow)
		{
			$num = count($spell['taughtbyquest']);
			$spell['taughtbyquest'][$num] = array();
			$spell['taughtbyquest'][$num] = questinfo2($questrow, 0);
		}
		
		// Список НПЦ, кастующих нужный квест, бла-бла-бла
		$taughtbytrainers = $DB->select('
			SELECT ?#, entry
			FROM creature_template, ?_factiontemplate
			WHERE
				entry IN (SELECT entry FROM npc_trainer WHERE spell in (?a))
				AND factiontemplateID=faction_A
			',
			$npc_cols[0],
			$taughtbyspells
		);
		foreach($taughtbytrainers as $i=>$npcrow)
		{
			$num = count($spell['taughtbynpc']);
			$spell['taughtbynpc'][$num] = array();
			$spell['taughtbynpc'][$num] = creatureinfo2($npcrow);
		}
		
		// Список книг, кастующих спелл, обучающий нужному спеллу
		$taughtbyitem = $DB->select('
			SELECT ?#, entry
			FROM item_template, ?_icons
			WHERE
				((spellid_1 IN (?a))
				OR (spellid_2 IN (?a))
				OR (spellid_3 IN (?a))
				OR (spellid_4 IN (?a))
				OR (spellid_5 IN (?a)))
				AND id=displayid
			',
			$item_cols[2],
			$taughtbyspells, $taughtbyspells, $taughtbyspells, $taughtbyspells, $taughtbyspells
		);
		foreach($taughtbyitem as $i=>$itemrow)
		{
			$num = count($spell['taughtbyitem']);
			$spell['taughtbyitem'][$num] = array();
			$spell['taughtbyitem'][$num] = iteminfo2($itemrow, 0);
		}
	}

	// Используется NPC:
	$spell['usedbynpc'] = array();
	$rows = $DB->select('
		SELECT ?#, entry
		FROM creature_template, ?_factiontemplate
		WHERE
			(spell1=?d
			OR spell2=?d
			OR spell3=?d
			OR spell4=?d)
			AND factiontemplateID=faction_A
		',
		$npc_cols[0],
		$spell['id'], $spell['id'], $spell['id'], $spell['id']
	);
	foreach($rows as $i=>$row)
	{
		$spell['usedbynpc'][$i] = array();
		$spell['usedbynpc'][$i] = creatureinfo2($row);
	}

	// Используется вещями:
	$spell['usedbyitem'] = array();
	$rows = $DB->select('
		SELECT ?#, entry
		FROM item_template, ?_icons
		WHERE
			(spellid_1=?d OR (spellid_2=?d AND spelltrigger_2!=6) OR spellid_3=?d OR spellid_4=?d OR spellid_5=?d)
			AND id=displayID
		',
		$item_cols[2],
		$spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id']
	);
	foreach($rows as $i => $row)
	{
		$spell['usedbyitem'][$i] = array();
		$spell['usedbyitem'][$i] = iteminfo2($row, 0);
	}

	// Используется наборами вещей:
	$spell['usedbyitemset'] = array();
	$rows = $DB->select('
		SELECT *
		FROM ?_itemset
		WHERE spell1=?d or spell2=?d or spell3=?d or spell4=?d or spell5=?d or spell6=?d or spell7=?d or spell8=?d
		',
		$spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id'], $spell['id']
	);
	foreach($rows as $i => $row)
	{
		$spell['usedbyitemset'][$i] = array();
		$spell['usedbyitemset'][$i] = itemsetinfo2($row);
	}

	// Спелл - награда за квест
	$spell['questreward'] = array();
	$rows = $DB->select('
		SELECT ?#
		FROM quest_template
		WHERE RewSpell=?d
		',
		$quest_cols[2],
		$spell['id']
	);
	foreach($rows as $i => $row)
	{
		$spell['questreward'][$i] = array();
		$spell['questreward'][$i] = questinfo2($row);
	}

	$smarty->assign('spell', $spell);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $spell['name'].' - '.$smarty->get_config_vars('Spells');
// Путь к этому разделу
$page['path'] = '[0,1]';
// Тип страницы
$page['type'] = 6;
$page['typeid'] = $spell['id'];
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
if (count($allspells)>=0)
	$smarty->assign('allspells',$allspells);
if (count($allitems)>=0)
	$smarty->assign('allitems',$allitems);

$smarty->display('spell.tpl');

?>
