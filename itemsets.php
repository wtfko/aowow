<?php

require_once('includes/allitemsets.php');

$smarty->config_load($conf_file);

// Заголовок страницы
$title = 'Item sets - AoWoW MangOS Interface';
$tab=0;

global $DB;
global $allitems;
global $itemset_col;

$rows = $DB->select("SELECT ?# from ?_itemset", $itemset_col[0]);

$itemsets = array();
foreach ($rows as $numRow=>$row)
	$itemsets[] = itemsetinfo2($row);
$smarty->assign('itemsets', $itemsets);

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('Item_Sets');
// Путь к этому разделу
$page['path'] = '[0, 2]';
// Тип страницы
$smarty->assign('page', $page);

// --Передаем данные шаблонизатору--
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (isset($allitems))
	$smarty->assign('allitems',$allitems);
// Запускаем шаблонизатор
$smarty->display('itemsets.tpl');

?>
