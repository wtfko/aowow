<?php

require_once ('includes/allobjects.php');
require_once ('includes/allitems.php');
require_once ('includes/allcomments.php');
require_once ('includes/allquests.php');

$smarty->config_load($conf_file, 'object');

// номер объекта;
$id = $podrazdel;

// БД
global $DB;

// Данные об объекте:
$object = array();
$object = objectinfo($id, 1);

// Лут...
$object['drop'] = array();
$object['drop'] = loot('gameobject_loot_template', $object['lootid']);

// Начиниают квесты...
$rows = $DB->select('
	SELECT ?#
	FROM gameobject_questrelation q, quest_template o
	WHERE
		q.id=?
		AND o.entry=q.quest
	',
	$quest_cols[2],
	$id
);
$object['starts'] = array();
foreach ($rows as $numRow=>$row) {
	$object['starts'][$numRow] = array();
	$object['starts'][$numRow] = questinfo2($row);
}

// Заканчивают квесты...
$rows = $DB->select('
	SELECT ?#
	FROM gameobject_involvedrelation i, quest_template q
	WHERE
		i.id=?
		AND q.entry=i.quest
	',
	$quest_cols[2],
	$id
);
$object['ends'] = array();
foreach ($rows as $numRow=>$row) {
	$object['ends'][$numRow] = array();
	$object['ends'][$numRow] = questinfo2($row);
}

// Положения объектофф:
$rows = $DB->select('SELECT position_y, position_x, map FROM gameobject WHERE id=?d', $id);
$data = array();
foreach ($rows as $numRow=>$row) {
	$data[$numRow] = array();
	$data[$numRow]['y'] = $row["position_y"];
	$data[$numRow]['x'] = $row["position_x"];
	$data[$numRow]['m'] = $row["map"];
}

if (count($data)>0)
{
	$data = mass_coord($data);
	$zonedata = array();

	unset($tmp);
	// Сортируем массив. Зачем???
	if ($data) sort($data);

	// Во временную переменную tmp заносим номер локации
	$j = 0;
	$tmp = $data[$j]['zone'];
	// Номер массива
	$n = 0;
	$k = 0;
	$zonedata[$n] = array();
	$exdata[$n] = array();
	$zonedata[$n]['zone'] = $data[$j]['zone'];
	$zonedata[$n]['name'] = $data[$j]['name'];

	for ($j=0;$j<count($data);$j++)
	{
		// Если изменился номер карты, то начинаем новый массив
		if ($tmp != $data[$j]['zone'])
		{
			// Количество объектов на зоне
			$zonedata[$n]['count'] = $k;
			$n++;
			$zonedata[$n] = array();
			$exdata[$n] = array();
			$tmp = $data[$j]['zone'];
			// Заносим номер зоны в список зон
			$zonedata[$n]['zone'] = $data[$j]['zone'];
			// TODO: Заносим название зоны в список зон
			$zonedata[$n]['name'] = $data[$j]['name'];
			$k = 0;
		}
		$exdata[$n][$k] = array();
		$exdata[$n][$k] = $data[$j];
		$k++;
	}

	// Количество объектов на зоне
	$zonedata[$n]['count'] = $k;

	// Сортировка массивов по количеству объектов на зоне.
	for ($i = 0; $i <= $n; $i++)
	{
		for ($j = $i; $j <= $n; $j++)
		{
			if ($zonedata[$j]['count'] > $zonedata[$i]['count'])
			{
				unset($tmp);
				$tmp = $zonedata[$i];
				$zonedata[$i] = $zonedata[$j];
				$zonedata[$j] = $tmp;
				unset($tmp);
				$tmp = $exdata[$i];
				$exdata[$i] = $exdata[$j];
				$exdata[$j] = $tmp;
			}
		}
	}

	$smarty->assign('zonedata', $zonedata);
	$smarty->assign('exdata', $exdata);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $object['name'].' - '.$smarty->get_config_vars('Objects');
// Путь к этому разделу
$page['path'] = '[0,5,'.$object['type'].']';
// Тип страницы
$page['type'] = 2;
$page['typeid'] = $object['id'];
$smarty->assign('page', $page);

// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

$smarty->assign('css_Mapper', true);
$smarty->assign('js_Mapper', true);
if (isset($allitems)) $smarty->assign('allitems', $allitems);
if (isset($object)) $smarty->assign('object', $object);
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
$smarty->display('object.tpl');

?>
