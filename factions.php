<?php
$smarty->config_load($conf_file, 'factions');

global $DB;

$rows = $DB->select('
	SELECT factionID, team, name, side
	FROM ?_factions
	WHERE
		reputationListID!=-1
	'
);

$i=0;
$factions = array();
foreach ($rows as $numRow=>$row)
{
	$factions[$i] = array();
	$factions[$i]['id'] = $row['factionID'];
	if ($row['team']!=0)
		$factions[$i]['group'] = $DB->selectCell('SELECT name FROM ?_factions WHERE factionID=? LIMIT 1', $row['team']);
	if ($row['side'])
		$factions[$i]['side'] = $row['side'];
	$factions[$i]['name'] = $row['name'];
	$i++;
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('Factions');
// Путь к этому разделу
$page['path'] = '[0, 7]';
$smarty->assign('page', $page);

// Статистика выполнения mysql запросов
$smarty->assign('mysql', $DB->getStatistics());
$smarty->assign('factions',$factions);
// Загружаем страницу
$smarty->display('factions.tpl');
?>
