<?php

$smarty->config_load($conf_file);

// Заголовок страницы
$smarty->assign('title', $smarty->get_config_vars('Maps'));
$smarty->assign('css_Mapper', true);
$smarty->assign('js_Mapper', true);

// Номер вкладки меню
$smarty->assign('tab', 1);

$smarty->display('maps.tpl');

?>
