<?php

// Необходима функция questinfo
require_once('includes/allquests.php');

$smarty->config_load($conf_file, 'quests');

// Разделяем из запроса класс и подкласс квестов
point_delim($podrazdel,$Type,$ZoneOrSort);

global $DB;

if (!IsSet($ZoneOrSort))
{
	switch($Type):
		case '':
			UnSet($arr);
			break;
		case 0:
			// Eastern Kingdoms
			$arr = array(36, 45, 3, 25, 4, 35, 46, 132, 279, 41, 154, 2257, 1, 10, 139, 12, 3430, 3433, 267, 1537, 131, 38, 24, 9, 44, 51, 3487, 130, 1519, 33, 3431, 8, 47, 85, 1497, 28, 40, 11);
			break;
		case 1:
			// Kalimdor
			$arr = array(2079, 3526, 331, 16, 3524, 3525, 148, 221, 1657, 405, 14, 15, 1116, 361, 357, 493, 215, 1637, 220, 702, 188, 1377, 406, 440, 141, 17, 3557, 400, 1638, 1216, 490, 363, 618);
			break;
		case 8:
			// Outland
			$arr = array(3522, 3483, 3518, 3523, 3520, 3703, 3679, 3519, 3696, 3521);
			break;
		case 2:
			// Dungeons
			$arr = array(3790, 3688, 719, 1584, 1583, 1941, 3607, 2557, 133, 3535, 3792, 2100, 2437, 722, 491, 796, 2057, 3789, 209, 2017, 1417, 3842, 1581, 3717, 3715, 717, 3716, 1337, 718, 978);
			break;
		case 3:
			// Raids
			$arr = array(3428, 2677, 3606, 2562, 3836, 2717, 3456, 2159, 3429, 3840, 19);
			break;
		case 4:
			// Classes
			$arr = array(-263, -261, -161, -141, -262, -162, -82, -61, -81);
			break;
		case 5:
			// Proffesions
			$arr = array(-181, -121, -304, -201, -324, -101, -24, -182, -264);
			break;
		case 6:
			// Battlegrouns
			$arr = array(3358, 2597, 3277);
			break;
		case 7:
			// Misc
			$arr = array(-365, -370, -364, -1, -368, -344, -366, -369, -367, -22, -284, -221);
			break;
		case -2:
			// Uncategorized
			$arr = array(0);
			break;
		default:
			break;
	endswitch;
}

$rows = $DB->select('
	SELECT *
	FROM quest_template
	WHERE
		1 = 1
		{ AND ZoneOrSort = ? }
		{ AND ZoneOrSort IN (?a) }
	ORDER BY Title
	LIMIT 200
	',
	(IsSet($ZoneOrSort))? $ZoneOrSort : DBSIMPLE_SKIP,
	(IsSet($arr))? $arr : DBSIMPLE_SKIP
);

$quests = array();
foreach ($rows as $numRow=>$row)
{
	$quests[$numRow] = array();
	$quests[$numRow] = questinfo2($row);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('Quests');
// Путь к этому разделу
$page['path'] = '[0, 3,'.$Type.' ,'.$ZoneOrSort.']';
$smarty->assign('page', $page);

// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (IsSet($allitems))
	$smarty->assign('allitems',$allitems);
if (IsSet($quests))
	$smarty->assign('quests',$quests);
// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());
// Загружаем страницу
$smarty->display('quests.tpl');

?>
