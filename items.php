<?php

// Необходима функция iteminfo
require_once('includes/allitems.php');

$smarty->config_load($conf_file, 'items');

// Разделяем из запроса класс и подкласс вещей
point_delim($podrazdel,$class,$subclass);

global $DB;

// Составляем запрос к БД, выполняющий поиск по заданным классу и подклассу
$rows = $DB->select('
	SELECT ?#, entry, maxcount
	FROM item_template, ?_icons
	WHERE
		1=1
		{ AND class=? }
		{ AND subclass=? }
		AND id=displayid
		ORDER BY quality DESC, name
	',
	$item_cols[2],
	($class!='')? $class: DBSIMPLE_SKIP,
	($subclass!='')? $subclass: DBSIMPLE_SKIP
);

$i=0;
$items = array();
foreach ($rows as $numRow=>$row)
{
	$items[$i] = array();
	$items[$i] = iteminfo2($row);
	$i++;
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $smarty->get_config_vars('Items');
// Путь к этому разделу
$page['path'] = "[0, 0, $class, $subclass]";
// Тип страницы
$smarty->assign('page', $page);

// Статистика выполнения mysql запросов
$smarty->assign('mysql', $DB->getStatistics());
// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (count($allitems)>=0)
	$smarty->assign('allitems',$allitems);
if (count($items>=0))
	$smarty->assign('items', $items);
// Загружаем страницу
$smarty->display('items.tpl');
?>
