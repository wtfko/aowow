<?php

require_once('includes/allspells.php');
require_once('includes/allquests.php');
require_once('includes/allnpcs.php');
require_once('includes/allcomments.php');

// Настраиваем Smarty ;)
$smarty->config_load($conf_file, 'npc');

global $DB;
global $spell_cols;
global $npc_cols;

// Заголовок страницы
$id = $podrazdel;

// Ищем NPC:
$npc = array();
$row = $DB->selectRow('
	SELECT
		?#, c.entry, c.name, c.armor as armor, c.mindmg as mindmg, c.maxdmg as maxdmg,
		f.name as `faction-name`, ft.factionID as `factionID`
	FROM creature_template c, ?_factiontemplate ft, ?_factions f
	WHERE
		c.entry=?
		AND ft.factiontemplateID=c.faction_A
		AND f.factionID=ft.factionID
		LIMIT 1
		',
	$npc_cols[1],
	$id
);
$sprow = $DB->selectRow('
	SELECT
		spawntimesecs
	FROM  creature
	WHERE
		id=?
		LIMIT 1
		',
	$id
);
if ($sprow)
{
	$npc['sptime'] = $sprow['spawntimesecs'];
}
if ($row)
{
	$npc['id'] = $row['entry'];
	$npc['name'] = $row['name'];
	$npc['subname'] = $row['subname'];
	$npc['type'] = $row['type'];
	$npc['minlevel'] = $row['minlevel'];
	$npc['maxlevel'] = $row['maxlevel'];
	$npc['mindmg'] = round($row['mindmg']);
	$npc['maxdmg'] = round($row['maxdmg']);
	$npc['rank'] = $smarty->get_config_vars('rank'.$row['rank']);
	// faction_A = faction_H
	$npc['faction_num'] = $row['factionID'];
	$npc['faction'] = $row['faction-name'];
	// TODO: сделать разделить тысяч
	// Min|Max здоровье
	$npc['minhealth'] = $row['minhealth'];
	$npc['armor'] = $row['armor'];
	$npc['maxhealth'] = $row['maxhealth'];
	// Min|Max мана
	$npc['minmana'] = $row['minmana'];
	$npc['maxmana'] = $row['maxmana'];
	// Деньги
	$money = ($row['mingold']+$row['maxgold']) / 2;
	$npc['moneygold'] = floor($money/10000);
	$npc['moneysilver'] = floor(($money - ($npc['moneygold']*10000))/100);
	$npc['moneycopper'] = floor($money - ($npc['moneygold']*10000) - ($npc['moneysilver']*100));
	// Отношение к фракциям
	$npc['A'] = $row['A'];
	$npc['H'] = $row['H'];
	// Дроп
	$lootid=$row['lootid'];
	// Используемые спеллы
	$npc['ablities'] = array();
	$i=0;
	for ($j=1;$j<=4;$j++)
		if ($row['spell'.$j] > 0)
		{
			$npc['abilities'][$i] = array();
			$npc['abilities'][$i] = spellinfo($row['spell'.$j], 0);
			$i++;
		}
}

// Обучает:
$npc['teaches'] = array();
// Если это пет со способностью:
$row = $DB->selectRow('
	SELECT Spell1, Spell2, Spell3, Spell4
	FROM petcreateinfo_spell
	WHERE
		entry=?d
	',
	$npc['id']
);
if ($row)
{
	for ($j=1;$j<=4;$j++)
		if ($row['Spell'.$j])
			for ($k=1;$k<=3;$k++)
			{
				$spellrow = $DB->selectRow('
					SELECT ?#, spellID
					FROM ?_spell, ?_spellicons
					WHERE
						spellID=(SELECT effect'.$k.'triggerspell FROM ?_spell WHERE spellID=?d AND (effect'.$k.'id IN (36,57)))
						AND id=spellicon
					LIMIT 1
					',
					$spell_cols[2],
					$row['Spell'.$j]
				);
				if ($spellrow)
				{
					$num = count($npc['teaches']);
					$npc['teaches'][$num] = array();
					$npc['teaches'][$num] = spellinfo2($spellrow);
				}
			}
}
// Если это просто тренер
$teachspells = $DB->select('
	SELECT ?#, spellID
	FROM npc_trainer, ?_spell, ?_spellicons
	WHERE
		entry=?d
		AND spellID=Spell
		AND id=spellicon
	',
	$spell_cols[2],
	$npc['id']
);
foreach ($teachspells as $teachspell)
{
//	for ($k=1;$k<=3;$k++)
//	{
//		$spellrows = $DB->select('
//			SELECT ?#, spellID
//			FROM ?_spell, ?_spellicons
//			WHERE
//				spellID = (SELECT effect'.$k.'triggerspell FROM ?_spell WHERE (spellID = ?d) AND (effect'.$k.'id IN (36,57)))
//				AND id=spellicon
//			',
//			$spell_cols[2],
//			$teachspell
//		);
//		foreach($spellrows as $i=>$spellrow)
//		{
			$num = count($npc['teaches']);
			$npc['teaches'][$num] = array();
			$npc['teaches'][$num] = spellinfo2($teachspell);
//		}
//	}
}

// Продает:
$rows = $DB->select('
	SELECT ?#, i.entry, i.maxcount, n.`maxcount` as `drop-maxcount`, n.ExtendedCost
	FROM npc_vendor n, item_template i, ?_icons
	WHERE
		n.entry=?
		AND i.entry=n.item
		AND id=i.displayid
	',
	$item_cols[2],
	$id
);
$npc['sells'] = array();
foreach ($rows as $numRow=>$row)
{
	$npc['sells'][$numRow] = array();
	$npc['sells'][$numRow] = iteminfo2($row);
	$npc['sells'][$numRow]['maxcount'] = $row['drop-maxcount'];
	$npc['sells'][$numRow]['cost'] = array();
	if ($row['ExtendedCost'])
	{
		$extcost = $DB->selectRow('SELECT * FROM ?_item_extended_cost WHERE extendedcostID=?d LIMIT 1', $row['ExtendedCost']);
		if ($extcost['reqhonorpoints']>0)
			$npc['sells'][$numRow]['cost']['honor'] = (($npc['A']==1)? 1: -1) * $extcost['reqhonorpoints'];
		if ($extcost['reqarenapoints']>0)
			$npc['sells'][$numRow]['cost']['arena'] = $extcost['reqarenapoints'];
		$npc['sells'][$numRow]['cost']['items'] = array();
		for ($j=1;$j<=5;$j++)
			if (($extcost['reqitem'.$j]>0) and ($extcost['reqitemcount'.$j]>0))
			{
				allitemsinfo($extcost['reqitem'.$j], 0);
				$npc['sells'][$numRow]['cost']['items'][] = array('item' => $extcost['reqitem'.$j], 'count' => $extcost['reqitemcount'.$j]);
			}
	}
	if ($row['BuyPrice']>0)
		$npc['sells'][$numRow]['cost']['money'] = $row['BuyPrice'];
}

// Дроп
$npc['drop'] = array();
$npc['drop'] = loot('creature_loot_template', $lootid);

// Кожа
$npc['skinning'] = array();
$npc['skinning'] = loot('skinning_loot_template', $lootid);

// Воруеццо
$npc['pickpocketing'] = array();
$npc['pickpocketing'] = loot('pickpocketing_loot_template', $lootid);

// Начиниают квесты...
$rows = $DB->select('
	SELECT ?#
	FROM creature_questrelation c, quest_template q
	WHERE
		c.id=?
		AND q.entry=c.quest
	',
	$quest_cols[2],
	$id
);
$npc['starts'] = array();
foreach ($rows as $numRow=>$row) {
	$npc['starts'][$numRow] = array();
	$npc['starts'][$numRow] = questinfo2($row);
}

// Заканчивают квесты...
$rows = $DB->select('
	SELECT ?#
	FROM creature_involvedrelation c, quest_template q
	WHERE
		c.id=?
		AND q.entry=c.quest
	',
	$quest_cols[2],
	$id
);
$npc['ends'] = array();
foreach ($rows as $numRow=>$row) {
	$npc['ends'][$numRow] = array();
	$npc['ends'][$numRow] = questinfo2($row);
}

// Необходимы для квеста..
$rows = $DB->select('
	SELECT ?#
	FROM quest_template
	WHERE
		ReqCreatureOrGOId1=?
		OR ReqCreatureOrGOId2=?
		OR ReqCreatureOrGOId3=?
		OR ReqCreatureOrGOId4=?
	',
	$quest_cols[2],
	$id, $id, $id, $id
);
$npc['objectiveof'] = array();
foreach ($rows as $numRow=>$row) {
	$npc['objectiveof'][$numRow] = array();
	$npc['objectiveof'][$numRow] = questinfo2($row);
}

// Положения созданий божих:
$rows = $DB->select('select * from creature where id=?', $id);
$data = array();
foreach ($rows as $numRow=>$row) {
	$data[$numRow] = array();
	$data[$numRow]['y'] = $row["position_y"];
	$data[$numRow]['x'] = $row["position_x"];
	$data[$numRow]['m'] = $row["map"];
}

if (count($data)>0)
{
	$data = mass_coord($data);

	$zonedata = array();

	UnSet($tmp);
	// Сортируем массив. Зачем???
	if ($data)
		sort($data);

	// Во временную переменную tmp заносим номер локации
	$j=0;
	$tmp=$data[$j]['zone'];
	// Номер массива
	$n = 0;
	$k=0;
	$zonedata[$n] = array();
	$exdata = array();
	$zonedata[$n]['zone'] = $data[$j]['zone'];
	$zonedata[$n]['name'] = $data[$j]['name'];

	for ($j=0;$j<count($data);$j++)
	{
		// Если изменился номер карты, то начинаем новый массив
		if ($tmp!=$data[$j]['zone'])
		{
			// Количество объектов на зоне
			$zonedata[$n]['count'] = $k;
			$n++;
			$exdata[$n] = array();
			$zonedata[$n] = array();
			$tmp=$data[$j]['zone'];
			// Заносим номер зоны в список зон
			$zonedata[$n]['zone'] = $data[$j]['zone'];
			// TODO: Заносим название зоны в список зон
			$zonedata[$n]['name'] = $data[$j]['name'];
			$k=0;
		}
		$exdata[$n][$k] = array();
		$exdata[$n][$k] = $data[$j];
		$k++;
	}

	// Количество объектов на зоне
	$zonedata[$n]['count'] = $k;

	// Сортировка массивов по количеству объектов на зоне.
	for ($i=0;$i<=$n;$i++)
	{
		for ($j=$i;$j<=$n;$j++)
		{
			if ($zonedata[$j]['count'] > $zonedata[$i]['count'])
			{
				UnSet($tmp);
				$tmp = $zonedata[$i];
				$zonedata[$i] = $zonedata[$j];
				$zonedata[$j] = $tmp;
				UnSet($tmp);
				$tmp = $exdata[$i];
				$exdata[$i] = $exdata[$j];
				$exdata[$j] = $tmp;
			}
		}
	}

	$smarty->assign('zonedata',$zonedata);
	$smarty->assign('exdata',$exdata);
}

// Параметры страницы
$page = array();
// Номер вкладки меню
$page['tab'] = 0;
// Заголовок страницы
$page['title'] = $npc['name'].' - '.$smarty->get_config_vars('NPCs');
// Тип страницы
$page['type'] = 1;
$page['typeid'] = $npc['id'];
// Путь
$page['path'] = '[0,4,'.$npc['type'].']';

$smarty->assign('page', $page);
// Комментарии
$smarty->assign('comments', getcomments($page['type'], $page['typeid']));

// Если хоть одна информация о вещи найдена - передаём массив с информацией о вещях шаблонизатору
if (IsSet($allitems))
	$smarty->assign('allitems',$allitems);
if (IsSet($allspells))
	$smarty->assign('allspells',$allspells);

$smarty->assign('npc',$npc);
$smarty->assign('css_Mapper', true);
$smarty->assign('js_Mapper', true);

// Количество MySQL запросов
$smarty->assign('mysql', $DB->getStatistics());

// Запускаем шаблонизатор
$smarty->display('npc.tpl');

?>
